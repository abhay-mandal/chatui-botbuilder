module.exports = {
  pg: {
    host: '127.0.0.1',
    database: 'db_name',
    //user: '',
    //password: '',
    charset: 'utf8'
  },
  mongodb: {
    url: 'mongodb://localhost:27017/db_name'
  },
  sessionSecret: 'zed_session',
  rcs : {
    mongodbUrl: process.env.MONGOLAB_URI || 'mongodb://dataadmin:1$mth30wn3r@172.16.14.211:56874/test?authSource=admin',
        API_URL: process.env.API_URL || 'http://localhost:7005',
        erpurl :"http://172.16.14.112:8080"
  },
  rcmconfigDb: {
      user: "devuser",
      password: "dev@123user",
      host: "10.20.48.153",
      database: process.env.DB_NAME || 'rcmconfig'
  }
};