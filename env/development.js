module.exports = {
  pg: {
    host: '103.228.83.42',
    database: 'bewoconsumer',
    //user: 'test',
    //password: 'test',
    charset: 'utf8'
  },
  mongodb: {
    url: 'mongodb://localhost:27017/test'
  },
  sessionSecret: 'zed_session',
  rcs: {
    mongodbUrl: process.env.MONGOLAB_URI || 'mongodb://dataadmin:1$mth30wn3r@172.16.14.211:56874/test?authSource=admin',
        API_URL: process.env.API_URL || 'http://localhost:7005',
        erpurl :"http://172.16.14.112:8080"
  },
  rcmconfigDb: {
      user: "devuser",
      password: "dev@123user",
      host: "10.20.48.153",
      database: process.env.DB_NAME || 'rcmconfig'
  }
};