module.exports = (sequelize, DataTypes) => {
    return sequelize.define('rcm_sandbox_config', {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        sender_id: {
            type: DataTypes.STRING
        },
        caller_number: {
            type: DataTypes.STRING
        },
        sandbox_id: {
            type: DataTypes.STRING
        },
        handover_url: {
            type: DataTypes.STRING
        }
    });
};