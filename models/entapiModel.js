var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var api_schema = new Schema({
    enterprise: { type: String },
    method: { type: String },
    ApiName : { type: String },
    api_info: { type: JSON },
    created_on: { type: Date, default: Date.now },
    updated_on: { type: Date },
    LastUpdatedBy:{ type: mongoose.Schema.Types.ObjectId, ref: 'user' },
    created_by: { type: mongoose.Schema.Types.ObjectId, ref: 'user' },
}); 
module.exports = mongoose.model("enterpriseAPI", api_schema);