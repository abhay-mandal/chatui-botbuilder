var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var session_schema = new Schema({
    esme: { type: String },
    firstname: { type: String },
    token : {type : String},
    created_on: { type: Date, default: Date.now },
    updated_on: { type: Date },
    LastUpdatedBy:{ type: mongoose.Schema.Types.ObjectId, ref: 'user' },
    created_by: { type: mongoose.Schema.Types.ObjectId, ref: 'user' },
}); 
module.exports = mongoose.model("session", session_schema);