'use strict';

const sandboxConfigModel = require('./sandboxConfigModel');

const Sequelize = require('sequelize');

const rcmconfigDbConfig  = require('../config').rcmconfigDb;
const logger = require('../log/logger');

const sequelize = new Sequelize(rcmconfigDbConfig.database, rcmconfigDbConfig.user, rcmconfigDbConfig.password, {
    host: rcmconfigDbConfig.host,
    dialect: 'mysql',
    define: {
        freezeTableName: true,
        timestamps: false
    }
});

const SandboxConfig = sandboxConfigModel(sequelize, Sequelize.DataTypes)

sequelize.authenticate()
    .then(() => {
        logger.info(`Sequelize authenticated to ${rcmconfigDbConfig.database}`);
    })

module.exports = {
    SandboxConfig
};


