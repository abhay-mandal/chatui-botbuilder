var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var carouselSchema = mongoose.Schema({
    footer: { type: String },
    bt_link: { type: String },
    bt_payload: { type: String },
    bt_act: { type: String },
    button_text: { type: String },
    small_desc2: { type: String },
    sub_title2: { type: String },
    title2: { type: String },
    img: { type: String },
    small_desc: { type: String },
    sub_title: { type: String },
    title: { type: String }
});

var flowSchema = mongoose.Schema({ 
    expresType : {type :String},
    state_name : { type: String},
    connected_state:{type: String},
    carousel_message :{type:String},
    template :{type: String},
    template_option :{type: String},
    state : {type: String},
    sugg_conn_state:{ type: JSON},
    cond_conn_state:{ type: JSON},
    star_states : {type: JSON},
    cond_exps : { type: Array},
    out_mess :{ type: Array},
    carousel :[carouselSchema],
    forms : { type: Array},
    input_option:{type: String},
    exp_in_mess : { type: JSON},
    multiple_conn : {type :Boolean},
    left:{type: Number},
    top:{type: Number},
    rule_id : {type:String},
    api_type :{type:String},
    api_url : {type : String},
    previous_res:{type : Boolean},
    send_res:{type : Boolean},
    condif : {type : Boolean},
    cond_details : {},
    conditional_states :{type: Array},
    entry_response : {type : Boolean},
    ent_api_details : {type : JSON},
    timeout_message :{type:String},
    timeout_state :{type:String},
    timeout_url : {type:String},
    timeout_delay : {type:String},
    res_timeout : {type : Boolean},
    res_timeout_type : {type:String},
    url_params: {type : Array},
    enable_multilang :{type : Boolean},
    language : {type : String}
});

var botFlowSchema = new Schema({
    sandbox_id : String,
    botId: String,
    botName: String,
    botDesc: String,
    webLink: String,
    postemail: String,
    tog_brand_icon: Boolean,
    titlebarColor: String,
    titleColor: String,
    backgroundColor: String,
    bbubbleColor: String,
    textfieldColor: String,
    buttonhoverColor: String,
    carousel_background: String,
    carousel_button: String,
    ububbleColor: String,
    buttonColor: String,
    suggesColor: String,
    fontColor: String,
    botImage: String,
    webImage: String,
    tog_normal_chat: Boolean,
    tog_whatsapp_chat: Boolean,
    botBgImage: String,
    startState : String,
    botFlow: [flowSchema],
    LastUpdatedTime: { type: Date },
    LastUpdatedBy: { type: mongoose.Schema.Types.ObjectId, ref: 'user' },
    created_by: { type: mongoose.Schema.Types.ObjectId, ref: 'user' },
    created_on: { type: Date, default: Date.now }
});

botFlowSchema.pre('update', function() {
    this.update({}, { $set: {LastUpdatedTime: new Date()}});
});

module.exports = mongoose.model("rcm_sandbox_botFlow", botFlowSchema);