var mongoose = require('mongoose');
let Schema = mongoose.Schema;

var bot_details = mongoose.Schema({
    bot_id : { type: String},
    state :{type: String}
});

var inter_schema = new Schema({
    sandbox_id : {type: String},    
    bot_details: bot_details,
    from: {type: String},    
    convers_id: {type: String},    
    message_by:{type: String},    
    to :{type: String},    
    id: {type: String},
    api: {type:JSON},  
    timestamp: {type: String},
    text: {type: JSON},    
    image:{type: JSON},    
    location:{type: JSON},    
    document:{type: JSON},    
    voice :{type: JSON},    
    type: {type: String},
    created_on: { type: Date, default: Date.now },
    updated_on: { type: Date, default: Date.now }
},
{
    collection:"sandbox_interactions"
});

module.exports = mongoose.model('SandboxInteractions', inter_schema);
