var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var states = mongoose.Schema({
    in_message : { type: String},
    next_state : { type: String}
});
var data = mongoose.Schema({
    sender_id : {type: String},
    interaction : {type: JSON},
    device_info : {type: JSON}
});
var inter_schema = new Schema({
    date_idx: { type: Number },
    bot_id: { type: String },
    sender_id: { type: String },
    convers_id: { type: String },
    interaction : {type: JSON},
    device_info : {type: JSON},
    created_on: { type: Date, default: Date.now },
    updated_on: { type: Date, default: Date.now }
}); 
module.exports = mongoose.model("Bot_interaction", inter_schema);