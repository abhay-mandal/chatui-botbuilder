var mongoose = require('mongoose');
var Schema = mongoose.Schema;

 var user_schema = new Schema({
        user_id: { type: Number },
        first_name: { type: String },
        username: { type: String, unique: true, index: true },
        email: { type: String },
        phone_no: { type: String },
        role_id: { type: String },
        password: { type: String },
        created_on: { type: Date, default: Date.now },
        updated_on: { type: Date, default: Date.now }
    }); 


module.exports = mongoose.model("user", user_schema);