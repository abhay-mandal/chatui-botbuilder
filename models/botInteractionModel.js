var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var  bot_details = mongoose.Schema({
    bot_id : { type: String},
    state :{type: String}
});

var inter_schema = new Schema({
    sandbox_id : {type: String},
    bot_details: bot_details,
    convers_id: {type: String},
    message_by:{type: String},
    from: {type: String},
    to :{type: String},
    api: {type:JSON},
    id: {type: String},
    timestamp: {type: String},
    text:  {type: JSON},
    image:{type: JSON},
    location:{type: JSON},
    document:{type: JSON},
    voice :{type: JSON},
    type: {type: String},
//  device_info : {type: JSON},
    created_on: { type: Date, default: Date.now },
    updated_on: { type: Date, default: Date.now }
}); 

module.exports = mongoose.model("user_interaction", inter_schema);