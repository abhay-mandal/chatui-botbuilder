var mongoose = require('mongoose');
var Schema = mongoose.Schema;

 var role_schema = new Schema({
        role_id: { type: Number },
        role: { type: String },
        created_on: { type: Date, default: Date.now },
        updated_on: { type: Date, default: Date.now }
    }); 
module.exports = mongoose.model("role", role_schema);