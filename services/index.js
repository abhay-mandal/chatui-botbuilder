var botFlowInfo = require('../models/botFlow.model');

var rcsService = {
    /**
     * save module information
     */

     saveInfo : function(info, callback) {
         
         var responseObj = {
             "message": "",
             "success": false
         };
         botFlowInfo.create(info, function(err, data){
            callback(null, {
                success: true,
                message: "Info added successfully"
              });
         })
     }
}


module.exports = rcsService;