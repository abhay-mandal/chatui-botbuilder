const logger = require('../log/logger');
const { SandboxConfig } = require('../models/sequelize');
exports.getAllSandboxConfig = async (req, res) => {
      logger.info("GET /sandboxconfig invoked");
      try {
            logger.info("GET /sandboxconfig invoked");
            const sandboxConfigs = await SandboxConfig.findAll();
            if (sandboxConfigs) {
                logger.info(`GET /sandboxconfig sends success response of length ${sandboxConfigs.length}`);
                res.send(sandboxConfigs);
            } else {
                logger.info(`GET /sandboxconfig sends empty response {}`);
                res.send({});
            }
        } catch (err) {
            logger.error(`GET /sandboxconfig failed ${err}`);
            res.send(err);
        }
}
