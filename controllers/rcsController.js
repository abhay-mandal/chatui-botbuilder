var express = require('express'),
    jsoncsv = require('express-json-csv')(express);
var config = require('../config');
var botModel = require('../models/botFlow.model');
var request = require("request");
var multer = require('multer');
var mkdirp = require('mkdirp');
var token = config.rcs.token;
var interaction = require('../models/interModel');
// var forEach =  require('async-foreach').forEach;
var replaceall = require("replaceall");
var archiver = require('archiver');
var file_system = require('fs');
var sendurl = config.rcs.sendurl;
var output = file_system.createWriteStream('target.zip');
var archive = archiver('zip');
var zipFolder = require('zip-folder');
var botname;
var filepath;
var zipdir = require('zip-dir');
var session = require('../models/sessionModel');
var jwt = require('jsonwebtoken');
var md5 = require('md5');
var CryptoJS = require("crypto-js");
var erpurl = config.rcs.erpurl;

var storage = multer.diskStorage({ //multers disk storage settings
    destination: function (req, file, cb) {
        console.log(req.body, cb, filepath, "filepath");
        //   cb(null, 'http://182.72.109.76/uploads/'+filepath+'/carousel');
        cb(null, './public/uploads/' + filepath + '/carousel');
    },
    filename: function (req, file, cb) {
        console.log("filename", file);
        cb(null, file.originalname);
    }
});
var storage_img = multer.diskStorage({ //multers disk storage settings
    destination: function (req, file, cb) {
        console.log(req.body, cb, filepath, "filepath");
        //   cb(null, 'http://182.72.109.76/uploads/'+filepath+'/carousel');
        cb(null, './public/uploads/' + filepath);
    },
    filename: function (req, file, cb) {
        console.log("filename", file);
        cb(null, file.originalname);
    }
});

var upload = multer({ //multer settings
    storage: storage
})
var upload_img = multer({ //multer settings
    storage: storage_img
})

var self = module.exports = {
    save_data: function (req, res) {
        console.log(req.body, req.headers.user);
        req.body.created_by = req.headers.user;
        var d = new Date();
        var year = d.getFullYear();
        var month = ('0' + (d.getMonth() + 1)).slice(-2);
        var day = ('0' + d.getDate()).slice(-2);
        var time_idx = d.getFullYear() + '' + ('0' + (d.getMonth() + 1)).slice(-2) + '' + ('0' + d.getDate()).slice(-2) + '' + (d.getHours()) + '' + (d.getMinutes()) + '' + (d.getSeconds()) + '' + (d.getMilliseconds());
        var bot_id = "BOT" + time_idx;
        var dateidx = ('0' + (d.getMonth() + 1)).slice(-2) + '' + ('0' + d.getDate()).slice(-2) + '' + (d.getHours()) + '' + (d.getMinutes()) + '' + (d.getSeconds()) + '' + (d.getMilliseconds());
        var idx = 1;
        botModel.find({}).sort({ _id: -1 }).limit(1).exec(function (err, docs) {
            // console.log("last record",docs[0].botFlow.length);
        });
        req.body.botFlow.forEach(function (doc) {
            doc["rule_id"] = dateidx + "" + idx;
            idx = idx + 1;
        })
        mkdirp('./public/uploads/' + bot_id + '/carousel', function (err) {
            //  mkdirp('http://182.72.109.76/uploads/'+bot_id+'/carousel', function (err) {
            if (err) console.error(err)
            else console.log('pow!')
        });
        req.body.botId = bot_id;
        var bot_name = req.body.botName
        console.log(bot_name);
        var sand_name = bot_name.replace(" ", "");
        sand_name = sand_name.toUpperCase();
        console.log(bot_name.replace(" ", ""), sand_name);
        if (req.headers.role_id) {
            req.body.sandbox_id = "SANDBOX" + sand_name
        }
        // forEach(doc, function(data, index, arr) {
        //     var done = this.async(); 
        //     (function updateruleid(data) {
        //         if(data.input_option == "Suggestion"){
        //             data.sugg_conn_state.forEach(function(doc,$index) {
        //                 var state = doc[$index];
        //                 console.log(state);

        //             })
        //         }else{

        //         }
        // })(data);
        // }, function(notAborted, arr) {
        //    console.log("successfully updated ruleid");
        // });
        var botflow = req.body.botFlow


        botModel.create(req.body, function (err) {
            if (err) return console.log("ERROR", err);
            return res.send({ "botid": bot_id, "status": "Accepted" });
        });
    },
    update_bot: function (req, res) {
        var id = req.body.botId
        req.body.LastUpdatedTime = new Date();
        req.body.LastUpdatedBy = req.headers.user;
        botModel.update({ botId: id },
            req.body,
            function (err, affected, resp) {
                if (!err) {
                    console.log("successfully updated");
                    res.send({ "botid": id, "status": "success" });
                }
            })
    },
    requestData: function (message, senderid) {

        var data = {
            recipient: { "id": senderid },
            message: message
        }
    },
    sendresponse: function (req, res) {

        var text = {
            "text": ""
        }
        var quickreply = {
            "text": "",
            "quick_replies": []
        }
        var location = {
            "text": "share location",
            "quick_replies": [{
                "text": "Share location",
                "content_type": "location"
            }]
        }
        var text_button = {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "button",
                    "text": "",
                    "buttons": []
                }
            }
        }
        var image_with_button_quick = {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": []
                }
            },
            "quick_replies": []
        }
        var generic_template = {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    "elements": []
                }
            }
        }
        var Location = {
            "text": "Share location",
            "quick_replies": [{
                "text": "Share location",
                "content_type": "location"
            }]
        }
        var button_template = {
            "attachment": {
                "type": "template",
                "payload": { "template_type": "button", "text": "", "buttons": [] }
            }
        };

        var sender_id = req.body.entry[0].messaging[0].sender.id;
        if (req.body.entry[0].messaging[0].delivery) {
            res.sendStatus(200);
        } else if (req.body.entry[0].messaging[0].read) {
            res.sendStatus(200);
        } else if (req.body.entry[0].messaging[0].postback) {
            res.sendStatus(200);
        } else {


            if (req.body.entry[0].messaging[0].message.quick_reply) {

                var text_res = req.body.entry[0].messaging[0].message.text;
                var botname = req.body.entry[0].messaging[0].message.quick_reply.payload.split('_', 1)[0];
                var ruleid = req.body.entry[0].messaging[0].message.quick_reply.payload.split('_').pop();

                botModel.aggregate([
                    { $match: { "botName": botname } },
                    { $unwind: "$botFlow" },
                    {
                        $match: {
                            'botFlow.state_name': ruleid
                        }
                    }
                ], function (err, data) {
                    if (err) return console.log("ERROR", err);
                    //return res.send(property);

                    if (data.length > 0) {

                        var rec_data = data[0].botFlow;
                        for (var i = 0; i < rec_data.out_mess.length; i++) {
                            if (rec_data.out_mess[i].text != "") {

                                if (i == rec_data.out_mess.length - 1) {
                                    if (rec_data.input_option == "Suggestion") {

                                        quickreply.text = rec_data.out_mess[i].text;

                                        for (var j = 0; j < rec_data.exp_in_mess.suggestion_box.length; j++) {
                                            var state_name = rec_data.sugg_conn_state;
                                            var rule_id = rec_data.exp_in_mess.suggestion_box[j];

                                            var rep = {
                                                "content_type": "text",
                                                "title": rec_data.exp_in_mess.suggestion_box[j],
                                                "payload": botname + "_" + state_name[rule_id]
                                            }
                                            quickreply.quick_replies.push(rep);
                                            if (j == rec_data.exp_in_mess.suggestion_box.length - 1) {

                                                if (rec_data.out_mess.length > 1) {
                                                    setTimeout(self.requestData, 3000, quickreply, sender_id);
                                                    res.sendStatus(200);
                                                } else {
                                                    self.requestData(quickreply, sender_id);
                                                    res.sendStatus(200);
                                                }

                                            }
                                        }
                                    } else if (rec_data.input_option == "Button") {
                                        text_button.attachment.payload.text = rec_data.out_mess[i].text;
                                        for (var k = 0; k < rec_data.exp_in_mess.button_data.length; k++) {
                                            if (rec_data.exp_in_mess.button_data[k].type == "phone") {
                                                var dat = {
                                                    "type": "phone_number",
                                                    "title": rec_data.exp_in_mess.button_data[k].title,
                                                    "payload": rec_data.exp_in_mess.button_data[k].phone
                                                }
                                                text_button.attachment.payload.buttons.push(dat);
                                                if (k == rec_data.exp_in_mess.button_data.length - 1) {
                                                    self.requestData(text_button, sender_id);
                                                    res.sendStatus(200);
                                                }

                                            } else if (rec_data.exp_in_mess.button_data[k].type == "url") {
                                                var dat = {
                                                    "type": "web_url",
                                                    "url": rec_data.exp_in_mess.button_data[k].url,
                                                    "title": rec_data.exp_in_mess.button_data[k].title,
                                                    "webview_height_ratio": "compact"
                                                }
                                                text_button.attachment.payload.buttons.push(dat);
                                                if (k == rec_data.exp_in_mess.button_data.length - 1) {
                                                    self.requestData(text_button, sender_id);
                                                    res.sendStatus(200);
                                                }
                                            } else if (rec_data.exp_in_mess.button_data[k].type == "payload") {
                                                var dat = {
                                                    "type": "postback",
                                                    "title": rec_data.exp_in_mess.button_data[k].title,
                                                    "payload": rec_data.exp_in_mess.button_data[k].phone
                                                }
                                                text_button.attachment.payload.buttons.push(dat)
                                                if (k == rec_data.exp_in_mess.button_data.length - 1) {
                                                    self.requestData(text_button, sender_id);
                                                    res.sendStatus(200);
                                                }
                                            }
                                        }
                                    } else if (rec_data.input_option == "Location") {
                                        self.requestData(location, sender_id);
                                        res.sendStatus(200);
                                    } else if (rec_data.input_option == "Text") {
                                        if (rec_data.exp_in_mess.input_type == "Full Name") {

                                        } else if (rec_data.exp_in_mess.input_type == "Email") {

                                        } else if (rec_data.exp_in_mess.input_type == "Telephone") {

                                        } else {

                                        }
                                    } else {
                                        text.text = rec_data.out_mess[i].text;
                                        self.requestData(text, sender_id);
                                    }
                                } else {
                                    if (rec_data.out_mess.length == 1) {
                                        res.sendStatus(200);
                                    }
                                    text.text = rec_data.out_mess[i].text;
                                    self.requestData(text, sender_id);

                                }
                            } else {

                                if (i == rec_data.out_mess.length - 1) {

                                    if (rec_data.input_option == "Suggestion") {

                                        var img = {
                                            title: rec_data.out_mess[i].title,
                                            subtitle: rec_data.out_mess[i].sub_title,
                                            image_url: rec_data.out_mess[i].img_url
                                        }
                                        image_with_button_quick.attachment.payload.elements.push(img);
                                        for (var j = 0; j < rec_data.exp_in_mess.suggestion_box.length; j++) {
                                            var state_name = rec_data.sugg_conn_state;
                                            var rule_id = rec_data.exp_in_mess.suggestion_box[j];

                                            var rep = {
                                                "content_type": "text",
                                                "title": rec_data.exp_in_mess.suggestion_box[j],
                                                "payload": botname + "_" + state_name[rule_id]
                                            }
                                            image_with_button_quick.quick_replies.push(rep);
                                            if (j == rec_data.exp_in_mess.suggestion_box.length - 1) {

                                                self.requestData(image_with_button_quick, sender_id);
                                                res.sendStatus(200);

                                            }

                                        }
                                    } else if (rec_data.input_option == "Button") {

                                        var img = {
                                            title: rec_data.out_mess[i].title,
                                            subtitle: rec_data.out_mess[i].sub_title,
                                            image_url: rec_data.out_mess[i].img_url,
                                            buttons: []
                                        }

                                        image_with_button_quick.attachment.payload.elements.push(img);

                                        var ele_len = image_with_button_quick.attachment.payload.elements.length;
                                        for (var k = 0; k < rec_data.exp_in_mess.button_data.length; k++) {

                                            if (rec_data.exp_in_mess.button_data[k].type == "phone") {
                                                var dat = {
                                                    "type": "phone_number",
                                                    "title": rec_data.exp_in_mess.button_data[k].title,
                                                    "payload": rec_data.exp_in_mess.button_data[k].phone
                                                }


                                                image_with_button_quick.attachment.payload.elements[ele_len - 1].buttons.push(dat);
                                                if (k == rec_data.exp_in_mess.button_data.length - 1) {
                                                    if (i > 0) {
                                                        setTimeout(self.requestData, 5000, image_with_button_quick, sender_id);
                                                        res.sendStatus(200);
                                                    } else {
                                                        self.requestData(image_with_button_quick, sender_id);
                                                        res.sendStatus(200);
                                                    }

                                                }

                                            } else if (rec_data.exp_in_mess.button_data[k].type == "url") {
                                                var dat = {
                                                    "type": "web_url",
                                                    "url": rec_data.exp_in_mess.button_data[k].url,
                                                    "title": rec_data.exp_in_mess.button_data[k].title,
                                                    "webview_height_ratio": "compact"
                                                }
                                                image_with_button_quick.attachment.payload.elements[ele_len - 1].buttons.push(dat);
                                                if (k == rec_data.exp_in_mess.button_data.length - 1) {

                                                    self.requestData(image_with_button_quick, sender_id);
                                                    res.sendStatus(200);
                                                }
                                            } else if (rec_data.exp_in_mess.button_data[k].type == "payload") {
                                                var dat = {
                                                    "type": "postback",
                                                    "title": rec_data.exp_in_mess.button_data[k].title,
                                                    "payload": rec_data.exp_in_mess.button_data[k].phone
                                                }
                                                image_with_button_quick.attachment.payload.elements[ele_len - 1].buttons.push(dat)
                                                if (k == rec_data.exp_in_mess.button_data.length - 1) {
                                                    self.requestData(image_with_button_quick, sender_id);
                                                    res.sendStatus(200);
                                                }
                                            }
                                        }
                                    } else if (rec_data.input_option == "Location") {
                                        self.requestData(location, sender_id);
                                        res.sendStatus(200);
                                    } else {
                                        console.log("in else");
                                    }
                                } else {
                                    var img = {
                                        title: rec_data.out_mess[i].title,
                                        subtitle: rec_data.out_mess[i].sub_title,
                                        image_url: rec_data.out_mess[i].img_url
                                    }
                                    generic_template.elements.push(img);
                                    self.requestData(generic_template, sender_id);
                                    res.sendStatus(200);
                                }
                            }

                        }
                    } else {
                        console.log("no data found");
                    }
                });

            }
        }
    },
    listbots: function (req, res) {
        botModel.find({ "created_by": req.headers.user }, function (err, bots) {
            // console.log(bots); 
            res.send(bots);
        });
    },
    getbot: function (req, res) {

        botModel.find({ "botId": req.query.botId }, function (err, bot) {
            // console.log(bots); 
            res.send(bot);
        });
    },
    deletebot: function (req, res) {


        botModel.remove({ "botId": req.body.botId }, function (err, data) {

            if (data.result.ok == 1) {
                res.send({
                    data: "successfully deleted",
                    status: "success"
                });
            }
        });
    },
    upload: function (req, res) {

        // mkdirp('./uploads/'+req.query.botId+'/carousel', function (err) {
        //     if (err) console.error("error in KASnkansdkj",err,'kbdabdabd');
        //     else console.log('pow!')
        // });

        filepath = req.query.botId;
        upload.any(console.log(req.query, req.files, req.file, "sjnksdn"))(req, res, function (err) {

            if (err) {
                console.log(err);
                res.json({ error_code: 1, err_desc: err });
                return;
            }
            res.json({ error_code: 0, err_desc: null });
        });
    },
    upload_bot_img: function (req, res) {

        filepath = req.query.botId;
        upload_img.any(console.log(req.query, req.files, req.file, "sjnksdn"))(req, res, function (err) {

            if (err) {
                console.log(err);
                res.json({ error_code: 1, err_desc: err });
                return;
            }
            res.json({ error_code: 0, err_desc: null });
        });
    },
    getformlist: function (req, res) {
        botModel.find({ "botId": req.query.bot_id }, function (err, bot) {

            var form_list = [];
            var botFlow = bot[0].botFlow;
            botFlow.forEach(function (flow) {
                if (flow.template_option == 'form') {
                    form_list.push(flow.state);
                } else {

                }

            })
            res.send(form_list);
        })
    },
    getform_conv: function (req, res) {

        var form_fields = ['User_Id'];
        botModel.find({ "botId": req.query.bot_id }, function (err, bot) {
            var botFlow = bot[0].botFlow;
            botFlow.forEach(function (flow) {
                if (flow.state == req.query.form) {
                    if (flow.template_option == 'form') {
                        flow.forms.forEach(function (form) {
                            form.fields.forEach(function (field) {
                                form_fields.push(field.name);
                            })
                        })
                    } else { }
                } else { }
            })

            form_fields.push('Time');
            var final_array = []
            interaction.find({
                "bot_id": req.query.bot_id, "created_on": {
                    $gte: new Date("" + req.query.fromdate + ""),
                    $lte: new Date("" + req.query.todate + "")
                }
            }, function (err, conver) {
                conver.forEach(function (con) {
                    if (con.interaction[req.query.form]) {
                        if (con.interaction[req.query.form].answer == undefined) {
                            con.interaction[req.query.form].answer = {};
                        }
                        con.interaction[req.query.form].answer['User_Id'] = con.sender_id;
                        con.interaction[req.query.form].answer['Time'] = (new Date(con.updated_on)).toLocaleString();
                        final_array.push(con.interaction[req.query.form].answer);
                    }
                });
                res.csv(
                    final_array, { fields: form_fields });
            })
        })
    },
    download_conv: function (req, res) {
        botModel.find({ "botId": req.query.bot_id }, function (err, bot) {
            var botFlow = bot[0].botFlow;
            var state_names = [];
            state_names.push('sender_id');
            var final_array = [];
            botFlow.forEach(function (state) {
                state_names.push(state.state);
            })


            interaction.find({
                "bot_id": req.query.bot_id, "created_on": {
                    $gte: new Date("" + req.query.fromdate + ""),
                    $lte: new Date("" + req.query.todate + "")
                }
            }, function (err, conver) {
                conver.forEach(function (con) {
                    var data = {}
                    for (var i = 0; i < state_names.length; i++) {
                        if (con.interaction) {
                            data['sender_id'] = con.sender_id;
                            if (con.interaction[state_names[i]]) {
                                data[state_names[i]] = con.interaction[state_names[i]].answer
                            } else {

                            }
                        } else { }
                        if (i == state_names.length - 1) {
                            final_array.push(data);
                        }
                    }
                })
                res.csv(
                    final_array, { fields: state_names });
            });
        });
    },
    download_media: function (req, res) {

        var d = new Date();
        var year = d.getFullYear();
        var month = ('0' + (d.getMonth() + 1)).slice(-2);
        var day = ('0' + d.getDate()).slice(-2);
        var time_idx = d.getFullYear() + '' + ('0' + (d.getMonth() + 1)).slice(-2) + '' + ('0' + d.getDate()).slice(-2);
        zipdir('./uploads/' + req.query.bot_id + '/chat_conv', { saveTo: './conversations_archive/' + req.query.bot_id + '_' + time_idx + '.zip' }, function (err, buffer) {
            res.download('./conversations_archive/' + req.query.bot_id + '_' + time_idx + '.zip');
            console.log(err, buffer);
        });
    },
    getform_conv_media: function (req, res) {

        var d = new Date();
        var year = d.getFullYear();
        var month = ('0' + (d.getMonth() + 1)).slice(-2);
        var day = ('0' + d.getDate()).slice(-2);
        var time_idx = d.getFullYear() + '' + ('0' + (d.getMonth() + 1)).slice(-2) + '' + ('0' + d.getDate()).slice(-2);
        zipdir('./uploads/' + req.query.bot_id + '/form_conv', { saveTo: './form_archive/' + req.query.bot_id + '_' + time_idx + '.zip' }, function (err, buffer) {
            res.download('./form_archive/' + req.query.bot_id + '_' + time_idx + '.zip');
            console.log(err, buffer);
        });
    },
    erplogin: function (req, res) {
        var random_numb = 1234567890434334
        req.body.user_cred['requestType'] = "login"
        var bytes = CryptoJS.AES.decrypt(req.body.user_cred.password.toString(), 'erplogin');
        var password = bytes.toString(CryptoJS.enc.Utf8);
        req.body.user_cred.password = md5(password);
        var method_det = JSON.stringify(req.body.user_cred) + "&" + req.body.agent + "&" + req.body.IP + "&" + random_numb;

        request.post({
            json: false,
            headers: { 'content-type': 'text/plain' },
            url: erpurl + "/unified/loginmgmt",
            body: method_det
        }, function (error, response, body) {

            if (response.body) {
                console.log("here", response.body);
                if (JSON.parse(response.body)) {
                    var json_resp = JSON.parse(response.body);
                    if (json_resp.status == "SUCCESS") {
                        var token = jwt.sign(json_resp.esmeAddr + md5(password), 'karixrcm');
                        var session_body = {
                            esme: json_resp.esmeAddr,
                            token: token,
                            firstname: json_resp.firstName
                        }
                        session.create(session_body, function (err, result) {
                            res.send({
                                username: json_resp.firstName,
                                token: token,
                                esme: json_resp.esmeAddr,
                                status: "SUCCESS"
                            })
                        })
                    }
                    else {
                        res.send(json_resp)
                    }
                }
            }
            else {
                res.send({
                    "reason": "server not reachable",
                    "status": "FAILURE"
                });
            }
        });
    },
    logout: function (req, res) {
        console.log(req.body,req.headers);
        session.find({ "firstname": req.body.username, token: req.headers.authorization }, function (err, result) {
            console.log(result);
            if (result.length > 0) {
                session.remove({ "firstname": req.body.username, token: req.headers.authorization }, function (err, delresult) {
                    console.log(delresult);
                    if(delresult.result.ok){
                        res.send({status : "successful"})
                    }
                })
            }
        })
    }
};