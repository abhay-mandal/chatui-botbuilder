var botModel = require('../models/botFlow.model');
var path = require('path');
var multer = require('multer')
var mkdirp = require('mkdirp');
var request = require("request").defaults({ strictSSL: false });
var querystring = require('querystring');
var bot_interaction = require('../models/botInteractionModel');
var apiModel = require('../models/entapiModel');
const replaceString = require('replace-string');
var nodemailer = require('nodemailer');
var uniqid = require('uniqid');
var filepath;
var forEach = require('async-foreach').forEach;

var config = require('../config');
var apiurl = config.rcs.API_URL;

var smtpTransport = nodemailer.createTransport({
    host: 'smtp.mgage.solutions',
    port: 25,
    auth: {
        user: 'chattest', // generated ethereal user
        pass: 'P8w1S6t1' // generated ethereal password
    }
});


// var storage = multer.diskStorage({
// 	destination: function(req, file, callback) {
//       
// 		callback(null, './uploads')
// 	},
// 	filename: function(req, file, callback) {
// 		callback(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
// 	}
// })


var storage = multer.diskStorage({ //multers disk storage settings
    destination: function (req, file, cb) {

        cb(null, filepath);
    },
    filename: function (req, file, cb) {

        cb(null, file.originalname);
    }
});

var upload = multer({ //multer settings
    storage: storage
})
var self = module.exports = {
    getinitmess: function (req, res) {
        botModel.find({ "botId": req.body.botId }, { startState: 1 }, function (err, data) {
            botModel.aggregate([
                { $match: { "botId": req.body.botId } },
                { $unwind: "$botFlow" },
                {
                    $match: {
                        'botFlow.state_name': data[0].startState
                    }
                }
            ], function (err, data) {
                var botflow = data[0].botFlow;
                var final_data = {
                    botName: data[0].botName,
                    botImage: data[0].botImage,
                    botDesc: data[0].botDesc,
                    webLink: data[0].webLink,
                    tog_brand_icon: data[0].tog_brand_icon,
                    botbgImage: data[0].botBgImage,
                    webbgImage: data[0].webImage,
                    titlebarColor: data[0].titlebarColor,
                    titleColor: data[0].titleColor,
                    backgroundColor: data[0].backgroundColor,
                    carousel_background: data[0].carousel_background,
                    carousel_button: data[0].carousel_button,
                    bbubbleColor: data[0].bbubbleColor,
                    ububbleColor: data[0].ububbleColor,
                    buttonColor: data[0].buttonColor,
                    suggesColor: data[0].suggesColor,
                    fontColor: data[0].fontColor,
                    textfieldColor: data[0].textfieldColor,
                    buttonhoverColor: data[0].buttonhoverColor,
                };
                final_data['mess_data'] = [];

                final_data['state'] = botflow.state

                final_data['optional_input'] = botflow.optional_input
                var d = new Date();
                var year = d.getFullYear();
                var month = ('0' + (d.getMonth() + 1)).slice(-2);
                var day = ('0' + d.getDate()).slice(-2);
                var date_idx = d.getFullYear() + '' + ('0' + (d.getMonth() + 1)).slice(-2) + '' + ('0' + d.getDate()).slice(-2);
                var interaction = {}

                var curr_msg_id = uniqid.time()
                var convers_id = "conv_" + uniqid.time()

                interaction = {
                    date_idx: date_idx,
                    bot_details: {
                        bot_id: req.body.botId,
                        state: botflow.state
                    },
                    id: curr_msg_id,
                    message_by: "bot",
                    convers_id: convers_id,
                    to: req.body.sender_id
                }
                interaction[botflow.mess_type] = {}


                var api_resp = "";

                if (botflow.entry_response) {
                    var failure_state = botflow.ent_api_details[0].failure_state;
                    apiModel.find({ ApiName: botflow.ent_api_details[0].api_name }, function (req, api_res) {
                        api_det = api_res[0].api_info
                        var method_det = {
                        }
                        method_det.url = api_det.url;
                        method_det.method = api_det.method
                        if (api_det.headerparams.length > 0) {
                            method_det.headers = {};
                            forEach(api_det.headerparams, function (item, index, arr) {
                                method_det.headers[item.paramkey] = item.paramvalue
                            })
                        }
                        if (api_det.body_type == "raw") {
                            if (botflow.previous_message) {
                                var keys = Object.keys(botflow.ent_api_details[0].apires[0]);
                                forEach(keys, function (key, index, arr) {
                                    var done = this.async();
                                    (function findindex(key) {
                                        if (botflow.ent_api_details[0].apires[0][key].includes("${")) {
                                            var spilt_str = botflow.ent_api_details[0].apires[0][key].split("${");
                                           
                                        }
                                        //  var streetaddress= spilt_str.substr(0, addy.indexOf(',')); }
                                        interaction.find({ "from": req.body.sender_id, "bot_details.state": botflow.ent_api_details[0].apires[0][key], convers_id: req.body.convers_id }).sort({ _id: -1 }).limit(1).exec(function (err, inter_docs) {
                                            if (inter_docs.length > 0) {
                                                var ans = inter_docs[0].text.body
                                                api_det.raw_json[key] = ans
                                                done();
                                            } else {
                                                done();
                                            }

                                        })
                                    })(key);
                                }, function (notAborted, arr) {
                                    method_det.body = api_det.raw_json;
                                    request.post({
                                        json: true,
                                        url: apiurl + "/execute_api",
                                        body: method_det
                                    }, function (error, response, body) {
                                        if (response) {
                                            var res_data = {
                                                data: response.body,
                                                status: response.statusCode
                                            }
                                            api_res = response.body;
                                            self.process_data(req, res, final_data, botflow, api_resp, interaction);
                                        }
                                        else {
                                            self.process_data(req, res, final_data, botflow, api_resp, interaction);
                                        }

                                    });
                                })

                            }
                            else {
                                method_det.body = api_det.raw_json;
                                request.post({
                                    json: true,
                                    url: apiurl + "/execute_api",
                                    body: method_det
                                }, function (error, response, body) {

                                    if (response) {
                                        var res_data = {
                                            data: response.body,
                                            status: response.statusCode
                                        }
                                        api_resp = response.body;

                                        self.process_data(req, res, final_data, botflow, api_resp, interaction);
                                    }
                                    else {
                                        self.process_data(req, res, final_data, botflow, api_resp, interaction);
                                    }

                                });
                            }

                        } else {
                            if (api_det.body.length > 0) {
                                method_det.body = {};
                                forEach(api_det.body, function (item, index, arr) {
                                    method_det.body[item.bodykey] = item.bodyvalue
                                })


                                request.post({
                                    json: true,
                                    url: apiurl + "/execute_api",
                                    body: method_det
                                }, function (error, response, body) {
                                    if (response) {
                                        var res_data = {
                                            data: response.body,
                                            status: response.statusCode
                                        }
                                        api_res = response.body;

                                        self.process_data(req, res, final_data, botflow, api_resp, interaction);
                                    }
                                    else {
                                        self.process_data(req, res, final_data, botflow, api_resp, interaction);
                                    }

                                });
                            }
                        }
                    })
                } else {
                    self.process_data(req, res, final_data, botflow, api_resp, interaction);
                }

            });
        })
    },
    process_data: function (req, res, final_data, botflow, api_resp, interaction) {
        var api = api_resp;
        var incom_req = req;

        interaction.message_by = "bot";
        interaction['api'] = api_resp;

        final_data['mess_data'] = [];
        final_data['msg_id'] = interaction.id;
        final_data['convers_id'] = interaction.convers_id

        final_data['state'] = botflow.state

        if (botflow.connected_state != '' && botflow.entry_response && botflow.input_option == "None") {
           

            self.save_conv(req, res, interaction);
            req.body.api_exec = true
            req.body.state = botflow.connected_state

            self.getmess(req, res)
        }
        else if (botflow.connected_state == '' && botflow.entry_response && botflow.input_option == "None" && botflow.condif) {
          
            self.save_conv(req, res, interaction);
            forEach(botflow.cond_details.A, function (conds, index, arr) {
                var new_cond;
                var done = this.async();
                (function findindex(conds) {
                    var cond_exp = botflow.cond_exps[index];
                    forEach(conds.cond, function (nest_cond, index1, arr) {
                        var done1 = this.async();
                        (function findindex(nest_cond) {

                            if (nest_cond.field.includes("${")) {
                                var spilt_str = nest_cond.field.split("${");

                                var after_spilt_str = spilt_str[1].substr(0, spilt_str[1].indexOf('.'));
                                var act_key = after_spilt_str.split(".");
                                var sub_key = spilt_str[1].split(act_key[0] + '.');


                                var evalu_str = "${" + sub_key[1]
                                
                                if (act_key[0] == interaction.bot_details.state) {
                                    var api = interaction.api;
                                 
                                    var ans = eval('`' + evalu_str + '`')
                                  
                                    botflow.cond_exps[index] = replaceString(botflow.cond_exps[index], nest_cond.field, ans);

                                    done1();
                                }
                                else {
                                    var match = { "to": incom_req.body.sender_id, "bot_details.state": act_key[0], "convers_id": incom_req.body.convers_id }
                                    bot_interaction.find(match).sort({ _id: -1 }).limit(1).exec(function (err, inter_docs) {
                                        if (inter_docs.length > 0) {

                                            var api = inter_docs[0].api;

                                            var ans = eval('`' + evalu_str + '`')
                                           
                                            botflow.cond_exps[index] = replaceString(botflow.cond_exps[index], nest_cond.field, ans);

                                            done1();
                                        } else {
                                            done1();
                                        }
                                    })
                                }
                            }
                            else if (nest_cond.field.includes("{{")) {
                                var found = "",          // an array to collect the strings that are 
                                    rxp = /{{([^}]+)}}/g,

                                    curMatch;

                                while (curMatch = rxp.exec(nest_cond.field)) {
                                    found = curMatch[1];
                                }
                                var match = { "from": incom_req.body.sender_id, "bot_details.state": found, "convers_id": incom_req.body.convers_id }
                                bot_interaction.find(match).sort({ _id: -1 }).limit(1).exec(function (err, inter_docs) {
                                    if (inter_docs.length > 0) {
                                        var ans = inter_docs[0].text.body

                                        botflow.cond_exps[index] = replaceString(botflow.cond_exps[index], nest_cond.field, ans);

                                        done1();
                                    } else {
                                        done1();
                                    }
                                })

                            }

                        })(nest_cond);

                    }, function (notAborted, arr) {
                      
                        var cond_t_test = botflow.cond_exps[index]
                        if (eval(cond_t_test) == true) {

                            req.body.state = botflow.cond_conn_state[botflow.conditional_states[index]]
                         
                            self.getmess(req, res)
                            done();
                        } else {
                         

                            if (index == botflow.cond_exps.length - 1) {
                                req.body.state = botflow.cond_conn_state['Default']
                                self.getmess(req, res)
                                done();
                            } else {
                                done1();
                            }
                        }

                    })
                })(conds);

            }, function (notAborted, arr) {


            })

        }
        else {




            if (botflow.send_res == true) {
                final_data['send_res'] = botflow.send_res
                final_data['api_type'] = botflow.api_type
                final_data['api_url'] = botflow.api_url
                final_data['previous_res'] = botflow.previous_res
            }
            if (botflow.condif == true) {
                final_data['condif'] = true;
                final_data['cond_conn_state'] = botflow.cond_conn_state;
                final_data['cond_exps'] = botflow.cond_exps;

            }
    
            if (botflow.template_option == 'form') {
                var data2 = {
                    "server": "true",
                    "message": botflow.forms,
                    "forms": true,
                    "next_rule": botflow.connected_state
                }
                final_data['mess_data'].push(data2);
                self.send_next_mess(req, res, botflow, final_data, interaction, api_resp);
            }
            else {
                forEach(botflow.out_mess, function (doc, index) {
                    var done = this.async();
                    (function findindex(doc) {
                        updated_text = "";
                        if (doc.mess_type == "text") {
                            var text = doc.text;
                            if (text.includes("${") || text.includes("{{")) {
                                var found = []
                                if (text.includes("{{")) {

                                    var rxp = /{{([^}]+)}}/g,

                                        curMatch;

                                    while (curMatch = rxp.exec(text)) {
                                        found.push(curMatch[1]);
                                    }
                                }
                                if (text.includes("${")) {
                                    // an array to collect the strings that are 
                                    var rxp = /\${([^}]+)}/g,
                                        curMatch;

                                    while (curMatch = rxp.exec(text)) {
                                        found.push(curMatch[1]);
                                    }
                                }
                            
                                forEach(found, function (key, index, arr) {
                                    var done1 = this.async();
                                    (function findindex(key) {

                                        if (key.includes(".api")) {
                                          
                                            var after_spilt_str = key.substr(0, key.indexOf('.'));
                                            var act_key = after_spilt_str.split(".");
                                            var sub_key = key.split(act_key[0] + '.');
                                         

                                            var evalu_str = "${" + sub_key[1] + "}"
                                         
                                        
                                            var match = { "to": incom_req.body.sender_id, "message_by": "bot", "convers_id": incom_req.body.convers_id, "bot_details.state": act_key[0] }
                                          
                                            if (act_key[0] == interaction.bot_details.state) {
                                                var api = interaction.api;
                                                var actual_text = "${" + act_key[0] + '.' + sub_key[1] + "}"
                                                var eval_text = eval('`' + evalu_str + '`')
                                                text = replaceString(text, actual_text, eval_text);

                                                done1();
                                            }
                                            else {
                                                bot_interaction.find(match).sort({ _id: -1 }).limit(1).exec(function (err, inter_docs) {
                                                 
                                                    var api = inter_docs[0].api
                                                    var actual_text = "${" + act_key[0] + '.' + sub_key[1] + "}"
                                                    var eval_text = eval('`' + evalu_str + '`')
                                                    text = replaceString(text, actual_text, eval_text);
                                                    done1();

                                                })
                                            }
                                        }
                                        else {
                                            //
                                          
                                            bot_interaction.find({ "from": incom_req.body.sender_id, "message_by": "user", "convers_id": incom_req.body.convers_id, "bot_details.state": key }).sort({ _id: -1 }).limit(1).exec(function (err, inter_docs) {

                                           
                                                if (inter_docs.length > 0) {
                                                    var ans = inter_docs[0].text.body
                                                    var actual_text = "{{" + key + "}}"

                                                    text = replaceString(text, actual_text, ans);

                                                    // updated_text = eval('`' + updated_text + '`')

                                                 
                                                    done1();

                                                }
                                                else {
                                                    done1();

                                                }

                                            })
                                        }

                                    })(key);
                                }, function (notAborted, arr) {
                                    var data1 = {
                                        "server": "true",
                                        "message": text,
                                        "text": true,
                                    }
                                    if (doc.buttons) {
                                        if (doc.buttons.length > 0) {
                                         
                                            data1['buttons'] = doc.buttons
                                        }
                                    }
                                    data1['icon'] = true;
                                    final_data['mess_data'].push(data1)
                                  
                                    done();

                                })
                            }
                            else {
                                var data1 = {
                                    "server": "true",
                                    "message": eval('`' + doc.text + '`'),
                                    "text": true,
                                }
                                if (doc.buttons) {
                                    if (doc.buttons.length > 0) {

                                        data1['buttons'] = doc.buttons
                                    }
                                }
                                data1['icon'] = true;
                                final_data['mess_data'].push(data1)
                                done();
                            }

                        } //end of text cond
                        else if (doc.videoID != "") {
                            var data1 = {
                                "server": "true",
                                "message": doc.videoID,
                                "video": true,
                            }
                            if (doc.buttons) {
                                if (doc.buttons.length > 0) {

                                    data1['buttons'] = doc.buttons
                                }
                            }
                            data1['icon'] = true;

                            final_data['mess_data'].push(data1)
                            done();
                        }
                        else {
                            var data1 = {
                                "server": "true",
                                "message": {
                                    "title": doc.title,
                                    "subtitle": doc.sub_title,
                                    "img": doc.img_name
                                },
                                "img": true,
                            }
                            if (doc.buttons) {
                                if (doc.buttons.length > 0) {

                                    data1['buttons'] = doc.buttons
                                }
                            }
                            data1['icon'] = true;

                            final_data['mess_data'].push(data1)
                          
                            done();
                        }

                        //  }
                        //    }
                    })(doc);
                }, function (notAborted, arr) {
                 
                    self.send_next_mess(req, res, botflow, final_data, interaction, api_resp);
                })

            }
        }

    },
    send_next_mess: function (req, res, botflow, final_data, interaction, api_resp) {

        if (botflow.input_option == 'Carousel') {
         
          
            if (botflow.exp_in_mess.carousel.length == 1) {
              
              
                if (api_resp.status == 200) {
                    carousel_array = [];

                    var api = api_resp;
                    forEach(api.data.data, function (apis, i, arr) {
                     
                        var keys = Object.keys(botflow.exp_in_mess.carousel[0]);

                        var new_car = {}
                        carousel_array[i] = {};

                        forEach(keys, function (key, index, arr) {
                            new_car[key] = "";
                            var eval_exp = eval('`' + botflow.exp_in_mess.carousel[0][key] + '`')
                            new_car[key] = eval_exp
                            carousel_array[i] = new_car

                        })



                    })
                    final_data['text'] = true;
                 
                    var data1 = {
                        "server": "true",
                        "next_rule": botflow.connected_state
                    }
                    var index = final_data['mess_data'].length;
                    final_data['mess_data'][index - 1]["carousel"] = true
                    final_data['mess_data'][index - 1]["carousel_array"] = carousel_array

                    final_data['mess_data'][index - 1]["next_rule"] = botflow.connected_state
                    self.save_conv(req, res, interaction);
                  
                    res.send(final_data);
                } else {
                    carousel_array = [];
                  
                    final_data['text'] = true;
                    var data1 = {
                        "server": "true",

                        "next_rule": botflow.connected_state
                    }
                  
                    var index = final_data['mess_data'].length;
                    final_data['mess_data'][index - 1]["next_rule"] = botflow.connected_state
                    final_data['mess_data'][index - 1]["carousel"] = true
                    final_data['mess_data'][index - 1]["carousel_array"] = carousel_array


                 
                    self.save_conv(req, res, interaction);
                    res.send(final_data);
                }


            }
            else {
                self.save_conv(req, res, interaction);
                final_data['text'] = true;
                var data1 = {
                    "server": "true",
                    "next_rule": botflow.connected_state
                }
             
                var index = final_data['mess_data'].length;
                final_data['mess_data'][index - 1]["next_rule"] = botflow.connected_state
                final_data['mess_data'][index - 1]["carousel"] = true
                final_data['mess_data'][index - 1]["carousel_array"] = botflow.exp_in_mess.carousel
                res.send(final_data);

            }
        }
        else if (botflow.input_option == 'Suggestion') {
            if (botflow.sugg_conn_state) {


                final_data['text'] = true;
                var data1 = {
                    "server": "true",
                    "options": botflow.sugg_conn_state,
                    "suggestions": true,
                }
                var index = final_data['mess_data'].length;
                final_data['mess_data'][index - 1]["suggestions"] = true
                final_data['mess_data'][index - 1]["options"] = botflow.sugg_conn_state

                //final_data['mess_data'].push(data1)
                self.save_conv(req, res, interaction);
                res.send(final_data);
            }
        } else if (botflow.input_option == 'Location') {
            final_data['text'] = true;
            var data1 = {
                "server": "true",
                "Location": "true",
                "next_rule": botflow.connected_state
            }
            var index = final_data['mess_data'].length;
            final_data['mess_data'][index - 1]["server"] = true
            final_data['mess_data'][index - 1]["Location"] = true
            final_data['mess_data'][index - 1]["next_rule"] = botflow.connected_state
            self.save_conv(req, res, interaction);
            res.send(final_data);
        }
        else if (botflow.input_option == 'Upload') {
            final_data['text'] = false;
            final_data['attach'] = true;
            var index = final_data['mess_data'].length;
            final_data['mess_data'][index - 1]["server"] = true
            final_data['mess_data'][index - 1]["Upload"] = true
            final_data['mess_data'][index - 1]["next_rule"] = botflow.connected_state
            self.save_conv(req, res, interaction);
            res.send(final_data);
        }
        else if (botflow.input_option == 'Star Rating') {

            var index = final_data['mess_data'].length;
            final_data['mess_data'][index - 1]["star"] = true;
            final_data['mess_data'][index - 1]["text"] = false;
            final_data['mess_data'][index - 1]["server"] = true
            final_data['mess_data'][index - 1]["star_states"] = botflow.star_states;
            self.save_conv(req, res, interaction);
            res.send(final_data);

        }
        else if (botflow.input_option == 'Text') {

            final_data['text'] = false;
            if (botflow.exp_in_mess.inputType == "Date") {
                var data1 = {
                    "server": "true",
                    "Date": "true",
                    "max_date": botflow.exp_in_mess.max_date,
                    "min_date": botflow.exp_in_mess.min_date,
                    "next_rule": botflow.connected_state
                }
                var index = final_data['mess_data'].length;
                final_data['mess_data'][index - 1]["server"] = true
                final_data['mess_data'][index - 1]["Date"] = true
                final_data['mess_data'][index - 1]["max_date"] = botflow.exp_in_mess.max_date
                final_data['mess_data'][index - 1]["min_date"] = botflow.exp_in_mess.min_date
                final_data['mess_data'][index - 1]["next_rule"] = botflow.connected_state
                // final_data['mess_data'].push(data1)
                self.save_conv(req, res, interaction);
                res.send(final_data);
            }
            else if (botflow.exp_in_mess.inputType == "Time") {
                var index = final_data['mess_data'].length;
                final_data['mess_data'][index - 1]["server"] = true
                final_data['mess_data'][index - 1]["Time"] = true
                final_data['mess_data'][index - 1]["next_rule"] = botflow.connected_state
                // final_data['mess_data'].push(data1)
                self.save_conv(req, res, interaction);
                res.send(final_data);
            }
            else if (botflow.exp_in_mess.inputType != "Date" && botflow.exp_in_mess.inputType != "Time") {
                var data1 = {
                    "server": "true",
                    "type": botflow.exp_in_mess.inputType,
                    "inputValidation": botflow.exp_in_mess.inputValidation,
                    "capitalizeText": botflow.exp_in_mess.capitalizeText,
                    "keyType": botflow.exp_in_mess.keyType,
                    "next_rule": botflow.connected_state
                }
                var index = final_data['mess_data'].length;
                final_data['mess_data'][index - 1]["type"] = botflow.exp_in_mess.inputType
                final_data['mess_data'][index - 1]["inputValidation"] = botflow.exp_in_mess.inputValidation
                final_data['mess_data'][index - 1]["capitalizeText"] = botflow.exp_in_mess.capitalizeText
                final_data['mess_data'][index - 1]["keyType"] = botflow.exp_in_mess.keyType
                final_data['mess_data'][index - 1]["next_rule"] = botflow.connected_state

                //final_data['mess_data'].push(data1)
                self.save_conv(req, res, interaction);
                res.send(final_data);
            }
            else {


            }
        }
        else {
            if (botflow.connected_state != '') {

                var index = final_data['mess_data'].length;
                final_data['mess_data'][index - 1]["next_rule"] = botflow.connected_state
            }
            else {

            }
            final_data['text'] = true;
         
            self.save_conv(req, res, interaction);
            res.send(final_data);
        }
    },
    getmess: function (req, res) {

        var incom_req = req;
        if (req.body.api_exec) {
         
        }
        else {

            bot_interaction.find({ "to": req.body.sender_id, "message_by": "bot", "convers_id": req.body.convers_id, 'id': req.body.msg_id }).sort({ _id: -1 }).limit(1).exec(function (err, inter_docs) {
                if (inter_docs.length > 0) {
                    interaction = {
                        bot_details: {
                            bot_id: req.body.botId,
                            state: inter_docs[0].bot_details.state
                        },
                        response: req.body.answer,
                        id: req.body.msg_id,
                        message_by: "user",
                        convers_id: req.body.convers_id,
                        from: req.body.sender_id,
                        type: req.body.type,

                    }
                    interaction[req.body.type] = req.body[req.body.type]
                    self.save_conv(incom_req, res, interaction);
                }

            })
        }


        botModel.aggregate([
            { $match: { "botId": req.body.botId } },
            { $unwind: "$botFlow" },
            {
                $match: {
                    'botFlow.state_name': req.body.state
                }
            }
        ], function (err, data) {

            var botflow = data[0].botFlow;

            var final_data = {
                carousel_background: data[0].carousel_background,
                carousel_button: data[0].carousel_button,
            };
            var curr_msg_id = uniqid.time()
            var interaction = {
                bot_details: {
                    bot_id: req.body.botId,
                    state: data[0].botFlow.state
                },
                id: curr_msg_id,
                message_by: "bot",
                convers_id: req.body.convers_id,
                to: req.body.sender_id
            }
            final_data['optional_input'] = botflow.optional_input

            if (botflow.input_option == 'None' && botflow.connected_state == '') {
                final_data['final_state'] = true;
            } else {
                final_data['final_state'] = false;
            }
            final_data['mess_data'] = [];
            final_data['msg_id'] = curr_msg_id;

            final_data['state'] = botflow.state


            var api_resp = "";
            var pos_body = {
            }
            var pos_params = {

            }
            var headers = {

            }


            if (botflow.entry_response) {
                var api_resp = ""
                var failure_state = botflow.ent_api_details[0].failure_state;
                apiModel.find({ ApiName: botflow.ent_api_details[0].api_name, "_id": botflow.ent_api_details[0].api_id }, function (req, api_res) {
                    api_det = api_res[0].api_info
                   
                    var method_det = {

                    }
                    method_det.url = api_det.url;
                    method_det.method = api_det.method
                    if (api_det.headerparams.length > 0) {
                        method_det.headers = {};
                        forEach(api_det.headerparams, function (item, index, arr) {
                            method_det.headers[item.paramkey] = item.paramvalue
                        })
                    }

                    if (botflow.ent_api_details[0].apiparams && botflow.ent_api_details[0].apiparams.length > 0) {
                     
                        var keys = Object.keys(botflow.ent_api_details[0].apiparams[0]);

                        forEach(keys, function (key, index, arr) {
                            var done = this.async();
                            (function findindex(key) {

                                if (botflow.ent_api_details[0].apiparams[0][key].includes("{{")) {
                                    var found = "",          // an array to collect the strings that are 
                                        rxp = /{{([^}]+)}}/g,

                                        curMatch;

                                    while (curMatch = rxp.exec(botflow.ent_api_details[0].apiparams[0][key])) {
                                        found = curMatch[1];
                                    }
                                    var match = { "from": incom_req.body.sender_id, "bot_details.state": found, "convers_id": incom_req.body.convers_id }
                                    bot_interaction.find(match).sort({ _id: -1 }).limit(1).exec(function (err, inter_docs) {
                                        if (inter_docs.length > 0) {
                                            var ans = inter_docs[0].text.body
                                            // api_det.params[key] = ans
                                            api_det.params[index]['paramvalue'] = ans
                                            done();
                                        } else {
                                            done();
                                        }
                                    })
                                }
                                else if (botflow.ent_api_details[0].apiparams[0][key].includes("${")) {
                                    var spilt_str = botflow.ent_api_details[0].apiparams[0][key].split("${");

                                    var after_spilt_str = spilt_str[1].substr(0, spilt_str[1].indexOf('.'));
                                    var act_key = after_spilt_str.split(".");
                                    var sub_key = spilt_str[1].split(act_key[0] + '.');
                              

                                    var evalu_str = "${" + sub_key[1]
                                 
                                

                                    var match = { "to": incom_req.body.sender_id, "bot_details.state": act_key[0], "convers_id": incom_req.body.convers_id }
                                    bot_interaction.find(match).sort({ _id: -1 }).limit(1).exec(function (err, inter_docs) {
                                        if (inter_docs.length > 0) {

                                            var api = inter_docs[0].api;

                                            var ans = eval('`' + evalu_str + '`')

                                            api_det.params[index]['paramvalue'] = ans
                                            done();
                                        } else {
                                            done();
                                        }
                                    })


                                }
                                else {
                                    done();
                                }

                            })(key);
                        }, function (notAborted, arr) {
                            // method_det.param = api_det.params;
                         
                            method_det.param = '';
                            forEach(api_det.params, function (item, index, arr) {
                            
                                if (index == 0) {
                                    method_det.param = [item.paramkey] + "=" + item.paramvalue + '&'
                                }
                                else {
                                    method_det.param = method_det.param + [item.paramkey] + "=" + item.paramvalue + '&'
                                }

                            })
                        
                            request.post({
                                json: true,
                                url: apiurl + "/execute_api",
                                body: method_det
                            }, function (error, response, body) {
                                console.log(error, body);
                                if (response) {
                                    var res_data = {
                                        data: response.body,
                                        status: response.statusCode
                                    }
                                  
                                    api_resp = response.body;
                                    //  flowarr.api_res = response.body;
                                    self.process_data(incom_req, res, final_data, botflow, api_resp, interaction);
                                   
                                }
                                else {
                                    self.process_data(incom_req, res, final_data, botflow, api_resp, interaction);
                                }

                            });

                        })
                    }

                    else if (botflow.ent_api_details[0].apires && botflow.ent_api_details[0].apires.length > 0 && api_det.body_type == "raw") {

                        if (botflow.previous_message) {
                            if (botflow.ent_api_details[0].apires.length > 0) {
                                var keys = Object.keys(botflow.ent_api_details[0].apires[0]);
                            }


                            forEach(keys, function (key, index, arr) {
                                var done = this.async();
                                (function findindex(key) {

                                    if (botflow.ent_api_details[0].apires[0][key].includes("${")) {
                                        var spilt_str = botflow.ent_api_details[0].apires[0][key].split("${");

                                        var after_spilt_str = spilt_str[1].substr(0, spilt_str[1].indexOf('.'));
                                        var act_key = after_spilt_str.split(".");
                                        var sub_key = spilt_str[1].split(act_key[0] + '.');
                                       
                                        var evalu_str = "${" + sub_key[1]
                                      
                            
                                        var match = { "to": incom_req.body.sender_id, "bot_details.state": act_key[0], "convers_id": incom_req.body.convers_id }
                                        bot_interaction.find(match).sort({ _id: -1 }).limit(1).exec(function (err, inter_docs) {
                                            if (inter_docs.length > 0) {

                                                var api = inter_docs[0].api;

                                                var ans = eval('`' + evalu_str + '`')

                                                api_det.raw_json[key] = ans
                                                done();
                                            } else {
                                                done();
                                            }
                                        })
                                    }
                                    else if (botflow.ent_api_details[0].apires[0][key].includes("{{")) {
                                        var found = "",          // an array to collect the strings that are 
                                            rxp = /{{([^}]+)}}/g,

                                            curMatch;

                                        while (curMatch = rxp.exec(botflow.ent_api_details[0].apires[0][key])) {
                                            found = curMatch[1];
                                        }
                                        var match = { "from": incom_req.body.sender_id, "bot_details.state": found, "convers_id": incom_req.body.convers_id }
                                        bot_interaction.find(match).sort({ _id: -1 }).limit(1).exec(function (err, inter_docs) {
                                            if (inter_docs.length > 0) {
                                                var ans = inter_docs[0].text.body
                                                // api_det.params[key] = ans
                                                api_det.raw_json[key] = ans
                                                done();
                                            } else {
                                                done();
                                            }
                                        })
                                    }
                                    else {
                                        var match = { "from": incom_req.body.sender_id, "bot_details.state": botflow.ent_api_details[0].apires[0][key], "convers_id": incom_req.body.convers_id }
                                        bot_interaction.find(match).sort({ _id: -1 }).limit(1).exec(function (err, inter_docs) {
                                            if (inter_docs.length > 0) {
                                                var ans = inter_docs[0].text.body
                                                api_det.raw_json[key] = ans
                                                done();
                                            } else {
                                                done();
                                            }
                                        })
                                    }



                                })(key);
                            }, function (notAborted, arr) {
                                method_det.body = api_det.raw_json;
                                request.post({
                                    json: true,
                                    url: apiurl + "/execute_api",
                                    body: method_det
                                }, function (error, response, body) {
                                    console.log(error, body);
                                    if (response) {
                                        var res_data = {
                                            data: response.body,
                                            status: response.statusCode
                                        }
                                      
                                        api_resp = response.body;
                                     
                                        self.process_data(incom_req, res, final_data, botflow, api_resp, interaction);
                                       
                                    }
                                    else {
                                        self.process_data(incom_req, res, final_data, botflow, api_resp, interaction);
                                    }

                                });
                            })

                        }
                    }
                    

                    else {
                        if (api_det.body.length > 0) {
                            method_det.body = {};
                            forEach(api_det.body, function (item, index, arr) {
                                method_det.body[item.bodykey] = item.bodyvalue
                            })
                        }
                        if (api_det.params.length > 0) {
                            method_det.param = '';
                            forEach(api_det.params, function (item, index, arr) {
                                if (index == 0) {
                                    method_det.param = [item.paramkey] + "=" + item.paramvalue + '&'
                                }
                                else {
                                    method_det.param = method_det.param + [item.paramkey] + "=" + item.paramvalue + '&'
                                }

                            })

                        }

                        request.post({
                            json: true,
                            url: "/execute_api",
                            body: method_det
                        }, function (error, response, body) {
                            if (response) {
                                var res_data = {
                                    data: response.body,
                                    status: response.statusCode
                                }
                                api_resp = response.body;

                                self.process_data(incom_req, res, final_data, botflow, api_resp, interaction);
                            }
                            else {
                                self.process_data(incom_req, res, final_data, botflow, api_resp, interaction);
                            }

                        });
                    }

                })
            } else {
                self.process_data(incom_req, res, final_data, botflow, api_resp, interaction);
            }

        });

    },
    upload_img: function (req, res) {

        filepath = './uploads/' + req.query.botId + '/chat_conv/' + req.query.sender_id;
        mkdirp('./uploads/' + req.query.botId + '/chat_conv/' + req.query.sender_id, function (err) {

            if (err) console.error(err)
            else console.log('pow!')
        });

        upload.any(console.log(req.body, "body", req.query, "sjnksdn"))(req, res, function (err) {

            if (err) {
                console.log(err);
                res.json({ error_code: 1, err_desc: err });
                return;
            }
            self.getmess(req, res);
            //res.json({error_code:0,err_desc:null});
        });

    },
    execute_api: function (req, res) {

        var url = req.body.url;
        var headers = req.body.headers;
        var pos_body = req.body.body;
        var pos_params = req.body.params;
        var params = req.body.param;

        if (req.body.method == 'get') {
            if (req.body.param) {
                url = url + '?' + params
            }

            request.get({
                headers: headers,
                json: true,
                url: url,
            }, function (error, response, body) {
                var res_data = {
                    data: response.body,
                    status: response.statusCode
                }
                res.send(res_data);
            });
        }
        else if (req.body.method == 'post') {

            if (headers['Content-Type'] == 'application/x-www-form-urlencoded') {

                var pos_body = querystring.stringify(pos_body);
                var contentLength = pos_body.length;
            }
            else {

                var pos_body = pos_body;
            }
          
            request.post({
                headers: headers,
                json: true,
                url: url,
                body: pos_body
            }, function (error, response, body) {

                var res_data = {
                    data: response.body,
                    status: response.statusCode
                }

                res.send(res_data);
            });
        }
        else {
            console.log("method not defined");
        }
    },
    save_conv: function (req, res, interaction) {
        bot_interaction.create(interaction, function (err, resp) {
         
          
        })
    },

    save_conv_old: function (req, res) {

        var d = new Date();
        var year = d.getFullYear();
        var month = ('0' + (d.getMonth() + 1)).slice(-2);
        var day = ('0' + d.getDate()).slice(-2);
        var date_idx = d.getFullYear() + '' + ('0' + (d.getMonth() + 1)).slice(-2) + '' + ('0' + d.getDate()).slice(-2);

        var data_body = {
            date_idx: date_idx,
            bot_id: req.body.bot_id,
            sender_id: req.body.sender_id,
            interaction: req.body.interactions,
            device_info: req.body.device_info
        }
        var interactions = {
            sender_id: req.body.sender_id,
            interaction: req.body.interactions,
            device_info: req.body.device_info
        }
        var state = req.body.curr_state


        bot_interaction.find({ bot_id: req.body.bot_id, date_idx: date_idx, sender_id: req.body.sender_id, convers_id: req.body.convers_id }, function (err, data) {

            if (err)
                console.log(err);
            if (data.length > 0) {
              
                //  if(data[0].interaction[req.body.curr_state].msg_id == req.body.interactions[req.body.curr_state].msg_id ){
                if (data[0].interaction) {
                    bot_interaction.update(
                        { bot_id: req.body.bot_id, date_idx: date_idx, sender_id: req.body.sender_id, convers_id: req.body.convers_id },
                        {
                            "$set":
                            {
                                ["interaction." + req.body.curr_state]: req.body.interactions[req.body.curr_state]
                            }
                        }, function (err, resp) {
                            if (!err) {
                                if (req.body.final || req.body.interrupt) {
                                    self.send_mail(req, res);
                                }
                            }
                        })
                }

            }
            else {
                bot_interaction.create(data_body, function (err, resp) {
                  
                    if (req.body.final) {
                        self.send_mail(req, res);
                    }
                })
            }


        })



    },
    send_mail: function (req, res) {

        var interaction = JSON.stringify(req.body.interactions)
        interaction = interaction.replace(/{/g, '');
        interaction = interaction.replace(/"/g, '');
        interaction = interaction.replace(/},/g, '<br>');
        interaction = interaction.replace(/}/g, '<br>');


        botModel.find({ botId: req.body.bot_id }, { postemail: 1, botName: 1 }, function (err, data) {

            if (data.length > 0) {
                var email = data[0].postemail;
                mailOpts = {
                    from: 'conversation@actonbot.com',
                    to: email,
                    subject: "Bot Conversation " + data[0].botName,
                    html: interaction
                };

                smtpTransport.sendMail(mailOpts, function (error, info) {
                    if (error) {
                        console.log(error);
                        return res.send(error);
                    }
                    console.log('Message sent: ' + info.response);
                    return res.send(info.messageId);
                });
            }
            else {
                console.log("no email id found");
            }
        })
    }

};