
var config = require('../config');
var request = require("request");
var forEach = require('async-foreach').forEach;


var Grid = require('gridfs-stream');
var fs = require('fs');
var _D = require('lodash');
var qs = require('qs');
var bodyParser = require('body-parser')

var user = require('../models/userModel');
var session = require('../models/sessionModel');
var jwt = require('jsonwebtoken');
var role = require('../models/userroleModel');
var crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'mgagebot';
var self = module.exports = {
    create_user: function (req, res) {
        var cipher = crypto.createCipher(algorithm, password)
        var crypted = cipher.update(req.body.password, 'utf8', 'hex')
        crypted += cipher.final('hex');
        req.body.password = crypted;
        user.find({ username: req.body.username}, function (err, doc) {
            console.log(doc);
            if(doc.length > 0){
                res.send("user name already exists");
            }
            else{
                user.create(req.body, function (err) {
                    if (err) return console.log("ERROR", err);
                    return res.sendStatus(202);
                });
            }
        });
       
    },
    login: function (req, res) {
        var cipher = crypto.createCipher(algorithm, password)
        var crypted = cipher.update(req.body.password, 'utf8', 'hex')
        crypted += cipher.final('hex');
        req.body.password = crypted;
        console.log(crypted);
        user.find({ username: req.body.username}, function (err, doc) {
            if (err) return res.send({ status: "err" });
            else{
                if(doc.length >0){
                    if(doc[0].password === crypted ){

                            var token = jwt.sign(doc[0].username+crypted, 'karixrcm');
                                var session_body = {
                                    user_id :doc[0]._id,
                                    token : token,
                                    firstname :doc[0].username
                                }
                                session.create(session_body,function(err,result){
                                    return  res.send({
                                        _id : doc[0]._id,
                                        role_id : doc[0].role_id,
                                        username:doc[0].username,
                                        token : token
                                    })
                                })
        
                        // return res.send({_id : doc[0]._id,
                        //                 role_id : doc[0].role_id,
                        //                 username:doc[0].username});
                    }
                    else{
                         return res.send({status : "password does not match"});
                    }
                }
                else{
                      return res.send({status : "user not found"});
                }
            }
          
        });
    },
    decrypt: function decrypt(req,res) { 
        var text= req.query.text;
        console.log(text);
         var decipher = crypto.createDecipher(algorithm, password)
         var dec = decipher.update(text, 'hex', 'utf8')
         dec += decipher.final('utf8');
         res.send(dec);
        
    },
    
    create_role : function(req,res) {
        role.create(req.body, function (err) {
            if (err) return console.log("ERROR", err);
            return res.sendStatus(202);
        });
    } ,
    get_roles : function(req,res){
        role.find({role_id :{$ne : 0} }, function (err, doc) {
            res.send(doc);
        })
    } 

};