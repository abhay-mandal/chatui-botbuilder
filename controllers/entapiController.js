var entapiModel =  require('../models/entapiModel');
var self = module.exports = {

    saveapi: function(req,res){
        console.log(req.body);
        req.body.created_by = req.headers.user;
        entapiModel.create(req.body,function(err){
            if(err)
                console.log(err);
            res.send({"message":"successfully saved","status":"ok"});
        })
    },
    getapi : function(req,res){
        entapiModel.find({}, function(err, apis) {
             res.send(apis);
               });
    },
    updateapi : function(req,res){
        var id = req.body._id
        req.body.updated_on = new Date();
        req.body.LastUpdatedBy = req.headers.user;
        entapiModel.update({ "_id": id },
            req.body,
            function(err, affected, resp) {
                if (!err) {
                    console.log("successfully updated");
                    res.send({ "status":"success" });
                }
            })
    },
    deleteapi : function(req,res){
        entapiModel.remove({ "_id": req.body._id }, function(err, data) {
             if (data.result.ok == 1) {
                 res.send({
                     data: "successfully deleted",
                     status: "success"
                 });
             }
         });
    }
};