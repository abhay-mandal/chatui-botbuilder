const SandboxInteraction = require('../models/sandboxInteractionModel');
const BotFlow = require('../models/botFlow.model');

const logger = require('../log/logger');
const USER_TYPE_ARRAY = ["BOT", "USER"];
const MESSAGE_BY_BOT = "BOT";
const MESSAGE_BY_USER  = "USER";
const FILE_TYPE_ARRAY = ["IMAGE", "DOCUMENT", "VOICE"];
const CONVERSATION_TYPE_TEXT = 'TEXT';
const CONVERSATION_TYPE_LOCATION = "LOCATION";
const STATS_TYPE_CONVERSATIONS = 'CONVERSATIONS';
const STATS_TYPE_MESSAGES = 'MESSAGES'; 
const STATS_TYPE_USERS = 'USERS';
const STATS_TYPE_FILES = 'FILES';
const CHART_TYPE_MESSAGES_USERS = "MESSAGES_USERS";
const CHART_TYPE_CONVERSATIONS_USERS = "CONVERSATIONS_USERS";
const CHART_TYPE_USERS_FILES = "USERS_FILES";
const CHART_TYPE_CONVERSATION_SUNBURST = "CONVERSATION_SUNBURST";


exports.getStats = (req, res) => {
  const type = req.query.type;
  const botId = req.query.botId;
  const apiName = "/stats";
  
  logger.info(`ReportController - GET /stats invoked with params ${JSON.stringify(req.query)}`);

  if(!this.isValidParam(type)){
    logger.error(`GET /${apiName} invoked without 'type', required field`);
    return res.status(400).send({ "error_message": "type is required" });
  }

  if(!this.isValidParam(botId)){
    logger.error(`GET /${apiName} invoked without 'botId', required field`);
    return res.status(400).send({ "error_message": "botId is required" });
  }

  var fromDate = req.query.fromDate;
  var toDate = req.query.toDate;

  if (fromDate && !this.isValidDate(fromDate)) {
    logger.error(`GET /${apiName} invoked with incorrect from date ${fromDate}`);
    res.status(400).send({ "error_message": "Invalid from date" });
  }
  if (toDate && !this.isValidDate(toDate)) {
    logger.error(`GET /${apiName} invoked with incorrect to date ${toDate}`);
    res.status(400).send({ "error_message": "Invalid to date" });
  }
  
  let isoDateStrings = this.convertDates(fromDate, toDate);

  switch(type.toUpperCase()) {
    case STATS_TYPE_CONVERSATIONS: this.findConversationStats(apiName, botId, isoDateStrings, res);
                                   break;
    case STATS_TYPE_MESSAGES: this.findMessagesStats(apiName, botId, req.query.messageBy, isoDateStrings, res);
                              break;
    case STATS_TYPE_FILES: this.findFilesStats(apiName, botId, req.query.messageBy, isoDateStrings, res);
                           break;
    case STATS_TYPE_USERS: this.findUsersStats(apiName, botId, isoDateStrings, res);
                           break;
    default: res.status(400).send({ "error_message": "Invalid type" });
             break;
  }
}

exports.findConversationStats = (apiName, botId, isoDateStrings, res) => {
    SandboxInteraction.distinct("convers_id", {
      "bot_details.bot_id": botId,
      created_on: {
        $gte: isoDateStrings['fromDate'],
        $lte: isoDateStrings['toDate']
      }
    }).then((doc) => {
      logger.info(`GET /${apiName} sends success response with data ${doc.length}`)
      res.send({ "total_conversations": doc.length });
    }).catch((e) => {
      logger.error(`GET /${apiName} error ${e}`);
      res.status(500).send({ "error_message": e });
    });
}

exports.findMessagesStats = (apiName, botId, messageBy, isoDateStrings, res) => {

  if (!this.isValidMessageBy(messageBy)) {
    logger.error(`GET /${apiName} invoked with invalid 'messageBy' ${messageBy}`);
    return res.status(400).send({ "error_message": "Invalid messageBy" });
  }

  let findOptions = {
    "bot_details.bot_id": botId,
    created_on: {
      $gte: isoDateStrings['fromDate'],
      $lte: isoDateStrings['toDate']
    }
  }

  if (messageBy) {
    findOptions["message_by"] = messageBy.toLowerCase()
  }

  SandboxInteraction.find(findOptions).then((doc) => {
    logger.info(`GET /${apiName} sends success response with data ${doc.length}`)
    res.send({ "total_messages": doc.length });
  }).catch((e) => {
    logger.error(`GET /${apiName} error ${e}`);
    res.status(500).send({ "error_message": e });
  });
}

exports.findUsersStats = (apiName, botId, isoDateStrings, res) => {
  
  SandboxInteraction.distinct("from", {
    "bot_details.bot_id": botId,
    "message_by": MESSAGE_BY_USER.toLowerCase(),
    created_on: {
      $gte: isoDateStrings['fromDate'],
      $lte: isoDateStrings['toDate']
    }
  }).then((doc) => {
    logger.info(`GET /${apiName} sends success response with data ${doc.length}`)
    res.send({ "total_users": doc.length });
  }).catch((e) => {
    logger.error(`GET /${apiName} error ${e}`);
    res.status(500).send({ "error_message": e });
  });
}

exports.findFilesStats = (apiName, botId, messageBy, isoDateStrings, res) => {

  if (!this.isValidMessageBy(messageBy)) {
    logger.error(`GET /${apiName} invoked with invalid 'messageBy' ${messageBy}`);
    return res.status(400).send({ "error_message": "Invalid messageBy" });
  }

  let findOptions = {
    "bot_details.bot_id": botId,
    type: { $in: ["image", "voice", "document"] },
    created_on: {
      $gte: isoDateStrings['fromDate'],
      $lte: isoDateStrings['toDate']
    }
  }

  if (messageBy) {
    findOptions["message_by"] = messageBy.toLowerCase()
  }

  SandboxInteraction.find(findOptions).then((doc) => {
    logger.info(`GET /${apiName} sends success response with data ${doc.length}`)
    res.send({ "total_files": doc.length });
  }, (e) => {
    logger.error(`GET /${apiName} error ${e}`);
    res.status(500).send({ "error_message": e });
  });
}

exports.getChartsData = (req, res) => {
  const type = req.query.type;
  const botId = req.query.botId;
  const apiName = "charts";

  logger.info(`ReportController - GET /charts invoked with params ${JSON.stringify(req.query)}`);

  if(!this.isValidParam(type)){
    logger.error(`GET /${apiName} invoked without 'type', required field`);
    return res.status(400).send({ "error_message": "type is required" });
  }

  if(!this.isValidParam(botId)){
    logger.error(`GET /${apiName} invoked without 'botId', required field`);
    return res.status(400).send({ "error_message": "botId is required" });
  }

  let fromDate = req.query.fromDate;
  let toDate = req.query.toDate;

  if (fromDate && !this.isValidDate(fromDate)) {
    logger.error(`GET /${apiName} invoked with incorrect from date ${fromDate}`);
    res.status(400).send({ "error_message": "Invalid from date" });
  }
  if (toDate && !this.isValidDate(toDate)) {
    logger.error(`GET /${apiName} invoked with incorrect to date ${toDate}`);
    res.status(400).send({ "error_message": "Invalid to date" });
  }

  var isoDateStrings = this.convertDates(fromDate, toDate);

  switch(type.toUpperCase()) {
    case CHART_TYPE_MESSAGES_USERS: this.findMessageVsUsersData(apiName, botId, req.query.messageBy, isoDateStrings, res);
                                   break;
    case CHART_TYPE_CONVERSATIONS_USERS: this.findConversationVsUsersData(apiName, botId, isoDateStrings, res);
                              break;
    case CHART_TYPE_USERS_FILES: this.findUsersVsFilesData(apiName, botId, req.query.messageBy, isoDateStrings, res);
                           break;
    case CHART_TYPE_CONVERSATION_SUNBURST: this.findConversationSunburstChartData(apiName, botId, isoDateStrings, res);
          break;
    default: res.status(400).send({ "error_message": "Invalid type" });
             break;
  }

}
exports.findMessageVsUsersData = (apiName, botId, messageBy, isoDateStrings, res) => {

  if (!this.isValidMessageBy(messageBy)) {
    logger.error(`GET /${apiName} invoked with invalid 'messageBy' ${messageBy}`);
    return res.status(400).send({ "error_message": "Invalid messageBy" });
  }

  SandboxInteraction.find({
    "bot_details.bot_id": botId,
    created_on: {
      $gte: isoDateStrings['fromDate'],
      $lte: isoDateStrings['toDate']
    }
  }).then((doc) => {
    var responseData = this.constructMessageAndUserMap(doc, isoDateStrings, messageBy);
    logger.info(`GET /${apiName} sends success response`)
    logger.debug(`GET /${apiName} sends success response ${responseData}`)
    res.send(responseData);
  }).catch((e)=> {
    logger.error(`GET /${apiName} error ${e}`);
    res.status(500).send({ "error_message": e });
  });
}


exports.findConversationVsUsersData = (apiName, botId, isoDateStrings, res) => {
  SandboxInteraction.find({
    "bot_details.bot_id": botId,
    created_on: {
      $gte: isoDateStrings['fromDate'],
      $lte: isoDateStrings['toDate']
    }
  }).then((interactionDocs) => {
    var responseData = this.constructConversationAndUserMap(interactionDocs, isoDateStrings);
    logger.info(`GET /${apiName} sends success response`)
    res.send(responseData);
  }).catch((e) => {
    logger.error(`GET /${apiName} error ${e}`);
    res.status(500).send({ "error_message": e });
  });
}

exports.findUsersVsFilesData = (apiName, botId, messageBy, isoDateStrings, res) => {
  if (!this.isValidMessageBy(messageBy)) {
    logger.error(`GET /${apiName} invoked with invalid 'messageBy' ${messageBy}`);
    return res.status(400).send({ "error_message": "Invalid messageBy" });
  }

  let findOptions = {
    "bot_details.bot_id": botId,
    created_on: {
      $gte: isoDateStrings['fromDate'],
      $lte: isoDateStrings['toDate']
    }
  }

  SandboxInteraction.find(findOptions).then((interactionDocs) => {
    var responseData = this.constructUsersAndFilesMap(interactionDocs, isoDateStrings, messageBy);
    logger.info(`GET /${apiName} sends success response`)
    res.send(responseData);
  }, (e) => {
    logger.error(`GET /${apiName} error ${e}`);
    res.status(500).send({ "error_message": e });
  });
}

exports.exportConversation = (req, res) => {
  var botId = req.query.botId;
  let apiName = "downloadConversation";
  
  if(!this.isValidParam(botId)){
    logger.error(`GET /${apiName} invoked without 'botId', required field`);
    return res.status(400).send({ "error_message": "botId is required" });
  }

  logger.info(`GET /${apiName} invoked with query params ${JSON.stringify(req.query)}`);
  var fromDate = req.query.fromDate
  var toDate = req.query.toDate

  if (fromDate && !this.isValidDate(fromDate)) {
    logger.error(`GET /${apiName} invoked with incorrect from date ${fromDate}`);
    res.status(400).send({ "error_message": "Invalid from date" });
  }
  if (toDate && !this.isValidDate(toDate)) {
    logger.error(`GET /${apiName} invoked with incorrect to date ${toDate}`);
    res.status(400).send({ "error_message": "Invalid to date" });
  }

  var isoDateStrings = this.convertDates(fromDate, toDate);

  SandboxInteraction.find({
    "bot_details.bot_id": botId ,
    created_on: {
      $gte: isoDateStrings['fromDate'],
      $lte: isoDateStrings['toDate']
    }
  }).then((interactionDocs) => {
    BotFlow.find({
      botId: botId
    }).then((botFlowDocument) => {
      var responseData = this.constructConversationCsv(botFlowDocument[0], interactionDocs);
      logger.info(`GET /${apiName} sends success response`);
      res.csv(responseData.dataRow, {fields:responseData.headerRow});
    }, (e) => {
      logger.error(`Failed to get bot flow document with ID ${botId} ${e}`);
      res.status(500).send({ "error_message": e });
    });
  }, (e) => {
    logger.error(`GET /${apiName} error ${e}`);
    res.status(500).send({ "error_message": e });
  });
}

exports.getReport = (req, res) => {
  var botId = req.query.botId;
  let apiName = "reports";
  let messageBy = req.query.messageBy;

  logger.info(`GET /${apiName} invoked with query params `+ JSON.stringify(req.query))

  if(!this.isValidParam(botId)){
    logger.error(`GET /${apiName} invoked without 'botId', required field`);
    return res.status(400).send({ "error_message": "botId is required" });
  }

  if (!this.isValidMessageBy(messageBy)) {
    logger.error(`GET /${apiName} invoked with invalid 'messageBy' ${messageBy}`);
    return res.status(400).send({ "error_message": "Invalid messageBy" });
  }

  var fromDate = req.query.fromDate;
  var toDate = req.query.toDate;

  if (fromDate && !this.isValidDate(fromDate)) {
    logger.error(`GET/${apiName} invoked with incorrect from date ${fromDate}`);
    res.status(400).send({ "error_message": "Invalid from date" });
  }
  if (toDate && !this.isValidDate(toDate)) {
    logger.error(`GET /${apiName} invoked with incorrect to date ${toDate}`);
    res.status(400).send({ "error_message": "Invalid to date" });
  }

  var isoDateStrings = this.convertDates(fromDate, toDate);
  let findOptions = {
    "bot_details.bot_id": botId,
    created_on: {
      $gte: isoDateStrings['fromDate'],
      $lte: isoDateStrings['toDate']
    }
  }

  SandboxInteraction.find(findOptions).then((interactionDocs) => {
    var conversationMap = this.constructConversationMap(interactionDocs, isoDateStrings);
    var responseData = this.constructReport(conversationMap, messageBy);
    if (req.query.export === "true") {
      const csvResponse = this.getReportCsvData(responseData);
      res.csv(csvResponse.dataRow, {fields: csvResponse.headerRow});
      logger.info(`GET /${apiName} downloads file successfully.`)
    } else {
      res.send(responseData);
      logger.info(`GET /${apiName} sends success respons`)
    }
  }).catch((e) => {
    logger.error(`GET /${apiName} error ${e}`);
    res.status(500).send({ "error_message": e });
  });
}

exports.findConversationSunburstChartData = (apiName, botId, isoDateStrings, res) => {
  SandboxInteraction.find({
    "bot_details.bot_id": botId,
    created_on: {
      $gte: isoDateStrings['fromDate'],
      $lte: isoDateStrings['toDate']
    }
  }).then((doc) => {
    var responseData = this.constructConvoReportSunburstChartMap(doc, botId);
    logger.info(`GET /api/${apiName} sends success response`, responseData)
    res.send(responseData);
  }).catch((e)=> {
    logger.error(`GET /api/${apiName} error` + e);
    res.status(500).send({ "error_message": e });
  });
}
  //------------Common functions------------------------------------

exports.isValidDate = (date) => {
  if (new Date(date).toString() === "Invalid Date")
    return false;
  else
    return true;
}

exports.isValidParam = (param) => {
  if (!param){
    return false;
  }
  else
    return true;
}

exports.isValidMessageBy = (messageBy) => {
  var isValid = true;
  if (messageBy) {
    messageBy =  messageBy.trim().toUpperCase();
    if (messageBy === '' ||  USER_TYPE_ARRAY.indexOf(messageBy) == -1) {
      isValid = false;
    } 
  }
  return isValid;
}

exports.convertDates = (fromDate, toDate) => {
  var thirtyDayBefore = new Date(new Date().setDate(new Date().getDate()- 30)).toISOString();
  var today = new Date().toISOString();
  if (fromDate && toDate) {
    fromDate = new Date(fromDate).toISOString();
    toDate = new Date(toDate).toISOString();
  } else if (fromDate && !toDate) {
    fromDate = new Date(fromDate).toISOString();
    toDate = today
  } else if (!fromDate && toDate) {
    toDate =  new Date(toDate).toISOString();
    fromDate = new Date(new Date().setDate(new Date(toDate).getDate()- 30)).toISOString();
  } else if (!fromDate && !toDate) {
    fromDate = thirtyDayBefore;
    toDate = today;
  }
  return { 'fromDate': fromDate, 'toDate': toDate };
}

exports.constructConversationMap = (interactionDocs, isoDateStrings) => {
  var conversationMap = {};
  interactionDocs.forEach(document => {
    var createdOnDate = document.created_on.toISOString().split('T')[0];
    if (conversationMap[createdOnDate] != undefined) {
      conversationMap[createdOnDate].push(document);
    } else {
      conversationMap[createdOnDate] = [];
      conversationMap[createdOnDate].push(document);
    }
  });

  if(interactionDocs.length > 0){
    conversationMap  = this.addMissingDates(conversationMap, isoDateStrings.fromDate.split('T')[0], isoDateStrings.toDate.split('T')[0]);
  }
  
  return conversationMap;
}

exports.addMissingDates = (data, fromDate, toDate) => {
  var responseData = {};
  while(new Date(fromDate) <= new Date(toDate)) {
    if (data[fromDate]) {
      responseData[fromDate] = data[fromDate]
    } else {
      responseData[fromDate] = [];
    }
    var nextDay = new Date(fromDate)
    nextDay.setDate(nextDay.getDate() + 1)
    fromDate = nextDay.toISOString().split('T')[0]
  }
  return responseData;
}

exports.filterDataByMessageBy = (dataArray, messageBy) => {
  if (messageBy) {
    messageBy = messageBy.toUpperCase();
    switch(messageBy) {
      case MESSAGE_BY_BOT: 
              dataArray = dataArray.filter(coversation => coversation.message_by.toUpperCase() === MESSAGE_BY_BOT);
              break;
      case MESSAGE_BY_USER: 
              dataArray = dataArray.filter(coversation => coversation.message_by.toUpperCase() === MESSAGE_BY_USER);
                break;
    }
  }
  return dataArray;
}

exports.constructMessageAndUserMap = (interactionDocs, isoDateStrings, messageBy) => {
  var conversationMap = this.constructConversationMap(interactionDocs, isoDateStrings);
  let responseData = [];
  for (const date in conversationMap) {
    if (conversationMap.hasOwnProperty(date)) {
      let dataArray = conversationMap[date];
      let usersData = dataArray.filter(coversation => coversation.message_by.toUpperCase() !== MESSAGE_BY_BOT);
      const uniqueUsers = [...new Set(usersData.map(obj => obj.from))];
      dataArray = this.filterDataByMessageBy(dataArray, messageBy)
      responseData.push({ 'date': date, 'message': dataArray.length, 'users': uniqueUsers.length });
    }
  }
  return responseData;
}

exports.constructConversationAndUserMap = (interactionDocs, isoDateStrings) => {
  var conversationMap = this.constructConversationMap(interactionDocs, isoDateStrings);;  
  var responseData = [];

  for (const date in conversationMap) {
    if (conversationMap.hasOwnProperty(date)) {
      var dataArray = conversationMap[date];
      const uniqueConversation = [...new Set(dataArray.map(obj => obj.convers_id))];
      dataArray = dataArray.filter(coversation => coversation.message_by.toUpperCase() !== MESSAGE_BY_BOT);
      const uniqueUsers = [...new Set(dataArray.map(obj => obj.from))];
      responseData.push({ 'date': date, 'conversations': uniqueConversation.length, 'users': uniqueUsers.length });
    }
  }
  return responseData;
}

exports.constructUsersAndFilesMap = (interactionDocs, isoDateStrings, messageBy) => {
  var conversationMap = this.constructConversationMap(interactionDocs, isoDateStrings);
  var responseData = [];
  for (const date in conversationMap) {
    if (conversationMap.hasOwnProperty(date)) {
      var dataArray = conversationMap[date];
      if (dataArray.length == 0) {
        responseData.push({ 'date': date, 'files': 0, 'users': 0 });
      } else {
        let usersArray = dataArray.filter(coversation => coversation.message_by && coversation.message_by.toUpperCase() === MESSAGE_BY_USER);
        const uniqueUsers = [...new Set(usersArray.map(obj => obj.from))];
        dataArray = this.filterDataByMessageBy(dataArray, messageBy);
        const fileTypeArray = dataArray.filter(coversation => (FILE_TYPE_ARRAY.indexOf(coversation.type && coversation.type.toUpperCase()) > -1));
        responseData.push({ 'date': date, 'files': fileTypeArray.length, 'users': uniqueUsers.length });
      }
    }
  }
  return responseData;
}

exports.constructConversationCsv = (botFlowDocument, interactionDocs) => {
  const botName = botFlowDocument.botName;
      const botId = botFlowDocument.botId;
      let headerRow = ['BOT ID', 'BOT NAME', 'CONVERSATION ID'];
      const botStates = botFlowDocument.botFlow.map(flow => flow.state);
      botStates.forEach(state => {
        headerRow.push(state);
      })
      interactionDocs = interactionDocs.filter(coversation => coversation.message_by.toUpperCase() !== 'BOT');
      var conversationMap = {};
      interactionDocs.forEach(document => {
        var conversationId = document.convers_id
        if (conversationMap[conversationId] != undefined) {
          conversationMap[conversationId].push(document);
        } else {
          conversationMap[conversationId] = [];
          conversationMap[conversationId].push(document);
        }
      });
      var rows = [];
      for (const conversationId in conversationMap) {
        var row = {};
        row[headerRow[0]] = botId; row[headerRow[1]] = botName; row[headerRow[2]] = conversationId;
        if (conversationMap.hasOwnProperty(conversationId)) {
          var conversationDocs = conversationMap[conversationId]
          conversationDocs.forEach(eachDoc => {
            const docType = eachDoc.type.toUpperCase()
            if (eachDoc.bot_details && eachDoc.bot_details.state != undefined) {
              if (docType === CONVERSATION_TYPE_TEXT) {
                row[eachDoc.bot_details.state] = eachDoc.text.body;
              } else if (FILE_TYPE_ARRAY.indexOf(docType) > -1) {
                row[eachDoc.bot_details.state] = `${eachDoc[eachDoc.type].file} ${eachDoc[eachDoc.type].caption}`;
              } else if (docType === CONVERSATION_TYPE_LOCATION) {
                row[eachDoc.bot_details.state] = `Longitude: ${eachDoc[eachDoc.type].longitude}, Latitude: ${eachDoc[eachDoc.type].latitude}`;
              }
            }
          });
          rows.push(row);
        } 
      }
      return {'dataRow': rows, 'headerRow': headerRow};
};

exports.constructReport = (conversationMap, messageBy) => {
  let responseData = [];
  for (const date in conversationMap) {
    if (conversationMap.hasOwnProperty(date)) {
      let dataArray = conversationMap[date];
      let usersData = dataArray.filter(coversation => coversation.message_by.toUpperCase() !== MESSAGE_BY_BOT);
      const uniqueUsers = [...new Set(usersData.map(obj => obj.from))];
      const uniqueConversations = [...new Set(dataArray.map(obj => obj.convers_id))]; 
      dataArray = this.filterDataByMessageBy(dataArray, messageBy);
      let filesCount = dataArray.filter(coversation => (FILE_TYPE_ARRAY.indexOf(coversation.type && coversation.type.toUpperCase()) > -1)).length;
      responseData.push({ 'date': date, 'users': uniqueUsers.length, 'messages': dataArray.length, 'conversations': uniqueConversations.length, 'files':  filesCount});
    }
  }
  return responseData;
}

exports.getReportCsvData = (data) => {
  let headerRow = ['Date', 'Number of Users', 'Number of Messages', 
                      'Number of Conversations', 'Files'];
  let rows = [];
  for (let index = 0; index < data.length; index++) {
    let row = {};
    row[headerRow[0]] = data[index].date;
    row[headerRow[1]] = data[index].users;
    row[headerRow[2]] = data[index].messages;
    row[headerRow[3]] = data[index].conversations;
    row[headerRow[4]] = data[index].files;
    rows.push(row); 
  }
  return  {'dataRow': rows, 'headerRow': headerRow};
}

exports.constructConvoReportSunburstChartMap = (interactionDocs, botId) => {
  var conversationMap = {};
  interactionDocs.forEach(document => {
    var createdOnDate = document.created_on.toISOString().split('T')[0];
    if (conversationMap[createdOnDate] != undefined) {
      conversationMap[createdOnDate].push(document);
    } else {
      conversationMap[createdOnDate] = [];
      conversationMap[createdOnDate].push(document);
    }
  });

  let responseData = [{name: botId, children: []}];
  
  for (const date in conversationMap) {
    if (conversationMap.hasOwnProperty(date)) {
      let dateHierarchyObject = {name: date, children: []};
      let dataArray = conversationMap[date];
      let usersDataArray = dataArray.filter(coversation => coversation.message_by.toUpperCase() !== MESSAGE_BY_BOT);
      const uniqueUsers = [...new Set(usersDataArray.map(obj => obj.from))];
      uniqueUsers.forEach(user => {
        let userObject = {name: user, children: []};
        let userSpecificConverastion = usersDataArray.filter(coversation => (coversation.from === user || conversationMap.to === user));
        let unquieConversation = [...new Set(userSpecificConverastion.map(obj => obj.convers_id))];
        unquieConversation.forEach(conversation => {
          let conversationObject = {name: conversation, children:[]};
          let messagesByConverastionId = userSpecificConverastion.filter(coversation => (coversation.convers_id === conversation));
          messagesByConverastionId.forEach(message => {
            let docType = message.type ? message.type.toUpperCase() : '';
            let name;
            if (docType === CONVERSATION_TYPE_TEXT) {
              name = message.text.body;
            } else if (FILE_TYPE_ARRAY.indexOf(docType) > -1) {
              name = `${message[message.type].file} ${message[message.type].caption}`;
            } else if (docType === CONVERSATION_TYPE_LOCATION) {
              name = `Longitude: ${message[message.type].longitude}, Latitude: ${message[message.type].latitude}`;
            }
            if (name) {
              conversationObject.children.push({name: name});
            }
          });
          userObject.children.push(conversationObject);
        });
        dateHierarchyObject.children.push(userObject);
      });
      responseData[0].children.push(dateHierarchyObject);
    }
  }
  return responseData;
}

