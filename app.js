const dotenv = require('dotenv');
dotenv.load();
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var config = require('./config');

var express = require('express');
var app = express();
var cors = require('cors');
app.use(cors());

var appLogger = require('./log/logger');

var mongoose = require('mongoose');
mongoose.connect(config.rcs.mongodbUrl, { useMongoClient: true });
mongoose.connection.on('connected', function() {
    appLogger.info('Connected to MongoDb '+ config.rcs.mongodbUrl);
});
mongoose.connection.on('error', function(err) {
    appLogger.info('Connecting to MongoDb error'+ err);
});
mongoose.connection.on('disconnected', function() {
    appLogger.info('MongoDb connection disconnected');
});




// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());


app.use(express.static(path.join(__dirname, 'public')));
var chat = require('./routes/chatRoutes')(app);
var rcs = require('./routes/rcsRoutes')(app);
var user = require('./routes/userRoutes')(app);
var entapi = require('./routes/entapiRoutes')(app);

const apiRoutes = require('./routes/index')(app);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// app.use(cors({
//     origin: 'http://localhost:3000'
//   }));

app.all("*", function(req, res, next) {
    console.log('here ');
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    // res.header("Access-Control-Allow-Methods", "GET, PUT, POST,OPTIONS");
    return next();
});

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Cache-Control, Pragma, Origin, Authorization, Content-Type, X-Requested-With, Accept");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST");
    return next();
});

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


app.listen(7005, function() {
    appLogger.info('App listening on port 7005');
});

module.exports = app;