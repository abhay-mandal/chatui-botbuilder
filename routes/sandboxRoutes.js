const sandboxController = require('../controllers/sandboxController');

module.exports = app => {
    app.get('/sandboxconfig', sandboxController.getAllSandboxConfig);
}