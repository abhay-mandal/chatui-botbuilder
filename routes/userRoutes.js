var express = require('express');
var userController = require("../controllers/userController.js");

module.exports = function(app) {
    app.post('/create_user', userController.create_user);
   app.post('/login', userController.login);
   app.get('/getpassword',userController.decrypt);
   app.post('/createrole',userController.create_role);
   app.get('/getroles',userController.get_roles);
};