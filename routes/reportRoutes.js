const reportController = require('../controllers/reportController');

module.exports = app => {
    app.get('/stats', reportController.getStats);
    app.get('/charts', reportController.getChartsData);
    app.get('/downloadConversation', reportController.exportConversation);
    app.get('/report', reportController.getReport);
}