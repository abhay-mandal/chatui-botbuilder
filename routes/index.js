
var reportRouter = require('./reportRoutes');
var sandboxRouter = require('./sandboxRoutes');
module.exports = app => {
    reportRouter(app),
    sandboxRouter(app)
}