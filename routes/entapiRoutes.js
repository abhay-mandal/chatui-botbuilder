var express = require('express');
var entapiController = require("../controllers/entapiController.js");

module.exports = function(app) {
    app.post('/save_entapi',entapiController.saveapi);
    app.get('/getapis',entapiController.getapi);
    app.post('/updateapi',entapiController.updateapi);
    app.post('/deleteapi',entapiController.deleteapi);
};
