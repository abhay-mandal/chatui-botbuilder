var express = require('express');
var chatController = require("../controllers/chatController.js");

module.exports = function(app) {
    app.post('/getfirst_mess',chatController.getinitmess);
    app.post('/send_value',chatController.getmess);
    app.post('/upload_img', chatController.upload_img);
    app.post('/execute_api', chatController.execute_api);
    app.post('/save_conv',chatController.save_conv);
    app.post('/send_email',chatController.send_mail);
};
