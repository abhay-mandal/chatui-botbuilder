var express = require('express');
var rcsController = require("../controllers/rcsController.js");

module.exports = function(app) {
    app.post('/savebotflow', rcsController.save_data);
    app.post('/', rcsController.sendresponse);
    app.get('/listbots', rcsController.listbots);
    app.get('/getbot', rcsController.getbot);
    app.put('/update_bot', rcsController.update_bot);
    app.post('/delete_bot', rcsController.deletebot);
    app.post('/upload', rcsController.upload);
    app.post('/upload_bot_img', rcsController.upload_bot_img);
    app.get('/download_conv', rcsController.download_conv);
    app.get('/getformlist', rcsController.getformlist);
    app.get('/getform_conv', rcsController.getform_conv);
    app.get('/download_media', rcsController.download_media);
    app.get('/getform_conv_media', rcsController.getform_conv_media);
    app.post('/erplogin',rcsController.erplogin);
    app.post('/logout',rcsController.logout);
};