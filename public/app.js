'use strict'
angular.module('webchatbot', ['ui.router', 'oc.lazyLoad', 'ui.bootstrap', 'ngMaterial', 'ngImageCompress', 'ngSanitize', 'dndLists', 'ui.sortable', 'moment-picker', 'ngIntlTelInput', 'ui.grid', 'ngStorage', 'ngFileUpload', 'mdColorPicker', 'naif.base64', 'ui.carousel', 'webcam', 'pdf', 'angAccordion']).config(['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider', '$httpProvider', function($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $httpProvider) {

        $ocLazyLoadProvider.config({
            debug: true,
            events: true
        })

        $urlRouterProvider.otherwise('/login');

        $stateProvider
            .state('dashboard', {
                templateUrl: 'views/main.html',
                url: '/dashboard',

                resolve: {
                    loadMyDirectives: function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'webchatbot',
                            files: [
                                // 'scripts/directives/header/header.js',
                                'scripts/directives/sidebar/sidebar.js',
                                'scripts/directives/loader/loader.js'
                            ]
                        })
                    }
                }
            })
            .state('sandboxlogin', {
                templateUrl: 'views/login.html',
                url: '/sandboxlogin',
                controller: 'LoginCtrl',

                resolve: {
                    loadMyFiles: function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'webchatbot',
                            files: [
                                'scripts/config.js',
                                'scripts/controllers/loginController.js',
                                'scripts/directives/loader/loader.js'
                            ]
                        })
                    }
                }
            })
            .state('login', {
                templateUrl: 'views/entlogin.html',
                url: '/login',
                controller: 'EntLoginCtrl',

                resolve: {
                    loadMyFiles: function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'webchatbot',
                            files: [
                                'scripts/config.js',
                                'scripts/controllers/entloginController.js',
                                'scripts/directives/loader/loader.js'
                            ]
                        })
                    }
                }
            })
            .state('register', {
                templateUrl: 'views/register.html',
                url: '/register',
                controller: 'RegisterCtrl',

                resolve: {
                    loadMyFiles: function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'webchatbot',
                            files: [
                                'scripts/config.js',
                                'scripts/controllers/registerController.js',
                                'scripts/directives/loader/loader.js'
                            ]
                        })
                    }
                }
            })
            .state('dashboard.home', {
                templateUrl: 'views/reportdashboard.html',
                url: '/home',
                controller: 'reportdashboardCtrl',

                resolve: {
                    loadMyFiles: function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'webchatbot',
                            serie: true,
                            files: [
                                'scripts/config.js',
                                'scripts/controllers/reportdashboardController.js',
                                'scripts/services/reportServices.js',
                                'scripts/js/report/charts.js',
                                'scripts/directives/loader/loader.js',
                                'stylesheets/report/daterangepicker.min.css',
                                'stylesheets/report/export.css',
                                'http://cdn.syncfusion.com/13.4.0.53/js/web/flat-azure/ej.web.all.min.css',
                                'stylesheets/report/report.css'
                            ]
                        })
                    }
                }
            })
            .state('dashboard.rcs_dashboard', {
                templateUrl: 'views/dashboard.html',
                url: '/rcs_dashboard',
                controller: 'PlumbCtrl',

                resolve: {
                    loadMyFiles: function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'webchatbot',
                            files: [
                                'scripts/config.js',
                                'scripts/controllers/rcsController.js',
                                'scripts/directives/loader/loader.js'
                            ]
                        })
                    },
                    info: function($stateParams) {
                        return $stateParams.username;
                    }

                },
                params: {
                    username: null
                }
            })
            .state('dashboard.list_bot', {
                templateUrl: 'views/botlist.html',
                url: '/botlist',
                controller: 'ListCtrl',

                resolve: {
                    loadMyFiles: function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'webchatbot',
                            files: [
                                'scripts/config.js',
                                'scripts/controllers/listController.js',
                                'scripts/directives/loader/loader.js'
                            ]
                        })
                    }
                }
            })
            .state('dashboard.adduser', {
                templateUrl: 'views/adduser.html',
                url: '/adduser',
                controller: 'AddUserCtrl',

                resolve: {
                    loadMyFiles: function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'webchatbot',
                            files: [
                                'scripts/config.js',
                                'scripts/controllers/adduserController.js',
                                'scripts/directives/loader/loader.js'
                            ]
                        })
                    }
                }
            })
            .state('dashboard.enduserlist', {
                templateUrl: 'views/sandboxuser.html',
                url: '/enduserlist',
                controller: 'SandboxUserController',
                resolve: {
                    loadMyFiles: function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'webchatbot',
                            files: [
                                'scripts/config.js',
                                'scripts/controllers/sandboxuserController.js',
                                'scripts/services/sandboxuserService.js',
                                'scripts/directives/loader/loader.js'
                            ]
                        })
                    }
                }
            })
            .state('dashboard.edit_bot', {
                templateUrl: 'views/editBot.html',
                url: '/editbot/:botId',
                controller: 'EditCtrl',

                resolve: {
                    loadMyFiles: function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'webchatbot',
                            files: [
                                'scripts/config.js',
                                'scripts/controllers/editController.js',
                                'scripts/directives/loader/loader.js'
                            ]
                        })
                    }
                }
            })
            .state('dashboard.view_bot', {
                templateUrl: 'views/viewBot.html',
                url: '/viewbot/:botId',
                controller: 'ViewCtrl',

                resolve: {
                    loadMyFiles: function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'webchatbot',
                            files: [
                                'scripts/config.js',
                                'scripts/controllers/viewController.js',
                                'scripts/directives/loader/loader.js'
                            ]
                        })
                    }
                }
            })
            .state('dashboard.viewreport', {
                templateUrl: 'views/viewreport.html',
                url: '/viewreport/:botId',
                controller: 'ViewReportCtrl',

                resolve: {
                    loadMyFiles: function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'webchatbot',
                            files: [
                                'scripts/config.js',
                                'scripts/controllers/viewReportController.js',
                                'scripts/directives/loader/loader.js'
                            ]
                        })
                    }
                }
            })
            .state('chat_bot', {
                templateUrl: 'views/chatbot.html',
                url: '/chatbot/:botId',
                controller: 'ChatCtrl',
                resolve: {
                    loadMyFiles: function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'webchatbot',
                            files: [
                                'scripts/config.js',
                                'scripts/controllers/chatbotController.js'
                            ]
                        })
                    }
                }
            })
            .state('dashboard.ent_api', {
                templateUrl: 'views/apiservices.html',
                url: '/EnterpriseAPI',
                controller: 'ApiCtrl',
                resolve: {
                    loadMyFiles: function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'webchatbot',
                            files: [
                                'scripts/config.js',
                                'scripts/controllers/apiController.js'
                            ]
                        })
                    }
                }
            })
            .state('dashboard.channel', {
                templateUrl: 'views/channel.html',
                url: '/Channel',
                controller: 'ChannelCtrl',
                resolve: {
                    loadMyFiles: function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'webchatbot',
                            files: [
                                'scripts/config.js',
                                'scripts/controllers/channelController.js'
                            ]
                        })
                    }
                }
            })
        $httpProvider.defaults.useXDomain = true;

        $httpProvider.defaults.headers.common['X-Requested-With'];
        //  $httpProvider.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
    }])
    .factory('beforeUnload', function($rootScope, $window) {
        // Events are broadcast outside the Scope Lifecycle

        $window.onbeforeunload = function(e) {

            var confirmation = {};
            var event = $rootScope.$broadcast('onBeforeUnload', confirmation);
            if (event.defaultPrevented) {
                return confirmation.message;
            }
        };

        $window.onunload = function() {

            $rootScope.$broadcast('onUnload');
        };
        return {};
    })
    .run(function(beforeUnload) {
        // Must invoke the service at least once
    });
(function(window, angular, undefined) {
    'use strict';
    var agl = angular || {};
    var ua = navigator.userAgent;

    agl.ISFF = ua.indexOf('Firefox') != -1;
    agl.ISOPERA = ua.indexOf('Opera') != -1;
    agl.ISCHROME = ua.indexOf('Chrome') != -1;
    agl.ISSAFARI = ua.indexOf('Safari') != -1 && !agl.ISCHROME;
    agl.ISWEBKIT = ua.indexOf('WebKit') != -1;

    agl.ISIE = ua.indexOf('Trident') > 0 || navigator.userAgent.indexOf('MSIE') > 0;
    agl.ISIE6 = ua.indexOf('MSIE 6') > 0;
    agl.ISIE7 = ua.indexOf('MSIE 7') > 0;
    agl.ISIE8 = ua.indexOf('MSIE 8') > 0;
    agl.ISIE9 = ua.indexOf('MSIE 9') > 0;
    agl.ISIE10 = ua.indexOf('MSIE 10') > 0;
    agl.ISOLD = agl.ISIE6 || agl.ISIE7 || agl.ISIE8; // MUST be here

    agl.ISIE11UP = ua.indexOf('MSIE') == -1 && ua.indexOf('Trident') > 0;
    agl.ISIE10UP = agl.ISIE10 || agl.ISIE11UP;
    agl.ISIE9UP = agl.ISIE9 || agl.ISIE10UP;

})(window, window.angular);