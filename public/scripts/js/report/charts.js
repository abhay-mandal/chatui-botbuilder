var date = new Date().toISOString().slice(0,19);
//console.log(document.getElementById('daterange').value());
function generateChartConversationVsUsers(data, dateRange) {

    var dateRange = new Date(dateRange.startDate).toISOString().slice(0,10) + "To" + new Date(dateRange.endDate).toISOString().slice(0,10);

    //console.log(dateRange);

    var chart = AmCharts.makeChart("chartdiv_ConversationVsUsers", {
        "type": "serial",
        "addClassNames": true,
        "theme": "none",
        "legend": {
            "position": "top",
            "align": "center",
            "equalWidths": false,
            "valueAlign": "left",
            //"periodValueText": "Total: [[value.sum]]"
        },
        "balloon": {
            "adjustBorderColor": false,
            "horizontalPadding": 10,
            "verticalPadding": 8,
            "color": "#ffffff"
        },
        "dataProvider": data,
        "valueAxes": [{
            "axisAlpha": 0,
            "gridAlpha": 0.05,
            "position": "left",
            "title": "Total Count"
        }],
        "chartCursor": {
            "cursorAlpha": 0,
            "zoomable": false
        },
        "startDuration": 1,
        "graphs": [{
            "lineColor": "#81D0FF",
            "id": "g1",
            "fillAlphas": 0.4,
            "title": "Conversations",
            "valueField": "conversations",
            "balloonText": "<div style='margin:5px; font-size:19px;'>Conversations:<b>[[value]]</b></div>"
        }, {
            "id": "graph2",
            "balloonText": "<span style='font-size:12px;'>[[title]] :<span style='font-size:20px;'>[[value]]",
            "bullet": "round",
            "lineThickness": 3,
            "bulletSize": 7,
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "useLineColorForBulletBorder": true,
            "bulletBorderThickness": 3,
            "fillAlphas": 0,
            "lineAlpha": 1,
            "title": "Users",
            "valueField": "users"
        }],
        "categoryField": "date",
        "categoryAxis": {
            "gridPosition": "start",
            "gridAlpha": 0.05
        },
        "export": {
            "enabled": true,
            "fileName":"ConversationVsUsers"+dateRange
        }
    });
}

function generateChartMessageVsUsers(data) {

    var chart = AmCharts.makeChart("chartdiv_MessageVsUsers", {
        "type": "serial",
        "addClassNames": true,
        "theme": "none",
        "legend": {
            "position": "top",
            "useGraphSettings": true,
            "align": "center",
            "equalWidths": false,
            "valueAlign": "left",
            //"periodValueText": "Total: [[value.sum]]"
        },
        "balloon": {
            "adjustBorderColor": false,
            "horizontalPadding": 10,
            "verticalPadding": 8,
            "color": "#ffffff"
        },
        "dataProvider": data,
        "valueAxes": [{
            "axisAlpha": 0,
            "gridAlpha": 0.05,
            "position": "left",
            "title" : "Total Count"
        }],
        "chartCursor": {
            "cursorAlpha": 0,
            "zoomable": false
        },
        "startDuration": 1,
        "graphs": [{
            "alphaField": "alpha",
            "balloonText": "<span style='font-size:12px;'>[[title]] :<span style='font-size:20px;'>[[value]]</span>",
            "fillAlphas": 1,
            "title": "Messages",
            "type": "column",
            "valueField": "message",
            "lineColor": "#1a2836"
        }, {
            "id": "graph2",
            "balloonText": "<span style='font-size:12px;'>[[title]]:<span style='font-size:20px;'>[[value]]</span>",
            "bullet": "round",
            "lineThickness": 3,
            "bulletSize": 7,
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "useLineColorForBulletBorder": true,
            "bulletBorderThickness": 3,
            "fillAlphas": 0,
            "lineAlpha": 1,
            "title": "Users",
            "valueField": "users"
        }],
        "categoryField": "date",
        "categoryAxis": {
            "gridPosition": "start",
            "gridAlpha": 0.05
        },
        "export": {
            "enabled": true
        }
    });
}

function generateChartUsersVsFiles(data) {

    var chart = AmCharts.makeChart("chartdiv_filesVsUsers", {
        "type": "serial",
        "addClassNames": true,
        "theme": "none",
        "legend": {
            "position": "top",
            "align": "center",
            "equalWidths": false,
            "valueAlign": "left",
            //"periodValueText": "Total: [[value.sum]]"
        },
        "balloon": {
            "adjustBorderColor": false,
            "horizontalPadding": 10,
            "verticalPadding": 8,
            "color": "#ffffff"
        },
        "dataProvider": data,
        "valueAxes": [{
            "axisAlpha": 0,
            "gridAlpha": 0.05,
            "position": "left",
            "title": "Total Count"
        }],
        "chartCursor": {
            "cursorAlpha": 0,
            "zoomable": false
        },
        "startDuration": 1,
        "graphs": [{
            "alphaField": "alpha",
            "balloonText": "<span style='font-size:12px;'>[[title]] :<span style='font-size:20px;'>[[value]]</span>",
            "fillAlphas": 1,
            "title": "Files",
            "type": "column",
            "valueField": "files",
            "lineColor": "#81D0FF"
        }, {
            "id": "graph2",
            "balloonText": "<span style='font-size:12px;'>[[title]] :<span style='font-size:20px;'>[[value]]</span>>",
            "bullet": "round",
            "lineThickness": 3,
            "bulletSize": 7,
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "useLineColorForBulletBorder": true,
            "bulletBorderThickness": 3,
            "fillAlphas": 0,
            "lineAlpha": 1,
            "title": "Users",
            "valueField": "users"
        }],
        "categoryField": "date",
        "categoryAxis": {
            "gridPosition": "start",
            "gridAlpha": 0.05
        },
        "export": {
            "enabled": true
        }
    });
}

function generateSunburstchart(data) {
    var width = $('#sunburstChart').width();
    var height = $('#sunburstChart').height();
    var radius = Math.min(width, height) / 2 - 10;
    var color = d3.scaleOrdinal(d3.schemeCategory20);

    var x = d3.scaleLinear()
        .range([0, 2 * Math.PI]);

    var y = d3.scaleSqrt()
        .range([0, radius]);

    var svg = d3.select("#sunburstChart").append("svg")
        .attr("width", width)
        .attr("height", height)
        .append("g")
        .attr("transform", "translate(" + width / 2 + "," + (height / 2 + 10) + ")");

    var arc = d3.arc()
        .padAngle(.01)
        .padRadius(radius / 3)
        .startAngle(function (d) { return Math.max(0, Math.min(2 * Math.PI, x(d.x0))); })
        .endAngle(function (d) { return Math.max(0, Math.min(2 * Math.PI, x(d.x1))); })
        .innerRadius(function (d) { return Math.max(0, y(d.y0)); })
        .outerRadius(function (d) { return Math.max(0, y(d.y1)); });

    var partition = d3.partition();
    var root = d3.hierarchy(data).count();

    var g = svg.selectAll("g")
        .data(partition(root).descendants(), function (d) { return d.data.name; })
        .enter().append("g");

    var tt = d3.select("body").append("tt")
        .attr("class", "tooltip")
        .style("opacity", 0);

    var path = g.append("path")
        .attr("d", arc)
        .style("stroke", "white")
        .style('stroke-width', 0.5)
        .attr("id", function (d) { return 'path' + d.data.name; })
        .style("fill", function (d) { return color((d.children ? d : d.parent).data.name); })
        .style("opacity", 0)
        .on("click", click)
        .on("mouseover", function (d) {
            tt.transition()
                .duration(200)
                .style("opacity", 1.0);
            tt.html(showTooltipInfo(d))
                .style("left", (d3.event.pageX) + "px")
                .style("top", (d3.event.pageY - 28) + "px");
        })
        .on("mouseout", function (d) {
            tt.transition()
                .duration(500)
                .style("opacity", 0);
        })
        .each(function (d, i) {
            this.xx0 = d.x0;
            this.xx1 = d.x1;
            this.yy0 = d.y0;
            this.yy1 = d.y1;
        })
        .transition()
        .duration(1000)
        .style("opacity", 1);

    function showTooltipInfo(d) {
        return d.data.name;
    }

    function click(d) {
        svg.selectAll("g").select("path")
            .transition()
            .duration(750)
            .attrTween("d", arcTweenZoom(d))
            .each(function (d, i) {
                this.xx0 = d.x0;
                this.xx1 = d.x1;
                this.yy0 = d.y0;
                this.yy1 = d.y1;
            });

        var selectedD = d;

        setTimeout(function () {
            var newRoot;
            if (selectedD.data.name === "analytics") {
                //newRoot = d3.hierarchy(data2).count();
                data.children[0].children[0];
                newRoot = d3.hierarchy(data2).count();
            }
            else
                return;

            var groups = svg.selectAll("g")
                .data(partition(newRoot).descendants(), function (d) { return d.data.name; });

            groups.exit()
                .transition()
                .duration(10)
                .style("opacity", 0)
                .remove();

            groups.select("path")
                .transition()
                .duration(750)
                .attrTween("d", arcTweenData)
                .each(function (d, i) {
                    this.xx0 = d.x0;
                    this.xx1 = d.x1;
                    this.yy0 = d.y0;
                    this.yy1 = d.y1;
                });

            groups.enter().append("g").append("path")
                .attr("d", arc)
                .style("stroke", "white")
                .style('stroke-width', 0.5)
                .attr("id", function (d) { return 'path' + d.data.name; })
                .style("fill", function (d) { return color((d.children ? d : d.parent).data.name); })
                .style("opacity", 0)
                .on("click", click)
                .on("mouseover", function (d) {
                    tt.transition()
                        .duration(200)
                        .style("opacity", 1.0);
                    tt.html(showTooltipInfo(d))
                        .style("left", (d3.event.pageX) + "px")
                        .style("top", (d3.event.pageY - 28) + "px");
                })
                .on("mouseout", function (d) {
                    tt.transition()
                        .duration(500)
                        .style("opacity", 0);
                })
                .each(function (d, i) {
                    this.xx0 = d.x0;
                    this.xx1 = d.x1;
                    this.yy0 = d.y0;
                    this.yy1 = d.y1;
                })
                .transition()
                .delay(250)
                .duration(750)
                .style("opacity", 1);
        }, 500);
    }

    function arcTweenData(a) {
        if (this.xx0 !== undefined) {
            var oi = d3.interpolate({ x0: this.xx0, x1: this.xx1, y0: this.yy0, y1: this.yy1 }, a);
            var that = this;
            return function (t) {
                var b = oi(t);
                that.xx0 = b.x0;
                that.xx1 = b.x1;
                that.yy0 = b.y0;
                that.yy1 = b.y1;
                return arc(b);
            };
        }
    }
    function arcTweenZoom(d) {
        var xd = d3.interpolate(x.domain(), [d.x0, d.x1]),
            yd = d3.interpolate(y.domain(), [d.y0, 1]),
            yr = d3.interpolate(y.range(), [d.y0 ? 20 : 0, radius]);
        return function (d, i) {
            return i ? function (t) { return arc(d) } : function (t) { x.domain(xd(t)); y.domain(yd(t)).range(yr(t)); return arc(d); }
        }
    }

    function computeTextRotation(d) {
        var angle = (d.x0 + d.x1) / Math.PI * 90;
    
        // Avoid upside-down labels; labels as rims
        return (angle < 120 || angle > 270) ? angle : angle + 180;
        //return (angle < 180) ? angle - 90 : angle + 90;  // labels as spokes
    }
}