var myApp = angular.module('webchatbot', ['ui.bootstrap', 'angularUtils.directives.dirPagination', 'ngAnimate', 'ngSanitize', 'ngToast']);
myApp.config(['ngToastProvider',
    function(ngToastProvider) {
        ngToastProvider.configure({
            animation: 'slide',
            horizontalPosition: 'center',
            verticalPosition: 'center'
        });
    }
]);
myApp.filter('myDateFormat', function myDateFormat($filter) {
    return function(input) {
        if (input != undefined) {
            return $filter('date')(new Date(input), "dd/MM/y hh:mm a");
        }
    };
});
myApp.controller('ApiCtrl', function($scope, $http, ngToast, $window, $modal) {
    if (!$window.localStorage.id) {
        $state.go("login");
    }
    $scope.apilist = true;
    $scope.apicreate = false;
    $scope.message = { raw: {} };
    $scope.script = { raw: {} };
    $scope.post_body = { type: "raw" };
    $http.defaults.headers.common['user'] = $window.sessionStorage.id;
    $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
    $scope.list = function() {
        $http.get("/getapis", {
            headers: { 'Content-Type': 'application/json' }
        }).then(function(res) {
            // console.log(res);
            $scope.gridOptions = res.data;
        });
    };
    $scope.list();
    $scope.sort_by = function(keyname) {
        $scope.sortKey = keyname; //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
    $scope.showSimpleToast = function(message, clsname) {
        ngToast.create({
            className: clsname,
            content: message,
            dismissOnTimeout: true,
            timeout: 2500,
            dismissButton: true,
            dismissButtonHtml: '&times',
            dismissOnClick: true
        });
    };

    $scope.createapi = function() {
        $scope.apilist = false;
        $scope.apicreate = true;
        $scope["ent_api_details"].method = "get";
        $scope.api_id = "";
    }

    $scope.backtoapilist = function() {
        $scope.apilist = true;
        $scope.apicreate = false;
        $scope.api_id = "";
        $scope.addentryres();
        $scope.ent_api_details.api_name = "";
        $scope.apiresponse = "";
        $scope.apistatus = "";
        $scope.apistatustext = "";
        $scope.message = { raw: {} };
        $scope.list();
    }

    $scope.rowClickedit = function(id) {
        $scope.apilist = false;
        $scope.apicreate = true;
        $scope.api_id = id;
        $scope.getapires();
    }

    $scope.rowClickdelete = function(apiId, ev) {
        var obj = {
            _id: apiId
        }
        $http.defaults.headers.common['user'] = $window.sessionStorage.id;
        $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
        $modal.open({
            // locals: { gambit_init_mess: $scope.gambit_init_mess },
            templateUrl: 'confirm.html',
            controller: function($scope) {
                $scope.ok = function() {
                    $scope.cancel();
                    $http.defaults.headers.common['user'] = $window.sessionStorage.id;
                    $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
                    $http.post("/deleteapi", obj, {
                        headers: { 'Content-Type': 'application/json' }
                    }).then(function(res) {
                        if (res.data.status == "success") {
                            $scope.showSimpleToast("Successfully deleted", "success");
                            $scope.list();
                        }
                    });
                };
                $scope.cancel = function() {
                    $scope.$close();
                    $('.modal-backdrop').remove()
                    $(document.body).removeClass("modal-open");
                }
            },
            scope: $scope,
            controllerAs: 'ctrl',
            targetEvent: ev,
            size: 'sm'
        });

    };

    $http.defaults.headers.common['user'] = $window.sessionStorage.id;
    $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
    $scope.getapires = function() {
        if ($scope.api_id != "") {
            var ind = $scope.gridOptions.findIndex(record => record._id === $scope.api_id);
            $scope["ent_api_details"].api_name = $scope.gridOptions[ind].ApiName;
            $scope["ent_api_details"].url = $scope.gridOptions[ind].api_info.url;
            $scope["ent_api_details"].body = $scope.gridOptions[ind].api_info.body;
            $scope["ent_api_details"].headerparams = $scope.gridOptions[ind].api_info.headerparams;
            $scope["ent_api_details"].params = $scope.gridOptions[ind].api_info.params;
            $scope["ent_api_details"].method = $scope.gridOptions[ind].api_info.method;
            $scope["ent_api_details"].body_type = $scope.gridOptions[ind].api_info.body_type;
            $scope["ent_api_details"].urlencoded_body = $scope.gridOptions[ind].api_info.urlencoded_body;
            $scope["ent_api_details"].form_data_body = $scope.gridOptions[ind].api_info.form_data_body;
            $scope["ent_api_details"].raw_json = $scope.gridOptions[ind].api_info.raw_json;
            $scope.post_body.type = ($scope["ent_api_details"].body_type != undefined) ? $scope["ent_api_details"].body_type : $scope.post_body.type;
            if ($scope["ent_api_details"].raw_json != undefined) {
                if (Object.keys($scope["ent_api_details"].raw_json).length > 0) {
                    $scope.message.raw = $scope["ent_api_details"].raw_json;
                    $scope.script.raw = JSON.stringify($scope.message.raw);
                } else {
                    $scope.message.raw = {};
                    $scope.script.raw = JSON.stringify($scope.message.raw);
                }
            } else {
                $scope.message.raw = {};
                $scope.script.raw = JSON.stringify($scope.message.raw);
            }

        }

        if ($scope["ent_api_details"].url == "") {
            $scope.showSimpleToast("Empty URL request", "warning");
        } else {

            var body_data = {}
            if ($scope.post_body.type == "raw" || $scope["ent_api_details"].body_type == "raw") {
                body_data = JSON.stringify($scope.message.raw);
                // $scopr.script.raw = body_data;
            } else if ($scope.post_body.type == "urlencoded" || $scope["ent_api_details"].body_type == "urlencoded") {
                if ($scope["ent_api_details"].urlencoded_body.length > 0 || $scope["ent_api_details"].body.length > 0) {
                    // $scope["ent_api_details"].urlencoded_body = $scope["ent_api_details"].body;
                    angular.forEach($scope["ent_api_details"].urlencoded_body, function(value, key) {
                        body_data[value.bodykey] = value.bodyvalue;
                    });
                }

            } else if ($scope.post_body.type == "form_data" || $scope["ent_api_details"].body_type == "form_data") {
                if ($scope["ent_api_details"].form_data_body.length > 0 || $scope["ent_api_details"].body.length > 0) {
                    // $scope["ent_api_details"].form_data_body = $scope["ent_api_details"].body;
                    angular.forEach($scope["ent_api_details"].form_data_body, function(value, key) {
                        body_data[value.bodykey] = value.bodyvalue;
                    });
                }


            }
            var header_data = { 'Content-Type': 'application/json' }
            if ($scope["ent_api_details"].headerparams.length > 0) {

                angular.forEach($scope["ent_api_details"].headerparams, function(value, key) {
                    header_data[value.paramkey] = value.paramvalue
                });
            }
            var parameter = ""
            if ($scope["ent_api_details"].params.length > 0) {
                angular.forEach($scope["ent_api_details"].params, function(value, key) {

                    parameter = parameter + value.paramkey + "=" + value.paramvalue + '&'
                });
            }
            //  var url = $scope["ent_api_details"].url + "/?"+parameter
            var req_body = {
                'method': $scope["ent_api_details"].method,
                'url': $scope["ent_api_details"].url,
                'headers': header_data,
                'body': body_data,
                'param': parameter
            }
            $http.defaults.headers.common['user'] = $window.sessionStorage.id;
            $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
            $http.post('/execute_api', req_body, {
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function(res) {

                $scope.apiresponse = JSON.stringify(res.data.data, undefined, 2);
                $scope.apistatus = res.data.status;
                $scope.apistatustext = res.data.statusText;
            })

        }
    }


    var api_obj = {
        "method": "get",
        "url": "",
        "params": [],
        "body": [],
        "headerparams": [],
        "auth": {},
        "showparams": false

    }
    $scope["ent_api_details"] = api_obj

    $scope.addentryres = function() {
        var api_obj = {
            "method": "get",
            "url": "",
            "params": [],
            "body": [],
            "headerparams": [],
            "auth": {},
            "showparams": false
        }
        $scope["ent_api_details"] = api_obj
    }

    $scope.apiparams = [{
        "paramkey": "",
        "paramvalue": ""
    }]

    $scope.headerparams = [{ "paramkey": "", "paramvalue": "" }]

    $scope.addparams = function() {
        if ($scope["ent_api_details"].params == undefined) {
            $scope["ent_api_details"].params = [];
        }
        var obj = {
            "paramkey": "",
            "paramvalue": ""
        }
        $scope["ent_api_details"].params.push(obj);
    }
    $scope.removeparams = function(paramsindex) {

        $scope["ent_api_details"].params.splice(paramsindex, 1);
    }

    $scope.addurlparams = function() {
        if ($scope["url_params"] == undefined) {
            $scope["url_params"] = [];
        }
        var obj = {
            "paramname": ""
        }
        $scope["url_params"].push(obj);
    }
    $scope.removeurlparams = function(paramsindex) {

        $scope["url_params"].splice(paramsindex, 1);
    }

    $scope.rmhdrparams = function(paramsindex) {

        $scope["ent_api_details"].headerparams.splice(paramsindex, 1);
    }
    $scope.rmformdatabody = function(paramsindex) {

        $scope["ent_api_details"].form_data_body.splice(paramsindex, 1);
    }
    $scope.rmurlencodedbody = function(paramsindex) {

        $scope["ent_api_details"].urlencoded_body.splice(paramsindex, 1);
    }

    $scope.addurlencodedbody = function() {
        if ($scope["ent_api_details"].urlencoded_body == undefined) {
            $scope["ent_api_details"].urlencoded_body = [];
        }
        var obj = {
            "paramkey": "",
            "paramvalue": ""
        }
        $scope["ent_api_details"].urlencoded_body.push(obj);
    }
    $scope.addformdatabody = function() {
        if ($scope["ent_api_details"].form_data_body == undefined) {
            $scope["ent_api_details"].form_data_body = [];
        }
        var obj = {
            "paramkey": "",
            "paramvalue": ""
        }
        $scope["ent_api_details"].form_data_body.push(obj);
    }

    $scope.showparams = true;
    $scope.toggleparams = function() {

        if ($scope["ent_api_details"].showparams == true) {
            $scope["ent_api_details"].showparams = false;
        } else {

            $scope["ent_api_details"].showparams = true
        }
    }

    $scope.addheadparams = function() {
        if ($scope["ent_api_details"].headerparams == undefined) {
            $scope["ent_api_details"].headerparams = [];
        }
        var obj = {
            "paramkey": "",
            "paramvalue": ""
        }
        $scope["ent_api_details"].headerparams.push(obj);
    }
    $scope.body_type = function(post_body_type) {
        // console.log(post_body_type);
        $scope.post_body.type = post_body_type;
    }


    $scope.saveapi = function() {
        if ($scope.ent_api_details.api_name == undefined) {
            $scope.showSimpleToast("Please enter API name", "danger");
        } else if ($scope["ent_api_details"].url == "") {
            $scope.showSimpleToast("Please enter API URL", "danger");
        } else {
            $scope.ent_api_details['body_type'] = $scope.post_body.type;
            if ($scope.post_body.type == 'raw') {
                $scope.ent_api_details['raw_json'] = $scope.message.raw;
                $scope.ent_api_details['urlencoded_body'] = [];
                $scope.post_body.type['form_data_body'] = [];
            } else if ($scope.post_body.type == 'form_data') {
                $scope.ent_api_details['raw_json'] = JSON.stringify({});
                $scope.ent_api_details['urlencoded_body'] = [];
            } else {
                $scope.ent_api_details['raw_json'] = JSON.stringify({});
                $scope.ent_api_details['form_data_body'] = [];
            }
            $http.defaults.headers.common['user'] = $window.sessionStorage.id;
            $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
            if ($scope.api_id != "") {
                var postdata = {
                    ApiName: $scope.ent_api_details.api_name,
                    api_info: $scope.ent_api_details,
                    _id: $scope.api_id
                }
                $http.post('/updateapi', postdata, {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function(res) {
                    // console.log(res);
                    $scope.showSimpleToast("API updated successfully", "success");
                    $scope.addentryres();
                    $scope.ent_api_details.api_name = "";
                    $scope.apiresponse = "";
                    $scope.apistatus = "";
                    $scope.apistatustext = "";
                    $scope.message = { raw: {} };
                })
            } else {
                var postdata = {
                    ApiName: $scope.ent_api_details.api_name,
                    api_info: $scope.ent_api_details
                }

                $http.post('/save_entapi', postdata, {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function(res) {
                    // console.log(res);
                    $scope.showSimpleToast("API saved successfully", "success");
                    $scope.addentryres();
                    $scope.ent_api_details.api_name = "";
                    $scope.apiresponse = "";
                    $scope.apistatus = "";
                    $scope.apistatustext = "";
                    $scope.message = { raw: {} };
                })
            }
        }

    }

    $scope.formatting = { color: 'green', 'background-color': '#d0e9c6' };


    $scope.isValid = true;

    $scope.script.raw = JSON.stringify($scope.message.raw);

    $scope.updateJsonObject = function() {
        try {
            JSON.parse($scope.script.raw);
        } catch (e) {
            $scope.isValid = false;
            $scope.formatting = { color: 'red', 'background-color': '#f2dede' };
        }

        $scope.message.raw = JSON.parse($scope.script.raw);
        $scope.isValid = true;
        $scope.formatting = { color: 'green', 'background-color': '#d0e9c6' };
    };

})