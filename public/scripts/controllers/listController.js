var myApp = angular.module('webchatbot', ['ui.bootstrap', 'angularUtils.directives.dirPagination', 'ngAnimate', 'ngSanitize', 'ngToast']);
myApp.filter('myDateFormat', function myDateFormat($filter) {
    return function(input) {
        if (input != undefined) {
            return $filter('date')(new Date(input), "dd/MM/y hh:mm:ss a");
        }
    };
});
myApp.config(['ngToastProvider',
    function(ngToastProvider) {
        ngToastProvider.configure({
            animation: 'slide',
            horizontalPosition: 'center',
            verticalPosition: 'top'
        });
    }
]);
angular.module('webchatbot').controller('ListCtrl', function($scope, $http, $state, $window, $location, $filter, $rootScope, ngToast, $modal) {
    if (!$window.localStorage.id) {
        $state.go("login");
    }
    $scope.page = 5;
    $scope.url_ip = $location.$$protocol + "://" + $location.$$host + ":" + $location.$$port

    $scope.rowClickview = function(botId) {
        // console.log("here", row.entity.botId);
        //$state.go('dashboard.view_bot',{botId:row.entity.botId});
        $window.open($scope.url_ip + "/#!/dashboard/viewbot/" + botId);
    };

    $scope.rowClickreport = function(botId) {
        // console.log("here", row.entity.botId);
        //$state.go('dashboard.view_bot',{botId:row.entity.botId});
        $window.open($scope.url_ip + "/#!/dashboard/viewreport/" + botId);
    };

    $scope.rowClickedit = function(botId) {
        // console.log("here", row.entity.botId);
        $window.open($scope.url_ip + "/#!/dashboard/editbot/" + botId);
    };
    $scope.rowClickdeploy = function(botId) {
        // console.log("here", row.entity.botId);
        $state.go("chat_bot", { 'botId': botId });
    };

    $scope.rowClickdelete = function(botId, ev) {
        var obj = {
            botId: botId
        }
        $http.defaults.headers.common['user'] = $window.sessionStorage.id;
        $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
        $modal.open({
            // locals: { gambit_init_mess: $scope.gambit_init_mess },
            templateUrl: 'confirm.html',
            controller: function($scope) {
                $scope.ok = function() {
                    $scope.cancel();
                    $http.defaults.headers.common['user'] = $window.sessionStorage.id;
                    $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
                    $http.post("/delete_bot", obj, {
                        headers: { 'Content-Type': 'application/json' }
                    }).then(function(res) {
                        if (res.data.status == "success") {
                            $scope.showSimpleToast("Successfully deleted", "success");
                            $scope.list();
                        }
                    });
                };
                $scope.cancel = function() {
                    $scope.$close();
                    $('.modal-backdrop').remove()
                    $(document.body).removeClass("modal-open");
                }
            },
            scope: $scope,
            controllerAs: 'ctrl',
            targetEvent: ev,
            size: 'sm'
        });
    };
    $http.defaults.headers.common['user'] = $window.sessionStorage.id;
    $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
    $scope.list = function() {
        $http.get("/listbots", {
            headers: { 'Content-Type': 'application/json' }
        }).then(function(res) {
            // console.log(res);
            $scope.gridOptions = res.data // { data: res.data };
            $rootScope.botCount = $scope.gridOptions.length;
        });
    };

    $scope.list();
    var last = {
        bottom: false,
        top: true,
        left: false,
        right: true
    };

    // $scope.toastPosition = angular.extend({}, last);

    // $scope.getToastPosition = function() {
    //     sanitizePosition();

    //     return Object.keys($scope.toastPosition)
    //         .filter(function(pos) { return $scope.toastPosition[pos]; })
    //         .join(' ');
    // };

    // function sanitizePosition() {
    //     var current = $scope.toastPosition;

    //     if (current.bottom && last.top) current.top = false;
    //     if (current.top && last.bottom) current.bottom = false;
    //     if (current.right && last.left) current.left = false;
    //     if (current.left && last.right) current.right = false;

    //     last = angular.extend({}, current);
    // }

    $scope.showSimpleToast = function(message, clsname) {
        ngToast.create({
            className: clsname,
            content: message,
            dismissOnTimeout: true,
            timeout: 2500,
            dismissButton: true,
            dismissButtonHtml: '&times',
            dismissOnClick: true
        });
    };
    // pagination setting/options

    $scope.sort_by = function(keyname) {
        $scope.sortKey = keyname; //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }

});