var myApp = angular.module('webchatbot', ['ui.bootstrap', 'ngAnimate', 'ngSanitize', 'ngToast', 'mgcrea.ngStrap.datepicker']);
myApp.config(['ngToastProvider',
    function(ngToastProvider) {
        ngToastProvider.configure({
            animation: 'slide',
            horizontalPosition: 'center',
            verticalPosition: 'top'
        });
    }
]);
myApp.controller('ChannelCtrl', function($scope, $state, $http, $rootScope, $window, $modal, ngToast) {
    $scope.botId = $state.params.botId;
    if (!$window.localStorage.id) {
        $state.go("login");
    }
    $scope.website_chatbot = true;
    $scope.whatsapp = false;
    $scope.rcm = false;
    $scope.showwebsite = function() {
        $scope.website_chatbot = true;
        $scope.whatsapp = false;
        $scope.rcm = false;
    }
    $scope.showwhatsapp = function() {
        $scope.whatsapp = true;
        $scope.website_chatbot = false;
        $scope.rcm = false;
    }
    $scope.showrcm = function() {
        $scope.whatsapp = false;
        $scope.website_chatbot = false;
        $scope.rcm = true;
    }
});