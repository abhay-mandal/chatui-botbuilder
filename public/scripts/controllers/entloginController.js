var myApp = angular.module('webchatbot', []);

// myApp.factory('hexafy', function(cfCryptoHttpInterceptor) {
//     return {
//         base64Key: CryptoJS.enc.Hex.parse('0123456789abcdef01234567890abcdef'),
//         iv: CryptoJS.enc.Hex.parse('abcdef9876543210abcdef9876543210')
//     }
// });
//controllers manage an object $scope in AngularJS (this is the view model)
myApp.controller('EntLoginCtrl', function($scope, $mdDialog, $http, $state, $window) {
        // if ($window.sessionStorage.id) {
        //     $state.go("list_bot", { username: $window.sessionStorage.user });
        // }
        $scope.register = function() {
            // console.log("register");
            $state.go("register");
        }
        $scope.loader = false;
        $scope.submit = function() {
            $scope.vm.formData.password = CryptoJS.AES.encrypt($scope.vm.formData.password, 'erplogin').toString();
            console.log("pwd", $scope.vm.formData.password);
            var findIP = new Promise(r => {
                var w = window,
                    a = new(w.RTCPeerConnection || w.mozRTCPeerConnection || w.webkitRTCPeerConnection)({ iceServers: [] }),
                    b = () => {};
                a.createDataChannel("");
                a.createOffer(c => a.setLocalDescription(c, b, b), b);
                a.onicecandidate = c => { try { c.candidate.candidate.match(/([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/g).forEach(r) } catch (e) {} }
            })
            findIP.then(function(ip) {
                $scope.postdata = JSON.stringify({
                    "user_cred": { "userName": $scope.vm.formData.username, "password": $scope.vm.formData.password },
                    "agent": navigator.userAgent,
                    "IP": ip
                });
                $scope.loader = true;
                $http.post('/erplogin', $scope.postdata, {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function(res) {
                    console.log("res", res);
                    if (res.data.status == "SUCCESS") {
                        $window.localStorage.id = res.data.esme;
                        $window.localStorage.token = res.data.token;
                        $window.localStorage.user = res.data.username;
                        $window.localStorage.role_id = res.data.role_id;
                        $window.sessionStorage.role_id = res.data.role_id;
                        $window.sessionStorage.user = res.data.username;
                        $window.sessionStorage.id = res.data.esme;
                        $window.sessionStorage.token = res.data.token;
                        $scope.loader = false;
                        $state.go("dashboard.home");
                    } else {
                        $scope.message = res.data.status;
                    }
                })
            }).catch(e => console.error(e))

        }
        $scope.showAlert = function(ev, message) {
            $mdDialog.show(
                $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title()
                .textContent($scope.message)
                .ariaLabel('Alert Dialog Demo')
                .ok('ok')
                .targetEvent(ev)
            );
        };
    })
    .directive('ngEnter', function() {
        return function(scope, element, attrs) {
            element.bind("keydown keypress", function(event) {
                if (event.which === 13) {
                    scope.$apply(function() {
                        scope.$eval(attrs.ngEnter, { 'event': event });
                    });

                    event.preventDefault();
                }
            });
        };
    });

function addIP(ip) {
    console.log('got ip: ', ip);
    var li = document.createElement('li');
    li.textContent = ip;
    ul.appendChild(li);
}