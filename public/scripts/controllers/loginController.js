var myApp = angular.module('webchatbot', []);

//controllers manage an object $scope in AngularJS (this is the view model)
myApp.controller('LoginCtrl', function($scope, $mdDialog, $http, $element, $state, $window, $rootScope) {
        if ($window.sessionStorage.id) {
            $state.go("list_bot", { username: $window.sessionStorage.user });
        }
        $scope.register = function() {
            // console.log("register");
            $state.go("register");
        }
        $scope.loader = false;
        $scope.submit = function() {
            $scope.postdata = $scope.vm.formData;
            $scope.loader = true;
            $http.post('/login', $scope.postdata, {
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function(res) {
                if (res.data._id) {
                    $window.localStorage.id = res.data._id;
                    $window.localStorage.token = res.data.token;
                    $window.localStorage.role_id = res.data.role_id;
                    $window.localStorage.user = res.data.username;
                    $window.sessionStorage.id = res.data._id;
                    $window.sessionStorage.user = res.data.username;
                    $window.sessionStorage.role_id = res.data.role_id;
                    $window.sessionStorage.token = res.data.token;
                    
                    $scope.loader = false;
                    $state.go("dashboard.home");
                } else {
                    $scope.message = res.data.status;
                }
            })
        }
        $scope.showAlert = function(ev, message) {
            $mdDialog.show(
                $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title()
                .textContent($scope.message)
                .ariaLabel('Alert Dialog Demo')
                .ok('ok')
                .targetEvent(ev)
            );
        };
    })
    .directive('ngEnter', function() {
        return function(scope, element, attrs) {
            element.bind("keydown keypress", function(event) {
                if (event.which === 13) {
                    scope.$apply(function() {
                        scope.$eval(attrs.ngEnter, { 'event': event });
                    });

                    event.preventDefault();
                }
            });
        };
    });