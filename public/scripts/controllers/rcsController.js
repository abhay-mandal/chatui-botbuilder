var myApp = angular.module('webchatbot', ['ui.bootstrap', 'mgcrea.ngStrap.datepicker', 'ng-mfb', 'ngAnimate', 'ngSanitize', 'ngToast', 'mgcrea.ngStrap.timepicker']);
var guid = 1;

function  za() {
    google.load("elements",  "1", {
        packages:   "transliteration"
    });
}

// function  languageChangeHandler() { 
//     var scope = angular.element(document.getElementById('container')).scope();
//     // scope.$apply(function() {
//     scope.languageChangeHandler();
//     // }); 
// }

$(function () {
    za();
    var  guid  =  1;
    var  dh  =  $(document).height();
    $('#sidebar-tab-content').height(dh  -  115);
    $('#main-content').height(dh  -  10);
});
myApp.directive('file', function() {
    return {
        scope: {
            file: '='
        },
        link: function(scope, el, attrs) {
            el.bind('change', function(event) {
                var files = event.target.files;
                var file = files[0];
                scope.file = file ? file.name : undefined;
                scope.$apply();
            });
        }
    };
});
myApp.config(['ngToastProvider',
    function(ngToastProvider) {
        ngToastProvider.configure({
            animation: 'slide',
            horizontalPosition: 'center',
            verticalPosition: 'top'
        });
    }
]);


//controllers manage an object $scope in AngularJS (this is the view model)
myApp.controller('PlumbCtrl', function($scope, $http, $window, $state, $stateParams, Upload, $modal, $rootScope, ngToast) {

    if (!$window.localStorage.id) {
        $state.go("login");
    }
    if (!localStorage.getItem("reload")) {
        /* set reload locally and then reload the page */
        localStorage.setItem("reload", "true");
        location.reload();
    }
    /* after reload clear the localStorage */
    else {
        localStorage.removeItem("reload");
        // localStorage.clear(); // an option
    }
    $rootScope.getBotId = $scope.botId;

    $scope.config = "message";
    var ids = [];

    $scope.username = $stateParams.username;
    // define a module with library id, schema id, etc.
    function module(library_id, schema_id, title, description, x, y) {
        this.library_id = library_id;
        this.schema_id = schema_id;
        this.title = title;
        this.description = description;
        this.x = x;
        this.y = y;
    }

    // module should be visualized by title, icon
    $scope.library = [];

    // library_uuid is a unique identifier per module type in the library
    $scope.library_uuid = 0;

    // state is [identifier, x position, y position, title, description]
    $scope.schema = [];

    // schema_uuid should always yield a unique identifier, can never be decreased
    $scope.schema_uuid = 0;

    // todo: find out how to go back and forth between css and angular
    $scope.library_topleft = {
        x: 15,
        y: 145,
        item_height: 50,
        margin: 5,
    };

    $scope.module_css = {
        width: 150,
        height: 100, // actually variable
    };


    $scope.redraw = function() {
        $scope.schema_uuid = 0;
        jsPlumb.detachEveryConnection();
        $scope.schema = [];
        $scope.library = [];
        $scope.addModuleToLibrary("Sum", "Aggregates an incoming sequences of values and returns the sum",
            $scope.library_topleft.x + $scope.library_topleft.margin,
            $scope.library_topleft.y + $scope.library_topleft.margin);
        $scope.addModuleToLibrary("Camera", "Hooks up to hardware camera and sends out an image at 20 Hz",
            $scope.library_topleft.x + $scope.library_topleft.margin,
            $scope.library_topleft.y + $scope.library_topleft.margin + $scope.library_topleft.item_height);
    };

    // add a module to the library
    $scope.addModuleToLibrary = function(title, description, posX, posY) {

        var library_id = $scope.library_uuid++;
        var schema_id = -1;
        var m = new module(library_id, schema_id, title, description, posX, posY);
        $scope.library.push(m);
    };

    // add a module to the schema
    $scope.addModuleToSchema = function(library_id, posX, posY) {

        var schema_id = $scope.schema_uuid++;
        var title = "Unknown";
        var description = "Likewise unknown";
        for (var i = 0; i < $scope.library.length; i++) {
            if ($scope.library[i].library_id == library_id) {
                title = $scope.library[i].title;
                description = $scope.library[i].description;
            }
        }
        var m = new module(library_id, schema_id, title, description, posX, posY);
        $scope.schema.push(m);
    };

    $scope.removeState = function(schema_id) {

        for (var i = 0; i < $scope.schema.length; i++) {
            // compare in non-strict manner
            if ($scope.schema[i].schema_id == schema_id) {

                $scope.schema.splice(i, 1);
            }
        }
    };
    $scope.submit = function(file) { //function to call on form submit
        $scope.upload(file);
    }
    $scope.upload = function(file) {
        Upload.upload({
            url: '/upload/?botId=' + $scope.botId, //webAPI exposed to upload the file
            data: { files: file } //pass file as data, should be user ng-model
        }).then(function(resp) {
            $scope.upload_botimg($scope.bot_images)
                //upload function returns a promise
            if (resp.data.error_code === 0) { //validate success
                $rootScope.botCount++;
                $scope.showSimpleToast('Successfully uploaded', 'success');
            } else {
                $scope.showSimpleToast('An error occured', 'danger');
            }
        }, function(resp) { //catch error
            $scope.showSimpleToast('Error status: ' + resp.status, 'danger');
        });
    };

    $scope.upload_botimg = function(file) {


        Upload.upload({
            url: '/upload_bot_img/?botId=' + $scope.botId, //webAPI exposed to upload the file
            data: { files: file } //pass file as data, should be user ng-model
        }).then(function(resp) {

            //upload function returns a promise
            if (resp.data.error_code === 0) { //validate success
                $scope.showSimpleToast('Media uploaded successfully.', 'success');

            } else {
                $scope.showSimpleToast('An error occured in media upload.', 'danger');
            }
        }, function(resp) { //catch error

            $scope.showSimpleToast('Error status: ' + resp.status, 'danger');
        });
    }

    $scope.logout = function() {

        delete $window.sessionStorage.id;
        delete $window.localStorage.id;
        $state.go('login');
    }
    $scope.list = function() {
        $state.go('list_bot', {}, { reload: true, notify: true });
    }
    $scope.init = function() {
        jsPlumb.bind("ready", function() {

            jsPlumb.bind("connection", function(info, ev) {
                // (jsPlumb.getEndpoints(info.sourceId), "ee");
                $scope.$apply(function() {

                    // info.setPaintStyle({ strokeWidth: 1 });
                    // info.repaint();

                    if ($scope[info.sourceId].input_option == "Suggestion") {
                        var endpoints_length = jsPlumb.getEndpoints(info.sourceId);

                        $scope[info.sourceId]['sugg_conn_state'] = {

                        };
                        if ($scope[info.sourceId].exp_in_mess.suggest_branches == false) {
                            $scope[info.sourceId].connected_state = info.targetId;
                            for (var i = 0; i < $scope[info.sourceId].exp_in_mess.suggestion_box.length; i++) {
                                $scope[info.sourceId]['sugg_conn_state'][$scope[info.sourceId].exp_in_mess.suggestion_box[i]] = info.targetId;

                            }

                        } else {
                            for (var i = 0; i < endpoints_length.length; i++) {
                                if (i == 0) {
                                    if (endpoints_length[i].isSource == true) {

                                        if (endpoints_length[i].connections.length > 0) {
                                            $scope[info.sourceId]['sugg_conn_state'][$scope[info.sourceId].exp_in_mess.suggestion_box[i]] = endpoints_length[i].connections[0].targetId;
                                        } else {

                                        }

                                    } else {

                                    }
                                } else {
                                    if (endpoints_length[i].isSource == true) {
                                        if (endpoints_length[i].connections.length > 0) {
                                            $scope[info.sourceId]['sugg_conn_state'][$scope[info.sourceId].exp_in_mess.suggestion_box[i - 1]] = endpoints_length[i].connections[0].targetId;
                                        } else {

                                        }
                                    } else {

                                    }
                                }
                            }
                        }
                    } else if ($scope[info.sourceId].condif == true) {
                        var endpoints_length = jsPlumb.getEndpoints(info.sourceId);
                        $scope[info.sourceId]['cond_conn_state'] = {}
                        var flag = true;
                        $scope.endpoints_length = []
                        angular.forEach(endpoints_length, function(value, key) {
                            if (value.isSource == true) {
                                $scope.endpoints_length.push(value);
                            } else {

                            }
                        });
                        for (var i = 0; i < $scope.endpoints_length.length; i++) {

                            if ($scope.endpoints_length[i].isSource == true) {

                                if ($scope.endpoints_length[i].connections.length > 0) {

                                    if (i == $scope.endpoints_length.length - 1) {
                                        if ($scope[info.sourceId].cond_details.A[i] == undefined) {
                                            var j = i - 1;
                                        } else {
                                            j = i
                                        }

                                        $scope[info.sourceId]['cond_conn_state']['Default'] = $scope.endpoints_length[i].connections[0].targetId;

                                    } else {
                                        if ($scope[info.sourceId].cond_details.A[i] == undefined) {
                                            var j = i - 1;
                                        } else {
                                            j = i
                                        }

                                        $scope[info.sourceId]['cond_conn_state']['cond' + j] = $scope.endpoints_length[i].connections[0].targetId;
                                    }
                                } else {

                                }
                            } else {

                            }
                        }

                    } else if ($scope[info.sourceId].input_option == "Star Rating") {
                        var endpoints_length = jsPlumb.getEndpoints(info.sourceId);
                        $scope[info.sourceId]['star_states'] = {

                        };
                        for (var i = 0; i < endpoints_length.length; i++) {
                            if (i == 0) {
                                if (endpoints_length[i].isSource == true) {

                                    if (endpoints_length[i].connections.length > 0) {
                                        $scope[info.sourceId]['star_states'][i + 1] = endpoints_length[i].connections[0].targetId;

                                    } else {

                                    }

                                } else {

                                }
                            } else {
                                if (endpoints_length[i].isSource == true) {
                                    if (endpoints_length[i].connections.length > 0) {
                                        $scope[info.sourceId]['star_states'][i] = endpoints_length[i].connections[0].targetId;

                                    } else {

                                    }
                                } else {

                                }
                            }
                        }

                    } else {
                        $scope[info.sourceId].connected_state = info.targetId;
                    }
                    console.log("gambit etails::" + endpoints_length);
                });
            });
            jsPlumb.bind("connectionDetached", function(info, ev) {

                // $scope.$apply(function() {

                /*    jsPlumb.connect({
                      source:info.sourceId,
                      target:info.targetId,
                      connector:[ "Straight", { spring:true } ],
                      paintStyle:{
                          lineWidth:1,
                          strokeStyle:"#456"
                      }
                  });*/
                if ($scope[info.sourceId].input_option == "Suggestion") {
                    var endpoints_length = jsPlumb.getEndpoints(info.sourceId);

                    $scope[info.sourceId]['sugg_conn_state'] = {

                    };
                    if ($scope[info.sourceId].exp_in_mess.suggest_branches == false) {
                        $scope[info.sourceId].connected_state = info.targetId;
                        for (var i = 0; i < $scope[info.sourceId].exp_in_mess.suggestion_box.length; i++) {
                            $scope[info.sourceId]['sugg_conn_state'][$scope[info.sourceId].exp_in_mess.suggestion_box[i]] = "";
                        }

                    } else {
                        for (var i = 0; i < endpoints_length.length; i++) {

                            if (i == 0) {
                                if (endpoints_length[i].isSource == true) {

                                    if (endpoints_length[i].connections.length > 0) {
                                        $scope[info.sourceId]['sugg_conn_state'][$scope[info.sourceId].exp_in_mess.suggestion_box[i]] = endpoints_length[i].connections[0].targetId;
                                    } else {

                                    }

                                } else {

                                }
                            } else {
                                if (endpoints_length[i].isSource == true) {

                                    if (endpoints_length[i].connections.length > 0) {
                                        $scope[info.sourceId]['sugg_conn_state'][$scope[info.sourceId].exp_in_mess.suggestion_box[i - 1]] = endpoints_length[i].connections[0].targetId;
                                    } else {

                                    }

                                } else {

                                }
                            }

                        }
                    }
                } else if ($scope[info.sourceId].condif == true) {
                    var endpoints_length = jsPlumb.getEndpoints(info.sourceId);
                    $scope[info.sourceId]['cond_conn_state'] = {

                    };
                    for (var i = 0; i < endpoints_length.length; i++) {


                        if (endpoints_length[i].isSource == true) {

                            if (endpoints_length[i].connections.length > 0) {

                                if (i == endpoints_length.length - 1) {

                                    $scope[info.sourceId]['cond_conn_state']['Default'] = endpoints_length[i].connections[0].targetId;

                                } else {

                                    $scope[info.sourceId]['cond_conn_state'][$scope[info.sourceId].conditional_states[i - 1]] = endpoints_length[i].connections[0].targetId;

                                }

                            } else {

                            }

                        } else {

                        }
                    }




                } else {
                    $scope[info.sourceId].connected_state = "";

                }

                // });
            });
        });
    }

    var i = 0;
    var top = 20,
        left = 30;
    $scope.startcheck = [{}];
    $scope.startState = "";
    $scope.apivalues = [];
    $scope.apilist = [];
    $scope.apiintlist = [];
    $scope.addgambit = function(e) {
        var newState = $('<div>').attr('id', 'state' + i).addClass('item');
        // var deldiv = $('<div>').attr('data-id', 'state' + i).addClass('del_gam');
        // var delState = "<i class='fa fa-times-circle-o' aria-hidden='true' style='width: 100%;'></i>";
        var start = $('<div>').attr('id', 'start' + i).addClass('fa fa-check-circle-o maincolor');
        var title = $('<div>').addClass('title').text('State ' + '[' + i + ']');
        var connect = $('<div>').addClass('connect');
        var divsize = ((Math.random() * 100) + 50).toFixed();
        start.css({
            'float': 'right',
            'margin': 5 + 'px',
            'font-size': 'large'
        });
        $newdiv = $('<div/>').css({
            'width': divsize + 'px',
            'height': divsize + 'px'
        });
        var key = '#start' + i;
        var val = 'state' + i;
        $scope.startcheck[0][key] = val;
        var posx = (Math.random() * ($(document).width() - divsize)).toFixed();
        var posy = (Math.random() * ($(document).height() - divsize)).toFixed();

        newState.css({
            'top': posx,
            'left': posy
        });


        if (i == 0) {

        } else {

        }
        jsPlumb.draggable(newState, {
            containment: 'body',
            endpoint: "Rectangle",
            grid: [10, 10]
        });

        var gambit_init_mess = [];
        var button_states = [];
        $scope.indexval = 0;
        $scope.buttonindex = 0;
        $scope.obj = {

        }
        $scope.buttonobj = {

        }

        $scope.text_mul_bub_Input = false;
        $scope.text_opt_Input = false;
        $scope.obj["message"] = { "preview_url": false, "text": "", "img_url": "", "title": "", "sub_title": "", "videoID": "", "img_form": "url", "image": "", "img_name": "", "buttons": [], "mess_type": "text", "media_type": "image", "media_id": "" };
        $scope.obj["input_fields"] = { "text": true, "image": false, "video": false };
        $scope.obj["ellipsis" + $scope.indexval] = false;
        $scope.obj["plus" + $scope.indexval] = false;
        $scope.obj["picture" + $scope.indexval] = false;
        $scope.obj["times" + $scope.indexval] = false;
        $scope.obj["text" + $scope.indexval] = false;
        $scope.obj["video" + $scope.indexval] = false;
        // $scope.obj["button_message"+$scope.buttonindex] = { "url": "", "phone": "", "payload": "" };
        $scope.buttonobj["button_inputFields"] = { "url": false, "phone": false, "payload": false };
        $scope.buttonobj["button_ellipsis" + $scope.buttonindex] = false;
        $scope.buttonobj["button_plus" + $scope.buttonindex] = false;
        $scope.buttonobj["button_external-link" + $scope.buttonindex] = false;
        $scope.buttonobj["button_phone" + $scope.buttonindex] = false;
        $scope.buttonobj["button_text" + $scope.buttonindex] = false;
        $scope.buttonobj["button_times" + $scope.buttonindex] = false;
        $scope.obj["button_states"] = []
        $scope.obj["button_states"].push($scope.buttonobj);
        $scope.exp_in_mess = {}
        gambit_init_mess.push($scope.obj);

        if ($scope.option == "Text") {
            $scope.exp_in_mess = {
                inputType: "Custom Text",
                keyType: "Normal Keyboard",
                capitalizeText: "Disabled",
                inputValidation: "No Validation",
                min_date: "",
                max_date: "",
                time: "",
                text_opt_Input: $scope.text_opt_Input,
                text_mul_bub_Input: $scope.text_mul_bub_Input
            }
        } else if ($scope.option == "Suggestion") {
            $scope.exp_in_mess = {
                suggestions: $scope.suggestions,
                suggest_preview_list: $scope.suggest_preview_list,
                suggest_opt_input: $scope.suggest_opt_input,
                suggest_mul_bub_Input: $scope.suggest_mul_bub_Input
            }
        } else if ($scope.option == "Carousel") {
            var carouselprop = {
                "title": "",
                "sub_title": "",
                "small_desc": "",
                "img_form": "url",
                "image": "",
                "img_name": "",
                "img_url": "",
                "title2": "",
                "sub_title2": "",
                "small_desc2": "",
                "button_text": "",
                "bt_act": "",
                "bt_payload": "",
                "bt_link": "",
                "footer": ""
            };
            $scope.exp_in_mess = {
                carousel: [carouselprop]
            }
        } else if ($scope.option == "Star Rating") {
            $scope.exp_in_mess = {
                star_rating: '',
                star_states: {}
            }
        } else {

        }
        $scope.carouselprop = {
            "title": "",
            "sub_title": "",
            "small_desc": "",
            "img_name": "",
            "title2": "",
            "sub_title2": "",
            "small_desc2": "",
            "button_text": "",
            "bt_act": "",
            "bt_payload": "",
            "bt_link": "",
            "footer": ""
        }
        $scope.carousel = [$scope.carouselprop];
        $scope['state' + i] = {
            ind: i,
            entry_response: false,
            send_res: false,
            res_timeout: false,
            state_name: 'state' + i,
            out_mess: [],
            carousel: $scope.carousel,
            template: "",
            carousel_message: "",
            template_option: "message",
            // state :'State' +'['+i+']',
            state: '',
            exp_in_mess: $scope.exp_in_mess,
            next_state: "",
            input_option: $scope.option,
            gambit_init_mess: gambit_init_mess,
            connected_state: '',
            button_states: button_states,
            multiple_conn: false,
            api_type: "POST",
            res_timeout_type: "message",
            api_url: "",
            previous_res: false,
            condif: false,
            conditional_states: [],
            timeout_message: '',
            timeout_state: '',
            timeout_url: '',
            timeout_delay: 50000,
            url_params: [{ paramname: '' }],
            expresType: "text",
            enable_multilang: false,
            language: "en",
            previous_message: false,
            optional_input: false
        }

        // start state
        start.on('click', function(e) {
            start.toggleClass("maincolor changecolor");
            var list = e.target.classList;
            $scope.startState = 'state' + e.target.id.substr(5);
            if (list.contains('maincolor')) {
                $.each($scope.startcheck[0], function(key1, val1) {
                    if ($scope.startState != val1) {
                        $(key1).attr('ng-disabled', 'false').css("pointer-events", "auto");
                    }
                });
                $scope.startState = "";
            } else if (list.contains('changecolor')) {
                $.each($scope.startcheck[0], function(key2, val2) {
                    if ($scope.startState != val2) {
                        $(key2).attr('ng-disabled', 'true').css("pointer-events", "none");
                    }
                });

            }
        });

        newState.append(start);
        newState.append(title);

        $('#diagramContainer1').append(newState);
        if ($scope.startState != "") {
            $.each($scope.startcheck[0], function(obj, value) {
                if ($scope.startState != value) {
                    $(obj).attr('ng-disabled', 'true');
                    $(obj).css("pointer-events", "none");
                }
            });
        }
        $scope.showAlert = function(ev) {
            $scope.statename = ev.currentTarget.id;

            var str = $scope.statename;
            var statenum = str.split("state");
            $scope.statenum = statenum[1];

            $modal.open({
                // locals: { gambit_init_mess: $scope.gambit_init_mess },
                templateUrl: 'gambitinfo.tmpl.html',
                controller: function($scope) {
                    ($scope.gambit_init_mess);
                    $scope.getSelectionHtml = function() {
                        var html = "";
                        if (typeof window.getSelection != "undefined") {
                            var sel = window.getSelection();
                            if (sel.rangeCount) {
                                var container = document.createElement("div");
                                for (var i = 0, len = sel.rangeCount; i < len; ++i) {
                                    container.appendChild(sel.getRangeAt(i).cloneContents());
                                }
                                html = container.innerHTML;
                            }
                        } else if (typeof document.selection != "undefined") {
                            if (document.selection.type == "Text") {
                                html = document.selection.createRange().htmlText;
                            }
                        }

                    }
                    $scope.prevvalue = function(statenum) {
                        $scope.previous = $scope["state" + statenum].state;
                    }
                    if ($scope.user.phone_number == true || $scope.user.phone_number == "true") {
                        var exists = $scope.apivalues.indexOf("bot_set_phonenumber");
                        if (exists == -1) {
                            $scope.apivalues.push("bot_set_phonenumber");
                        }
                    } else {
                        var ind = $scope.apivalues.indexOf("bot_set_phonenumber");
                        if (ind != -1) {
                            $scope.apivalues.splice(ind, 1);
                        }
                    }

                    if ($scope.time.stamp == true || $scope.time.stamp == "true") {
                        var exists = $scope.apivalues.indexOf("bot_set_timestamp");
                        if (exists == -1) {
                            $scope.apivalues.push("bot_set_timestamp");
                        }
                    } else {
                        var ind = $scope.apivalues.indexOf("bot_set_timestamp");
                        if (ind != -1) {
                            $scope.apivalues.splice(ind, 1);
                        }
                    }

                    $scope.updatestatename = function(statenum) {
                        var name = $scope["state" + statenum].state;
                        var exists = $scope.apivalues.indexOf($scope.previous);
                        if (exists == -1) {
                            $scope.apivalues.push(name);
                            if ($scope['state' + $scope.statenum]["ent_api_details"] != undefined) {
                                var apivalues = $scope.apivalues.filter(e => e !== $scope['state' + statenum].state);
                                $scope['state' + $scope.statenum]["ent_api_details"].apires[0][$scope['state' + $scope.statenum]["ent_api_details"].apikey[0].mainkey] = apivalues[0];
                            }
                            if ($scope['state' + $scope.statenum]["out_api_details"] != undefined) {
                                var apivalues = $scope.apivalues.filter(e => e !== $scope['state' + statenum].state);
                                $scope['state' + $scope.statenum]["out_api_details"].apires[0][$scope['state' + $scope.statenum]["out_api_details"].apikey[0].mainkey] = apivalues[0];
                            }
                        } else {
                            $scope.apivalues[exists] = name;
                        }
                    }
                    $scope.ok = function() {
                            $scope.$dismiss();
                            $('.modal-backdrop').remove()
                            $(document.body).removeClass("modal-open");
                        }
                        // $scope['state' + $scope.statenum].state = 'state' + $scope.statenum;
                    $scope.msgmenu = true;
                    $scope.showconfigs = function(type) {
                        switch (type) {
                            case 'msgsett':
                                $scope.msgmenu = false;
                                $scope.botmsgsett = true;
                                $scope.usrresptimeout = false;
                                $scope.apiintg = false;
                                $scope.condjump = false;
                                $scope.urlparams = false;
                                break;

                            case 'restimout':
                                $scope.msgmenu = false;
                                $scope.botmsgsett = false;
                                $scope.usrresptimeout = true;
                                $scope.apiintg = false;
                                $scope.condjump = false;
                                $scope.urlparams = false;
                                break;

                            case 'api':
                                $scope.msgmenu = false;
                                $scope.botmsgsett = false;
                                $scope.usrresptimeout = false;
                                $scope.apiintg = true;
                                $scope.condjump = false;
                                $scope.urlparams = false;
                                break;

                            case 'cond':
                                $scope.msgmenu = false;
                                $scope.botmsgsett = false;
                                $scope.usrresptimeout = false;
                                $scope.apiintg = false;
                                $scope.condjump = true;
                                $scope.urlparams = false;
                                break;

                            case 'urlparam':
                                $scope.msgmenu = false;
                                $scope.botmsgsett = false;
                                $scope.usrresptimeout = false;
                                $scope.apiintg = false;
                                $scope.condjump = false;
                                $scope.urlparams = true;
                                break;

                            default:
                                $scope.msgmenu = false;
                                $scope.botmsgsett = true;
                                $scope.usrresptimeout = false;
                                $scope.apiintg = false;
                                $scope.condjump = false;
                                $scope.urlparams = false;
                                break;
                        }
                    }
                    $scope.addapiintegration = function(statenum) {
                        if ($scope['state' + statenum].send_res) {
                            // var apivalues = $scope.apivalues.filter(e => e !== $scope['state' + statenum].state + '.ans');
                            var api_obj = {
                                "method": "get",
                                "url": "",
                                "params": [],
                                "body": [],
                                "headerparams": [],
                                "auth": {},
                                "showparams": false,
                                "body_type": "raw",
                                "raw_json": JSON.stringify({}),
                                "api_name": "",
                                "apires": [{}],
                                "api_url": "",
                                "api_id": "",
                                "failure_state": ""
                            }
                            $scope['state' + statenum]["out_api_details"] = api_obj;
                            $http.defaults.headers.common['user'] = $window.sessionStorage.id;
                            $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
                            $scope.apiintlist = [];
                            $scope.apiintkeys = [{ mainkey: "", subkey: "", apivalue: "" }];
                            $http.get('/getapis', {}).then(function(res) {
                                for (var i = 0; i < res.data.length; i++) {
                                    $scope.apiintlist[i] = {
                                        api_id: res.data[i]._id,
                                        api_name: (res.data[i].ApiName == undefined) ? res.data[i].api_info.url : res.data[i].ApiName,
                                        method: res.data[i].api_info.method,
                                        params: res.data[i].api_info.params,
                                        body: res.data[i].api_info.body,
                                        headerparams: res.data[i].api_info.headerparams,
                                        showparams: res.data[i].api_info.showparams,
                                        url: res.data[i].api_info.url,
                                        body_type: res.data[i].api_info.body_type,
                                        raw_json: res.data[i].api_info.raw_json
                                    };
                                }
                                $scope['state' + statenum]['out_api_details'].api_url = $scope.apiintlist[0];
                                $scope.getapiintres(statenum, $scope['state' + statenum]['out_api_details'].api_url);
                            });
                        }
                    }
                    $scope.addentryres = function(statenum) {
                        if ($scope['state' + statenum].entry_response == true) {
                            // var apivalues = $scope.apivalues.filter(e => e !== $scope['state' + statenum].state);
                            var api_obj = {
                                "method": "get",
                                "url": "",
                                "params": [],
                                "body": [],
                                "headerparams": [],
                                "auth": {},
                                "showparams": false,
                                "body_type": "raw",
                                "raw_json": {},
                                "api_name": "",
                                "apires": [{}],
                                "api_id": "",
                                "failure_state": ""
                            }
                            $scope['state' + statenum]["ent_api_details"] = api_obj;
                            $scope.entry_response_msg = "";
                            $http.defaults.headers.common['user'] = $window.sessionStorage.id;
                            $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
                            $scope.apilist = [];
                            $scope.apikeys = [{ mainkey: "", subkey: "", apivalue: "" }];
                            $http.get('/getapis', {}).then(function(res) {
                                for (var i = 0; i < res.data.length; i++) {
                                    $scope.apilist[i] = {
                                        api_id: res.data[i]._id,
                                        api_name: (res.data[i].ApiName == undefined) ? res.data[i].api_info.url : res.data[i].ApiName,
                                        method: res.data[i].api_info.method,
                                        params: res.data[i].api_info.params,
                                        body: res.data[i].api_info.body,
                                        headerparams: res.data[i].api_info.headerparams,
                                        showparams: res.data[i].api_info.showparams,
                                        url: res.data[i].api_info.url,
                                        body_type: res.data[i].api_info.body_type,
                                        raw_json: res.data[i].api_info.raw_json
                                    };
                                }
                                $scope['state' + statenum].api_name = $scope.apilist[0];
                                $scope.getapires(statenum, $scope['state' + statenum].api_name);
                            });
                        } else {
                            $scope['state' + statenum]["ent_api_details"] = {};
                            $scope.entry_response_msg = "Take Gambit Entry Api Response";
                        }
                    }
                    $scope.apiparams = [{
                        "paramkey": "",
                        "paramvalue": ""
                    }]
                    $scope.apiintparams = [{
                        "paramkey": "",
                        "paramvalue": ""
                    }]
                    $scope.headerparams = [{ "paramkey": "", "paramvalue": "" }]
                    $scope.headerintparams = [{ "paramkey": "", "paramvalue": "" }]

                    $scope.addparams = function(statenum) {
                        if ($scope['state' + statenum]["ent_api_details"].params == undefined) {
                            $scope['state' + statenum]["ent_api_details"].params = [];
                        }
                        var obj = {
                            "paramkey": "",
                            "paramvalue": ""
                        }
                        $scope['state' + statenum]["ent_api_details"].params.push(obj);
                    }
                    $scope.addintparams = function(statenum) {
                        if ($scope['state' + statenum]["out_api_details"].params == undefined) {
                            $scope['state' + statenum]["out_api_details"].params = [];
                        }
                        var obj = {
                            "paramkey": "",
                            "paramvalue": ""
                        }
                        $scope['state' + statenum]["out_api_details"].params.push(obj);
                    }
                    $scope.removeparams = function(statenum, paramsindex) {

                        $scope['state' + statenum]["ent_api_details"].params.splice(paramsindex, 1);
                    }
                    $scope.removeintparams = function(statenum, paramsindex) {

                        $scope['state' + statenum]["out_api_details"].params.splice(paramsindex, 1);
                    }

                    $scope.addurlparams = function(statenum) {
                        if ($scope['state' + statenum]["url_params"] == undefined) {
                            $scope['state' + statenum]["url_params"] = [];
                        }
                        var obj = {
                            "paramname": ""
                        }
                        $scope['state' + statenum]["url_params"].push(obj);
                    }
                    $scope.removeurlparams = function(statenum, paramsindex) {

                        $scope['state' + statenum]["url_params"].splice(paramsindex, 1);
                    }

                    $scope.rmhdrparams = function(statenum, paramsindex) {

                            $scope['state' + statenum]["ent_api_details"].headerparams.splice(paramsindex, 1);
                        }
                        // $scope.rmhdrparams = function(statenum, paramsindex) {

                    //     $scope['state' + statenum]["ent_api_details"].headerparams.splice(paramsindex, 1);
                    // }
                    $scope.rmbodyparams = function(statenum, paramsindex) {

                        $scope['state' + statenum]["ent_api_details"].body.splice(paramsindex, 1);
                    }

                    $scope.addbodyparams = function(statenum) {
                        if ($scope['state' + statenum]["ent_api_details"].body == undefined) {
                            $scope['state' + statenum]["ent_api_details"].body = [];
                        }
                        var obj = {
                            "paramkey": "",
                            "paramvalue": ""
                        }
                        $scope['state' + statenum]["ent_api_details"].body.push(obj);
                    }
                    $scope.addformdatabody = function(statenum) {
                        if ($scope['state' + statenum]["ent_api_details"].form_data_body == undefined) {
                            $scope['state' + statenum]["ent_api_details"].form_data_body = [];
                        }
                        var obj = {
                            "paramkey": "",
                            "paramvalue": ""
                        }
                        $scope['state' + statenum]["ent_api_details"].form_data_body.push(obj);
                    }
                    $scope.rmformdatabody = function(statenum, paramsindex) {

                        $scope['state' + statenum]["ent_api_details"].form_data_body.splice(paramsindex, 1);
                    }
                    $scope.addintformdatabody = function(statenum) {
                        if ($scope['state' + statenum]["out_api_details"].form_data_body == undefined) {
                            $scope['state' + statenum]["out_api_details"].form_data_body = [];
                        }
                        var obj = {
                            "paramkey": "",
                            "paramvalue": ""
                        }
                        $scope['state' + statenum]["out_api_details"].form_data_body.push(obj);
                    }
                    $scope.rmintformdatabody = function(statenum, paramsindex) {

                        $scope['state' + statenum]["out_api_details"].form_data_body.splice(paramsindex, 1);
                    }
                    $scope.rmurlencodedbody = function(statenum, paramsindex) {

                        $scope['state' + statenum]["ent_api_details"].urlencoded_body.splice(paramsindex, 1);
                    }

                    $scope.addurlencodedbody = function(statenum) {
                        if ($scope['state' + statenum]["ent_api_details"].urlencoded_body == undefined) {
                            $scope['state' + statenum]["ent_api_details"].urlencoded_body = [];
                        }
                        var obj = {
                            "paramkey": "",
                            "paramvalue": ""
                        }
                        $scope['state' + statenum]["ent_api_details"].urlencoded_body.push(obj);
                    }
                    $scope.rminturlencodedbody = function(statenum, paramsindex) {

                        $scope['state' + statenum]["out_api_details"].urlencoded_body.splice(paramsindex, 1);
                    }

                    $scope.addinturlencodedbody = function(statenum) {
                        if ($scope['state' + statenum]["out_api_details"].urlencoded_body == undefined) {
                            $scope['state' + statenum]["out_api_details"].urlencoded_body = [];
                        }
                        var obj = {
                            "paramkey": "",
                            "paramvalue": ""
                        }
                        $scope['state' + statenum]["out_api_details"].urlencoded_body.push(obj);
                    }
                    $scope.showparams = true;
                    $scope.showintparams = true;
                    $scope.toggleparams = function(statenum) {

                        if ($scope['state' + statenum]["ent_api_details"].showparams == true) {
                            $scope['state' + statenum]["ent_api_details"].showparams = false;
                        } else {

                            $scope['state' + statenum]["ent_api_details"].showparams = true
                        }
                    }

                    $scope.addheadparams = function(statenum) {
                        if ($scope['state' + statenum]["ent_api_details"].headerparams == undefined) {
                            $scope['state' + statenum]["ent_api_details"].headerparams = [];
                        }
                        var obj = {
                            "paramkey": "",
                            "paramvalue": ""
                        }
                        $scope['state' + statenum]["ent_api_details"].headerparams.push(obj);
                    }
                    $scope.apikeys = [];
                    $scope.getapires = function(statenum, item) {
                        var ind = $scope.apilist.findIndex(record => record.api_id === item.api_id); // $scope.apilist.indexOf(item);
                        $scope.listind = ind;
                        $scope['state' + statenum]["ent_api_details"].api_id = $scope.apilist[ind].api_id;
                        $scope['state' + statenum]["ent_api_details"].api_name = $scope.apilist[ind].api_name;
                        $scope['state' + statenum]["ent_api_details"].url = $scope.apilist[ind].url;
                        $scope['state' + statenum]["ent_api_details"].body = $scope.apilist[ind].body;
                        $scope['state' + statenum]["ent_api_details"].headerparams = $scope.apilist[ind].headerparams;
                        $scope['state' + statenum]["ent_api_details"].params = $scope.apilist[ind].params;
                        $scope['state' + statenum]["ent_api_details"].method = $scope.apilist[ind].method;
                        $scope['state' + statenum]["ent_api_details"].body_type = $scope.apilist[ind].body_type;
                        $scope['state' + statenum]["ent_api_details"].raw_json = JSON.stringify($scope.apilist[ind].raw_json);
                        $scope['state' + statenum].api_url = $scope.apilist[ind].url;
                        $scope['state' + statenum].api_type = $scope.apilist[ind].method;
                        // $scope.apires = $scope.apilist[ind].raw_json;
                        $scope['state' + $scope.statenum].api_name = $scope['state' + $scope.statenum].api_name
                            // var apivalues = $scope.apivalues.filter(e => e !== $scope['state' + statenum].state);

                        if ($scope['state' + statenum]["ent_api_details"].url == "") {
                            $scope.showSimpleToast("Empty URL request", "warning");
                        } else {
                            $scope.apires = {};
                            var body_data = {};
                            var header_data = { 'Content-Type': 'application/json' };
                            $scope['state' + statenum]["ent_api_details"].showparams = false;

                            if ($scope['state' + statenum]["ent_api_details"].headerparams.length > 0) {

                                angular.forEach($scope['state' + statenum]["ent_api_details"].headerparams, function(value, key) {

                                    header_data[value.paramkey] = value.paramvalue
                                });
                            }
                            if ($scope['state' + statenum]["ent_api_details"].body_type == "urlencoded") {
                                if ($scope["ent_api_details"].urlencoded_body.length > 0) {
                                    angular.forEach($scope["ent_api_details"].urlencoded_body, function(value, key) {
                                        body_data[value.bodykey] = value.bodyvalue;
                                        $scope.apires[value.bodykey] = value.bodyvalue;
                                    });
                                }

                            } else if ($scope['state' + statenum]["ent_api_details"].body_type == "form_data") {
                                if ($scope['state' + statenum]["ent_api_details"].form_data_body.length > 0) {
                                    angular.forEach($scope['state' + statenum]["ent_api_details"].form_data_body, function(value, key) {
                                        body_data[value.bodykey] = value.bodyvalue;
                                        $scope.apires[value.bodykey] = value.bodyvalue;
                                    });
                                }


                            } else {
                                if ($scope['state' + statenum]["ent_api_details"].raw_json != undefined) {
                                    if (Object.keys($scope['state' + statenum]["ent_api_details"].raw_json).length > 0) {
                                        body_data = JSON.parse($scope['state' + statenum]["ent_api_details"].raw_json);
                                        $scope.apires = body_data;
                                    } else {
                                        body_data = {};
                                    }
                                } else {
                                    body_data = {};
                                }
                            }
                            var parameter = ""
                            if ($scope['state' + statenum]["ent_api_details"].params.length > 0) {
                                $scope['state' + statenum]["ent_api_details"].showparams = true;
                                angular.forEach($scope['state' + statenum]["ent_api_details"].params, function(value, key) {

                                    parameter = parameter + value.paramkey + "=" + value.paramvalue + '&';
                                    $scope.apires[value.paramkey] = value.paramvalue;
                                });
                            }
                            //  var url = $scope['state' + statenum]["ent_api_details"].url + "/?"+parameter
                            var req_body = {
                                'method': $scope['state' + statenum]["ent_api_details"].method,
                                'url': $scope['state' + statenum]["ent_api_details"].url,
                                'headers': header_data,
                                'body': body_data,
                                'param': parameter
                            }
                            $scope.apiresponse = "";
                            $scope.apistatus = "";
                            $scope.apistatustext = "";

                            $http.defaults.headers.common['user'] = $window.sessionStorage.id;
                            $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
                            $http.post('/execute_api', req_body, {
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(function(res) {

                                $scope.apiresponse = JSON.stringify(res.data, undefined, 2);
                                $scope.apistatus = res.data.status;
                                $scope.apistatustext = res.data.statusText;
                            });
                            if ($scope.apires !== undefined && Object.keys($scope.apires).length !== 0) {
                                if ($scope.apikeys == undefined) {
                                    $scope.apikeys = [{ mainkey: "", subkey: "", apivalue: "" }];
                                }
                                $scope.obj = {};
                            } else {
                                $scope['state' + $scope.statenum]["ent_api_details"].apires[0] = {};
                            }
                        }
                    }
                    $scope.getapiintres = function(statenum, item) {
                        var ind = $scope.apiintlist.findIndex(record => record.api_id === item.api_id); // $scope.apilist.indexOf(item);
                        $scope.intlist = ind;
                        $scope['state' + statenum]["out_api_details"].api_id = $scope.apiintlist[ind].api_id;
                        $scope['state' + statenum]["out_api_details"].api_name = $scope.apiintlist[ind].api_name;
                        $scope['state' + statenum]["out_api_details"].url = $scope.apiintlist[ind].url;
                        $scope['state' + statenum]["out_api_details"].body = $scope.apiintlist[ind].body;
                        $scope['state' + statenum]["out_api_details"].headerparams = $scope.apiintlist[ind].headerparams;
                        $scope['state' + statenum]["out_api_details"].params = $scope.apiintlist[ind].params;
                        $scope['state' + statenum]["out_api_details"].method = $scope.apiintlist[ind].method;
                        $scope['state' + statenum]["out_api_details"].body_type = $scope.apiintlist[ind].body_type;
                        $scope['state' + statenum]["out_api_details"].raw_json = JSON.stringify($scope.apiintlist[ind].raw_json);
                        $scope['state' + statenum].api_url = $scope.apiintlist[ind].url;
                        $scope['state' + statenum].api_type = $scope.apiintlist[ind].method;
                        // $scope.apiintres = $scope.apiintlist[ind].raw_json;
                        $scope['state' + statenum]["out_api_details"].api_url = $scope['state' + statenum]["out_api_details"].api_url;

                        if ($scope['state' + statenum]["out_api_details"].url == "") {
                            $scope.showSimpleToast("Empty URL request", "warning");
                        } else {
                            $scope.apiintres = {};
                            var body_data = {}
                            var header_data = { 'Content-Type': 'application/json' };
                            $scope['state' + statenum]["out_api_details"].showparams = false;
                            if ($scope['state' + statenum]["out_api_details"].headerparams.length > 0) {

                                angular.forEach($scope['state' + statenum]["out_api_details"].headerparams, function(value, key) {

                                    header_data[value.paramkey] = value.paramvalue;

                                });
                            }
                            if ($scope['state' + statenum]["out_api_details"].body_type == "urlencoded") {
                                if ($scope["out_api_details"].urlencoded_body.length > 0) {
                                    angular.forEach($scope["out_api_details"].urlencoded_body, function(value, key) {
                                        body_data[value.bodykey] = value.bodyvalue;
                                        $scope.apiintres[value.bodykey] = value.bodyvalue;
                                    });
                                }

                            } else if ($scope['state' + statenum]["out_api_details"].body_type == "form_data") {
                                if ($scope['state' + statenum]["out_api_details"].form_data_body.length > 0) {
                                    angular.forEach($scope['state' + statenum]["out_api_details"].form_data_body, function(value, key) {
                                        body_data[value.bodykey] = value.bodyvalue;
                                        $scope.apiintres[value.bodykey] = value.bodyvalue;
                                    });
                                }


                            } else {
                                if ($scope['state' + statenum]["out_api_details"].raw_json != undefined) {
                                    if (Object.keys($scope['state' + statenum]["out_api_details"].raw_json).length > 0) {
                                        body_data = JSON.parse($scope['state' + statenum]["out_api_details"].raw_json);
                                        $scope.apiintres = body_data;
                                    } else {
                                        body_data = {};
                                    }
                                } else {
                                    body_data = {};
                                }

                            }
                            var parameter = ""
                            if ($scope['state' + statenum]["out_api_details"].params.length > 0) {
                                angular.forEach($scope['state' + statenum]["out_api_details"].params, function(value, key) {

                                    parameter = parameter + value.paramkey + "=" + value.paramvalue + '&';
                                    $scope['state' + statenum]["out_api_details"].showparams = true;
                                });
                            }
                            //  var url = $scope['state' + statenum]["ent_api_details"].url + "/?"+parameter
                            var req_body = {
                                'method': $scope['state' + statenum]["out_api_details"].method,
                                'url': $scope['state' + statenum]["out_api_details"].url,
                                'headers': header_data,
                                'body': body_data,
                                'param': parameter
                            }
                            $scope.apiintresponse = "";
                            $scope.apiintstatus = "";
                            $scope.apiintstatustext = "";

                            $http.defaults.headers.common['user'] = $window.sessionStorage.id;
                            $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
                            $http.post('/execute_api', req_body, {
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(function(res) {

                                $scope.apiintresponse = JSON.stringify(res.data, undefined, 2);
                                $scope.apiintstatus = res.data.status;
                                $scope.apiintstatustext = res.data.statusText;
                            });
                            if ($scope.apiintres !== undefined && Object.keys($scope.apiintres).length !== 0) {
                                if ($scope.apiintkeys == undefined) {
                                    $scope.apiintkeys = [{ mainkey: "", subkey: "", apivalue: "" }];
                                }
                                $scope.intobj = {};
                            } else {
                                $scope['state' + $scope.statenum]["out_api_details"].apires[0] = {};
                            }
                        }
                    }
                    $scope.subapikeyres = function(prop, i) {
                        // var obj = {};
                        if (typeof($scope.apires) == 'object' && !Array.isArray($scope.apires)) {
                            if (typeof($scope.apires[prop]) == 'object' && !Array.isArray($scope.apires[prop])) {
                                // $scope['state' + $scope.statenum]["ent_api_details"].apikeys[i].subkeys = Object.keys($scope.apires[prop]);
                                // if ($scope['state' + $scope.statenum]["ent_api_details"].apikeys[0].subkey == "") {
                                //     $scope['state' + $scope.statenum]["ent_api_details"].apikeys[0].subkey = $scope['state' + $scope.statenum]["ent_api_details"].apikeys[0].subkeys[0];
                                // }
                            } else if (Array.isArray($scope.apires[prop])) {
                                // $.each(res.data.data,function(obj,value){
                                $scope['state' + $scope.statenum]["ent_api_details"].apikeys[i].subkeys = Object.keys($scope.apires[prop][0]);
                                $scope.obj[prop] = [];
                                $scope['state' + $scope.statenum]["ent_api_details"].apikeys[i].apires[0] = $scope.obj;
                                // if ($scope['state' + $scope.statenum]["ent_api_details"].apikeys[0].subkey == "") {
                                //     $scope['state' + $scope.statenum]["ent_api_details"].apikeys[0].subkey = $scope['state' + $scope.statenum]["ent_api_details"].apikeys[0].subkeys[0];
                                // }
                                $scope['state' + $scope.statenum]["ent_api_details"].apires[0] = $scope.obj;
                                // });
                            } else if ($scope.apires[prop] != undefined) {
                                $scope['state' + $scope.statenum]["ent_api_details"].apikeys[i].subkeys = [];
                                $scope.obj[prop] = $scope['state' + $scope.statenum]["ent_api_details"].apikeys[0].apivalue;
                                $scope['state' + $scope.statenum]["ent_api_details"].apikeys[i].apires[0] = $scope.obj;
                                // if ($scope['state' + $scope.statenum]["ent_api_details"].apikeys[0].subkey == "") {
                                //     $scope['state' + $scope.statenum]["ent_api_details"].apikeys[0].subkey = $scope['state' + $scope.statenum]["ent_api_details"].apikeys[0].subkeys[0];
                                // }
                                $scope['state' + $scope.statenum]["ent_api_details"].apires[0] = $scope.obj;
                            }
                        } else if (Array.isArray($scope.apires)) {
                            if (typeof($scope.apires[0][prop]) == 'object' && !Array.isArray($scope.apires[0][prop])) {
                                $scope['state' + $scope.statenum]["ent_api_details"].apikeys[i].subkeys = Object.keys($scope.apires[0][prop]);
                                // if ($scope['state' + $scope.statenum]["ent_api_details"].apikeys[0].subkey == "") {
                                //     $scope['state' + $scope.statenum]["ent_api_details"].apikeys[0].subkey = $scope['state' + $scope.statenum]["ent_api_details"].apikeys[0].subkeys[0];
                                // }
                                // obj[prop] = {};
                                // $scope['state' + $scope.statenum]["ent_api_details"].apikeys[i].apires[0] = obj;
                            } else if (Array.isArray($scope.apires[0][prop])) {
                                // $.each(res.data.data,function(obj,value){
                                $scope['state' + $scope.statenum]["ent_api_details"].apikeys[i].subkeys = Object.keys($scope.apires[0][prop]);
                                $scope.obj[prop] = [];
                                $scope['state' + $scope.statenum]["ent_api_details"].apikeys[i].apires[0] = $scope.obj;
                                // if ($scope['state' + $scope.statenum]["ent_api_details"].apikeys[0].subkey == "") {
                                //     $scope['state' + $scope.statenum]["ent_api_details"].apikeys[0].subkey = $scope['state' + $scope.statenum]["ent_api_details"].apikeys[0].subkeys[0];
                                // }
                                $scope['state' + $scope.statenum]["ent_api_details"].apires[0] = $scope.obj;
                                // });
                            } else if ($scope.apires[prop] != undefined) {
                                $scope['state' + $scope.statenum]["ent_api_details"].apikeys[i].subkeys = [];
                                $scope.obj[prop] = $scope['state' + $scope.statenum]["ent_api_details"].apikeys[0].apivalue;
                                $scope['state' + $scope.statenum]["ent_api_details"].apikeys[i].apires[0] = $scope.obj;
                                // if ($scope['state' + $scope.statenum]["ent_api_details"].apikeys[0].subkey == "") {
                                //     $scope['state' + $scope.statenum]["ent_api_details"].apikeys[0].subkey = $scope['state' + $scope.statenum]["ent_api_details"].apikeys[0].subkeys[0];
                                // }
                                $scope['state' + $scope.statenum]["ent_api_details"].apires[0] = $scope.obj;
                            }
                        }
                    }
                    $scope.subapiintkeyres = function(prop, i) {
                        // var obj = {};
                        if (typeof($scope.apiintres) == 'object' && !Array.isArray($scope.apiintres)) {
                            if (typeof($scope.apiintres[prop]) == 'object' && !Array.isArray($scope.apiintres[prop])) {
                                $scope['state' + $scope.statenum]["out_api_details"].apikeys[i].subkeys = Object.keys($scope.apiintres[prop]);
                                // if ($scope['state' + $scope.statenum]["out_api_details"].apikeys[0].subkey == "") {
                                //     $scope['state' + $scope.statenum]["out_api_details"].apikeys[0].subkey = $scope['state' + $scope.statenum]["out_api_details"].apikeys[0].subkeys[0];
                                // }
                            } else if (Array.isArray($scope.apiintres[prop])) {
                                // $.each(res.data.data,function(obj,value){
                                $scope['state' + $scope.statenum]["out_api_details"].apikeys[i].subkeys = Object.keys($scope.apiintres[prop][0]);
                                $scope.intobj[prop] = [];
                                $scope['state' + $scope.statenum]["out_api_details"].apikeys[i].apires[0] = $scope.intobj;
                                // if ($scope['state' + $scope.statenum]["out_api_details"].apikeys[0].subkey == "") {
                                //     $scope['state' + $scope.statenum]["out_api_details"].apikeys[0].subkey = $scope['state' + $scope.statenum]["out_api_details"].apikeys[0].subkeys[0];
                                // }
                                $scope['state' + $scope.statenum]["out_api_details"].apires[0] = $scope.intobj;
                                // });
                            } else if ($scope.apiintres[prop] != undefined) {
                                $scope['state' + $scope.statenum]["out_api_details"].apikeys[i].subkeys = [];
                                $scope.intobj[prop] = $scope['state' + $scope.statenum]["out_api_details"].apikeys[0].apivalue;
                                $scope['state' + $scope.statenum]["out_api_details"].apikeys[i].apires[0] = $scope.intobj;
                                // if ($scope['state' + $scope.statenum]["out_api_details"].apikeys[0].subkey == "") {
                                //     $scope['state' + $scope.statenum]["out_api_details"].apikeys[0].subkey = $scope['state' + $scope.statenum]["out_api_details"].apikeys[0].subkeys[0];
                                // }
                                $scope['state' + $scope.statenum]["out_api_details"].apires[0] = $scope.intobj;
                            }
                        } else if (Array.isArray($scope.apires)) {
                            if (typeof($scope.apires[0][prop]) == 'object' && !Array.isArray($scope.apires[0][prop])) {
                                $scope['state' + $scope.statenum]["out_api_details"].apikeys[i].subkeys = Object.keys($scope.apires[0][prop]);
                                // if ($scope['state' + $scope.statenum]["out_api_details"].apikeys[0].subkey == "") {
                                //     $scope['state' + $scope.statenum]["out_api_details"].apikeys[0].subkey = $scope['state' + $scope.statenum]["out_api_details"].apikeys[0].subkeys[0];
                                // }
                                // // obj[prop] = {};
                                // $scope['state' + $scope.statenum]["ent_api_details"].apikeys[i].apires[0] = obj;
                            } else if (Array.isArray($scope.apires[0][prop])) {
                                // $.each(res.data.data,function(obj,value){
                                $scope['state' + $scope.statenum]["out_api_details"].apikeys[i].subkeys = Object.keys($scope.apires[0][prop]);
                                $scope.intobj[prop] = [];
                                $scope['state' + $scope.statenum]["out_api_details"].apikeys[i].apires[0] = $scope.intobj;
                                // if ($scope['state' + $scope.statenum]["out_api_details"].apikeys[0].subkey == "") {
                                //     $scope['state' + $scope.statenum]["out_api_details"].apikeys[0].subkey = $scope['state' + $scope.statenum]["out_api_details"].apikeys[0].subkeys[0];
                                // }
                                $scope['state' + $scope.statenum]["out_api_details"].apires[0] = $scope.intobj;
                                // });
                            } else if ($scope.apiintres[prop] != undefined) {
                                $scope['state' + $scope.statenum]["out_api_details"].apikeys[i].subkeys = [];
                                $scope.intobj[prop] = $scope['state' + $scope.statenum]["out_api_details"].apikeys[0].apivalue;
                                $scope['state' + $scope.statenum]["out_api_details"].apikeys[i].apires[0] = $scope.intobj;
                                // if ($scope['state' + $scope.statenum]["out_api_details"].apikeys[0].subkey == "") {
                                //     $scope['state' + $scope.statenum]["out_api_details"].apikeys[0].subkey = $scope['state' + $scope.statenum]["out_api_details"].apikeys[0].subkeys[0];
                                // }
                                $scope['state' + $scope.statenum]["out_api_details"].apires[0] = $scope.intobj;
                            }
                        }
                    }
                    $scope.prevkey = function(key) {
                        $scope.prev_key = key;
                    }
                    $scope.addapikey = function(prop, i) {
                        if (prop !== $scope.prev_key) {
                            var exists = $scope['state' + $scope.statenum]["ent_api_details"].apires.indexOf($scope.prev_key);
                            if (exists != -1) {
                                delete $scope['state' + $scope.statenum]["ent_api_details"].apires[0][$scope.prev_key];
                            }
                            var keys = prop.split("&");
                            var subprop = keys[1];
                            // var obj = {};
                            if (keys[0] != null) {
                                if (typeof($scope.apires) == 'object' && !Array.isArray($scope.apires)) {
                                    if (typeof($scope.apires[keys[0]]) == 'object' && !Array.isArray($scope.apires[keys[0]])) {
                                        $scope.obj['"' + prop + '"'] = $scope.apikeys[i].apivalue;
                                        $scope['state' + $scope.statenum]["ent_api_details"].apires[0] = $scope.obj;
                                    } else if (Array.isArray($scope.apires[keys[0]])) {
                                        $scope.obj[subprop] = $scope.apikeys[i].apivalue;
                                        $scope['state' + $scope.statenum]["ent_api_details"].apires[0][keys[0]][0] = $scope.obj;

                                    } else {
                                        $scope.obj[prop] = $scope.apikeys[i].apivalue;
                                        $scope['state' + $scope.statenum]["ent_api_details"].apires[0] = $scope.obj;
                                    }
                                } else if (Array.isArray($scope.apires)) {
                                    if (typeof($scope.apires[0][keys[0]]) == 'object' && !Array.isArray($scope.apires[0][keys[0]])) {
                                        $scope.obj['"' + prop + '"'] = $scope.apikeys[i].apivalue;
                                        $scope['state' + $scope.statenum]["ent_api_details"].apires[0] = $scope.obj;
                                    } else if (Array.isArray($scope.apires[0][keys[0]])) {
                                        $scope.obj[subprop] = $scope.apikeys[i].apivalue;
                                        $scope['state' + $scope.statenum]["ent_api_details"].apires[0][keys[0]][0] = $scope.obj;
                                    } else {
                                        $scope.obj[prop] = $scope.apikeys[i].apivalue;
                                        $scope['state' + $scope.statenum]["ent_api_details"].apires[0] = $scope.obj;
                                    }
                                }
                            }
                        } else {

                        }
                    }
                    $scope.previntkey = function(key) {
                        $scope.prev_int_key = key;
                    }
                    $scope.addapiintkey = function(prop, i) {
                        if (prop !== $scope.prev_int_key) {
                            var keys = prop.split("&");
                            var subprop = keys[1];
                            // var obj = {};
                            if (prop != null && subprop != null) {
                                if (typeof($scope.apiintres) == 'object' && !Array.isArray($scope.apiintres)) {
                                    if (typeof($scope.apiintres[keys[0]]) == 'object' && !Array.isArray($scope.apiintres[prop])) {
                                        $scope.intobj['"' + prop + '"'] = $scope.apiintkeys[0].apivalue;
                                        $scope['state' + $scope.statenum]["out_api_details"].apires[0] = $scope.intobj;
                                    } else if (Array.isArray($scope.apiintres[keys[0]])) {
                                        $scope.intobj[subprop] = $scope.apiintkeys[0].apivalue;
                                        $scope['state' + $scope.statenum]["out_api_details"].apires[0][keys[0]][0] = $scope.intobj;
                                    } else {
                                        $scope.obj[prop] = $scope.apikeys[i].apivalue;
                                        $scope['state' + $scope.statenum]["out_api_details"].apires[0] = $scope.obj;
                                    }
                                } else if (Array.isArray($scope.apiintres)) {
                                    if (typeof($scope.apiintres[0][keys[0]]) == 'object' && !Array.isArray($scope.apiintres[0][keys[0]])) {
                                        $scope.intobj['"' + prop + '&' + subprop + '"'] = $scope.apiintkeys[0].apivalue;
                                        $scope['state' + $scope.statenum]["out_api_details"].apires[0] = $scope.intobj;
                                    } else if (Array.isArray($scope.apiintres[0][keys[0]])) {
                                        $scope.intobj[subprop] = $scope.apiintkeys[0].apivalue;
                                        $scope['state' + $scope.statenum]["out_api_details"].apires[0][prop][0] = $scope.intobj;
                                    } else {
                                        $scope.obj[prop] = $scope.apiintkeys[i].apivalue;
                                        $scope['state' + $scope.statenum]["out_api_details"].apires[0] = $scope.obj;
                                    }
                                }
                            }
                        }
                    }
                    $scope.focusval = function(val) {
                        $scope.prevsval = val;
                    }
                    $scope.addapivalue = function(prop, val, i) {
                        if (val !== $scope.prevsval) {
                            var keys = prop.split("&");
                            var subprop = keys[1];
                            if (typeof($scope.apires) == 'object' && !Array.isArray($scope.apires)) {
                                if (typeof($scope.apires[keys[0]]) == 'object' && !Array.isArray($scope.apires[keys[0]])) {
                                    $scope['state' + $scope.statenum]["ent_api_details"].apires[0]['"' + prop + '"'] = val;
                                } else if (Array.isArray($scope.apires[keys[0]])) {
                                    $scope['state' + $scope.statenum]["ent_api_details"].apires[0][keys[0]][0][subprop] = val;
                                } else {
                                    $scope['state' + $scope.statenum]["ent_api_details"].apires[0][prop] = val;
                                }
                            } else if (Array.isArray($scope.apires)) {
                                if (typeof($scope.apires[0][keys[0]]) == 'object' && !Array.isArray($scope.apires[0][keys[0]])) {
                                    $scope['state' + $scope.statenum]["ent_api_details"].apires[0]['"' + prop + '"'] = val;
                                } else if (Array.isArray($scope.apires[0][keys[0]])) {
                                    $scope['state' + $scope.statenum]["ent_api_details"].apires[0][keys[0]][0][subprop] = val;
                                } else {
                                    $scope['state' + $scope.statenum]["ent_api_details"].apires[0][prop] = val;
                                }
                            }
                        }
                    }
                    $scope.focusintval = function(val) {
                        $scope.prevsintval = val;
                    }
                    $scope.addapiintvalue = function(prop, val, i) {
                        if (val !== $scope.prevsintval) {
                            var keys = prop.split("&");
                            var subprop = keys[1];
                            if (typeof($scope.apiintres) == 'object' && !Array.isArray($scope.apiintres)) {
                                if (typeof($scope.apiintres[keys[0]]) == 'object' && !Array.isArray($scope.apiintres[keys[0]])) {
                                    $scope['state' + $scope.statenum]["out_api_details"].apires[0]['"' + prop + '"'] = val;
                                } else if (Array.isArray($scope.apiintres[keys[0]])) {
                                    $scope['state' + $scope.statenum]["out_api_details"].apires[0][prop][0][subprop] = val;
                                } else {
                                    $scope['state' + $scope.statenum]["out_api_details"].apires[0][prop] = val;
                                }
                            } else if (Array.isArray($scope.apiintres)) {
                                if (typeof($scope.apiintres[0][keys[0]]) == 'object' && !Array.isArray($scope.apiintres[0][keys[0]])) {
                                    $scope['state' + $scope.statenum]["out_api_details"].apires[0]['"' + prop + '"'] = val;
                                } else if (Array.isArray($scope.apiintres[0][keys[0]])) {
                                    $scope['state' + $scope.statenum]["out_api_details"].apires[0][prop][0][subprop] = val;
                                } else {
                                    $scope['state' + $scope.statenum]["out_api_details"].apires[0][prop] = val;
                                }
                            }
                        }
                    }
                    $scope.addapikeyvalues = function(statenum) {
                        var apivalues = $scope.apivalues.filter(e => e !== $scope['state' + statenum].state);
                        var obj = {
                            mainkey: "",
                            subkey: "",
                            apivalue: ""
                        };
                        $scope.apikeys.push(obj);
                    };
                    $scope.addapiintkeyvalues = function(statenum) {
                        // var apivalues = $scope.apivalues.filter(e => e !== $scope['state' + statenum].state + '.ans');
                        var obj = {
                            mainkey: "",
                            subkey: "",
                            apivalue: ""
                        };
                        $scope.apiintkeys.push(obj);
                    };
                    $scope.rmapikeyvalues = function(statenum, keysindex) {
                        var key = "";
                        if ($scope.apikeys[keysindex].subkey == undefined || $scope.apikeys[keysindex].subkey == "") {
                            key = $scope.apikeys[keysindex].mainkey;
                        } else {
                            key = $scope.apikeys[keysindex].mainkey + '&' + $scope.apikeys[keysindex].subkey;
                        }
                        delete $scope['state' + statenum]["ent_api_details"].apires[0][key];
                        $scope.apikeys.splice(keysindex, 1);
                    };
                    $scope.rmapiintkeyvalues = function(statenum, keysindex) {
                        var key = "";
                        if ($scope.apiintkeys[keysindex].subkey == undefined || $scope.apiintkeys[keysindex].subkey == "") {
                            key = $scope.apiintkeys[keysindex].mainkey;
                        } else {
                            key = $scope.apiintkeys[keysindex].mainkey + '&' + $scope.apiintkeys[keysindex].subkey;
                        }
                        delete $scope['state' + statenum]["out_api_details"].apires[0][key];
                        $scope.apiintkeys.splice(keysindex, 1);
                    };
                    $scope.backtomenu = function() {
                            $scope.msgmenu = true;
                            $scope.botmsgsett = false;
                            $scope.usrresptimeout = false;
                            $scope.apiintg = false;
                            $scope.condjump = false;
                            $scope.urlparams = false;
                        }
                        // api code starts
                    if ($scope['state' + $scope.statenum].entry_response == true) {
                        if ($scope['state' + $scope.statenum]["ent_api_details"] == undefined) {
                            var apivalues = $scope.apivalues.filter(e => e !== $scope['state' + $scope.statenum].state);
                            var api_obj = {
                                "method": "get",
                                "url": "",
                                "params": [],
                                "body": [],
                                "headerparams": [],
                                "auth": {},
                                "showparams": false,
                                "body_type": "raw",
                                "raw_json": JSON.stringify({}),
                                "api_name": "",
                                "apires": [{}],
                                "api_id": "",
                                "failure_state": ""
                            }
                            $scope['state' + $scope.statenum]["ent_api_details"] = api_obj;
                            $scope.entry_response_msg = "";

                            $http.defaults.headers.common['user'] = $window.sessionStorage.id;
                            $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
                            $scope.apilist = [];
                            $scope.apikeys = [{ mainkey: "", subkey: "", apivalue: "" }];
                            $http.get('/getapis', {}).then(function(res) {
                                for (var i = 0; i < res.data.length; i++) {
                                    $scope.apilist[i] = {
                                        api_id: res.data[i]._id,
                                        api_name: (res.data[i].ApiName == undefined) ? res.data[i].api_info.url : res.data[i].ApiName,
                                        method: res.data[i].api_info.method,
                                        params: res.data[i].api_info.params,
                                        body: res.data[i].api_info.body,
                                        headerparams: res.data[i].api_info.headerparams,
                                        showparams: res.data[i].api_info.showparams,
                                        url: res.data[i].api_info.url,
                                        body_type: res.data[i].api_info.body_type,
                                        raw_json: res.data[i].api_info.raw_json
                                    };
                                }
                                $scope.getapires($scope.statenum, $scope['state' + $scope.statenum].api_name);
                            });
                        } else {
                            if ($scope.apilist.length != 0) {
                                $scope.getapires($scope.statenum, $scope['state' + $scope.statenum].api_name);
                            } else {
                                $http.defaults.headers.common['user'] = $window.sessionStorage.id;
                                $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
                                $scope.apilist = [];
                                $scope.apikeys = [{ mainkey: "", subkey: "", apivalue: "" }];
                                $http.get('/getapis', {}).then(function(res) {
                                    for (var i = 0; i < res.data.length; i++) {
                                        $scope.apilist[i] = {
                                            api_id: res.data[i]._id,
                                            api_name: (res.data[i].ApiName == undefined) ? res.data[i].api_info.url : res.data[i].ApiName,
                                            method: res.data[i].api_info.method,
                                            params: res.data[i].api_info.params,
                                            body: res.data[i].api_info.body,
                                            headerparams: res.data[i].api_info.headerparams,
                                            showparams: res.data[i].api_info.showparams,
                                            url: res.data[i].api_info.url,
                                            body_type: res.data[i].api_info.body_type,
                                            raw_json: res.data[i].api_info.raw_json
                                        };
                                    }
                                    $scope.getapires($scope.statenum, $scope['state' + $scope.statenum].api_name);
                                });
                            }
                        }
                    } else {
                        $scope.entry_response_msg = "Take Gambit Entry Api Response";
                    }

                    if ($scope['state' + $scope.statenum].send_res == true) {
                        if ($scope['state' + $scope.statenum]["out_api_details"] == undefined) {
                            // var apivalues = $scope.apivalues.filter(e => e !== $scope['state' + $scope.statenum].state + '.ans');
                            var api_obj = {
                                "method": "get",
                                "url": "",
                                "params": [],
                                "body": [],
                                "headerparams": [],
                                "auth": {},
                                "showparams": false,
                                "body_type": "raw",
                                "raw_json": JSON.stringify({}),
                                // "apikeys": [{ mainkeys: [], subkeys: [], mainkey: "", subkey: "", apivalues: $scope.apivalues, apivalue: "", apires: [] }],
                                "api_name": "",
                                // "apiintlist": [],
                                "apires": [{}],
                                "api_url": "",
                                "api_id": ""
                            }
                            $scope['state' + $scope.statenum]["out_api_details"] = api_obj;
                            $http.defaults.headers.common['user'] = $window.sessionStorage.id;
                            $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
                            $scope.apiintlist = [];
                            $scope.apiintkeys = [{ mainkey: "", subkey: "", apivalue: "" }];
                            $http.get('/getapis', {}).then(function(res) {
                                for (var i = 0; i < res.data.length; i++) {
                                    $scope.apiintlist[i] = {
                                        api_id: res.data[i]._id,
                                        api_name: (res.data[i].ApiName == undefined) ? res.data[i].api_info.url : res.data[i].ApiName,
                                        method: res.data[i].api_info.method,
                                        params: res.data[i].api_info.params,
                                        body: res.data[i].api_info.body,
                                        headerparams: res.data[i].api_info.headerparams,
                                        showparams: res.data[i].api_info.showparams,
                                        url: res.data[i].api_info.url,
                                        body_type: res.data[i].api_info.body_type,
                                        raw_json: res.data[i].api_info.raw_json
                                    };
                                }
                                $scope.getapiintres($scope.statenum, $scope['state' + $scope.statenum]["out_api_details"].api_url);
                            });
                        } else {
                            if ($scope.apiintlist.length != 0) {
                                $scope.getapiintres($scope.statenum, $scope['state' + $scope.statenum]["out_api_details"].api_url);
                            } else {
                                $http.defaults.headers.common['user'] = $window.sessionStorage.id;
                                $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
                                $scope.apiintlist = [];
                                $scope.apiintkeys = [{ mainkey: "", subkey: "", apivalue: "" }];
                                $http.get('/getapis', {}).then(function(res) {
                                    for (var i = 0; i < res.data.length; i++) {
                                        $scope.apiintlist[i] = {
                                            api_id: res.data[i]._id,
                                            api_name: (res.data[i].ApiName == undefined) ? res.data[i].api_info.url : res.data[i].ApiName,
                                            method: res.data[i].api_info.method,
                                            params: res.data[i].api_info.params,
                                            body: res.data[i].api_info.body,
                                            headerparams: res.data[i].api_info.headerparams,
                                            showparams: res.data[i].api_info.showparams,
                                            url: res.data[i].api_info.url,
                                            body_type: res.data[i].api_info.body_type,
                                            raw_json: res.data[i].api_info.raw_json
                                        };
                                    }
                                    $scope.getapiintres($scope.statenum, $scope['state' + $scope.statenum]["out_api_details"].api_url);
                                });
                            }

                        }
                    }
                    $scope.body_type = function(post_body_type) {
                        // // console.log(post_body_type);
                    }
                    $scope.body_type_int = function(post_body_type) {
                        // console.log(post_body_type);
                    }
                    $scope.post_body_type = "raw";

                    $scope.formatting = { color: 'green', 'background-color': '#d0e9c6' };
                    $scope.message = {};

                    $scope.isValid = true;

                    $scope.script = JSON.stringify($scope.message);

                    $scope.updateJsonObject = function() {
                        try {
                            JSON.parse($scope['state' + $scope.statenum]['ent_api_details'].raw_json);
                        } catch (e) {
                            $scope.isValid = false;
                            $scope.formatting = { color: 'red', 'background-color': '#f2dede' };
                        }

                        $scope.message = JSON.parse($scope['state' + $scope.statenum]['ent_api_details'].raw_json);
                        $scope.isValid = true;
                        $scope.formatting = { color: 'green', 'background-color': '#d0e9c6' };
                    };
                    $scope.updateapiJsonObject = function() {
                        try {
                            JSON.parse($scope['state' + $scope.statenum]['out_api_details'].raw_json);
                        } catch (e) {
                            $scope.isValid = false;
                            $scope.formatting = { color: 'red', 'background-color': '#f2dede' };
                        }

                        $scope.message = JSON.parse($scope['state' + $scope.statenum]['out_api_details'].raw_json);
                        $scope.isValid = true;
                        $scope.formatting = { color: 'green', 'background-color': '#d0e9c6' };
                    };


                    // api code ends
                    // suggestion list
                    $scope.suggestionList = function(suggests) {

                        if (suggests === 'Days of Week') {
                            $scope['state' + $scope.statenum].exp_in_mess.suggestion_box = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
                        }

                        if (suggests === 'Months of Year') {
                            $scope['state' + $scope.statenum].exp_in_mess.suggestion_box = ["January", "Febrauary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                        }
                        if (suggests === 'Custom List') {
                            $scope['state' + $scope.statenum].exp_in_mess.suggestion_box = [];
                        }
                    };
                    //gambit template default
                    $scope.gambittemplate = function(value, statenum) {

                            ids = [];
                            if (value == "form") {
                                if ($scope['state' + statenum]['forms'] == undefined) {
                                    $scope['state' + statenum]['forms'] = [];
                                }
                                $scope.fields = {
                                    "header": "Form 1",
                                    fields: [{
                                        type: 'text',
                                        name: 'Name',
                                        placeholder: 'Please enter your name',
                                        order: 1
                                    }]
                                };

                                $scope['state' + statenum]['forms'].push($scope.fields);
                                $scope['state' + statenum].forms[0].heading = "Form 1";
                            }
                        }
                        // state delete code
                    $scope.delete_gambit = function(targetBoxId, ev) {
                            $modal.open({
                                // locals: { gambit_init_mess: $scope.gambit_init_mess },
                                templateUrl: 'confirm.html',
                                controller: function($scope) {
                                    $scope.confirm = function() {
                                        $scope.close();
                                        jsPlumb.detachAllConnections(targetBoxId);
                                        jsPlumb.removeAllEndpoints(targetBoxId);
                                        jsPlumb.detach(targetBoxId);
                                        $('#' + targetBoxId).remove();
                                        $scope.ok();
                                        $scope.final_obj.find(function(item, i) {
                                            if (item.state_name === targetBoxId) {
                                                $scope.final_obj.splice(i, 1);
                                            }
                                        });
                                    };
                                    $scope.close = function() {
                                        $scope.$close();
                                        $('.modal-backdrop').remove()
                                        $(document.body).removeClass("modal-open");
                                    }
                                },
                                scope: $scope,
                                controllerAs: 'ctrl',
                                targetEvent: ev,
                                size: 'sm'
                            });

                        }
                        // multi conn code starts
                    $scope.mul_conn = function(statenum) {

                        var ep = jsPlumb.selectEndpoints({ scope: 'state' + statenum }).getParameter("maxConnections");

                        if ($scope['state' + statenum].multiple_conn == true) {
                            var ep = jsPlumb.selectEndpoints({ target: 'state' + statenum }).getParameter("maxConnections");

                            jsPlumb.deleteEndpoint(ep[0][1]);

                            var commons = {
                                isTarget: true,
                                maxConnections: -1,
                                connector: ["Straight"],
                                endpoint: "Rectangle",
                                paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "blue" }
                            };
                            jsPlumb.addEndpoint('state' + statenum, {
                                anchors: ["Top"]
                            }, commons);
                        } else {
                            var ep = jsPlumb.selectEndpoints({ target: 'state' + statenum }).getParameters();
                            jsPlumb.deleteEndpoint(ep[0][1]);

                            var commons = {
                                isTarget: true,
                                maxConnections: 1,
                                connector: ["Straight"],
                                endpoint: "Rectangle",
                                paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "blue" }
                            };
                            jsPlumb.addEndpoint('state' + statenum, {
                                anchors: ["Top"]
                            }, commons);
                        }
                    }
                    $scope.change = function(statenum, option) {

                            if (option == "Text") {
                                $scope.suggest_branches = false;
                                $scope.exp_in_mess = {
                                    inputType: "Custom Text",
                                    keyType: "Normal Keyboard",
                                    capitalizeText: "Disabled",
                                    inputValidation: "No Validation",
                                    min_date: "",
                                    max_date: "",
                                    time: "",
                                    text_opt_Input: $scope.text_opt_Input,
                                    text_mul_bub_Input: $scope.text_mul_bub_Input
                                }
                                $scope['state' + statenum].exp_in_mess = $scope.exp_in_mess;
                            } else if (option == "Suggestion") {
                                $scope.suggest_branches = true;
                                $scope.exp_in_mess = {
                                    inputType: "Custom Text",
                                    suggestions: 'Custom List',
                                    suggestion_box: [],
                                    suggest_preview_list: $scope.suggest_preview_list,
                                    suggest_opt_input: $scope.suggest_opt_input,
                                    suggest_mul_bub_Input: $scope.suggest_mul_bub_Input,
                                    suggest_branches: $scope.suggest_branches
                                }
                                $scope['state' + statenum].exp_in_mess = $scope.exp_in_mess;
                            } else if (option == "Carousel") {

                                $scope.suggest_branches = false;
                                var carouselprop = {
                                    "title": "",
                                    "sub_title": "",
                                    "small_desc": "",
                                    "img_form": "url",
                                    "image": "",
                                    "img_name": "",
                                    "img_url": "",
                                    "title2": "",
                                    "sub_title2": "",
                                    "small_desc2": "",
                                    "button_text": "",
                                    "bt_act": "",
                                    "bt_payload": "",
                                    "bt_link": "",
                                    "footer": ""
                                };
                                $scope.exp_in_mess = {
                                    carousel: [carouselprop]
                                }
                                $scope['state' + statenum].exp_in_mess = $scope.exp_in_mess;

                            } else {
                                $scope.suggest_branches = false;

                            }
                        }
                        // multi conn code ends
                        // multi lingual code start
                    $scope.enable_multi_language = function() {
                        if ($scope.check.multi == true) {
                            $scope.onLoad1();
                            $scope['state' + $scope.statenum].enable_multilang = true;
                        } else if ($scope.check.multi == false) {
                            $scope.checkboxClickHandler();
                            $scope['state' + $scope.statenum].enable_multilang = false;
                        }
                    }

                    $scope.check = { multi: false };
                    $scope.add_to_ids = function() {
                        if (transliterationControl.isTransliterationEnabled() == true) {
                            var current_element_id = document.activeElement.id;
                            var index_of_elem = ids.indexOf(current_element_id);


                            if (index_of_elem == -1) {

                                ids.push(current_element_id);

                                transliterationControl.makeTransliteratable(ids);
                                $scope.languageChangeHandler();
                            } else {
                                // if ($scope.check == true) {
                                // ids.push(current_element_id);

                                transliterationControl.makeTransliteratable(ids);
                                $scope.languageChangeHandler();
                                // }
                            }
                        } else {}


                    }
                    var transliterationControl;
                    var options = {
                        sourceLanguage: 'en',
                        destinationLanguage: ['ar', 'hi', 'kn', 'ml', 'ta', 'te'],
                        transliterationEnabled: false,
                        shortcutKey: 'ctrl+g'
                    };
                    transliterationControl =
                        new google.elements.transliteration.TransliterationControl(options);
                    // transliterationControl.disableTransliteration()
                    $scope.onLoad1 = function() {

                        var options = {
                            sourceLanguage: 'en',
                            destinationLanguage: ['ar', 'hi', 'kn', 'ml', 'ta', 'te'],
                            transliterationEnabled: true,
                            shortcutKey: 'ctrl+g'
                        };

                        // Create an instance on TransliterationControl with the required
                        // options.
                        transliterationControl =
                            new google.elements.transliteration.TransliterationControl(options);

                        // Enable transliteration in the textfields with the given ids.

                        // transliterationControl.makeTransliteratable(ids);

                        // Add the STATE_CHANGED event handler to correcly maintain the state
                        // of the checkbox.
                        transliterationControl.addEventListener(
                            google.elements.transliteration.TransliterationControl.EventType.STATE_CHANGED,
                            $scope.transliterateStateChangeHandler);

                        // Add the SERVER_UNREACHABLE event handler to display an error message
                        // if unable to reach the server.
                        transliterationControl.addEventListener(
                            google.elements.transliteration.TransliterationControl.EventType.SERVER_UNREACHABLE,
                            serverUnreachableHandler);

                        // Add the SERVER_REACHABLE event handler to remove the error message
                        // once the server becomes reachable.
                        transliterationControl.addEventListener(
                            google.elements.transliteration.TransliterationControl.EventType.SERVER_REACHABLE,
                            serverReachableHandler);

                        // Set the checkbox to the correct state.
                        // document.getElementById('checkboxId').checked = transliterationControl.isTransliterationEnabled();

                        // Populate the language dropdown
                        var destinationLanguage =
                            transliterationControl.getLanguagePair().destinationLanguage;
                        var languageSelect = document.getElementById('languageDropDown');
                        var supportedDestinationLanguages =
                            google.elements.transliteration.getDestinationLanguages(
                                google.elements.transliteration.LanguageCode.ENGLISH);
                        for (var lang in supportedDestinationLanguages) {
                            var opt = document.createElement('option');
                            opt.text = lang;
                            opt.value = supportedDestinationLanguages[lang];
                            if ($scope.check.multi) {
                                if (destinationLanguage == opt.value) {
                                    opt.selected = true;
                                }
                            }

                            try {
                                languageSelect.add(opt, null);
                            } catch (ex) {
                                languageSelect.add(opt);
                            }
                        }
                    }

                    // Handler for STATE_CHANGED event which makes sure checkbox status
                    // reflects the transliteration enabled or disabled status.
                    $scope.transliterateStateChangeHandler = function(e) {
                        document.getElementById('checkboxId').checked = e.transliterationEnabled;
                    }

                    // Handler for checkbox's click event.  Calls toggleTransliteration to toggle
                    $scope.checkboxClickHandler = function() {
                        transliterationControl.toggleTransliteration();
                    }

                    // Handler for dropdown option change event.  Calls setLanguagePair to
                    // set the new language.
                    $scope.languageChangeHandler = function() {
                            var dropdown = document.getElementById('languageDropDown');
                            transliterationControl.setLanguagePair(
                                google.elements.transliteration.LanguageCode.ENGLISH,
                                dropdown.options[dropdown.selectedIndex].value);
                            $scope['state' + $scope.statenum].language = dropdown.options[dropdown.selectedIndex].value;
                        }
                        // SERVER_UNREACHABLE event handler which displays the error message.
                    function serverUnreachableHandler(e) {
                        document.getElementById("errorDiv").innerHTML =
                            "Transliteration Server unreachable";
                    }
                    // SERVER_UNREACHABLE event handler which clears the error message.
                    function serverReachableHandler(e) {
                        document.getElementById("errorDiv").innerHTML = "";
                    }
                    // multi lingual code ends
                    // message setting code start
                    $scope.show_option = function(e) {
                        var ind = e.currentTarget.dataset.value;
                        $scope['state' + $scope.statenum].gambit_init_mess[ind]["ellipsis" + ind] = true;
                        $scope['state' + $scope.statenum].gambit_init_mess[ind]["plus" + ind] = true;
                    }

                    $scope.hide_option = function(e) {
                        var ind = e.currentTarget.dataset.value;
                        $scope['state' + $scope.statenum].gambit_init_mess[ind]["ellipsis" + ind] = false;
                        $scope['state' + $scope.statenum].gambit_init_mess[ind]["picture" + ind] = false;
                        $scope['state' + $scope.statenum].gambit_init_mess[ind]["times" + ind] = false;
                        $scope['state' + $scope.statenum].gambit_init_mess[ind]["plus" + ind] = false;
                        $scope['state' + $scope.statenum].gambit_init_mess[ind]["text" + ind] = false;
                        $scope['state' + $scope.statenum].gambit_init_mess[ind]["video" + ind] = false;
                    }
                    $scope.apply_line = function() {
                        document.execCommand('underline');
                    }
                    $scope.apply_italic = function() {
                        document.execCommand('italic');
                    }

                    $scope.apply_bold = function(e) {
                        document.execCommand('bold');
                    }
                    $scope.apply_href = function() {}
                    $scope.apply_h = function() {}
                    $scope.tbubble = "12px";
                    $scope.toppos = "39px";
                    $scope.getSelectionText = function(event, index, type) {
                        var text = "";
                        if (window.getSelection) {
                            text = window.getSelection().toString();
                        } else if (document.selection && document.selection.type != "Control") {
                            text = document.selection.createRange().text;
                        }
                        if (text.length > 0) {
                            document.getElementById("state_tool" + index).style.display = "block"
                            $scope.tbubble = event.offsetX + "px";
                            $scope.toppos = (type == "image") ? '99px' : '39px';
                        } else {
                            document.getElementById("state_tool" + index).style.display = "none";
                            $scope.display_tool = "none";
                        }
                    }
                    $scope.showButtonMenu = function(statenum, e, index, parentidx) {
                        var indexValue = e.currentTarget.dataset.value;
                        $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["ellipsis" + indexValue] = true;
                        $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["plus" + indexValue] = true;
                    };

                    $scope.hideButtonMenu = function(statenum, e, index, parentidx) {
                        var indexValue = e.currentTarget.dataset.value;
                        $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["ellipsis" + indexValue] = false;
                        $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["plus" + indexValue] = false;
                        $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["external-link" + indexValue] = false;
                        $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["phone" + indexValue] = false;
                        $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["text" + indexValue] = false;
                        $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["times" + indexValue] = false;
                    };
                    $scope.addOption = function(statenum, index) {
                        $scope.buttonindex = $scope.buttonindex + 1;
                        $scope.buttonobj = {};
                        $scope.buttonobj["button_inputFields"] = { "url": false, "phone": false, "payload": false };
                        $scope.buttonobj["button_ellipsis" + $scope.buttonindex] = false;
                        $scope.buttonobj["button_plus" + $scope.buttonindex] = false;
                        $scope.buttonobj["button_external-link" + $scope.buttonindex] = false;
                        $scope.buttonobj["button_phone" + $scope.buttonindex] = false;
                        $scope.buttonobj["button_text" + $scope.buttonindex] = false;
                        $scope.buttonobj["button_times" + $scope.buttonindex] = false;
                        $scope['state' + statenum].gambit_init_mess[index]["button_states"].push($scope.buttonobj);

                        var button_value = { "url": "", "phone": "", "payload": "", "title": "Option Text", "type": "" };

                        $scope['state' + statenum].gambit_init_mess[index].message.buttons.push(button_value);
                    }
                    $scope.ShowButtonIcons = function(statenum, e, index, parentidx) {
                        var indexValue = e.currentTarget.dataset.value;

                        if ($scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["phone" + indexValue] == true) {
                            $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["external-link" + indexValue] = false;
                            $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["phone" + indexValue] = false;
                            $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["text" + indexValue] = false;
                            $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["times" + indexValue] = false;
                        } else {
                            $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["external-link" + indexValue] = true;
                            $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["phone" + indexValue] = true;
                            $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["text" + indexValue] = true;
                            $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["times" + indexValue] = true;
                        }
                    }
                    $scope.externalLink = function(statenum, indexValue, index) {

                        $scope['state' + statenum].gambit_init_mess[index].message.buttons[indexValue].type = "externalLink";
                        $scope['state' + statenum].gambit_init_mess[index].button_states[indexValue].button_inputFields.url = true;
                        $scope['state' + statenum].gambit_init_mess[index].button_states[indexValue].button_inputFields.phone = false;
                        $scope['state' + statenum].gambit_init_mess[index].button_states[indexValue].button_inputFields.payload = false;
                    }

                    $scope.phoneContact = function(statenum, indexValue, index) {

                        $scope['state' + statenum].gambit_init_mess[index].message.buttons[indexValue].type = "phone";
                        $scope.maxlength = "13";
                        $scope['state' + statenum].gambit_init_mess[index].button_states[indexValue].button_inputFields.url = false;
                        $scope['state' + statenum].gambit_init_mess[index].button_states[indexValue].button_inputFields.phone = true;
                        $scope['state' + statenum].gambit_init_mess[index].button_states[indexValue].button_inputFields.payload = false;
                    }

                    $scope.payloadMessage = function(statenum, indexValue, index) {

                        $scope['state' + statenum].gambit_init_mess[index].message.buttons[indexValue].type = "payload";
                        $scope['state' + statenum].gambit_init_mess[index].button_states[indexValue].button_inputFields.url = false;
                        $scope['state' + statenum].gambit_init_mess[index].button_states[indexValue].button_inputFields.payload = true;
                        $scope['state' + statenum].gambit_init_mess[index].button_states[indexValue].button_inputFields.phone = false;
                    }

                    $scope.removeOption = function(statenum, indexValue, index) {
                        $scope['state' + statenum].gambit_init_mess[index].message.buttons.splice(indexValue, 1);
                        $scope['state' + statenum].gambit_init_mess[index].button_states.splice(indexValue, 1);
                    }
                    $scope.enable_text_input = function(statenum, ind) {
                        $scope['state' + statenum].gambit_init_mess[ind].input_fields.text = true;
                        $scope['state' + statenum].gambit_init_mess[ind].input_fields.image = false;
                        $scope['state' + statenum].gambit_init_mess[ind].input_fields.video = false;
                        $scope['state' + statenum].gambit_init_mess[ind].message.mess_type = "text";
                    }
                    $scope.enable_video = function(statenum, ind) {
                        $scope['state' + statenum].gambit_init_mess[ind].input_fields.text = false;
                        $scope['state' + statenum].gambit_init_mess[ind].input_fields.video = true;
                        $scope['state' + statenum].gambit_init_mess[ind].input_fields.image = false;
                    }
                    $scope.enable_image_input = function(statenum, ind) {
                            $scope['state' + statenum].gambit_init_mess[ind].input_fields.text = false;
                            $scope['state' + statenum].gambit_init_mess[ind].input_fields.image = true;
                            $scope['state' + statenum].gambit_init_mess[ind].input_fields.video = false;
                            $scope['state' + statenum].gambit_init_mess[ind].message.mess_type = "media";
                            $scope['state' + statenum].gambit_init_mess[ind].message.media_type = "image";
                        }
                        // message setting code end
                        // carousel code start
                    $scope.templates = [
                        "Templateone"
                    ]
                    $scope.clear_image_type = function(statenum, ind) {
                            if ($scope['state' + statenum].exp_in_mess.carousel[ind].img_form == 'url') {
                                $scope['state' + statenum].exp_in_mess.carousel[ind].image = "";
                                $scope['state' + statenum].exp_in_mess.carousel[ind].img_name = "";
                            } else if ($scope['state' + statenum].exp_in_mess.carousel[ind].img_form == 'upload') {
                                $scope['state' + statenum].exp_in_mess.carousel[ind].img_url = "";
                            }
                        }
                        // carousel code end
                        // state save
                    $scope.crtstateobj = function(statename, statenum) {
                        ids = [];
                        var endpoints = jsPlumb.getEndpoints(statename);
                        if ($scope[statename].state) {
                            $('#' + statename + ' .title').text($scope[statename].state + '[' + statenum + ']');
                        }
                        if ($scope[statename].exp_in_mess != undefined && $scope[statename].exp_in_mess.carousel != undefined) {
                            if ($scope[statename].entry_response == true || $scope[statename].entry_response == "true") {
                                var carousel = $scope[statename].exp_in_mess.carousel[0];
                                $scope[statename].exp_in_mess.carousel = [{}];
                                for (var key in carousel) {
                                    if (carousel[key] != "") {
                                        $scope[statename].exp_in_mess.carousel[0][key] = "";
                                        $scope[statename].exp_in_mess.carousel[0][key] = carousel[key];
                                        if ($scope[statename].exp_in_mess.carousel[0].image != undefined && $scope[statename].exp_in_mess.carousel[0].image['$ngfBlobUrl'] !== undefined) {
                                            delete $scope[statename].exp_in_mess.carousel[0].image['$ngfBlobUrl'];
                                        }
                                    }
                                }

                            } else {
                                for (var i = 0; i < $scope[statename].exp_in_mess.carousel.length; i++) {
                                    if ($scope[statename].exp_in_mess.carousel[i].image != undefined && $scope[statename].exp_in_mess.carousel[i].image['$ngfBlobUrl'] !== undefined) {
                                        delete $scope[statename].exp_in_mess.carousel[i].image['$ngfBlobUrl'];
                                    }
                                }
                            }
                        }

                        if ($scope.final_obj.length == 0) {
                            $scope[statename].out_mess = [];
                            for (var i = 0; i < $scope[statename].gambit_init_mess.length; i++) {
                                if ($scope[statename].gambit_init_mess[i].message.image != undefined && $scope[statename].gambit_init_mess[i].message.image['$ngfBlobUrl'] !== undefined) {
                                    delete $scope[statename].gambit_init_mess[i].message.image['$ngfBlobUrl'];
                                }
                                $scope[statename].out_mess.push($scope[statename].gambit_init_mess[i].message);
                            }

                            $scope.final_obj.push($scope[statename]);
                            $scope.final_obj.sort(function(a, b) {
                                var nameA = a.state_name.substr(5),
                                    nameB = b.state_name.substr(5)
                                return nameA - nameB
                            });
                            $scope.ok();
                            console.log($scope.final_obj, "final");
                        } else {
                            for (var i = 0; i < $scope.final_obj.length; i++) {

                                if ($scope.final_obj[i].state_name == statename) {
                                    $scope.final_obj.splice(i, 1);
                                    $scope[statename].out_mess = [];
                                    for (var i = 0; i < $scope[statename].gambit_init_mess.length; i++) {
                                        if ($scope[statename].gambit_init_mess[i].message.image != undefined && $scope[statename].gambit_init_mess[i].message.image['$ngfBlobUrl'] !== undefined) {
                                            delete $scope[statename].gambit_init_mess[i].message.image['$ngfBlobUrl'];
                                        }
                                        $scope[statename].out_mess.push($scope[statename].gambit_init_mess[i].message);
                                    }
                                    $scope.final_obj.push($scope[statename]);
                                    $scope.final_obj.sort(function(a, b) {
                                        var nameA = a.state_name.substr(5),
                                            nameB = b.state_name.substr(5)
                                        return nameA - nameB
                                    });
                                    $scope.ok();
                                    console.log($scope.final_obj, "final1");
                                    break;
                                } else {
                                    if (i == $scope.final_obj.length - 1) {
                                        $scope[statename].out_mess = [];
                                        for (var i = 0; i < $scope[statename].gambit_init_mess.length; i++) {
                                            if ($scope[statename].gambit_init_mess[i].message.image != undefined && $scope[statename].gambit_init_mess[i].message.image['$ngfBlobUrl'] !== undefined) {
                                                delete $scope[statename].gambit_init_mess[i].message.image['$ngfBlobUrl'];
                                            }
                                            $scope[statename].out_mess.push($scope[statename].gambit_init_mess[i].message);
                                        }
                                        $scope.final_obj.push($scope[statename]);
                                        $scope.final_obj.sort(function(a, b) {
                                            var nameA = a.state_name.substr(5),
                                                nameB = b.state_name.substr(5)
                                            return nameA - nameB
                                        });
                                        console.log($scope.final_obj, "final2");
                                    }
                                }
                            }
                        }
                        if ($scope[statename].input_option == "Suggestion") {
                            if ($scope[statename].exp_in_mess.suggest_branches == true) {
                                var length = $scope[statename].exp_in_mess.suggestion_box.length;
                                for (var k = 0; k < $scope[statename].exp_in_mess.suggestion_box.length; k++) {

                                    var new_sugg = $scope[statename].exp_in_mess.suggestion_box[k].replace('.', '');
                                    $scope[statename].exp_in_mess.suggestion_box[k] = new_sugg;

                                }

                                var endlength = endpoints.length - 1;
                                var newanchor = length - endlength;
                                if (endpoints.length == 2) {
                                    var left_val = 0.6;
                                    var j = 0;
                                } else if (endpoints.length > length) {
                                    for (var i = (endpoints.length - 1); i >= (length + 1); i--) {
                                        var ep_id = endpoints[i];
                                        jsPlumb.deleteEndpoint(ep_id);
                                    }
                                } else {
                                    left_val = 0.6 + ((endlength - 1) * 0.1);
                                    j = 0;
                                }
                                for (var i = 0; i < newanchor; i++) {
                                    var common = {
                                        isSource: true,
                                        maxConnections: 1,
                                        deleteEndpointsOnDetach: false,
                                        connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                                        endpoint: ["Rectangle", { width: 10, height: 8 }],
                                        paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "black" }
                                    };
                                    if (j > 0) {
                                        left_val = left_val + 0.1;
                                    }
                                    j = j + 1;
                                    var state = statename;
                                    if (length > 1) {
                                        var width = document.getElementById(state).offsetWidth;
                                        document.getElementById(state).style.width = width + 10 + "px";
                                    }
                                    var newval = i * 5;
                                    jsPlumb.addEndpoint(state, {
                                        anchor: [
                                            [left_val, 1.07, 1, 1, 1, -4], "Continuous"
                                        ],
                                        maxConnections: 1,
                                        connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                                        endpoint: ["Rectangle", { width: 10, height: 9 }],
                                        paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "black" }
                                        // anchor:[ "Bottom","Bottom"],
                                    }, common);
                                    if (i == newanchor - 1) {
                                        $scope.repaint();
                                        // jsPlumb.recalculateOffsets(state);
                                    }
                                }

                            } else {

                                var endpoints = jsPlumb.getEndpoints(statename);

                                if (endpoints.length > 2) {
                                    for (var i = 2; i < endpoints.length; i++) {
                                        var ep_id = endpoints[i];
                                        jsPlumb.deleteEndpoint(ep_id);
                                    }
                                }
                            }
                        } else if ($scope[statename].input_option != "Suggestion" && $scope[statename].condif == false && $scope[statename].input_option != "Star Rating") {
                            var endpoints = jsPlumb.getEndpoints(statename);

                            if (endpoints.length > 2) {
                                for (var i = 2; i < endpoints.length; i++) {
                                    var ep_id = endpoints[i];
                                    jsPlumb.deleteEndpoint(ep_id);
                                }
                            }
                        } else {


                        }

                        if ($scope[statename].input_option == "Star Rating") {

                            var length = $scope[statename].exp_in_mess.star_rating;

                            var endlength = endpoints.length - 1;
                            var starcount = length - endlength;

                            if (endpoints.length == 2) {
                                var left_val = 0.6;
                                var j = 0;
                            } else if (endpoints.length > length) {
                                var starlength = parseInt(length);

                                for (var i = (endpoints.length - 1); i >= (starlength + 1); i--) {

                                    var ep_id = endpoints[i];
                                    jsPlumb.deleteEndpoint(ep_id);
                                }
                            } else {
                                left_val = 0.6 + ((endlength - 1) * 0.1);
                                j = 0;
                            }

                            for (var i = 0; i < starcount; i++) {
                                var common = {
                                    isSource: true,
                                    maxConnections: 1,
                                    deleteEndpointsOnDetach: false,
                                    connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                                    endpoint: ["Rectangle", { width: 10, height: 8 }],
                                    paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "black" }
                                };
                                if (j > 0) {
                                    left_val = left_val + 0.1;
                                }
                                j = j + 1;
                                var state = statename;
                                if (length > 1) {
                                    var width = document.getElementById(state).offsetWidth;
                                    document.getElementById(state).style.width = width + 20 + "px";
                                }
                                var newval = i * 5;
                                jsPlumb.addEndpoint(state, {
                                    anchor: [
                                        [left_val, 1.07, 1, 1, 1, -4], "Bottom"
                                    ],
                                    // anchor:[ "Bottom","Bottom"],
                                }, common);
                                if (i == starcount - 1) {
                                    $scope.repaint();

                                }
                            }
                        }
                        if ($scope[statename].condif == true) {

                            $scope[statename]['cond_exps'] = [];
                            var exp = "";
                            var inter_exp = "";
                            for (var j = 0; j < $scope[statename].cond_details.A.length; j++) {

                                for (var k = 0; k < $scope[statename].cond_details.A[j].cond.length; k++) {


                                    if ($scope[statename].cond_details.A[j].cond[k].cond == "is") {
                                        inter_exp = "=="
                                    } else if ($scope[statename].cond_details.A[j].cond[k].cond == "is not") {
                                        inter_exp = "!="
                                    } else if ($scope[statename].cond_details.A[j].cond[k].cond == "equals") {
                                        inter_exp = "==="
                                    } else if ($scope[statename].cond_details.A[j].cond[k].cond == "not equals") {
                                        inter_exp = "!="
                                    } else if ($scope[statename].cond_details.A[j].cond[k].cond == "less than") {
                                        inter_exp = "<"
                                    } else if ($scope[statename].cond_details.A[j].cond[k].cond == "greater than") {
                                        inter_exp = ">"
                                    } else {
                                        // string.indexOf(substring) !== -1
                                    }
                                    if (k == 0) {

                                        exp = "(" + $scope[statename].cond_details.A[j].cond[k].field + " " + inter_exp + " '" + $scope[statename].cond_details.A[j].cond[k].value + "' )";
                                    } else {
                                        var cond_exp = "";
                                        if ($scope[statename].cond_details.A[j].cond[k].intercond == "OR") {
                                            cond_exp = "||"
                                        } else {
                                            cond_exp = "&&"
                                        }
                                        exp = exp + " " + cond_exp + "(" + $scope[statename].cond_details.A[j].cond[k].field + " " + inter_exp + "  '" + $scope[statename].cond_details.A[j].cond[k].value + "' )";
                                    }
                                    if (k == $scope[statename].cond_details.A[j].cond.length - 1) {

                                        $scope[statename]['cond_exps'].push(exp)

                                    }

                                }
                            }

                            var length = $scope[statename].cond_details.A.length + 1;
                            var endlength = endpoints.length - 1;
                            var newanchor = length - endlength;
                            if (endpoints.length == 2) {
                                var left_val = 0.6;
                                var j = 0;
                            } else if (endpoints.length > length) {

                                for (var i = (endpoints.length - 1); i >= (length + 1); i--) {


                                    var ep_id = endpoints[i];
                                    jsPlumb.deleteEndpoint(ep_id);
                                }
                            } else {
                                left_val = 0.6 + ((endlength - 1) * 0.1);
                                j = 0;
                            }
                            for (var i = 0; i < newanchor; i++) {
                                var common = {
                                    isSource: true,
                                    connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                                    endpoint: ["Rectangle", { width: 10, height: 8 }],
                                    paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "black" }
                                };
                                if (j > 0) {
                                    left_val = left_val + 0.1;
                                }
                                j = j + 1;
                                var state = statename;
                                if (length > 1) {
                                    var width = document.getElementById(state).offsetWidth;
                                    document.getElementById(state).style.width = width + 10 + "px";
                                }

                                jsPlumb.addEndpoint(state, {
                                    anchor: [
                                        [left_val, 1.07, 1, 1, 1, -4], "Continuous"
                                    ],
                                    maxConnections: 1,
                                    connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                                    endpoint: ["Rectangle", { width: 10, height: 9 }],
                                    paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "black" }
                                }, common);
                                if (i == newanchor - 1) {
                                    // $scope.repaint();
                                    jsPlumb.recalculateOffsets(state);
                                }
                            }
                            //jsPlumb.recalculateOffsets(state)
                        } else {

                        }
                        // // console.log("state:", $scope[statename]);
                    }
                },
                scope: $scope,
                controllerAs: 'ctrl',
                targetEvent: ev,
                size: 'lg',
                resolve: {
                    gambit_init_mess: function() {
                        return $scope.gambit_init_mess;
                    }
                }
            });
        };
        newState.dblclick(function(e) {
            $scope.showAlert(e);
        });

        // $timeout(function() {
        var state = 'state' + i;
        var common = {
            isSource: true,
            maxConnections: 1,
            deleteEndpointsOnDetach: false,
            connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
            endpoint: ["Rectangle", { width: 10, height: 9 }],
            paintStyle: { strokeStyle: "blue", lineWidth: 1, fillStyle: "black" }
        };

        jsPlumb.addEndpoint(state, {
            anchors: ["Bottom", "Continuous"],
        }, common);


        var commons = {
            isTarget: true,
            maxConnections: 1,
            connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
            endpoint: "Rectangle",
            paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "blue" }
        };
        jsPlumb.addEndpoint(state, {
            anchors: ["Top"]
        }, commons);
        i++;
        // }, 1500);
        // $scope.repaint(state);

    }
    $scope.repaint = function(state) {
        // var state = "state0";
        jsPlumb.recalculateOffsets(state)
        var endpoints = jsPlumb.getEndpoints(state);
        jsPlumb.repaintEverything();
        for (var i = 0; i < endpoints.length; i++) {
            if (endpoints[i].isSource == true) {
                endpoints[i].endpoint.bounds.minY = endpoints[i].endpoint.bounds.minY + 56;
            }
        }
    }
    $scope.ellipsis0 = false;
    $scope.plus0 = false;
    $scope.picture0 = false;
    $scope.times0 = false;
    $scope.text0 = false;
    $scope.options = [
        "Text",
        "Suggestion",
        "Carousel",
        "Location",
        "Upload",
        "None",
        "Star Rating"
    ];
    $scope.inter_cond_options = ["AND", "OR"];
    $scope.cond_options = [
        "is",
        "is not",
        "equals",
        "not equals",
        "less than",
        "greater than"
        // "contains",
        // "not contains",
        // " is unknown",
        // "has any value"
    ];

    $scope.inputTypes = [
        "Full Name",
        "Email",
        "Telephone",
        "Date",
        "Time",
        "Date Time",
        "Address",
        "Password",
        "Custom Text"
    ];
    $scope.option = "None";
    //$scope.inputType="Custom Text";
    $scope.remove_message = function(statenum, ind) {

        $scope['state' + statenum].gambit_init_mess.splice(ind, 1);
    }

    $scope.addendpoint = function() {
        var common = {
            isSource: true,
            connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
            endpoint: ["Rectangle", { width: 10, height: 8 }],
            paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "black" }
        };
        if (j > 0) {
            left_val = left_val + 0.2;
        }
        j = j + 1;
        var state = "state0";
        jsPlumb.addEndpoint(state, {
            anchor: [
                [left_val, 1.07, 1, 1, 1, -4], "Bottom"
            ],
            maxConnections: 1,
            connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
            endpoint: ["Rectangle", { width: 10, height: 8 }],
            paintStyle: { strokeStyle: "blue", strokeWidth: 1 }
        }, common);

    }

    $scope.add_timeout_endpoint = function(state) {
        var common = {
            isSource: true,
            connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
            endpoint: ["Rectangle", { width: 10, height: 8 }],
            paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "black" }
        };


        jsPlumb.addEndpoint(state, {
            anchors: ["Left"],
            maxConnections: 1,
            connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
            endpoint: ["Rectangle", { width: 10, height: 8 }],
            paintStyle: { strokeStyle: "blue", strokeWidth: 1 }
        }, common);

    }


    $scope.final_obj = [];
    $scope.state_img = [];


    $scope.images = [];
    $scope.bot_images = [];
    $scope.mess_img = function(file, i, statenum) {
        var fileexts = ['jpg', 'jpeg', 'png', 'gif', 'doc', 'docx', 'svg', 'xls', 'xlsx', 'pdf', '3g2', '3gp', 'mp4', 'mpeg', 'mpg', 'txt', 'ppt', 'pptx', 'aac', 'm4a', 'amr', 'mp3', 'ogg', 'opus'];
        var filename = file.name;
        var n = filename.lastIndexOf('.');
        var ext = filename.substring(n + 1);
        if (fileexts.indexOf(ext) != -1) {
            filename = "state" + statenum + "_mess" + i + "." + ext;
            if ($scope.bot_images.length > 0) {
                var index = 0;
                angular.forEach($scope.bot_images, function(value, key) {
                    if (value.ngfName == filename) {
                        $scope.bot_images.splice(index, 1);
                    }
                    index = index + 1;
                });
            }
            file = Upload.rename(file, filename);
            $scope['state' + statenum].gambit_init_mess[i].message.img_name = filename;
            // $scope['state' + statenum].gambit_init_mess[i].message.image = filename;
            $scope.bot_images.push(file);
        } else {
            $scope.showSimpleToast("Please upload appropiate file type.", "danger");
        }
    }

    $scope.settings_img = function(file, newname, scopevar) {
        var filename = file.name;
        var n = filename.lastIndexOf('.');
        var ext = filename.substring(n + 1);
        filename = newname + "." + ext;
        if ($scope.bot_images.length > 0) {
            var index = 0;
            angular.forEach($scope.bot_images, function(value, key) {
                if (value.ngfName == filename) {

                    $scope.bot_images.splice(index, 1);
                }
                index = index + 1;
            });
        }
        var field = scopevar;
        file = Upload.rename(file, filename);
        $scope[field] = filename;
        $scope.bot_images.push(file);

    }

    $scope.imgfun = function(file, i, statenum) {
        var filename = file.name;
        var n = filename.lastIndexOf('.');
        var ext = filename.substring(n + 1);
        filename = "state" + statenum + "_car" + i + "." + ext;
        if ($scope.images.length > 0) {
            var index = 0;
            angular.forEach($scope.images, function(value, key) {
                if (value.ngfName == filename) {
                    $scope.images.splice(index, 1);
                }
                index = index + 1;
            });
        }

        file = Upload.rename(file, filename);
        $scope['state' + statenum].exp_in_mess.carousel[i].img_name = filename;
        $scope.images.push(file);
    }



    $scope.branches = function(statenum) {
        //$scope['state' + statenum].exp_in_mess.suggest_branches = "false";
    }
    $scope.save_data = function() {
        var db_obj = {
            botId: "BOT001",
            botName: "TEST_BOT",
            botFlow: $scope.final_obj
        }
        for (var i = 0; i < $scope.final_obj; i++) {

        }

    }
    $scope.optionIndexVal = 0;
    $scope.items = [];

    $scope.onLoad = function(e, reader, file, fileList, fileOjects, fileObj) {
        alert('this is handler for file reader onload event!');
    };
    $scope.file = {

    }
    $scope.bandColor = "#cc0000"
    $scope.titleColor = "#ffffff"
    $scope.backgroundColor = "#ffffff"
    $scope.bbubbleColor = "#99ccff"
    $scope.ububbleColor = "#cc99ff"
    $scope.buttonColor = "#cc6699"
    $scope.suggesColor = "#cc6699"
    $scope.fontColor = "#000000"
    $scope.textfieldColor = "#ffffff"
    $scope.buttonhoverColor = "#cc0000"



    $scope.showsettings = function(ev) {
        $modal.open({
            // locals: { gambit_init_mess: $scope.gambit_init_mess },
            templateUrl: 'botsettings.html',
            controller: function($scope) {
                $scope.ok = function() {
                    $scope.$close();
                    $('.modal-backdrop').remove()
                    $(document.body).removeClass("modal-open");
                }

            },
            scope: $scope,
            controllerAs: 'ctrl',
            targetEvent: ev,
            size: 'lg',
            resolve: {
                gambit_init_mess: function() {
                    return $scope.gambit_init_mess;
                }
            }
        });
    }

    $scope.band = { "Color": "#cc0000" };
    $scope.title = { "Color": "#ffffff" };
    $scope.background = { "Color": "#ffffff" };
    $scope.bbubble = { "Color": "#99ccff" };
    $scope.ububble = { "Color": "#cc99ff" };
    $scope.button = { "Color": "#cc6699" };
    $scope.sugges = { "Color": "#cc6699" };
    $scope.font = { "Color": "#000000" };
    $scope.textfield = { "Color": "#ffffff" };
    $scope.buttonhover = { "Color": "#ffffff" };
    $scope.bot = { "Name": "" };
    $scope.bot = { "Description": "" };
    $scope.web = { "link": "" };
    $scope.post = { "email": "" };
    $scope.carouselbackground = { "Color": "#cc6699" };
    $scope.carousel = { "button": "#cc6699" };
    $scope.bot_img = { "logo": "" };
    $scope.web_bg = { "logo": "" };
    $scope.bot_bg = { "logo": "" };
    $scope.tog_normal = { chat: false };
    $scope.tog_brand = { icon: false };
    $scope.tog_whatsapp = { chat: false };
    $scope.user = { phone_number: false };
    $scope.time = { stamp: false };
    $scope.sender = { id: "" };
    $scope.showPrompt = function(ev) {
        // Appending dialog to document.body to cover sidenav in docs app
        if ($scope.bot.Name == undefined) {
            var confirm = prompt("Bot Name", "");
            if (confirm == "") {
                $scope.showSimpleToast("Please enter bot name", "danger");
            } else if (confirm == null) {
                $scope.status = 'You Cancelled';
            } else {
                $scope.bot.Name = confirm;
                $scope.save_and_update(1);
                $scope.status = 'You decided to name your Bot ' + confirm + '.';
            }
        } else if ($scope.botId == undefined) {
            $scope.save_and_update(1);
        } else {

            $scope.save_and_update(2);
        }
    };



    $scope.save_and_update = function(flag) {
        if ($scope.startState == "") {
            $scope.showSimpleToast("Please select the start state!!", "danger");
        } else {
            var db_obj = {
                botId: "",
                botName: $scope.bot.Name,
                titlebarColor: $scope.band.Color,
                botDesc: $scope.bot.Description,
                webLink: $scope.web.link,
                postemail: $scope.post.email,
                tog_brand_icon: $scope.tog_brand.icon,
                tog_normal_chat: $scope.tog_normal.chat,
                tog_whatsapp_chat: $scope.tog_whatsapp.chat,
                titleColor: $scope.title.Color,
                textfieldColor: $scope.textfield.Color,
                buttonhoverColor: $scope.buttonhover.Color,
                backgroundColor: $scope.background.Color,
                bbubbleColor: $scope.bbubble.Color,
                carousel_background: $scope.carouselbackground.Color,
                carousel_button: $scope.carousel.button,
                ububbleColor: $scope.ububble.Color,
                buttonColor: $scope.button.Color,
                suggesColor: $scope.sugges.Color,
                fontColor: $scope.font.Color,
                botImage: $scope.bot_img_logo,
                webImage: $scope.web_bg_logo,
                botBgImage: $scope.bot_bg_logo,
                botFlow: $scope.final_obj,
                startState: $scope.startState,
                userPhoneNumber: $scope.user.phone_number,
                timeStamp: $scope.time.stamp,
                sender_id: $scope.sender.id
            }

            if ($scope.final_obj.length == 0) {
                $scope.showSimpleToast("Please fill the gambits!!", "danger");
            } else {
                for (var i = 0; i < $scope.final_obj.length; i++) {
                    $scope.final_obj[i]['left'] = angular.element('#' + $scope.final_obj[i].state_name).prop('offsetLeft');
                    $scope.final_obj[i]['top'] = angular.element('#' + $scope.final_obj[i].state_name).prop('offsetTop');
                    if (i == $scope.final_obj.length - 1) {
                        $http.defaults.headers.common['user'] = $window.sessionStorage.id;
                        $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
                        if (flag == 1) {
                            $http.post('/savebotflow', db_obj, {
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(function(res) {
                                $scope.botId = res.data.botid
                                $scope.upload($scope.images);
                            })
                        } else {
                            db_obj.botId = $scope.botId;
                            $http.put('/update_bot', db_obj, {
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(function(res) {
                                $scope.botId = res.data.botid
                                $scope.upload($scope.images);
                            })
                        }

                    }
                }
            }
        }
    }
    $scope.save_new = function() {
        $scope.save_and_update(1);
    }
    var last = {
        bottom: false,
        top: true,
        left: false,
        right: true
    };

    $scope.toastPosition = angular.extend({}, last);
    $scope.getToastPosition = function() {
        sanitizePosition();
        return Object.keys($scope.toastPosition)
            .filter(function(pos) { return $scope.toastPosition[pos]; })
            .join(' ');
    };

    function sanitizePosition() {
        var current = $scope.toastPosition;
        if (current.bottom && last.top) current.top = false;
        if (current.top && last.bottom) current.bottom = false;
        if (current.right && last.left) current.left = false;
        if (current.left && last.right) current.right = false;
        last = angular.extend({}, current);
    }

    $scope.showSimpleToast = function(message, clsname) {
        ngToast.create({
            className: clsname,
            content: message,
            dismissOnTimeout: true,
            timeout: 2500,
            dismissButton: true,
            dismissButtonHtml: '&times',
            dismissOnClick: true
        });
    };



    $scope.show_icons = function(e) {
        var ind = e.currentTarget.dataset.value;
        if ($scope['state' + $scope.statenum].gambit_init_mess[ind]["picture" + ind] == true) {
            $scope['state' + $scope.statenum].gambit_init_mess[ind]["picture" + ind] = false;
            $scope['state' + $scope.statenum].gambit_init_mess[ind]["times" + ind] = false;
            $scope['state' + $scope.statenum].gambit_init_mess[ind]["text" + ind] = false;
            $scope['state' + $scope.statenum].gambit_init_mess[ind]["video" + ind] = false;
        } else {
            $scope['state' + $scope.statenum].gambit_init_mess[ind]["picture" + ind] = true;
            $scope['state' + $scope.statenum].gambit_init_mess[ind]["times" + ind] = true;
            $scope['state' + $scope.statenum].gambit_init_mess[ind]["text" + ind] = true;
            $scope['state' + $scope.statenum].gambit_init_mess[ind]["video" + ind] = true;
        }
    }


    $scope.addnewtxt = function() {


        $scope.indexval = $scope.indexval + 1;
        var text = "text" + $scope.indexval;
        $scope.obj = {

        }

        $scope.obj["message"] = { "preview_url": false, "text": "", "img_url": "", "title": "", "sub_title": "", "videoID": "", "img_form": "url", "image": "", "img_name": "", "buttons": [], "mess_type": "text", "media_type": "image", "media_id": "" };
        $scope.obj["input_fields"] = { "text": true, "image": false };
        $scope.obj["ellipsis" + $scope.indexval] = false;
        $scope.obj["plus" + $scope.indexval] = false;
        $scope.obj["picture" + $scope.indexval] = false;
        $scope.obj["times" + $scope.indexval] = false;
        $scope.obj["text" + $scope.indexval] = false;

        $scope.buttonobj["button_inputFields"] = { "url": false, "phone": false, "payload": false };
        $scope.buttonobj["button_ellipsis" + $scope.buttonindex] = false;
        $scope.buttonobj["button_plus" + $scope.buttonindex] = false;
        $scope.buttonobj["button_external-link" + $scope.buttonindex] = false;
        $scope.buttonobj["button_phone" + $scope.buttonindex] = false;
        $scope.buttonobj["button_text" + $scope.buttonindex] = false;
        $scope.buttonobj["button_times" + $scope.buttonindex] = false;
        $scope.obj["button_states"] = []
        $scope.obj["button_states"].push($scope.buttonobj);
        $scope['state' + $scope.statenum].gambit_init_mess.push($scope.obj);


    }


    $scope.addCarouselItem = function() {

        var carousel = {
            "title": "",
            "sub_title": "",
            "small_desc": "",
            "img_form": "url",
            "image": "",
            "img_name": "",
            "img_url": "",
            "title2": "",
            "sub_title2": "",
            "small_desc2": "",
            "button_text": "",
            "bt_act": "",
            "bt_payload": "",
            "bt_link": "",
            "footer": ""
        }
        $scope['state' + $scope.statenum].exp_in_mess.carousel.push(carousel);

    }

    $scope.removecarousel = function(index) {
        $scope['state' + $scope.statenum].exp_in_mess.carousel.splice(index, 1);
    }

    $scope.textInputOption = function(inputType) {

        $scope.isDisabled = false;
        if (inputType === 'Full Name') {
            $scope['state' + $scope.statenum].exp_in_mess.keyType = 'Normal Keyboard';
            $scope['state' + $scope.statenum].exp_in_mess.capitalizeText = 'Word Start';
            $scope['state' + $scope.statenum].exp_in_mess.inputValidation = 'Fullname Validator';

            $scope.isDisabled = true;
        }

        if (inputType === 'Email') {

            $scope['state' + $scope.statenum].exp_in_mess.keyType = 'Email Keyboard';
            $scope['state' + $scope.statenum].exp_in_mess.capitalizeText = 'Disabled';
            $scope['state' + $scope.statenum].exp_in_mess.inputValidation = 'Email Validator';
            $scope.isDisabled = true;
        }

        if (inputType === 'Telephone') {

            $scope['state' + $scope.statenum].exp_in_mess.keyType = 'Telephone Keyboard';
            $scope['state' + $scope.statenum].exp_in_mess.capitalizeText = 'Disabled';
            $scope['state' + $scope.statenum].exp_in_mess.inputValidation = 'Mobile Number Validator';
            $scope.isDisabled = true;
        }

        if (inputType === 'Address') {

            $scope['state' + $scope.statenum].exp_in_mess.keyType = 'Normal Keyboard';
            $scope['state' + $scope.statenum].exp_in_mess.capitalizeText = 'Scentence Start';
            $scope['state' + $scope.statenum].exp_in_mess.inputValidation = 'No Validation';
            $scope.isDisabled = true;

        }

        if (inputType === 'Password') {

            $scope['state' + $scope.statenum].exp_in_mess.keyType = 'Password Keyboard';
            $scope['state' + $scope.statenum].exp_in_mess.capitalizeText = 'Disabled';
            $scope['state' + $scope.statenum].exp_in_mess.inputValidation = 'No Validation';
            $scope.isDisabled = true;

        }

        if (inputType === 'Custom Text') {

            $scope['state' + $scope.statenum].exp_in_mess.keyType = 'Normal Keyboard',
                $scope['state' + $scope.statenum].exp_in_mess.capitalizeText = 'Disabled',
                $scope['state' + $scope.statenum].exp_in_mess.inputValidation = 'No Validation'
            $scope.isDisabled = false;

        }

    }


    $scope.dragElements = [{
        'Name': "Single Text",
        'Type': "text",
        'ModelVal': '',
        'Settings': [{
                'Name': 'Field Label',
                'Value': 'Single Text',
                'Type': 'text'
            }, {
                'Name': 'Short Label',
                'Value': 'Single Text',
                'Type': 'text'
            }, {
                'Name': 'Internal Name',
                'Value': 'xSingle_Text',
                'Type': 'text'
            }, {
                'Name': 'Field Type',
                'Value': 'Single Text',
                'Type': 'string'
            }, {
                'Name': 'Single Line Text Options',
                'Value': '',
                'Type': 'label'
            }, {
                'Name': 'Max Input Length',
                'Value': '50',
                'Type': 'text'
            }, {
                'Name': 'Url Template',
                'Value': '',
                'Type': 'text'
            }, {
                'Name': 'Column Span',
                'Value': '1',
                'Type': 'dropdown',
                'PossibleValue': ['1', '2']
            }, {
                'Name': 'General Options',
                'Type': 'checkBoxZone',
                'Options': [{
                    'Name': 'Required',
                    'Value': false
                }]
            }

        ]
    }, {
        'Name': "Phone",
        'Type': "number",
        'ModelVal': '',
        'Settings': [{
                'Name': 'Field Label',
                'Value': 'Phone',
                'Type': 'text'
            }, {
                'Name': 'Short Label',
                'Value': 'Phone',
                'Type': 'text'
            }, {
                'Name': 'Internal Name',
                'Value': 'xPhone',
                'Type': 'text'
            }, {
                'Name': 'Field Type',
                'Value': 'Phone',
                'Type': 'number'
            }, {
                'Name': 'Single Line Text Options',
                'Value': '',
                'Type': 'label'
            }, {
                'Name': 'Max Input Length',
                'Value': '50',
                'Type': 'text'
            }, {
                'Name': 'Url Template',
                'Value': '',
                'Type': 'text'
            }, {
                'Name': 'Column Span',
                'Value': '1',
                'Type': 'dropdown',
                'PossibleValue': ['1', '2']
            }, {
                'Name': 'General Options',
                'Type': 'checkBoxZone',
                'Options': [{
                    'Name': 'Required',
                    'Value': false
                }]
            }

        ]
    }, {
        'Name': "Date",
        'Type': "date",
        'ModelVal': '',
        'Settings': [{
                'Name': 'Field Label',
                'Value': 'Field Label',
                'Type': 'text'
            }, {
                'Name': 'Short Label',
                'Value': 'Short Label',
                'Type': 'text'
            }, {
                'Name': 'Internal Name',
                'Value': 'Internal Name',
                'Type': 'text'
            }, {
                'Name': 'Field Type',
                'Value': 'Date',
                'Type': 'string'
            }, {
                'Name': 'Display Type',
                'Value': '',
                'Type': 'radio',
                'PossibleValue': [{
                        'Text': 'DateTimeInstance',
                        'Checked': true
                    },
                    {
                        'Text': 'DateTimeLocal',
                        'Checked': false
                    },
                    {
                        'Text': 'DateLocal',
                        'Checked': false
                    },
                    {
                        'Text': 'Time',
                        'Checked': false
                    },
                ]
            }, {
                'Name': 'Column Span',
                'Value': '1',
                'Type': 'dropdown',
                'PossibleValue': ['1', '2']
            }, {
                'Name': 'General Options',
                'Type': 'checkBoxZone',
                'Options': [{
                    'Name': 'Required',
                    'Value': false
                }]
            }

        ]
    }, {
        'Name': "Single Selection",
        "Type": "dropdown",
        'ModelVal': '',
        'Settings': [{
                'Name': 'Field Label',
                'Value': 'Field Label',
                'Type': 'text'
            }, {
                'Name': 'Short Label',
                'Value': '',
                'Type': 'text'
            }, {
                'Name': 'Internal Name',
                'Value': 'Short Label',
                'Type': 'text'
            }, {
                'Name': 'Field Type',
                'Value': 'Single Selection',
                'Type': 'string'
            }, {
                'Name': 'Display Type',
                'Value': '',
                'Type': 'radio',
                'PossibleValue': [{
                        'Text': 'Dropdown',
                        'Checked': true
                    },
                    {
                        'Text': 'Radio List',
                        'Checked': false
                    }
                ]
            }, {
                'Name': 'Choice',
                'Type': 'dropdown_increment',
                'PossibleValue': [{
                    'Text': 'Choice 1',

                }, {
                    'Text': 'Choice 2'
                }]
            }, {
                'Name': 'Column Span',
                'Value': '1',
                'Type': 'dropdown',
                'PossibleValue': ['1', '2']
            }, {
                'Name': 'General Options',
                'Type': 'checkBoxZone',
                'Options': [{
                    'Name': 'Required',
                    'Value': false
                }]
            }

        ]
    }, {
        'Name': "Pagaraph Text",
        "Type": "textarea",
        'ModelVal': '',
        'Settings': [{
                'Name': 'Field Label',
                'Value': '',
                'Type': 'text'
            }, {
                'Name': 'Short Label',
                'Value': '',
                'Type': 'text'
            }, {
                'Name': 'Internal Name',
                'Value': '',
                'Type': 'text'
            }, {
                'Name': 'Field Type',
                'Value': 'Paragraph Text',
                'Type': 'string'
            }, {
                'Name': 'Column Span',
                'Value': '1',
                'Type': 'dropdown',
                'PossibleValue': ['1', '2']
            }, {
                'Name': 'General Options',
                'Type': 'checkBoxZone',
                'Options': [{
                    'Name': 'Required',
                    'Value': false
                }]
            }

        ]
    }];


    $scope.addform = function(statenum) {
        var first_form = {
            "id": $scope['state' + statenum]['forms'].length + 1,
            "title": "Form Heading",
            "formFields": []
        }
        $scope['state' + statenum]['forms'].push(first_form);

    }

    $scope.delform = function(statenum, index) {

        $scope['state' + statenum]['forms'].splice(index, 1);
    }
    $scope.formFields = [];

    $scope.current_field = {};

    var createNewField = function() {
        return {
            'id': ++guid,
            'Name': '',
            'Settings': [],
            'Active': true,
            'ChangeFieldSetting': function(Value, SettingName) {
                switch (SettingName) {
                    case 'Field Label':
                    case 'Short Label':
                    case 'Internal Name':
                        $scope.current_field.Name = Value;
                        $scope.current_field.Settings[0].Value = $scope.current_field.Name;
                        $scope.current_field.Settings[1].Value = $scope.current_field.Name;
                        $scope.current_field.Settings[2].Value = 'x' + $scope.current_field.Name.replace(/\s/g, '_');
                        break;
                    default:
                        break;
                }
            },
            'GetFieldSetting': function(settingName) {
                var result = {};
                var settings = this.Settings;
                $.each(settings, function(index, set) {
                    if (set.Name == settingName) {
                        result = set;
                        return;
                    }
                });
                if (!Object.keys(result).length) {

                    $.each(settings[settings.length - 1].Options, function(index, set) {
                        if (set.Name == settingName) {
                            result = set;
                            return;
                        }
                    });
                }
                return result;

            }
        };
    }

    $scope.changeFieldName = function(Value) {
        $scope.current_field.Name = Value;
        $scope.current_field.Settings[0].Value = $scope.current_field.Name;
        $scope.current_field.Settings[1].Value = $scope.current_field.Name;
        $scope.current_field.Settings[2].Value = 'x' + $scope.current_field.Name.replace(/\s/g, '_');
    }

    $scope.removeElement = function(statenum, parentind, idx) {

        $scope['state' + statenum]['forms'][parentind].formFields.splice(idx, 1);

    };

    $scope.addElement = function(ele, idx, statenum) {

        $scope.current_field.Active = false;

        $scope.current_field = createNewField();
        //Merge setting from template object
        angular.merge($scope.current_field, ele);

        if ($scope['state' + statenum]['forms'][idx].formFields.length == 0) {

            $scope['state' + statenum]['forms'][idx].formFields.splice(idx, 0, $scope.current_field);

        } else {

            $scope['state' + statenum]['forms'][idx].formFields.push($scope.current_field);
            // $('#fieldSettingTab_lnk'+idx).tab('show');
        }

    };

    $scope.activeField = function(f) {
        $scope.current_field.Active = false;
        $scope.current_field = f;
        f.Active = true;
        //   $('#fieldSettingTab_lnk' +idx).tab('show');
    };

    $scope.formbuilderSortableOpts = {
        'ui-floating': true,

    };

    $scope.addcond = function(statenum) {

        if ($scope['state' + statenum].condif == true) {
            $scope.cond = {
                "cond": [{
                    "cond": "is",
                    "field": "",
                    "value": "",
                    "intercond": "OR"
                }]
            }

            $scope['state' + statenum].cond_details.A.push($scope.cond);
            var length = $scope['state' + statenum].cond_details.A.length - 1;
            var ind = $scope['state' + statenum].conditional_states.indexOf('Default');
            $scope['state' + statenum].conditional_states.splice(ind, 0, 'cond' + length);
        } else {
            // $scope['state' + statenum]["cond_tetails"] = []
        }

    }
    $scope.addfirstcond = function(statenum, i) {


        $scope['state' + statenum]['cond_details'] = {
            "A": []
        }
        $scope.cond = {
            "cond": [{
                "cond": "is",
                "field": "",
                "value": "",
                "intercond": "OR"
            }]
        }

        $scope['state' + statenum].cond_details.A.push($scope.cond);
        $scope['state' + statenum].conditional_states.push('cond0');
        $scope['state' + statenum].conditional_states.push('Default');
        //   $scope['state' + statenum].cond_details['A'].push($scope.cond);

    }
    $scope.addchildcond = function(statenum, index) {
        var new_cond = {
            "cond": "is",
            "field": "",
            "value": "",
            "intercond": "OR"
        }

        $scope['state' + statenum].cond_details.A[index].cond.push(new_cond);
    }

    $scope.del_childcond = function(statenum, parentindex, childindex) {

        $scope['state' + statenum].cond_details.A[parentindex].cond.splice(childindex, 1);
        //$scope.schema.splice(i, 1);
    }

    $scope.del_parentcond = function(statenum, index) {
        $scope['state' + statenum].cond_details.A.splice(index, 1);
        $scope['state' + statenum].conditional_states.splice(index, 1);
        if ($scope['state' + statenum].cond_conn_state != undefined && $scope['state' + statenum].cond_conn_state['cond' + index] != undefined) {
            delete $scope['state' + statenum].cond_conn_state['cond' + index];
        }
    }


    $scope.getapires_1 = function(statenum) {

        if ($scope['state' + statenum]["ent_api_details"].url == "") {

        } else {
            if ($scope['state' + statenum]["ent_api_details"].method == "get") {
                $http.defaults.headers.common['user'] = $window.sessionStorage.id;
                $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
                var url = $scope['state' + statenum]["ent_api_details"].url
                $http.get(url, {
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    }

                ).then(function(res) {

                    $scope.apiresponse = JSON.stringify(res.data, undefined, 2);
                    $scope.apistatus = res.status;
                    $scope.apistatustext = res.statusText;

                })

            } else {
                var url = $scope['state' + statenum]["ent_api_details"].url
                var body_data = {}
                var header_data = { 'Content-Type': 'application/json' }
                if ($scope['state' + statenum]["ent_api_details"].body.length > 0) {

                    angular.forEach($scope['state' + statenum]["ent_api_details"].body, function(value, key) {
                        body_data[value.bodykey] = value.bodyvalue
                    });

                }

                if ($scope['state' + statenum]["ent_api_details"].headerparams.length > 0) {

                    angular.forEach($scope['state' + statenum]["ent_api_details"].headerparams, function(value, key) {

                        header_data[value.paramkey] = value.paramvalue
                    });
                }


                $http.defaults.headers.common['user'] = $window.sessionStorage.id;
                $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
                $http.post("http://172.16.8.211:9122/keyCloakLoginAssist/api/validateUser", body_data, {
                    headers: header_data
                }).then(function(res) {

                    $scope.apiresponse = JSON.stringify(res.data, undefined, 2);
                    $scope.apistatus = res.status;
                    $scope.apistatustext = res.statusText;
                })
            }
        }
    }

    $scope.dropCallback = function(item, type, listName) {

        return item; // return false to disallow drop
    };




    $scope.display_tool = "none";





    /* form */


    $scope.newField = {};
    $scope.form_list = [];
    // $scope.fields = {
    //     "header": "Form 1",
    //     fields: [{
    //         type: 'text',
    //         name: 'Name',
    //         placeholder: 'Please enter your name',
    //         order: 1
    //     }]
    // };
    // $scope.form_list.push($scope.fields);
    $scope.editing = false;
    $scope.add_new_form = function(statenum) {
        if ($scope['state' + statenum].forms == undefined) {
            $scope['state' + statenum].forms = [];
        }
        $scope.fields = {
            "header": "",
            fields: [{
                type: 'text',
                name: 'Name',
                placeholder: 'Please enter your name',
                order: 1
            }]
        };

        $scope['state' + statenum].forms.push($scope.fields);
        var len = $scope['state' + statenum].forms.length;
        $scope['state' + statenum].forms[len - 1].heading = "Form " + len;
    }
    $scope.tokenize = function(slug1, slug2) {
        var result = slug1;
        result = result.replace(/[^-a-zA-Z0-9,&\s]+/ig, '');
        result = result.replace(/-/gi, "_");
        result = result.replace(/\s/gi, "-");
        if (slug2) {
            result += '-' + $scope.token(slug2);
        }
        return result;
    };
    $scope.saveField = function(index, statenum) {
        ids = [];
        if ($scope.newField.type == 'checkboxes') {
            $scope.newField.value = {};
        }
        if ($scope.editing !== false) {
            $scope.fields.fields[$scope.editing] = $scope.newField;
            $scope.editing = false;
        } else {
            if ($scope.newField.type == 'date') {

                if ($scope.newField.mindate.getDate() < 10) {
                    var min_date = '0' + $scope.newField.mindate.getDate()
                } else {
                    var min_date = $scope.newField.mindate.getDate()
                }

                if ($scope.newField.maxdate.getDate() < 10) {
                    var max_date = '0' + $scope.newField.maxdate.getDate()
                } else {
                    var max_date = $scope.newField.maxdate.getDate()
                }
                if ($scope.newField.mindate.getMonth() < 10) {
                    var min_month = '0' + ($scope.newField.mindate.getMonth() + 1)
                } else {
                    var min_month = $scope.newField.mindate.getDate()
                }

                if ($scope.newField.maxdate.getMonth() < 10) {
                    var max_month = '0' + ($scope.newField.maxdate.getMonth() + 1)
                } else {
                    var max_month = $scope.newField.maxdate.getMonth()
                }
                $scope.newField.mindate = $scope.newField.mindate.getFullYear() + '-' + min_month + '-' + min_date;
                $scope.newField.maxdate = $scope.newField.maxdate.getFullYear() + '-' + max_month + '-' + max_date;
                $scope['state' + statenum].forms[index].fields.header = $scope['state' + statenum].forms[index].heading;
                $scope['state' + statenum].forms[index].fields.push($scope.newField);
            } else {
                $scope['state' + statenum].forms[index].fields.push($scope.newField);
            }

        }
        $scope.newField = {
            order: 0
        };
    };
    $scope.editField = function(field, formindex, fieldindex, statenum) {
        $scope.editing = $scope['state' + statenum].forms[formindex].fields[fieldindex];
        $scope.newField = field;
    };
    $scope.splice = function(field, formindex, fieldindex, statenum) {

        $scope['state' + statenum].forms[formindex].fields.splice(fieldindex, 1);
    };
    $scope.addOptions = function() {
        if ($scope.newField.options === undefined) {
            $scope.newField.options = [];
        }
        $scope.newField.options.push({
            order: 0
        });
    };
    $scope.typeSwitch = function(type) {

        if (type == 'checkboxes')
            return 'multiple';
        if (type == 'select')
            return 'multiple';
        if (type == 'radio')
            return 'multiple';

        return type;
    }

    /* end of form*/

    $scope.timeout_type = function(statenum, type) {

        if (type == 'state') {
            $scope.add_timeout_endpoint('state' + statenum);
        } else {

        }

    }


});

myApp.directive('contenteditable', ['$sce', function($sce) {
    return {
        restrict: 'A', // only activate on element attribute
        require: '?ngModel', // get a hold of NgModelController
        link: function(scope, element, attrs, ngModel) {
            if (!ngModel) return; // do nothing if no ng-model

            // Specify how UI should be updated
            ngModel.$render = function() {
                element.html($sce.getTrustedHtml(ngModel.$viewValue || ''));
                read(); // initialize
            };

            // Listen for change events to enable binding
            element.on('blur keyup change', function() {
                scope.$evalAsync(read);
            });

            // Write data to the model
            function read() {
                var html = element.html();
                // When we clear the content editable the browser leaves a <br> behind
                // If strip-br attribute is provided then we strip this out
                if (attrs.stripBr && html == '<br>') {
                    html = '';
                }
                ngModel.$setViewValue(html);
            }
        }
    };
}]);

myApp.directive('elementDraggable', ['$document', function($document) {
    return {
        link: function(scope, element, attr) {
            element.on('dragstart', function(event) {

                event.originalEvent.dataTransfer.setData('templateIdx', $(element).data('index'));
            });
        }
    };
}]);

myApp.directive('elementDrop', ['$document', function($document) {
    return {
        link: function(scope, element, attr) {

            element.on('dragover', function(event) {
                event.preventDefault();
            });

            $('.drop').on('dragenter', function(event) {
                event.preventDefault();
            })
            element.on('drop', function(event) {
                event.stopPropagation();
                var self = $(this);
                scope.$apply(function() {

                    var idx = event.originalEvent.dataTransfer.getData('templateIdx');

                    var insertIdx = self.data('index')

                    scope.addElement(scope.dragElements[idx], insertIdx);
                });
            });
        }
    };
}]);
myApp.directive('ngDynamicForm', function() {
    return {
        // We limit this directive to attributes only.
        restrict: 'A',

        // We will not replace the original element code
        replace: false,

        // We must supply at least one element in the code
        templateUrl: 'dynamicForms.html'
    }
});