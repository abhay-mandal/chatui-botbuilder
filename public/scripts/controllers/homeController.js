var myApp = angular.module('webchatbot', ['ui.bootstrap', 'ngAnimate', 'ngSanitize', 'ngToast', 'mgcrea.ngStrap.datepicker', 'mgcrea.ngStrap.typeahead']);
myApp.config(['ngToastProvider',
    function(ngToastProvider) {
        ngToastProvider.configure({
            animation: 'slide',
            horizontalPosition: 'center',
            verticalPosition: 'top'
        });
    }
]);
myApp.config(function($typeaheadProvider) {
    angular.extend($typeaheadProvider.defaults, {
        animation: 'am-flip-x',
        minLength: 2,
        limit: 8
    });
});
myApp.controller('HomeCtrl', function($scope, $state, $http, $rootScope, $window, $modal, ngToast, $filter) {
    $scope.botId = $state.params.botId;
    if (!$window.localStorage.id) {
        $state.go("login");
    }
    $(document).ready(function() {
        $('[data-toggle=tooltip]').hover(function() {
            // on mouseenter
            $(this).tooltip('show');
        }, function() {
            // on mouseleave
            $(this).tooltip('hide');
        });
    });
    $scope.page = 5;
    $scope.createbot = function() {
        $state.go('dashboard.rcs_dashboard', { reload: true });
    };
    $http.defaults.headers.common['user'] = $window.sessionStorage.id;
    $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
    $http.get("/listbots", {
        headers: { 'Content-Type': 'application/json' }
    }).then(function(res) {
        var data = [];
        angular.forEach(res.data, function(obj) {
            data.push({ "botID": obj.botId, "botname": obj.botName, "fullname": (obj.botId + ':' + obj.botName) });
        });
        $rootScope.botCount = res.data.length;
        $scope.gridOptions = data;
        if (res.data.length == 0) {
            $scope.exists = true;
        } else {
            $scope.exists = false;
        }
    });

    // if ($scope.gridOptions != undefined) {
    //     $scope.formatLabel = function(model) {
    //         for (var i = 0; i < $scope.gridOptions.length; i++) {
    //             if (model === $scope.gridOptions[i].botID || model === $scope.gridOptions[i].botname) {
    //                 return $scope.gridOptions[i].botID + '\n' + $scope.gridOptions[i].botname;
    //             }
    //         }
    //     }
    // }

    // pagination setting/options

    $scope.sort_by = function(keyname) {
        $scope.sortKey = keyname; //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }

    $scope.search_report = function(ev) {
        switch ($scope.selected_report) {
            case 'chat':
                // console.log($scope.search);
                $scope.download_conv($scope.search.botID, $scope.min_date, $scope.max_date);
                break;
            case 'chat_media':
                $scope.download_media($scope.search.botID, $scope.min_date, $scope.max_date);
                break;
            case 'form_data':
                $scope.openform(ev, $scope.search.botID, "data", $filter('date')(new Date($scope.min_date), "y-MM-dd"), $filter('date')(new Date($scope.max_date), "y-MM-dd"));
                break;
            case 'form_media':
                $scope.openform(ev, $scope.search.botID, "media", $filter('date')(new Date($scope.min_date), "y-MM-dd"), $filter('date')(new Date($scope.max_date), "y-MM-dd"));
                break;
        }
    }

    $scope.download_conv = function(botId, fromdate, todate) {

        var url = '/download_conv?csv=true&bot_id=' + botId + '&fromdate=' + fromdate + '&todate=' + todate;
        window.open(url);

    }

    $scope.download_media = function(botId, fromdate, todate) {
        var url = '/download_media?bot_id=' + botId + '&fromdate=' + fromdate + '&todate=' + todate;
        window.open(url);
    }
    $scope.openform = function(ev, botId, type, fromdate, todate) {
        var db_obj = {
                "bot_id": botId
            }
            // console.log(fromdate + ":" + todate);
        $http.defaults.headers.common['user'] = $window.sessionStorage.id;
        $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
        $http.get('/getformlist?bot_id=' + botId, {
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(res) {
            // console.log(res);
            $scope.bot_forms = res.data;
            if ($scope.bot_forms.length > 0) {
                $modal.open({
                    templateUrl: 'report.tmpl.html',
                    controller: function($scope) {
                        $scope.ok = function() {
                            $scope.$close();
                        }
                        $scope.$watch('selected_form', function(value) {
                            if (type == "data") {
                                if (value != 'none') {
                                    var url = '/getform_conv?csv=true&bot_id=' + botId + '&media_files=' + "" + "&form=" + value + '&fromdate=' + fromdate + '&todate=' + todate;;
                                    window.open(url);
                                    $scope.ok();
                                } else {
                                    $scope.showSimpleToast("Please select the form!!", "danger");
                                }
                            } else if (type == "media") {
                                if (value != 'none') {
                                    var url = '/getform_conv_media?bot_id=' + botId + "&form=" + value + '&fromdate=' + fromdate + '&todate=' + todate;;
                                    window.open(url);
                                    $scope.ok();
                                } else {
                                    $scope.showSimpleToast("Please select the form!!", "danger");
                                }
                            }
                        })
                    },
                    scope: $scope,
                    controllerAs: 'ctrl',
                    targetEvent: ev,
                    size: 'md'
                });
            } else {
                $scope.showSimpleToast("There are no forms for this bot", "warning");
            }
        });
    }
    $scope.showSimpleToast = function(message, clsname) {
        ngToast.create({
            className: clsname,
            content: message,
            dismissOnTimeout: true,
            timeout: 2500,
            dismissButton: true,
            dismissButtonHtml: '&times',
            dismissOnClick: true
        });
    };
});