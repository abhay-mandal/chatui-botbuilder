var myApp = angular.module('webchatbot', ['ui.bootstrap', 'mgcrea.ngStrap.datepicker', 'ng-mfb', 'ngAnimate', 'ngSanitize', 'ngToast', 'mgcrea.ngStrap.timepicker']);
myApp.directive('file', function() {
    return {
        scope: {
            file: '='
        },
        link: function(scope, el, attrs) {
            el.bind('change', function(event) {
                var files = event.target.files;
                var file = files[0];
                scope.file = file ? file.name : undefined;
                scope.$apply();
            });
        }
    };
});
myApp.config(['ngToastProvider',
    function(ngToastProvider) {
        ngToastProvider.configure({
            animation: 'slide',
            horizontalPosition: 'center',
            verticalPosition: 'top'
        });
    }
]);
//controllers manage an object $scope in AngularJS (this is the view model)
myApp.controller('EditCtrl', function($scope, $mdDialog, $http, $element, $state, $window, $mdToast, Upload, $modal, $rootScope, $timeout, ngToast) {
    if (!$window.localStorage.id) {
        $state.go("login");
    }

    $scope.botId = $state.params.botId;
    var botFlow = [];
    var ids = [];
    $scope.startcheck = [{}];
    $scope.startState = "";
    $scope.apivalues = [];
    $scope.statind = 0;
    $http.defaults.headers.common['user'] = $window.sessionStorage.id;
    $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
    $http.get("/getbot/?botId=" + $scope.botId, {
        headers: { 'Content-Type': 'application/json' }
    }).then(function(res) {
        // console.log(res, "res from db");
        botFlow = res.data[0].botFlow;
        botFlow.sort(function(a, b) {
            var nameA = a.state_name.substr(5),
                nameB = b.state_name.substr(5)
            return nameA - nameB
        });
        $scope.botFlow = res.data[0].botFlow;
        $scope.botFlow.sort(function(a, b) {
            var nameA = a.state_name.substr(5),
                nameB = b.state_name.substr(5)
            return nameA - nameB
        });
        $scope.i = $scope.botFlow.length;
        $scope.band = { "Color": "#cc0000" };
        $scope.title = { "Color": "#ffffff" };
        $scope.background = { "Color": "#ffffff" };
        $scope.bbubble = { "Color": "#99ccff" };
        $scope.ububble = { "Color": "#cc99ff" };
        $scope.button = { "Color": "#cc6699" };
        $scope.sugges = { "Color": "#cc6699" };
        $scope.font = { "Color": "#000000" };
        $scope.textfield = { "Color": "#ffffff" };
        $scope.buttonhover = { "Color": "#ffffff" };
        $scope.carouselbackground = { "Color": "#ffffff" };
        $scope.carousel = { "button": "#ffffff" };
        $scope.bot = { "Name": res.data[0].botName };
        $scope.band.Color = res.data[0].titlebarColor;
        $scope.title.Color = res.data[0].titleColor;
        $scope.background.Color = res.data[0].backgroundColor;
        $scope.bbubble.Color = res.data[0].bbubbleColor;
        $scope.ububble.Color = res.data[0].ububbleColor;
        $scope.button.Color = res.data[0].buttonColor;
        $scope.sugges.Color = res.data[0].suggesColor;
        $scope.font.Color = res.data[0].fontColor;
        $scope.textfield.Color = res.data[0].textfieldColor;
        $scope.buttonhover.Color = res.data[0].buttonhoverColor;
        $scope.bot = { "Description": res.data[0].botDesc };
        $scope.web = { "link": res.data[0].webLink };
        $scope.post = { "email": res.data[0].postemail };
        $scope.tog_brand = { icon: res.data[0].tog_brand_icon };
        $scope.tog_normal = { chat: res.data[0].tog_normal_chat };
        $scope.tog_whatsapp = { chat: res.data[0].tog_whatsapp_chat };
        $scope.carouselbackground = { "Color": res.data[0].carousel_background };
        $scope.carousel = { "button": res.data[0].carousel_button };
        $scope.bot_img = { "logo": "/uploads/" + $scope.botId + "/" + res.data[0].botImage };
        $scope.web_bg = { "logo": "/uploads/" + $scope.botId + "/" + res.data[0].webImage };
        $scope.bot_bg = { "logo": "/uploads/" + $scope.botId + "/" + res.data[0].botBgImage };
        $scope.bot_img_logo = res.data[0].botImage;
        $scope.web_bg_logo = res.data[0].webImage;
        $scope.bot_bg_logo = res.data[0].botBgImage;
        $scope.startState = res.data[0].startState;
        $scope.sandbox = { "id": res.data[0].sandbox_id };
        $scope.user = { phone_number: res.data[0].userPhoneNumber };
        $scope.time = { stamp: res.data[0].timeStamp };
        $scope.sender = { id: res.data[0].sender_id };
        console.log(res.data[0]);
        for (let i = 0; i < botFlow.length; i++) {
            $timeout(function() {
                $scope.addgambit_1(botFlow[i], i);
                // console.log("ind", i);
                if (i == botFlow.length - 1) {
                    for (var j = 0; j < botFlow.length; j++) {
                        if (botFlow[j].exp_in_mess) {
                            if (botFlow[j].input_option == 'Suggestion') {
                                if (botFlow[j].exp_in_mess.suggestion_box) {
                                    if (botFlow[j].exp_in_mess.suggestion_box.length > 0) {

                                        if (botFlow[j].exp_in_mess.suggest_branches == false) {
                                            var conn = jsPlumb.connect({ source: $scope[botFlow[j].state_name + "_src"], target: $scope[botFlow[j].connected_state + "_tgt"] });
                                        } else {
                                            // console.log("else");
                                            // console.log(botFlow[j]);
                                            for (var k = 0; k < botFlow[j].exp_in_mess.suggestion_box.length; k++) {
                                                // console.log(k, "in for", botFlow[j].state_name);
                                                var exp = (botFlow[j].exp_in_mess.suggestion_box[k].match(/[^a-zA-Z0-9\s]/g)) ? botFlow[j].exp_in_mess.suggestion_box[k].replace(/[^a-zA-Z0-9]/g, "") : botFlow[j].exp_in_mess.suggestion_box[k];
                                                var conn = jsPlumb.connect({
                                                    source: $scope[botFlow[j].state_name + "_src_" + k],
                                                    target: $scope[botFlow[j].sugg_conn_state[exp] + "_tgt"]
                                                });
                                            }
                                        }
                                    } else if (botFlow[j].connected_state != "") {
                                        var conn = jsPlumb.connect({ source: $scope[botFlow[j].state_name + "_src"], target: $scope[botFlow[j].connected_state + "_tgt"] });
                                    } else {

                                    }
                                } else {
                                    // console.log("does not have");
                                    // botFlow[j].exp_in_mess.suggestions = "Custom List";

                                    if (botFlow[j].connected_state != "") {
                                        var conn = jsPlumb.connect({ source: $scope[botFlow[j].state_name + "_src"], target: $scope[botFlow[j].connected_state + "_tgt"] });
                                    } else if (botFlow[j].connected_state == "") {

                                    } else {

                                    }

                                }
                            } else if (botFlow[j].input_option == 'Star Rating') {

                                for (var k = 0; k < botFlow[j].exp_in_mess.star_rating; k++) {
                                    //   // console.log(k,"in for", botFlow[j]);
                                    //var conn = jsPlumb.connect({source: $scope[botFlow[j].state_name+"_src_"+k], target: $scope[botFlow[j].sugg_conn_state[botFlow[j].exp_in_mess.suggestion_box[k]]+"_tgt"] });
                                    var conn = jsPlumb.connect({ source: $scope[botFlow[j].state_name + "_src_" + k], target: $scope[botFlow[j].star_states[k + 1] + "_tgt"] });

                                }
                            } else if (botFlow[j].condif == true) {
                                // console.log("cond", botFlow[j].state_name);
                                $scope[botFlow[j].state_name]['cond_details'] == botFlow[j].cond_details;
                                for (var k = 0; k < botFlow[j].conditional_states.length; k++) {
                                    //   // console.log(k,"in for",botFlow[j].conditional_states,botFlow[j].cond_conn_state[conditional_states[k]], botFlow[j].conditional_states[botFlow[j].cond_conn_state[k]]);
                                    var conn = jsPlumb.connect({
                                        source: $scope[botFlow[j].state_name + "_src_" + k],
                                        target: $scope[botFlow[j].cond_conn_state[botFlow[j].conditional_states[k]] + "_tgt"]
                                    });
                                }
                            } else {
                                if (botFlow[j].connected_state != "") {
                                    // console.log(botFlow[j]);
                                    var conn = jsPlumb.connect({ source: $scope[botFlow[j].state_name + "_src"], target: $scope[botFlow[j].connected_state + "_tgt"] });
                                } else if (botFlow[j].connected_state == "") {

                                } else {

                                }
                            }
                        } else if (botFlow[j].condif == true) {
                            // console.log(botFlow[j].cond_details);
                            $scope[botFlow[j].state_name]['cond_details'] == botFlow[j].cond_details;
                            for (var k = 0; k < botFlow[j].conditional_states.length; k++) {
                                //   // console.log(k,"in for",botFlow[j].conditional_states,botFlow[j].cond_conn_state[conditional_states[k]], botFlow[j].conditional_states[botFlow[j].cond_conn_state[k]]);
                                var conn = jsPlumb.connect({ source: $scope[botFlow[j].state_name + "_src_" + k], target: $scope[botFlow[j].cond_conn_state[botFlow[j].conditional_states[k]] + "_tgt"] });
                            }
                        } else {
                            // console.log("its in else kjhsbcjh ");
                            if (botFlow[j].connected_state != "") {
                                // console.log(botFlow[j]);
                                var conn = jsPlumb.connect({ source: $scope[botFlow[j].state_name + "_src"], target: $scope[botFlow[j].connected_state + "_tgt"] });
                            } else if (botFlow[j].connected_state == "") {

                            } else {

                            }
                        }
                    }
                }
            }, 1500);



        }
        // $timeout(function() {
        //     if ($scope.startState != "") {
        //         $.each($scope.startcheck, function(key, val) {

        //         });
        //     }
        // }, 2000);
    });

    function module(library_id, schema_id, title, description, x, y) {
        this.library_id = library_id;
        this.schema_id = schema_id;
        this.title = title;
        this.description = description;
        this.x = x;
        this.y = y;
    }

    // module should be visualized by title, icon
    $scope.library = [];

    // library_uuid is a unique identifier per module type in the library
    $scope.library_uuid = 0;

    // state is [identifier, x position, y position, title, description]
    $scope.schema = [];

    // schema_uuid should always yield a unique identifier, can never be decreased
    $scope.schema_uuid = 0;

    // todo: find out how to go back and forth between css and angular
    $scope.library_topleft = {
        x: 15,
        y: 145,
        item_height: 50,
        margin: 5,
    };

    $scope.module_css = {
        width: 150,
        height: 100, // actually variable
    };

    $scope.submit = function(file) { //function to call on form submit
        $scope.upload(file);
    }
    $scope.upload = function(file) {
        Upload.upload({
            url: '/upload/?botId=' + $scope.botId, //webAPI exposed to upload the file
            data: { files: file } //pass file as data, should be user ng-model
        }).then(function(resp) {
            $scope.upload_botimg($scope.bot_images)
                //upload function returns a promise
            if (resp.data.error_code === 0) { //validate success
                $rootScope.botCount++;
                $scope.showSimpleToast('Successfully uploaded', 'success');
            } else {
                $scope.showSimpleToast('An error occured', 'danger');
            }
        }, function(resp) { //catch error
            $scope.showSimpleToast('Error status: ' + resp.status, 'danger');
        });
    };

    $scope.upload_botimg = function(file) {


        Upload.upload({
            url: '/upload_bot_img/?botId=' + $scope.botId, //webAPI exposed to upload the file
            data: { files: file } //pass file as data, should be user ng-model
        }).then(function(resp) {

            //upload function returns a promise
            if (resp.data.error_code === 0) { //validate success
                $scope.showSimpleToast('Media uploaded successfully.', 'success');

            } else {
                $scope.showSimpleToast('An error occured in media upload.', 'danger');
            }
        }, function(resp) { //catch error

            $scope.showSimpleToast('Error status: ' + resp.status, 'danger');
        });
    }

    $scope.redraw = function() {
        $scope.schema_uuid = 0;
        jsPlumb.detachEveryConnection();
        $scope.schema = [];
        $scope.library = [];
        $scope.addModuleToLibrary("Sum", "Aggregates an incoming sequences of values and returns the sum",
            $scope.library_topleft.x + $scope.library_topleft.margin,
            $scope.library_topleft.y + $scope.library_topleft.margin);
        $scope.addModuleToLibrary("Camera", "Hooks up to hardware camera and sends out an image at 20 Hz",
            $scope.library_topleft.x + $scope.library_topleft.margin,
            $scope.library_topleft.y + $scope.library_topleft.margin + $scope.library_topleft.item_height);
    };

    // add a module to the library
    $scope.addModuleToLibrary = function(title, description, posX, posY) {
        // console.log("Add module " + title + " to library, at position " + posX + "," + posY);
        var library_id = $scope.library_uuid++;
        var schema_id = -1;
        var m = new module(library_id, schema_id, title, description, posX, posY);
        $scope.library.push(m);
    };

    // add a module to the schema
    $scope.addModuleToSchema = function(library_id, posX, posY) {
        // console.log("Add module " + title + " to schema, at position " + posX + "," + posY);
        var schema_id = $scope.schema_uuid++;
        var title = "Unknown";
        var description = "Likewise unknown";
        for (var i = 0; i < $scope.library.length; i++) {
            if ($scope.library[i].library_id == library_id) {
                title = $scope.library[i].title;
                description = $scope.library[i].description;
            }
        }
        var m = new module(library_id, schema_id, title, description, posX, posY);
        $scope.schema.push(m);
    };


    $scope.init = function() {
            jsPlumb.bind("ready", function() {
                jsPlumb.Defaults.Container = $("#diagramContainer");
                // jsPlumb.setSuspendDrawing(true);
                jsPlumb.bind("connection", function(info, ev) {
                    // console.log("in connection", info, ev);
                    // if (ev != undefined) {
                    $scope.$apply(function() {

                        // console.log("Possibility to push connection into array", info.sourceId, info.targetId, "info", info, ev);

                        if ($scope[info.sourceId].input_option == "Suggestion") {
                            // console.log("i am here");
                            var endpoints_length = jsPlumb.getEndpoints(info.sourceId);

                            $scope[info.sourceId]['sugg_conn_state'] = {

                            };
                            if ($scope[info.sourceId].exp_in_mess.suggest_branches == false) {
                                $scope[info.sourceId].connected_state = info.targetId;
                                for (var i = 0; i < $scope[info.sourceId].exp_in_mess.suggestion_box.length; i++) {
                                    $scope[info.sourceId]['sugg_conn_state'][$scope[info.sourceId].exp_in_mess.suggestion_box[i]] = info.targetId;
                                }

                            } else {
                                for (var i = 0; i < endpoints_length.length; i++) {

                                    if (i == 0) {
                                        if (endpoints_length[i].isSource == true) {

                                            if (endpoints_length[i].connections.length > 0) {
                                                $scope[info.sourceId]['sugg_conn_state'][$scope[info.sourceId].exp_in_mess.suggestion_box[i]] = endpoints_length[i].connections[0].targetId;
                                            } else {

                                            }

                                        } else {

                                        }
                                    } else {
                                        if (endpoints_length[i].isSource == true) {

                                            if (endpoints_length[i].connections.length > 0) {
                                                $scope[info.sourceId]['sugg_conn_state'][$scope[info.sourceId].exp_in_mess.suggestion_box[i - 1]] = endpoints_length[i].connections[0].targetId;
                                            } else {

                                            }

                                        } else {

                                        }
                                    }

                                }
                            }
                        } else if ($scope[info.sourceId].input_option == "Star Rating") {
                            var endpoints_length = jsPlumb.getEndpoints(info.sourceId);
                            $scope[info.sourceId]['star_states'] = {

                            };
                            for (var i = 0; i < endpoints_length.length; i++) {
                                if (i == 0) {
                                    if (endpoints_length[i].isSource == true) {
                                        // console.log($scope[info.sourceId]);
                                        if (endpoints_length[i].connections.length > 0) {
                                            $scope[info.sourceId]['star_states'][i + 1] = endpoints_length[i].connections[0].targetId;
                                            // console.log($scope[info.sourceId]['star_states']);
                                        } else {

                                        }

                                    } else {

                                    }
                                } else {
                                    if (endpoints_length[i].isSource == true) {
                                        if (endpoints_length[i].connections.length > 0) {
                                            $scope[info.sourceId]['star_states'][i] = endpoints_length[i].connections[0].targetId;
                                            // console.log($scope[info.sourceId]['star_states']);
                                        } else {

                                        }
                                    } else {

                                    }
                                }
                            }

                        } else if ($scope[info.sourceId].condif == true) {
                            var endpoints_length = jsPlumb.getEndpoints(info.sourceId);
                            // $scope[info.sourceId]['cond_conn_state'] = {}
                            var flag = true;
                            $scope.endpoints_length = []
                            angular.forEach(endpoints_length, function(value, key) {
                                if (value.isSource == true) {
                                    $scope.endpoints_length.push(value);
                                } else {

                                }

                            });
                            for (var i = 0; i < $scope.endpoints_length.length; i++) {

                                if ($scope.endpoints_length[i].isSource == true) {

                                    if ($scope.endpoints_length[i].connections.length > 0) {

                                        if (i == $scope.endpoints_length.length - 1) {
                                            if ($scope[info.sourceId].cond_details.A[i] == undefined) {
                                                var j = i - 1;
                                            } else {
                                                j = i
                                            }

                                            $scope[info.sourceId]['cond_conn_state']['Default'] = $scope.endpoints_length[i].connections[0].targetId;

                                        } else {
                                            if ($scope[info.sourceId].cond_details.A[i] == undefined) {
                                                var j = i - 1;
                                            } else {
                                                j = i
                                            }

                                            $scope[info.sourceId]['cond_conn_state']['cond' + j] = $scope.endpoints_length[i].connections[0].targetId;

                                        }

                                    } else {

                                    }

                                } else {


                                }
                            }
                        } else {
                            $scope[info.sourceId].connected_state = info.targetId;
                            // console.log($scope[info.sourceId]);
                        }

                    });
                    // } else {

                    // }
                    // jsPlumb.setSuspendDrawing(false, true);
                });
                jsPlumb.bind("connectionDetached", function(info, ev) {

                    // $scope.$apply(function() {

                    if ($scope[info.sourceId].input_option == "Suggestion") {
                        var endpoints_length = jsPlumb.getEndpoints(info.sourceId);

                        $scope[info.sourceId]['sugg_conn_state'] = {

                        };
                        if ($scope[info.sourceId].exp_in_mess.suggest_branches == false) {
                            $scope[info.sourceId].connected_state = info.targetId;
                            for (var i = 0; i < $scope[info.sourceId].exp_in_mess.suggestion_box.length; i++) {
                                $scope[info.sourceId]['sugg_conn_state'][$scope[info.sourceId].exp_in_mess.suggestion_box[i]] = "";
                            }

                        } else {
                            for (var i = 0; i < endpoints_length.length; i++) {

                                if (i == 0) {
                                    if (endpoints_length[i].isSource == true) {

                                        if (endpoints_length[i].connections.length > 0) {
                                            $scope[info.sourceId]['sugg_conn_state'][$scope[info.sourceId].exp_in_mess.suggestion_box[i]] = endpoints_length[i].connections[0].targetId;
                                        } else {

                                        }

                                    } else {

                                    }
                                } else {
                                    if (endpoints_length[i].isSource == true) {

                                        if (endpoints_length[i].connections.length > 0) {
                                            $scope[info.sourceId]['sugg_conn_state'][$scope[info.sourceId].exp_in_mess.suggestion_box[i - 1]] = endpoints_length[i].connections[0].targetId;
                                        } else {

                                        }

                                    } else {

                                    }
                                }

                            }
                        }
                    } else if ($scope[info.sourceId].condif == true) {
                        var endpoints_length = jsPlumb.getEndpoints(info.sourceId);
                        $scope[info.sourceId]['cond_conn_state'] = {

                        };
                        for (var i = 0; i < endpoints_length.length; i++) {


                            if (endpoints_length[i].isSource == true) {

                                if (endpoints_length[i].connections.length > 0) {

                                    if (i == endpoints_length.length - 1) {

                                        $scope[info.sourceId]['cond_conn_state']['Default'] = endpoints_length[i].connections[0].targetId;

                                    } else {

                                        $scope[info.sourceId]['cond_conn_state'][$scope[info.sourceId].conditional_states[i - 1]] = endpoints_length[i].connections[0].targetId;

                                    }

                                } else {

                                }
                            } else {

                            }
                        }
                        console.log(" $scope[info.sourceId]", $scope[info.sourceId]);
                    } else {
                        $scope[info.sourceId].connected_state = "";
                    }
                    // });
                });
            });
        }
        // $timeout(function() {
    $scope.init();
    // }, 2000);


    $scope.textInputOption = function(inputType) {

        $scope.isDisabled = false;
        if (inputType === 'Full Name') {
            $scope['state' + $scope.statenum].exp_in_mess.keyType = 'Normal Keyboard';
            $scope['state' + $scope.statenum].exp_in_mess.capitalizeText = 'Word Start';
            $scope['state' + $scope.statenum].exp_in_mess.inputValidation = 'Fullname Validator';

            $scope.isDisabled = true;
        }

        if (inputType === 'Email') {

            $scope['state' + $scope.statenum].exp_in_mess.keyType = 'Email Keyboard';
            $scope['state' + $scope.statenum].exp_in_mess.capitalizeText = 'Disabled';
            $scope['state' + $scope.statenum].exp_in_mess.inputValidation = 'Email Validator';
            $scope.isDisabled = true;
        }

        if (inputType === 'Telephone') {

            $scope['state' + $scope.statenum].exp_in_mess.keyType = 'Telephone Keyboard';
            $scope['state' + $scope.statenum].exp_in_mess.capitalizeText = 'Disabled';
            $scope['state' + $scope.statenum].exp_in_mess.inputValidation = 'Mobile Number Validator';
            $scope.isDisabled = true;
        }

        if (inputType === 'Address') {

            $scope['state' + $scope.statenum].exp_in_mess.keyType = 'Normal Keyboard';
            $scope['state' + $scope.statenum].exp_in_mess.capitalizeText = 'Scentence Start';
            $scope['state' + $scope.statenum].exp_in_mess.inputValidation = 'No Validation';
            $scope.isDisabled = true;

        }

        if (inputType === 'Password') {

            $scope['state' + $scope.statenum].exp_in_mess.keyType = 'Password Keyboard';
            $scope['state' + $scope.statenum].exp_in_mess.capitalizeText = 'Disabled';
            $scope['state' + $scope.statenum].exp_in_mess.inputValidation = 'No Validation';
            $scope.isDisabled = true;

        }

        if (inputType === 'Custom Text') {

            $scope['state' + $scope.statenum].exp_in_mess.keyType = 'Normal Keyboard',
                $scope['state' + $scope.statenum].exp_in_mess.capitalizeText = 'Disabled',
                $scope['state' + $scope.statenum].exp_in_mess.inputValidation = 'No Validation'
            $scope.isDisabled = false;

        }

    }

    // jsPlumb.repaintEverything();
    $scope.final_obj = [];

    $scope.inter_cond_options = ["AND", "OR"];
    $scope.cond_options = [
        "is",
        "is not",
        "equals",
        "not equals",
        "less than",
        "greater than"
        // "contains",
        // "not contains",
        // " is unknown",
        // "has any value"
    ];
    $scope.apikeys = [];
    $scope.apiintkeys = [];
    $scope.addgambit_1 = function(state, i) {
        if (botFlow[i].sugg_conn_state != undefined) {
            Object.keys(botFlow[i].sugg_conn_state).forEach(function(key) {
                if (key.match(/[^a-zA-Z0-9\s]/g)) {
                    var newkey = key.replace(/[^a-zA-Z0-9]/g, "");
                    botFlow[i].sugg_conn_state[newkey] = botFlow[i].sugg_conn_state[key];
                    delete botFlow[i].sugg_conn_state[key];
                }
            });
        }
        var top = state.top,
            left = state.left;
        // console.log(top, left);
        var newState = $('<div>').attr('id', state.state_name).addClass('item');
        $scope.statind = parseInt(state.state_name.substr(5));
        var start = $('<div>').attr('id', 'start' + $scope.statind).addClass('fa fa-check-circle-o maincolor');
        if (state.state == "" || state.state == undefined) {
            var title = $('<div>').addClass('title').text(state.state_name);

        } else {
            var title = $('<div>').addClass('title').text(state.state);
        }
        if ($scope.user.phone_number == true || $scope.user.phone_number == "true") {
            var exists = $scope.apivalues.indexOf("bot_set_phonenumber");
            if (exists == -1) {
                $scope.apivalues.push("bot_set_phonenumber");
            }
        }

        if ($scope.time.stamp == true || $scope.time.stamp == "true") {
            var exists = $scope.apivalues.indexOf("bot_set_timestamp");
            if (exists == -1) {
                $scope.apivalues.push("bot_set_timestamp");
            }
        }
        $scope.apivalues.push(state.state);
        // $scope['ent_api_details' + state.state_name] =
        var connect = $('<div>').addClass('connect');
        var divsize = ((Math.random() * 100) + 50).toFixed();
        start.css({
            'float': 'right',
            'margin': 5 + 'px',
            'font-size': 'large'
        });
        var key = '#start' + $scope.statind;
        var val = state.state_name;
        $scope.startcheck[0][key] = val;
        $newdiv = $('</div>').css({
            'width': divsize + 'px',
            'height': divsize + 'px'
        });

        var posx = (Math.random() * ($(document).width() - divsize)).toFixed();
        var posy = (Math.random() * ($(document).height() - divsize)).toFixed();

        newState.css({
            'top': top,
            'left': left
        });
        // start state
        // start state
        start.on('click', function(e) {
            start.toggleClass("maincolor changecolor");
            var list = e.target.classList;
            $scope.startState = 'state' + e.target.id.substr(5);
            if (list.contains('maincolor')) {
                $.each($scope.startcheck[0], function(key1, val1) {
                    if ($scope.startState != val1) {
                        $(key1).attr('ng-disabled', 'false').css("pointer-events", "auto");
                    }
                });
                $scope.startState = "";
            } else if (list.contains('changecolor')) {
                $.each($scope.startcheck[0], function(key2, val2) {
                    if ($scope.startState != val2) {
                        $(key2).attr('ng-disabled', 'true').css("pointer-events", "none");
                    }
                });

            }
        });
        newState.dblclick(function(e) {
            $scope.showAlert(e);
        });
        newState.append(start);
        newState.append(title);
        // newState.append(connect);

        $('#diagramContainer').append(newState);
        if ($scope.startState != state.state_name) {
            $('#start' + $scope.statind).attr('ng-disabled', 'true').css("pointer-events", "none");
        } else {
            $('#start' + $scope.statind).removeClass("maincolor");
            $('#start' + $scope.statind).addClass("changecolor");
            $('#start' + $scope.statind).attr('ng-disabled', 'false').css("pointer-events", "auto");
        }
        jsPlumb.draggable(newState, {
            containment: 'body',
            endpoint: "Rectangle"
        });
        // var state = state.state_name;
        // // console.log("current state",state);
        /*  var st_name = botFlow[j].state_name*/
        // console.log(state);
        if (state.exp_in_mess) {
            if (state.input_option == 'Suggestion') {
                if (state.exp_in_mess.suggestion_box) {
                    if (state.exp_in_mess.suggestion_box.length > 0) {
                        if (state.exp_in_mess.suggest_branches == false) {
                            $scope[state.state_name + "_src"] = jsPlumb.addEndpoint(state.state_name, {
                                isSource: true,
                                anchors: ["Bottom"],
                                deleteEndpointsOnDetach: false,
                                connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                                endpoint: ["Rectangle", { width: 10, height: 8 }],
                                paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "black" }

                            });

                            if (state.multiple_conn == true) {
                                $scope[state.state_name + "_tgt"] = jsPlumb.addEndpoint(state.state_name, {
                                    isTarget: true,
                                    anchors: ["Top"],
                                    maxConnections: -1,
                                    connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                                    endpoint: "Rectangle",
                                    paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "blue" }
                                });
                            } else {
                                $scope[state.state_name + "_tgt"] = jsPlumb.addEndpoint(state.state_name, {
                                    isTarget: true,
                                    anchors: ["Top"],
                                    connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                                    endpoint: "Rectangle",
                                    paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "blue" }
                                });
                            }
                            $scope[state.state_name + "_tgt"] = jsPlumb.addEndpoint(state.state_name, {
                                isTarget: true,
                                anchors: ["Top"],
                                maxConnections: -1,
                                connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                                endpoint: "Rectangle",
                                paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "blue" }
                            });

                        } else {
                            if (state.multiple_conn == true) {
                                $scope[state.state_name + "_tgt"] = jsPlumb.addEndpoint(state.state_name, {
                                    isTarget: true,
                                    anchors: ["Top"],
                                    maxConnections: -1,
                                    connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                                    endpoint: "Rectangle",
                                    paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "blue" }
                                });
                            } else {
                                $scope[state.state_name + "_tgt"] = jsPlumb.addEndpoint(state.state_name, {
                                    isTarget: true,
                                    anchors: ["Top"],
                                    connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                                    endpoint: "Rectangle",
                                    paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "blue" }
                                });
                            }
                            var width = document.getElementById(state.state_name).offsetWidth;
                            document.getElementById(state.state_name).style.width = width + 10 + "px";
                            var left_val = 0.6;
                            for (var k = 0; k < state.exp_in_mess.suggestion_box.length; k++) {
                                left_val = left_val + 0.1;
                                $scope[state.state_name + "_src_" + k] = jsPlumb.addEndpoint(state.state_name, {
                                    isSource: true,
                                    anchor: [
                                        [left_val, 1.07, 1, 1, 1, -4]
                                    ],
                                    deleteEndpointsOnDetach: false,
                                    connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                                    endpoint: ["Rectangle", { width: 10, height: 8 }],
                                    paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "black" }

                                });
                            }
                        }
                    } else {
                        $scope[state.state_name + "_src"] = jsPlumb.addEndpoint(state.state_name, {
                            isSource: true,
                            anchors: ["Bottom"],
                            deleteEndpointsOnDetach: false,
                            connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                            endpoint: ["Rectangle", { width: 10, height: 8 }],
                            paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "black" }

                        });
                        $scope[state.state_name + "_tgt"] = jsPlumb.addEndpoint(state.state_name, {
                            isTarget: true,
                            anchors: ["Top"],
                            maxConnections: -1,
                            connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                            endpoint: "Rectangle",
                            paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "blue" }
                        });
                    }
                } else {
                    $scope[state.state_name + "_src"] = jsPlumb.addEndpoint(state.state_name, {
                        isSource: true,
                        anchors: ["Bottom"],
                        deleteEndpointsOnDetach: false,
                        connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                        endpoint: ["Rectangle", { width: 10, height: 8 }],
                        paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "black" }

                    });
                    $scope[state.state_name + "_tgt"] = jsPlumb.addEndpoint(state.state_name, {
                        isTarget: true,
                        anchors: ["Top"],
                        maxConnections: -1,
                        connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                        endpoint: "Rectangle",
                        paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "blue" }
                    });
                }
            } else if (state.input_option == 'Star Rating') {
                if (state.multiple_conn == true) {
                    $scope[state.state_name + "_tgt"] = jsPlumb.addEndpoint(state.state_name, {
                        isTarget: true,
                        anchors: ["Top"],
                        maxConnections: -1,
                        connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                        endpoint: "Rectangle",
                        paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "blue" }
                    });
                } else {
                    $scope[state.state_name + "_tgt"] = jsPlumb.addEndpoint(state.state_name, {
                        isTarget: true,
                        anchors: ["Top"],
                        connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                        endpoint: "Rectangle",
                        paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "blue" }
                    });
                }
                var width = document.getElementById(state.state_name).offsetWidth;
                document.getElementById(state.state_name).style.width = width + 30 + "px";
                var left_val = 0.4;
                for (var k = 0; k < state.exp_in_mess.star_rating; k++) {
                    left_val = left_val + 0.1;
                    $scope[state.state_name + "_src_" + k] = jsPlumb.addEndpoint(state.state_name, {
                        isSource: true,
                        anchor: [
                            [left_val, 1.07, 1, 1, 1, -4]
                        ],
                        deleteEndpointsOnDetach: false,
                        connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                        endpoint: ["Rectangle", { width: 10, height: 8 }],
                        paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "black" }

                    });
                }
            } else if (state.condif == true) {

                if (state.multiple_conn == true) {
                    $scope[state.state_name + "_tgt"] = jsPlumb.addEndpoint(state.state_name, {
                        isTarget: true,
                        anchors: ["Top"],
                        maxConnections: -1,
                        connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                        endpoint: "Rectangle",
                        paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "blue" }
                    });
                } else {
                    $scope[state.state_name + "_tgt"] = jsPlumb.addEndpoint(state.state_name, {
                        isTarget: true,
                        anchors: ["Top"],
                        connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                        endpoint: "Rectangle",
                        paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "blue" }
                    });
                }
                var width = document.getElementById(state.state_name).offsetWidth;
                document.getElementById(state.state_name).style.width = width + 30 + "px";
                var left_val = 0.6;
                for (var k = 0; k < state.conditional_states.length; k++) {
                    left_val = left_val + 0.1;
                    $scope[state.state_name + "_src_" + k] = jsPlumb.addEndpoint(state.state_name, {
                        isSource: true,
                        anchor: [
                            [left_val, 1.07, 1, 1, 1, -4]
                        ],
                        deleteEndpointsOnDetach: false,
                        connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                        endpoint: ["Rectangle", { width: 10, height: 8 }],
                        paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "black" }

                    });
                }
            } else {

                $scope[state.state_name + "_src"] = jsPlumb.addEndpoint(state.state_name, {
                    isSource: true,
                    anchors: ["Bottom"],
                    deleteEndpointsOnDetach: false,
                    connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                    endpoint: ["Rectangle", { width: 10, height: 8 }],
                    paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "black" }

                });
                // console.log("src", state.state_name, $scope[state.state_name + "_src"]);
                $scope[state.state_name + "_tgt"] = jsPlumb.addEndpoint(state.state_name, {
                    isTarget: true,
                    anchors: ["Top"],
                    maxConnections: -1,
                    connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                    endpoint: "Rectangle",
                    paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "blue" }
                });
                // console.log("tgt", state.state_name, $scope[state.state_name + "_tgt"]);
            }
        } else if (state.condif == true) {

            if (state.multiple_conn == true) {
                $scope[state.state_name + "_tgt"] = jsPlumb.addEndpoint(state.state_name, {
                    isTarget: true,
                    anchors: ["Top"],
                    maxConnections: -1,
                    connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                    endpoint: "Rectangle",
                    paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "blue" }
                });
            } else {
                $scope[state.state_name + "_tgt"] = jsPlumb.addEndpoint(state.state_name, {
                    isTarget: true,
                    anchors: ["Top"],
                    connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                    endpoint: "Rectangle",
                    paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "blue" }
                });
            }
            var width = document.getElementById(state.state_name).offsetWidth;
            document.getElementById(state.state_name).style.width = width + 30 + "px";
            var left_val = 0.4;
            for (var k = 0; k < state.conditional_states.length; k++) {
                left_val = left_val + 0.1;
                $scope[state.state_name + "_src_" + k] = jsPlumb.addEndpoint(state.state_name, {
                    isSource: true,
                    anchor: [
                        [left_val, 1.07, 1, 1, 1, -4]
                    ],
                    deleteEndpointsOnDetach: false,
                    connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                    endpoint: ["Rectangle", { width: 10, height: 8 }],
                    paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "black" }

                });
            }
        } else {
            $scope[state.state_name + "_src"] = jsPlumb.addEndpoint(state.state_name, {
                isSource: true,
                anchors: ["Bottom"],
                deleteEndpointsOnDetach: false,
                connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                endpoint: ["Rectangle", { width: 10, height: 8 }],
                paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "black" }

            });
            // console.log("src1", state.state_name, $scope[state.state_name + "_src"]);
            if (state.multiple_conn == true) {
                $scope[state.state_name + "_tgt"] = jsPlumb.addEndpoint(state.state_name, {
                    isTarget: true,
                    anchors: ["Top"],
                    maxConnections: -1,
                    connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                    endpoint: "Rectangle",
                    paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "blue" }
                });
            } else {
                $scope[state.state_name + "_tgt"] = jsPlumb.addEndpoint(state.state_name, {
                    isTarget: true,
                    anchors: ["Top"],
                    connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                    endpoint: "Rectangle",
                    paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "blue" }
                });
            }
            // console.log("tgt1", state.state_name, $scope[state.state_name + "_tgt"]);
        }


        var gambit_init_mess = [];
        var button_states = [];
        $scope.indexval = 0;
        $scope.buttonindex = 0;

        $scope.buttonobj = {

        }


        $scope.text_mul_bub_Input = false;
        $scope.text_opt_Input = false;
        $scope.option = state.input_option;

        for (var i = 0; i < state.out_mess.length; i++) {
            $scope.obj = {

            }

            if (state.out_mess[i].text != "") {
                // console.log(state.out_mess[i].text, "this is the mess");
                $scope.obj["message"] = { "preview_url": state.out_mess[i].preview_url, "text": state.out_mess[i].text, "img_url": "", "title": "", "sub_title": "", "videoID": "", "img_form": "url", "image": "", "img_name": "", "buttons": state.out_mess[i].buttons, "mess_type": state.out_mess[i].mess_type, "media_type": state.out_mess[i].media_type, "media_id": state.out_mess[i].media_id };
                $scope.obj["input_fields"] = { "text": true, "image": false };
                $scope.obj["ellipsis" + $scope.indexval] = false;
                $scope.obj["plus" + $scope.indexval] = false;
                $scope.obj["picture" + $scope.indexval] = false;
                $scope.obj["times" + $scope.indexval] = false;
                $scope.obj["text" + $scope.indexval] = false;
                $scope.obj["video" + $scope.indexval] = false;

            } else {
                $scope.obj["message"] = { "preview_url": state.out_mess[i].preview_url, "text": "", "img_url": state.out_mess[i].img_url, "title": state.out_mess[i].title, "sub_title": state.out_mess[i].sub_title, "videoID": state.out_mess[i].videoID, "img_form": state.out_mess[i].img_form, "image": (state.out_mess[i].image.ngfName != undefined) ? '/uploads/' + $scope.botId + '/' + state.out_mess[i].image.ngfName : state.out_mess[i].image, "img_name": state.out_mess[i].img_name, "buttons": state.out_mess[i].buttons, "mess_type": state.out_mess[i].mess_type, "media_type": state.out_mess[i].media_type, "media_id": state.out_mess[i].media_id };
                $scope.obj["input_fields"] = { "text": false, "image": true };
                $scope.obj["ellipsis" + $scope.indexval] = false;
                $scope.obj["plus" + $scope.indexval] = false;
                $scope.obj["picture" + $scope.indexval] = false;
                $scope.obj["times" + $scope.indexval] = false;
                $scope.obj["text" + $scope.indexval] = false;
                $scope.obj["video" + $scope.indexval] = false;
            }
            if (state.out_mess[i].buttons != undefined) {
                $scope.obj["button_states"] = [];
                for (var j = 0; j < state.out_mess[i].buttons.length; j++) {
                    $scope.buttonobj["button_inputFields"] = { "url": false, "phone": false, "payload": false };
                    $scope.buttonobj["ellipsis" + $scope.buttonindex] = false;
                    $scope.buttonobj["plus" + $scope.buttonindex] = false;
                    $scope.buttonobj["external-link" + $scope.buttonindex] = false;
                    $scope.buttonobj["phone" + $scope.buttonindex] = false;
                    $scope.buttonobj["text" + $scope.buttonindex] = false;
                    $scope.buttonobj["times" + $scope.buttonindex] = false;
                    $scope.obj["button_states"].push($scope.buttonobj);
                }
            }
            gambit_init_mess.push($scope.obj);

        }

        // $scope.obj["button_message"+$scope.buttonindex] = { "url": "", "phone": "", "payload": "" };

        if ($scope.option == "Text") {
            $scope.exp_in_mess = {
                inputType: state.exp_in_mess.inputType,
                keyType: state.exp_in_mess.keyType,
                capitalizeText: state.exp_in_mess.capitalizeText,
                inputValidation: state.exp_in_mess.inputValidation,
                text_opt_Input: state.exp_in_mess.text_opt_Input,
                text_mul_bub_Input: state.exp_in_mess.text_mul_bub_Input,
                min_date: state.exp_in_mess.min_date,
                max_date: state.exp_in_mess.max_date,
                time: state.exp_in_mess.time,
                suggestions: '',
                suggestion_box: '',
                suggest_preview_list: $scope.suggest_preview_list,
                suggest_opt_input: $scope.suggest_opt_input,
                suggest_mul_bub_Input: $scope.suggest_mul_bub_Input
            }
        } else if ($scope.option == "Suggestion") {
            $scope.exp_in_mess = {
                inputType: state.exp_in_mess.inputType,
                suggestion_box: state.exp_in_mess.suggestion_box,
                suggestions: state.exp_in_mess.suggestions,
                suggest_preview_list: state.exp_in_mess.suggest_preview_list,
                suggest_opt_input: state.exp_in_mess.suggest_opt_input,
                suggest_mul_bub_Input: state.exp_in_mess.suggest_mul_bub_Input,
                suggest_branches: state.exp_in_mess.suggest_branches
            }
        } else if ($scope.option == "Carousel") {
            if (state.entry_response) {
                var carouselprop = {
                    "title": state.exp_in_mess.carousel[0].title,
                    "sub_title": state.exp_in_mess.carousel[0].sub_title,
                    "small_desc": state.exp_in_mess.carousel[0].small_desc,
                    "img_form": state.exp_in_mess.carousel[0].img_form,
                    "image": (state.exp_in_mess.carousel[0].image != undefined && state.exp_in_mess.carousel[0].image.ngfName != undefined) ? '/uploads/' + $scope.botId + '/carousel/' + state.exp_in_mess.carousel[0].image.ngfName : state.exp_in_mess.carousel[0].image,
                    "img_name": state.exp_in_mess.carousel[0].img_name,
                    "img_url": state.exp_in_mess.carousel[0].img_url,
                    "title2": state.exp_in_mess.carousel[0].title2,
                    "sub_title2": state.exp_in_mess.carousel[0].sub_title2,
                    "small_desc2": state.exp_in_mess.carousel[0].small_desc2,
                    "button_text": state.exp_in_mess.carousel[0].button_text,
                    "bt_act": state.exp_in_mess.carousel[0].bt_act,
                    "bt_payload": state.exp_in_mess.carousel[0].bt_payload,
                    "bt_link": state.exp_in_mess.carousel[0].bt_link,
                    "footer": state.exp_in_mess.carousel[0].footer
                };
                if (carouselprop.img_form == 'url') {
                    carouselprop.image = "";
                    carouselprop.img_name = "";
                } else if (carouselprop.img_form == 'upload') {
                    carouselprop.img_url = "";
                }
                $scope.exp_in_mess = {
                    carousel: [carouselprop]
                }
            } else {
                $scope.exp_in_mess = {
                    carousel: []
                }
                state.exp_in_mess.carousel.forEach(function(obj, ind) {
                    obj.image = (obj.image != undefined && obj.image.ngfName != undefined) ? '/uploads/' + $scope.botId + '/carousel/' + obj.image.ngfName : obj.image;
                    if (obj.img_form == 'url') {
                        obj.image = "";
                        obj.img_name = "";
                    } else if (obj.img_form == 'upload') {
                        obj.img_url = "";
                    }

                    $scope.exp_in_mess.carousel.push(obj);
                });
            }

        } else if ($scope.option == "Star Rating") {
            $scope.exp_in_mess = {
                star_rating: state.exp_in_mess.star_rating
            }
        } else {

        }
        $scope[state.state_name] = {
            entry_response: state.entry_response,
            send_res: state.send_res,
            res_timeout: state.res_timeout,
            state_name: state.state_name,
            out_mess: state.out_mess,
            carousel: state.carousel,
            template: state.template,
            carousel_message: state.carousel_message,
            template_option: state.template_option,
            state: state.state,
            exp_in_mess: $scope.exp_in_mess,
            next_state: "",
            input_option: $scope.option,
            sugg_conn_state: state.sugg_conn_state,
            gambit_init_mess: gambit_init_mess,
            connected_state: state.connected_state,
            button_states: button_states,
            multiple_conn: state.multiple_conn,
            api_type: state.api_type,
            res_timeout_type: state.res_timeout_type,
            api_url: state.api_url,
            previous_res: state.previous_res,
            condif: state.condif,
            conditional_states: state.conditional_states,
            cond_conn_state: state.cond_conn_state,
            cond_details: state.cond_details,
            timeout_message: state.timeout_message,
            timeout_state: state.timeout_state,
            timeout_url: state.timeout_url,
            timeout_delay: state.timeout_delay,
            url_params: state.url_params,
            expresType: state.expresType,
            enable_multilang: state.enable_multilang,
            language: state.language,
            previous_message: state.previous_message,
            ent_api_details: state.ent_api_details,
            out_api_details: state.out_api_details,
            optional_input: state.optional_input
        }

        if ($scope.option == "Star Rating") {
            $scope[state.state_name]['star_states'] = state.star_states
        }
        $scope.final_obj.push($scope[state.state_name]);
        // console.log($scope[state.state_name], "to check button");
        if (state.template_option == 'form') {
            $scope[state.state_name]['forms'] = state.forms;
        }

    }
    $scope.options = [
        "Text",
        "Suggestion",
        "Carousel",
        "Location",
        "Upload",
        "None",
        "Star Rating"
    ];

    $scope.change = function(statenum, option) {
        // console.log("funct", option);
        if (option == "Text") {
            $scope.suggest_branches = false;
            $scope.exp_in_mess = {
                inputType: "Custom Text",
                keyType: "Normal Keyboard",
                capitalizeText: "Disabled",
                inputValidation: "No Validation",
                text_opt_Input: $scope.text_opt_Input,
                text_mul_bub_Input: $scope.text_mul_bub_Input,
                suggestions: '',
                suggestion_box: '',
                suggest_preview_list: $scope.suggest_preview_list,
                suggest_opt_input: $scope.suggest_opt_input,
                suggest_mul_bub_Input: $scope.suggest_mul_bub_Input
            }
            $scope['state' + statenum].exp_in_mess = $scope.exp_in_mess;
        } else if (option == "Suggestion") {
            $scope.suggest_branches = true;
            $scope.exp_in_mess = {
                inputType: "Custom Text",
                suggestions: 'Custom List',
                suggestion_box: [],
                suggest_preview_list: $scope.suggest_preview_list,
                suggest_opt_input: $scope.suggest_opt_input,
                suggest_mul_bub_Input: $scope.suggest_mul_bub_Input,
                suggest_branches: $scope.suggest_branches
            }
            $scope['state' + statenum].exp_in_mess = $scope.exp_in_mess;
        } else if (option == "Carousel") {

            $scope.suggest_branches = false;
            var carouselprop = {
                "title": "",
                "sub_title": "",
                "small_desc": "",
                "img_form": "url",
                "image": "",
                "img_name": "",
                "img_url": "",
                "title2": "",
                "sub_title2": "",
                "small_desc2": "",
                "button_text": "",
                "bt_act": "",
                "bt_payload": "",
                "bt_link": "",
                "footer": ""
            };
            $scope.exp_in_mess = {
                carousel: [carouselprop]
            }
            $scope['state' + statenum].exp_in_mess = $scope.exp_in_mess;

        } else {
            $scope.suggest_branches = false;
        }
    }
    $scope.state_img = [];


    $scope.images = [];
    $scope.bot_images = [];
    $scope.mess_img = function(file, i, statenum) {
        var fileexts = ['jpg', 'jpeg', 'png', 'gif', 'doc', 'docx', 'svg', 'xls', 'xlsx', 'pdf', '3g2', '3gp', 'mp4', 'mpeg', 'mpg', 'txt', 'ppt', 'pptx', 'aac', 'm4a', 'amr', 'mp3', 'ogg', 'opus'];
        var filename = file.name;
        var n = filename.lastIndexOf('.');
        var ext = filename.substring(n + 1);
        if (fileexts.indexOf(ext) != -1) {
            filename = "state" + statenum + "_mess" + i + "." + ext;
            if ($scope.bot_images.length > 0) {
                var index = 0;
                angular.forEach($scope.bot_images, function(value, key) {
                    if (value.ngfName == filename) {
                        $scope.bot_images.splice(index, 1);
                    }
                    index = index + 1;
                });
            }
            file = Upload.rename(file, filename);
            $scope['state' + statenum].gambit_init_mess[i].message.img_name = filename;
            // $scope['state' + statenum].gambit_init_mess[i].message.image = filename;
            $scope.bot_images.push(file);
        } else {
            $scope.showSimpleToast("Please upload appropiate file type.", "danger");
        }
    }
    $scope.settings_img = function(file, newname, scopevar) {
        // console.log(file);
        var filename = file.name;
        var n = filename.lastIndexOf('.');
        var ext = filename.substring(n + 1);
        filename = newname + "." + ext;
        if ($scope.bot_images.length > 0) {
            var index = 0;
            angular.forEach($scope.bot_images, function(value, key) {
                if (value.ngfName == filename) {

                    $scope.bot_images.splice(index, 1);
                }
                index = index + 1;
            });
        }
        var field = scopevar;
        file = Upload.rename(file, filename);
        $scope[field] = filename;
        $scope.bot_images.push(file);

    }

    $scope.imgfun = function(file, i, statenum) {
        var filename = file.name;
        var n = filename.lastIndexOf('.');
        var ext = filename.substring(n + 1);
        filename = "state" + statenum + "_car" + i + "." + ext;
        if ($scope.images.length > 0) {
            var index = 0;
            angular.forEach($scope.images, function(value, key) {
                if (value.ngfName == filename) {
                    $scope.images.splice(index, 1);
                }
                index = index + 1;
            });
        }

        file = Upload.rename(file, filename);
        $scope['state' + statenum].exp_in_mess.carousel[i].img_name = filename;
        $scope.images.push(file);
    }
    $scope.enable_text_input = function(statenum, ind) {
        $scope['state' + statenum].gambit_init_mess[ind].input_fields.text = true;
        $scope['state' + statenum].gambit_init_mess[ind].input_fields.image = false;
        // $scope['state' + statenum].gambit_init_mess[ind].input_fields.video = false;
        // $scope['state' + statenum].gambit_init_mess[ind].message.mess_type = "text";
    }
    $scope.enable_image_input = function(statenum, ind) {
            $scope['state' + statenum].gambit_init_mess[ind].input_fields.text = false;
            $scope['state' + statenum].gambit_init_mess[ind].input_fields.image = true;
            // $scope['state' + statenum].gambit_init_mess[ind].input_fields.video = false;
            // $scope['state' + statenum].gambit_init_mess[ind].message.mess_type = "media";

        }
        // $scope.crtstateobj = function(statename, statenum) {

    //     if ($scope[statename].state) {
    //         $('#' + statename + ' .title').text($scope[statename].state + '[' + statenum + ']');
    //     }
    //     if ($scope.final_obj.length == 0) {
    //         $scope[statename].out_mess = [];
    //         for (var i = 0; i < $scope[statename].gambit_init_mess.length; i++) {
    //             if ($scope[statename].gambit_init_mess[i].message.image['$ngfBlobUrl'] != undefined) {
    //                 delete $scope[statename].gambit_init_mess[i].message.image['$ngfBlobUrl'];
    //             }
    //             $scope[statename].out_mess.push($scope[statename].gambit_init_mess[i].message);
    //         }

    //         $scope.final_obj.push($scope[statename]);
    //         // console.log($scope.final_obj, "i = 0");
    //         $mdDialog.hide();
    //     } else {

    //         for (var i = 0; i < $scope.final_obj.length; i++) {

    //             if ($scope.final_obj[i].state_name == statename) {
    //                 // console.log("found");
    //                 $scope.final_obj.splice(i, 1);
    //                 $scope[statename].out_mess = [];
    //                 for (var i = 0; i < $scope[statename].gambit_init_mess.length; i++) {
    //                     if ($scope[statename].gambit_init_mess[i].message.image['$ngfBlobUrl'] != undefined) {
    //                         delete $scope[statename].gambit_init_mess[i].message.image['$ngfBlobUrl'];
    //                     }
    //                     $scope[statename].out_mess.push($scope[statename].gambit_init_mess[i].message);
    //                 }
    //                 $scope.final_obj.push($scope[statename]);
    //                 // console.log($scope.final_obj, "here in for");
    //                 $mdDialog.hide();
    //                 break;
    //             } else {
    //                 if (i == $scope.final_obj.length - 1) {
    //                     $scope[statename].out_mess = [];
    //                     for (var i = 0; i < $scope[statename].gambit_init_mess.length; i++) {
    //                         if ($scope[statename].gambit_init_mess[i].message.image['$ngfBlobUrl'] != undefined) {
    //                             delete $scope[statename].gambit_init_mess[i].message.image['$ngfBlobUrl'];
    //                         }
    //                         $scope[statename].out_mess.push($scope[statename].gambit_init_mess[i].message);
    //                     }
    //                     $scope.final_obj.push($scope[statename]);
    //                     // console.log($scope.final_obj, "in for");
    //                     $mdDialog.hide();
    //                 }
    //             }
    //         }
    //     }

    //     // console.log($scope[statename].input_option, "option");
    //     if ($scope[statename].input_option == "Suggestion") {
    //         if ($scope[statename].exp_in_mess.suggest_branches == true) {
    //             var state_endpoints = jsPlumb.getEndpoints(statename);
    //             // console.log(state_endpoints);
    //             var length = $scope[statename].exp_in_mess.suggestion_box.length;
    //             var endlength = state_endpoints.length - 1;
    //             var newanchor = length - endlength;

    //             // console.log($scope[statename].exp_in_mess.suggestion_box.length, "length of sugg", endlength, newanchor, state_endpoints.length);
    //             if (state_endpoints.length == 2) {
    //                 var left_val = 0.6;
    //                 var j = 0;
    //             } else if (state_endpoints.length - 1 > length) {
    //                 // console.log("it is greater", state_endpoints.length, length);
    //                 for (var i = (state_endpoints.length - 1); i >= (length + 1); i--) {
    //                     var ep_id = state_endpoints[i];
    //                     jsPlumb.deleteEndpoint(ep_id);
    //                 }
    //             } else {
    //                 left_val = 0.6 + ((endlength - 1) * 0.1);
    //                 j = 0;
    //             }

    //             for (var i = 0; i < newanchor; i++) {
    //                 var common = {
    //                     isSource: true,
    //                     connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }]
    //                 };
    //                 if (j > 0) {
    //                     left_val = left_val + 0.1;
    //                 }
    //                 j = j + 1;
    //                 var state = statename;
    //                 if (length > 1) {
    //                     var width = document.getElementById(state).offsetWidth;
    //                     document.getElementById(state).style.width = width + 10 + "px";
    //                 }
    //                 jsPlumb.addEndpoint(state, {
    //                     anchor: [
    //                         [left_val, 1.07, 1, 1, 1, -4]
    //                     ],
    //                     maxConnections: 1,
    //                     connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
    //                     endpoint: ["Rectangle", { width: 10, height: 9 }],
    //                     paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "black" }
    //                 }, common);
    //                 // if(i == newanchor - 1){
    //                 //   jsPlumb.recalculateOffsets(state);
    //                 // }
    //             }
    //             jsPlumb.recalculateOffsets(state)
    //         } else {
    //             var endpoints = jsPlumb.getEndpoints(statename);
    //             // console.log(endpoints, "ep");
    //             if (endpoints.length > 2) {
    //                 for (var i = 2; i < endpoints.length; i++) {
    //                     var ep_id = endpoints[i];
    //                     jsPlumb.deleteEndpoint(ep_id);
    //                 }
    //             }
    //         }
    //     } else if ($scope[statename].input_option != "Suggestion" && $scope[statename].condif == false && $scope[statename].input_option != "Star Rating") {
    //         var endpoints = jsPlumb.getEndpoints(statename);
    //         // console.log(endpoints, "ep");
    //         if (endpoints.length > 2) {
    //             for (var i = 2; i < endpoints.length; i++) {
    //                 var ep_id = endpoints[i];
    //                 jsPlumb.deleteEndpoint(ep_id);
    //             }
    //         }

    //     } else {

    //     }

    //     if ($scope[statename].input_option == "Star Rating") {
    //         var endpoints = jsPlumb.getEndpoints(statename);
    //         var length = $scope[statename].exp_in_mess.star_rating;
    //         // console.log(endpoints.length, length);
    //         var endlength = endpoints.length - 1;
    //         var starcount = length - endlength;

    //         if (endpoints.length == 2) {
    //             var left_val = 0.6;
    //             var j = 0;
    //         } else if (endpoints.length > length) {
    //             var starlength = parseInt(length);
    //             // console.log("in else", starlength);
    //             for (var i = (endpoints.length - 1); i >= (starlength + 1); i--) {
    //                 // console.log(i, "in for", endpoints, endpoints[i], ep_id);
    //                 var ep_id = endpoints[i];
    //                 jsPlumb.deleteEndpoint(ep_id);
    //             }
    //         } else {
    //             left_val = 0.6 + ((endlength - 1) * 0.1);
    //             j = 0;
    //         }

    //         for (var i = 0; i < starcount; i++) {
    //             var common = {
    //                 isSource: true,
    //                 maxConnections: 1,
    //                 deleteEndpointsOnDetach: false,
    //                 connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
    //                 endpoint: ["Rectangle", { width: 10, height: 8 }],
    //                 paintStyle: { strokeStyle: "blue", strokeWidth: 3, fillStyle: "black" }
    //             };
    //             if (j > 0) {
    //                 left_val = left_val + 0.1;
    //             }
    //             j = j + 1;
    //             var state = statename;
    //             if (length > 1) {
    //                 var width = document.getElementById(state).offsetWidth;
    //                 document.getElementById(state).style.width = width + 20 + "px";
    //             }
    //             var newval = i * 5;
    //             jsPlumb.addEndpoint(state, {
    //                 anchor: [
    //                     [left_val, 1.07, 1, 1, 1, -4], "Bottom"
    //                 ],
    //                 // anchor:[ "Bottom","Bottom"],
    //             }, common);
    //             if (i == starcount - 1) {
    //                 //   $scope.repaint();

    //             }
    //         }
    //     }
    //     if ($scope[statename].condif == true) {
    //         var endpoints = jsPlumb.getEndpoints(statename);
    //         var exsists = $scope[statename].conditional_states.indexOf('Default');
    //         if (exsists == -1) {
    //             $scope[statename].conditional_states.push('Default');
    //         }

    //         $scope[statename]['cond_exps'] = [];
    //         var exp = "";
    //         var inter_exp = "";
    //         for (var j = 0; j < $scope[statename].cond_details.A.length; j++) {

    //             for (var k = 0; k < $scope[statename].cond_details.A[j].cond.length; k++) {


    //                 if ($scope[statename].cond_details.A[j].cond[k].cond == "is") {
    //                     inter_exp = "=="
    //                 } else if ($scope[statename].cond_details.A[j].cond[k].cond == "is not") {
    //                     inter_exp = "!="
    //                 } else if ($scope[statename].cond_details.A[j].cond[k].cond == "equals") {
    //                     inter_exp = "==="
    //                 } else if ($scope[statename].cond_details.A[j].cond[k].cond == "not equals") {
    //                     inter_exp = "!="
    //                 } else if ($scope[statename].cond_details.A[j].cond[k].cond == "less than") {
    //                     inter_exp = "<"
    //                 } else if ($scope[statename].cond_details.A[j].cond[k].cond == "greater than") {
    //                     inter_exp = ">"
    //                 } else {
    //                     // string.indexOf(substring) !== -1
    //                 }
    //                 if (k == 0) {

    //                     exp = "(" + $scope[statename].cond_details.A[j].cond[k].field + " " + inter_exp + " '" + $scope[statename].cond_details.A[j].cond[k].value + "' )";
    //                 } else {
    //                     var cond_exp = "";
    //                     if ($scope[statename].cond_details.A[j].cond[k].intercond == "OR") {
    //                         cond_exp = "||"
    //                     } else {
    //                         cond_exp = "&&"
    //                     }
    //                     exp = exp + " " + cond_exp + "(" + $scope[statename].cond_details.A[j].cond[k].field + " " + inter_exp + "  '" + $scope[statename].cond_details.A[j].cond[k].value + "' )";
    //                 }
    //                 if (k == $scope[statename].cond_details.A[j].cond.length - 1) {

    //                     $scope[statename]['cond_exps'].push(exp)

    //                 }

    //             }
    //         }

    //         var length = $scope[statename].cond_details.A.length + 1;
    //         var endlength = endpoints.length - 1;
    //         var newanchor = length - endlength;
    //         if (endpoints.length == 2) {
    //             var left_val = 0.6;
    //             var j = 0;
    //         } else if (endpoints.length > length) {

    //             for (var i = (endpoints.length - 1); i >= (length + 1); i--) {


    //                 var ep_id = endpoints[i];
    //                 jsPlumb.deleteEndpoint(ep_id);
    //             }
    //         } else {
    //             left_val = 0.6 + ((endlength - 1) * 0.1);
    //             j = 0;
    //         }
    //         for (var i = 0; i < newanchor; i++) {
    //             var common = {
    //                 isSource: true,
    //                 connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
    //                 endpoint: ["Rectangle", { width: 10, height: 8 }],
    //                 paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "black" }
    //             };
    //             if (j > 0) {
    //                 left_val = left_val + 0.1;
    //             }
    //             j = j + 1;
    //             var state = statename;
    //             if (length > 1) {
    //                 var width = document.getElementById(state).offsetWidth;
    //                 document.getElementById(state).style.width = width + 10 + "px";
    //             }

    //             jsPlumb.addEndpoint(state, {
    //                 anchor: [
    //                     [left_val, 1.07, 1, 1, 1, -4], "Continuous"
    //                 ],
    //                 maxConnections: 1,
    //                 connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
    //                 endpoint: ["Rectangle", { width: 10, height: 9 }],
    //                 paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "black" }
    //             }, common);
    //             if (i == newanchor - 1) {
    //                 // $scope.repaint();
    //                 jsPlumb.recalculateOffsets(state);
    //             }
    //         }
    //         //jsPlumb.recalculateOffsets(state)
    //     } else {

    //     }
    // }

    $scope.addnewtxt = function() {
        // console.log("function called");
        $scope.indexval = $scope.indexval + 1;
        var text = "text" + $scope.indexval;
        $scope.obj = {

        }
        $scope.obj["message"] = { "preview_url": false, "text": "", "img_url": "", "title": "", "sub_title": "", "mess_type": "text", "media_type": "image", "media_id": "" };
        $scope.obj["input_fields"] = { "text": true, "image": false };
        $scope.obj["ellipsis" + $scope.indexval] = false;
        $scope.obj["plus" + $scope.indexval] = false;
        $scope.obj["picture" + $scope.indexval] = false;
        $scope.obj["times" + $scope.indexval] = false;
        $scope.obj["text" + $scope.indexval] = false;
        $scope.buttonobj["button_inputFields"] = { "url": false, "phone": false, "payload": false };
        $scope.buttonobj["ellipsis" + $scope.buttonindex] = false;
        $scope.buttonobj["plus" + $scope.buttonindex] = false;
        $scope.buttonobj["external-link" + $scope.buttonindex] = false;
        $scope.buttonobj["phone" + $scope.buttonindex] = false;
        $scope.buttonobj["text" + $scope.buttonindex] = false;
        $scope.buttonobj["times" + $scope.buttonindex] = false;
        $scope.obj["button_states"] = [];
        $scope.obj["button_states"].push($scope.buttonobj);
        $scope['state' + $scope.statenum].gambit_init_mess.push($scope.obj);
    }

    $scope.show_option = function(e) {

        // console.log(e, e.currentTarget.dataset.value);
        var ind = e.currentTarget.dataset.value;
        $scope['state' + $scope.statenum].gambit_init_mess[ind]["ellipsis" + ind] = true;
        $scope['state' + $scope.statenum].gambit_init_mess[ind]["plus" + ind] = true;

    }
    $scope.hide_option = function(e) {
        // console.log(e.currentTarget.dataset.index, e.currentTarget.dataset.value);
        var ind = e.currentTarget.dataset.value;

        $scope['state' + $scope.statenum].gambit_init_mess[ind]["ellipsis" + ind] = false;
        $scope['state' + $scope.statenum].gambit_init_mess[ind]["picture" + ind] = false;
        $scope['state' + $scope.statenum].gambit_init_mess[ind]["times" + ind] = false;
        $scope['state' + $scope.statenum].gambit_init_mess[ind]["plus" + ind] = false;
        $scope['state' + $scope.statenum].gambit_init_mess[ind]["text" + ind] = false;

    }

    $scope.remove_message = function(statenum, ind) {
        $scope['state' + statenum].gambit_init_mess.splice(ind, 1);
    }

    $scope.save = function() {
        var db_obj = {
            botId: $scope.botId,
            botName: $scope.bot.Name,
            titlebarColor: $scope.band.Color,
            botDesc: $scope.bot.Description,
            webLink: $scope.web.link,
            postemail: $scope.post.email,
            tog_brand_icon: $scope.tog_brand.icon,
            tog_normal_chat: $scope.tog_normal.chat,
            tog_whatsapp_chat: $scope.tog_whatsapp.chat,
            titleColor: $scope.title.Color,
            textfieldColor: $scope.textfield.Color,
            buttonhoverColor: $scope.buttonhover.Color,
            backgroundColor: $scope.background.Color,
            bbubbleColor: $scope.bbubble.Color,
            carousel_background: $scope.carouselbackground.Color,
            carousel_button: $scope.carousel.button,
            ububbleColor: $scope.ububble.Color,
            buttonColor: $scope.button.Color,
            suggesColor: $scope.sugges.Color,
            fontColor: $scope.font.Color,
            botImage: $scope.bot_img_logo,
            webImage: $scope.web_bg_logo,
            botBgImage: $scope.bot_bg_logo,
            botFlow: $scope.final_obj,
            startState: $scope.startState,
            userPhoneNumber: $scope.user.phone_number,
            timeStamp: $scope.time.stamp,
            sender_id: $scope.sender.id
        }
        for (var i = 0; i < $scope.final_obj.length; i++) {
            // console.log($scope.final_obj[i].state_name);

            // console.log(angular.element('#' + $scope.final_obj[i].state_name).prop('offsetLeft'), angular.element('#' + $scope.final_obj[i].state_name).prop('offsetTop'));
            $scope.final_obj[i]['left'] = angular.element('#' + $scope.final_obj[i].state_name).prop('offsetLeft');
            $scope.final_obj[i]['top'] = angular.element('#' + $scope.final_obj[i].state_name).prop('offsetTop');
            if (i == $scope.final_obj.length - 1) {
                // console.log($scope.final_obj);

                $http.defaults.headers.common['user'] = $window.sessionStorage.id;
                $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;

                $http.put('/update_bot', db_obj, {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function(res) {
                    // console.log(res);
                    if (res.data.status == "success") {
                        $scope.showSimpleToast("Successfully Updated", "success");
                        $scope.upload($scope.images);
                    }

                })
            }
        }
    }

    $scope.show_icons = function(e) {
        var ind = e.currentTarget.dataset.value;
        if ($scope['state' + $scope.statenum].gambit_init_mess[ind]["picture" + ind] == true) {
            $scope['state' + $scope.statenum].gambit_init_mess[ind]["picture" + ind] = false;
            $scope['state' + $scope.statenum].gambit_init_mess[ind]["times" + ind] = false;
            $scope['state' + $scope.statenum].gambit_init_mess[ind]["text" + ind] = false;
            $scope['state' + $scope.statenum].gambit_init_mess[ind]["video" + ind] = false;
        } else {
            $scope['state' + $scope.statenum].gambit_init_mess[ind]["picture" + ind] = true;
            $scope['state' + $scope.statenum].gambit_init_mess[ind]["times" + ind] = true;
            $scope['state' + $scope.statenum].gambit_init_mess[ind]["text" + ind] = true;
            $scope['state' + $scope.statenum].gambit_init_mess[ind]["video" + ind] = true;
        }
    }

    $scope.showAlert = function(ev) {
        $scope.statename = ev.currentTarget.id;
        var str = $scope.statename;
        var statenum = str.split("state");
        $scope.statenum = statenum[1];
        $modal.open({
            templateUrl: 'dialog1.tmpl.html',
            controller: function($scope) {

                $scope.prevvalue = function(statenum) {
                    $scope.previous = $scope["state" + statenum].state;
                }
                if ($scope.user.phone_number == true || $scope.user.phone_number == "true") {
                    var exists = $scope.apivalues.indexOf("bot_set_phonenumber");
                    if (exists == -1) {
                        $scope.apivalues.push("bot_set_phonenumber");
                    }
                } else {
                    var ind = $scope.apivalues.indexOf("bot_set_phonenumber");
                    if (ind != -1) {
                        $scope.apivalues.splice(ind, 1);
                    }
                }

                if ($scope.time.stamp == true || $scope.time.stamp == "true") {
                    var exists = $scope.apivalues.indexOf("bot_set_timestamp");
                    if (exists == -1) {
                        $scope.apivalues.push("bot_set_timestamp");
                    }
                } else {
                    var ind = $scope.apivalues.indexOf("bot_set_timestamp");
                    if (ind != -1) {
                        $scope.apivalues.splice(ind, 1);
                    }
                }
                $scope.clear_image_type = function(statenum, ind) {
                    if ($scope['state' + statenum].exp_in_mess.carousel[ind].img_form == 'url') {
                        $scope['state' + statenum].exp_in_mess.carousel[ind].image = "";
                        $scope['state' + statenum].exp_in_mess.carousel[ind].img_name = "";
                    } else if ($scope['state' + statenum].exp_in_mess.carousel[ind].img_form == 'upload') {
                        $scope['state' + statenum].exp_in_mess.carousel[ind].img_url = "";
                    }
                }
                $scope.updatestatename = function(statenum) {
                    var name = $scope["state" + statenum].state;
                    var exists = $scope.apivalues.indexOf($scope.previous);
                    if (exists == -1) {
                        $scope.apivalues.push(name);
                        if ($scope['state' + $scope.statenum]["ent_api_details"] != undefined) {
                            var apivalues = $scope.apivalues.filter(e => e !== $scope['state' + statenum].state);
                            $scope['state' + $scope.statenum]["ent_api_details"].apires[0][$scope['state' + $scope.statenum]["ent_api_details"].apikey[0].mainkey] = apivalues[0];
                        }
                        if ($scope['state' + $scope.statenum]["out_api_details"] != undefined) {
                            var apivalues = $scope.apivalues.filter(e => e !== $scope['state' + statenum].state);
                            $scope['state' + $scope.statenum]["out_api_details"].apires[0][$scope['state' + $scope.statenum]["out_api_details"].apikey[0].mainkey] = apivalues[0];
                        }
                    } else {
                        $scope.apivalues[exists] = name;
                    }
                }
                $scope.ok = function() {
                        // $scope.$close();
                        $scope.$dismiss();
                        $('.modal-backdrop').remove()
                        $(document.body).removeClass("modal-open");
                    }
                    // $scope['state' + $scope.statenum].state = 'state' + $scope.statenum;
                $scope.msgmenu = true;

                // suggestion list
                $scope.suggestionList = function(suggests) {

                    if (suggests === 'Days of Week') {
                        $scope['state' + $scope.statenum].exp_in_mess.suggestion_box = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
                    }

                    if (suggests === 'Months of Year') {
                        $scope['state' + $scope.statenum].exp_in_mess.suggestion_box = ["January", "Febrauary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                    }
                    if (suggests === 'Custom List') {
                        $scope['state' + $scope.statenum].exp_in_mess.suggestion_box = [];
                    }
                };

                $scope.showconfigs = function(type) {
                    switch (type) {
                        case 'msgsett':
                            $scope.msgmenu = false;
                            $scope.botmsgsett = true;
                            $scope.usrresptimeout = false;
                            $scope.apiintg = false;
                            $scope.condjump = false;
                            $scope.urlparams = false;
                            $scope['state' + $scope.statenum].template_option = $scope['state' + $scope.statenum].template_option;
                            if ($scope['state' + $scope.statenum].enable_multilang) {
                                $timeout(function() {
                                    $scope.onLoad1();
                                    var dropdown = document.getElementById('languageDropDown');
                                    let arr = Array.from(dropdown.options);
                                    var index = arr.findIndex(x => x.value == $scope['state' + $scope.statenum].language);
                                    dropdown.selectedIndex = index;
                                    dropdown.options[dropdown.selectedIndex].value = $scope['state' + $scope.statenum].language;
                                }, 1000);

                            }
                            break;

                        case 'restimout':
                            $scope.msgmenu = false;
                            $scope.botmsgsett = false;
                            $scope.usrresptimeout = true;
                            $scope.apiintg = false;
                            $scope.condjump = false;
                            $scope.urlparams = false;
                            break;

                        case 'api':
                            $scope.msgmenu = false;
                            $scope.botmsgsett = false;
                            $scope.usrresptimeout = false;
                            $scope.apiintg = true;
                            $scope.condjump = false;
                            $scope.urlparams = false;
                            break;

                        case 'cond':
                            $scope.msgmenu = false;
                            $scope.botmsgsett = false;
                            $scope.usrresptimeout = false;
                            $scope.apiintg = false;
                            $scope.condjump = true;
                            $scope.urlparams = false;
                            break;

                        case 'urlparam':
                            $scope.msgmenu = false;
                            $scope.botmsgsett = false;
                            $scope.usrresptimeout = false;
                            $scope.apiintg = false;
                            $scope.condjump = false;
                            $scope.urlparams = true;
                            break;

                        default:
                            $scope.msgmenu = false;
                            $scope.botmsgsett = true;
                            $scope.usrresptimeout = false;
                            $scope.apiintg = false;
                            $scope.condjump = false;
                            $scope.urlparams = false;
                            break;
                    }
                }
                $scope.backtomenu = function() {
                        $scope.msgmenu = true;
                        $scope.botmsgsett = false;
                        $scope.usrresptimeout = false;
                        $scope.apiintg = false;
                        $scope.condjump = false;
                        $scope.urlparams = false;
                    }
                    // api code starts

                $scope.addapiintegration = function(statenum) {
                    if ($scope['state' + statenum].send_res) {
                        // var apivalues = $scope.apivalues.filter(e => e !== $scope['state' + statenum].state + '.ans');
                        var api_obj = {
                            "method": "get",
                            "url": "",
                            "params": [],
                            "body": [],
                            "headerparams": [],
                            "auth": {},
                            "showparams": false,
                            "body_type": "raw",
                            "raw_json": JSON.stringify({}),
                            "api_name": "",
                            "apires": [{}],
                            "api_url": "",
                            "api_id": ""
                        }
                        $scope['state' + statenum]["out_api_details"] = api_obj;
                        $http.defaults.headers.common['user'] = $window.sessionStorage.id;
                        $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
                        $scope.apiintlist = [];
                        $scope.apiintkeys = [{ mainkey: "", subkey: "", apivalue: "" }];
                        $http.get('/getapis', {}).then(function(res) {
                            for (var i = 0; i < res.data.length; i++) {
                                $scope.apiintlist[i] = {
                                    api_id: res.data[i]._id,
                                    api_name: (res.data[i].ApiName == undefined) ? res.data[i].api_info.url : res.data[i].ApiName,
                                    method: res.data[i].api_info.method,
                                    params: res.data[i].api_info.params,
                                    body: res.data[i].api_info.body,
                                    headerparams: res.data[i].api_info.headerparams,
                                    showparams: res.data[i].api_info.showparams,
                                    url: res.data[i].api_info.url,
                                    body_type: res.data[i].api_info.body_type,
                                    raw_json: res.data[i].api_info.raw_json
                                };
                            }
                            $scope['state' + statenum]['out_api_details'].api_url = $scope.apiintlist[0];
                            $scope.getapiintres(statenum, $scope['state' + statenum]['out_api_details'].api_url, "change");
                        });
                    }
                }
                $scope.addentryres = function(statenum) {
                    // $scope['state' + statenum].entry_response;
                    if ($scope['state' + statenum].entry_response == true) {
                        // var apivalues = $scope.apivalues.filter(e => e !== $scope['state' + statenum].state);
                        var api_obj = {
                            "method": "get",
                            "url": "",
                            "params": [],
                            "body": [],
                            "headerparams": [],
                            "auth": {},
                            "showparams": false,
                            "body_type": "raw",
                            "raw_json": JSON.stringify({}),
                            "apikeys": [{ mainkey: "", subkey: "", apivalue: "", apires: [] }],
                            //"apikeys": [{ mainkeys: [], subkeys: [], mainkey: "", subkey: "", apivalues: apivalues, apivalue: "", apires: [] }],
                            "api_name": "",
                            "apilist": [],
                            "apires": [{}],
                            "api_id": "",
                            "failure_state": ""
                        }
                        $scope['state' + statenum]["ent_api_details"] = api_obj;
                        $scope.entry_response_msg = "";
                        $http.defaults.headers.common['user'] = $window.sessionStorage.id;
                        $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
                        $scope.apilist = [];
                        $scope.apikeys = [{ mainkey: "", subkey: "", apivalue: "" }];
                        $http.get('/getapis', {}).then(function(res) {
                            for (var i = 0; i < res.data.length; i++) {
                                $scope.apilist[i] = {
                                    api_id: res.data[i]._id,
                                    api_name: (res.data[i].ApiName == undefined) ? res.data[i].api_info.url : res.data[i].ApiName,
                                    method: res.data[i].api_info.method,
                                    params: res.data[i].api_info.params,
                                    body: res.data[i].api_info.body,
                                    headerparams: res.data[i].api_info.headerparams,
                                    showparams: res.data[i].api_info.showparams,
                                    url: res.data[i].api_info.url,
                                    body_type: res.data[i].api_info.body_type,
                                    raw_json: res.data[i].api_info.raw_json
                                };
                            }
                            $scope['state' + statenum].api_name = $scope.apilist[0];
                            $scope.getapires(statenum, $scope['state' + statenum].api_name, "change");
                        });
                    } else {
                        $scope['state' + statenum]["ent_api_details"] = {};
                        $scope.entry_response_msg = "Take Gambit Entry Api Response";
                    }
                }
                $scope.apiparams = [{
                    "paramkey": "",
                    "paramvalue": ""
                }]
                $scope.apiintparams = [{
                    "paramkey": "",
                    "paramvalue": ""
                }]
                $scope.headerparams = [{ "paramkey": "", "paramvalue": "" }]
                $scope.headerintparams = [{ "paramkey": "", "paramvalue": "" }]

                $scope.addparams = function(statenum) {
                    if ($scope['state' + statenum]["ent_api_details"].params == undefined) {
                        $scope['state' + statenum]["ent_api_details"].params = [];
                    }
                    var obj = {
                        "paramkey": "",
                        "paramvalue": ""
                    }
                    $scope['state' + statenum]["ent_api_details"].params.push(obj);
                }
                $scope.addintparams = function(statenum) {
                    if ($scope['state' + statenum]["out_api_details"].params == undefined) {
                        $scope['state' + statenum]["out_api_details"].params = [];
                    }
                    var obj = {
                        "paramkey": "",
                        "paramvalue": ""
                    }
                    $scope['state' + statenum]["out_api_details"].params.push(obj);
                }
                $scope.removeparams = function(statenum, paramsindex) {

                    $scope['state' + statenum]["ent_api_details"].params.splice(paramsindex, 1);
                }
                $scope.removeintparams = function(statenum, paramsindex) {

                    $scope['state' + statenum]["out_api_details"].params.splice(paramsindex, 1);
                }

                $scope.addurlparams = function(statenum) {
                    if ($scope['state' + statenum]["url_params"] == undefined) {
                        $scope['state' + statenum]["url_params"] = [];
                    }
                    var obj = {
                        "paramname": ""
                    }
                    $scope['state' + statenum]["url_params"].push(obj);
                }
                $scope.removeurlparams = function(statenum, paramsindex) {

                    $scope['state' + statenum]["url_params"].splice(paramsindex, 1);
                }

                $scope.rmhdrparams = function(statenum, paramsindex) {

                        $scope['state' + statenum]["ent_api_details"].headerparams.splice(paramsindex, 1);
                    }
                    // $scope.rmhdrparams = function(statenum, paramsindex) {

                //     $scope['state' + statenum]["ent_api_details"].headerparams.splice(paramsindex, 1);
                // }
                $scope.rmbodyparams = function(statenum, paramsindex) {

                    $scope['state' + statenum]["ent_api_details"].body.splice(paramsindex, 1);
                }

                $scope.addbodyparams = function(statenum) {
                    if ($scope['state' + statenum]["ent_api_details"].body == undefined) {
                        $scope['state' + statenum]["ent_api_details"].body = [];
                    }
                    var obj = {
                        "paramkey": "",
                        "paramvalue": ""
                    }
                    $scope['state' + statenum]["ent_api_details"].body.push(obj);
                }
                $scope.addformdatabody = function(statenum) {
                    if ($scope['state' + statenum]["ent_api_details"].form_data_body == undefined) {
                        $scope['state' + statenum]["ent_api_details"].form_data_body = [];
                    }
                    var obj = {
                        "paramkey": "",
                        "paramvalue": ""
                    }
                    $scope['state' + statenum]["ent_api_details"].form_data_body.push(obj);
                }
                $scope.rmformdatabody = function(statenum, paramsindex) {

                    $scope['state' + statenum]["ent_api_details"].form_data_body.splice(paramsindex, 1);
                }
                $scope.addintformdatabody = function(statenum) {
                    if ($scope['state' + statenum]["out_api_details"].form_data_body == undefined) {
                        $scope['state' + statenum]["out_api_details"].form_data_body = [];
                    }
                    var obj = {
                        "paramkey": "",
                        "paramvalue": ""
                    }
                    $scope['state' + statenum]["out_api_details"].form_data_body.push(obj);
                }
                $scope.rmintformdatabody = function(statenum, paramsindex) {

                    $scope['state' + statenum]["out_api_details"].form_data_body.splice(paramsindex, 1);
                }
                $scope.rmurlencodedbody = function(statenum, paramsindex) {

                    $scope['state' + statenum]["ent_api_details"].urlencoded_body.splice(paramsindex, 1);
                }

                $scope.addurlencodedbody = function(statenum) {
                    if ($scope['state' + statenum]["ent_api_details"].urlencoded_body == undefined) {
                        $scope['state' + statenum]["ent_api_details"].urlencoded_body = [];
                    }
                    var obj = {
                        "paramkey": "",
                        "paramvalue": ""
                    }
                    $scope['state' + statenum]["ent_api_details"].urlencoded_body.push(obj);
                }
                $scope.rminturlencodedbody = function(statenum, paramsindex) {

                    $scope['state' + statenum]["out_api_details"].urlencoded_body.splice(paramsindex, 1);
                }

                $scope.addinturlencodedbody = function(statenum) {
                    if ($scope['state' + statenum]["out_api_details"].urlencoded_body == undefined) {
                        $scope['state' + statenum]["out_api_details"].urlencoded_body = [];
                    }
                    var obj = {
                        "paramkey": "",
                        "paramvalue": ""
                    }
                    $scope['state' + statenum]["out_api_details"].urlencoded_body.push(obj);
                }
                $scope.showparams = true;
                $scope.showintparams = true;
                $scope.toggleparams = function(statenum) {

                    if ($scope['state' + statenum]["ent_api_details"].showparams == true) {
                        $scope['state' + statenum]["ent_api_details"].showparams = false;
                    } else {

                        $scope['state' + statenum]["ent_api_details"].showparams = true
                    }
                }

                $scope.addheadparams = function(statenum) {
                    if ($scope['state' + statenum]["ent_api_details"].headerparams == undefined) {
                        $scope['state' + statenum]["ent_api_details"].headerparams = [];
                    }
                    var obj = {
                        "paramkey": "",
                        "paramvalue": ""
                    }
                    $scope['state' + statenum]["ent_api_details"].headerparams.push(obj);
                }
                $scope.getapires = function(statenum, item, type) {
                        if (type === 'change') {
                            $scope['state' + statenum]["ent_api_details"].apires = [];
                            // var apivalues = $scope.apivalues.filter(e => e !== $scope['state' + statenum].state);
                            $scope.apikeys = [{ mainkey: "", subkey: "", apivalue: "" }];

                        } else {
                            console.log("i am here");
                        }
                        var ind = $scope.apilist.findIndex(record => record.api_id === ((item.api_id != undefined) ? item.api_id : item)); // $scope.apilist.indexOf(item);
                        $scope.listind = ind;
                        $scope['state' + statenum]["ent_api_details"].api_id = $scope.apilist[ind].api_id;
                        $scope['state' + statenum]["ent_api_details"].api_name = $scope.apilist[ind].api_name;
                        $scope['state' + statenum]["ent_api_details"].url = $scope.apilist[ind].url;
                        $scope['state' + statenum]["ent_api_details"].body = $scope.apilist[ind].body;
                        $scope['state' + statenum]["ent_api_details"].headerparams = $scope.apilist[ind].headerparams;
                        $scope['state' + statenum]["ent_api_details"].params = $scope.apilist[ind].params;
                        $scope['state' + statenum]["ent_api_details"].method = $scope.apilist[ind].method;
                        $scope['state' + statenum]["ent_api_details"].body_type = $scope.apilist[ind].body_type;
                        $scope['state' + statenum]["ent_api_details"].raw_json = $scope.apilist[ind].raw_json;
                        $scope['state' + statenum].api_url = $scope.apilist[ind].url;
                        $scope['state' + statenum].api_type = $scope.apilist[ind].method;
                        $scope['state' + statenum].api_name = $scope.apilist[ind];
                        // $scope.apires = $scope.apilist[ind].raw_json;
                        $scope['state' + $scope.statenum].api_name = $scope['state' + $scope.statenum].api_name;

                        if ($scope['state' + statenum]["ent_api_details"].url == "") {
                            $scope.showSimpleToast("Empty URL request", "warning");
                        } else {
                            $scope.apires = {};
                            var body_data = {};
                            var header_data = { 'Content-Type': 'application/json' };
                            $scope['state' + statenum]["ent_api_details"].showparams = false;

                            if ($scope['state' + statenum]["ent_api_details"].headerparams.length > 0) {

                                angular.forEach($scope['state' + statenum]["ent_api_details"].headerparams, function(value, key) {

                                    header_data[value.paramkey] = value.paramvalue;
                                });
                            }
                            if ($scope['state' + statenum]["ent_api_details"].body_type == "urlencoded") {
                                if ($scope["ent_api_details"].urlencoded_body.length > 0) {
                                    angular.forEach($scope["ent_api_details"].urlencoded_body, function(value, key) {
                                        body_data[value.bodykey] = value.bodyvalue;
                                        $scope.apires[value.bodykey] = value.bodyvalue;
                                    });
                                }

                            } else if ($scope['state' + statenum]["ent_api_details"].body_type == "form_data") {
                                if ($scope['state' + statenum]["ent_api_details"].form_data_body.length > 0) {
                                    angular.forEach($scope['state' + statenum]["ent_api_details"].form_data_body, function(value, key) {
                                        body_data[value.bodykey] = value.bodyvalue;
                                        $scope.apires[value.bodykey] = value.bodyvalue;
                                    });
                                }


                            } else {
                                if ($scope['state' + statenum]["ent_api_details"].raw_json != undefined) {
                                    if (Object.keys($scope['state' + statenum]["ent_api_details"].raw_json).length > 0) {
                                        body_data = $scope['state' + statenum]["ent_api_details"].raw_json;
                                        $scope['state' + statenum]["ent_api_details"].raw_json = JSON.stringify($scope['state' + statenum]["ent_api_details"].raw_json);
                                        $scope.apires = body_data;
                                    } else {
                                        body_data = {};
                                        $scope['state' + statenum]["out_api_details"].raw_json = '{}';
                                    }
                                } else {
                                    body_data = {};
                                    $scope['state' + statenum]["ent_api_details"].raw_json = '{}';
                                }
                            }
                            var parameter = ""
                            if ($scope['state' + statenum]["ent_api_details"].params.length > 0) {
                                $scope['state' + statenum]["ent_api_details"].showparams = true;
                                angular.forEach($scope['state' + statenum]["ent_api_details"].params, function(value, key) {

                                    parameter = parameter + value.paramkey + "=" + value.paramvalue + '&';
                                    $scope.apires[value.paramkey] = value.paramvalue;
                                });
                            }
                            //  var url = $scope['state' + statenum]["ent_api_details"].url + "/?"+parameter
                            var req_body = {
                                'method': $scope['state' + statenum]["ent_api_details"].method,
                                'url': $scope['state' + statenum]["ent_api_details"].url,
                                'headers': header_data,
                                'body': body_data,
                                'param': parameter
                            }
                            $scope.apiresponse = "";
                            $scope.apistatus = "";
                            $scope.apistatustext = "";

                            $http.defaults.headers.common['user'] = $window.sessionStorage.id;
                            $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
                            $http.post('/execute_api', req_body, {
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(function(res) {

                                $scope.apiresponse = JSON.stringify(res.data, undefined, 2);
                                $scope.apistatus = res.data.status;
                                $scope.apistatustext = res.data.statusText;
                            });
                            if ($scope.apires !== undefined && Object.keys($scope.apires).length !== 0) {
                                // if ($scope['state' + statenum]["ent_api_details"].apikeys == undefined) {
                                //     $scope['state' + statenum]["ent_api_details"].apikeys = [{ mainkeys: [], subkeys: [], mainkey: "", subkey: "", apivalues: apivalues, apivalue: "", apires: [] }];
                                // }
                                $scope.obj = {};
                                if ($scope['state' + statenum]["ent_api_details"].apires.length != 0) {
                                    var apival = Object.keys($scope['state' + statenum]["ent_api_details"].apires[0]);
                                    $scope.obj = $scope['state' + statenum]["ent_api_details"].apires[0];
                                    var keys = [];

                                    apival.forEach(function(obj, i) {
                                        if (obj.includes("&")) {
                                            keys = obj.replace(/['"]+/g, '').split("&");
                                            $scope.apikeys[i].mainkey = keys[0];
                                            $scope.apikeys[i].subkey = keys[1];
                                        } else {
                                            $scope.apikeys[i].mainkey = obj;
                                        }
                                    });
                                    // if (keys.length == 0) {
                                    //     $scope.apikeys.reverse();
                                    // }
                                    $scope.apikeys.forEach(function(obj, i) {
                                        var key = obj.mainkey;
                                        var subkey = (obj.subkey != undefined) ? obj.subkey : "";
                                        if (typeof($scope.apires) == 'object' && !Array.isArray($scope.apires)) {
                                            if (typeof($scope.apires[key]) == 'object' && !Array.isArray($scope.apires[key])) {
                                                $scope.apikeys[i].apivalue = $scope['state' + statenum]["ent_api_details"].apires[0]['"' + key + '&' + subkey + '"'];
                                            } else if (Array.isArray($scope.apires[key])) {
                                                $scope.apikeys[i].apivalue = $scope['state' + statenum]["ent_api_details"].apires[0][key][0][subkey];
                                            } else {
                                                $scope.apikeys[i].apivalue = $scope['state' + statenum]["ent_api_details"].apires[0][key];
                                            }
                                        } else if (Array.isArray($scope.apires)) {
                                            if (typeof($scope.apires[0][key]) == 'object' && !Array.isArray($scope.apires[0][key])) {
                                                $scope.apikeys[i].apivalue = $scope['state' + statenum]["ent_api_details"].apires[0]['"' + key + '&' + subkey + '"'];
                                            } else if (Array.isArray($scope.apires[0][key])) {
                                                $scope.apikeys[i].apivalue = $scope['state' + statenum]["ent_api_details"].apires[0][key][0][subkey];
                                            } else {
                                                $scope.apikeys[i].apivalue = $scope['state' + statenum]["ent_api_details"].apires[0][key];
                                            }
                                        } else {
                                            $scope.apikeys[i].apivalue = $scope['state' + statenum]["ent_api_details"].apires[0][key];
                                        }

                                    });
                                }
                            } else {
                                $scope['state' + $scope.statenum]["ent_api_details"].apires[0] = {};
                            }
                        }
                    }
                    // var apiintvalues = [];
                    // $scope.$watch('state' + $scope.statenum + '[previous_res]', function () {
                    //     console.log("watching");
                    // });
                $scope.previousres = function() {
                    if ($scope['state' + $scope.statenum].previous_res == false && $scope.statenum !== "0") {
                        apiintvalues = $scope.apivalues.filter(e => e == $scope['state' + $scope.statenum].state);
                    } else {
                        apiintvalues = $scope.apivalues;
                    }
                    var api_keys = $scope['state' + $scope.statenum]["out_api_details"].apikeys;
                    api_keys.forEach(function(obj, i) {
                        $scope['state' + $scope.statenum]["out_api_details"].apikeys[i].apivalues = apiintvalues;
                        $scope['state' + $scope.statenum]["out_api_details"].apikeys[i].apivalue = apiintvalues[0];
                    });
                }
                $scope.getapiintres = function(statenum, item, type) {
                    if (type == 'change') {
                        $scope['state' + statenum]["out_api_details"].apires = [];
                        if ($scope['state' + $scope.statenum].previous_res == false && $scope.statenum !== "0") {
                            apiintvalues = $scope.apivalues.filter(e => e == $scope['state' + $scope.statenum].state);
                        } else {
                            apiintvalues = $scope.apivalues;
                        }
                        $scope.apiintkeys = [{ mainkey: "", subkey: "", apivalue: "" }];

                    }
                    var ind = $scope.apiintlist.findIndex(record => record.api_id === ((item.api_id != undefined) ? item.api_id : item)); // $scope.apilist.indexOf(item);
                    $scope.intlist = ind;
                    $scope['state' + statenum]["out_api_details"].api_id = $scope.apiintlist[ind].api_id;
                    $scope['state' + statenum]["out_api_details"].api_name = $scope.apiintlist[ind].api_name;
                    $scope['state' + statenum]["out_api_details"].url = $scope.apiintlist[ind].url;
                    $scope['state' + statenum]["out_api_details"].body = $scope.apiintlist[ind].body;
                    $scope['state' + statenum]["out_api_details"].headerparams = $scope.apiintlist[ind].headerparams;
                    $scope['state' + statenum]["out_api_details"].params = $scope.apiintlist[ind].params;
                    $scope['state' + statenum]["out_api_details"].method = $scope.apiintlist[ind].method;
                    $scope['state' + statenum]["out_api_details"].body_type = $scope.apiintlist[ind].body_type;
                    $scope['state' + statenum]["out_api_details"].raw_json = $scope.apiintlist[ind].raw_json;
                    $scope['state' + statenum].api_url = $scope.apiintlist[ind].url;
                    $scope['state' + statenum].api_type = $scope.apiintlist[ind].method;
                    $scope['state' + statenum]["out_api_details"].api_url = $scope.apiintlist[ind];
                    // $scope.apiintres = $scope.apiintlist[ind].raw_json;
                    $scope['state' + statenum]["out_api_details"].api_url = $scope['state' + statenum]["out_api_details"].api_url;
                    // var apivalues = $scope.apivalues.filter(e => e !== $scope['state' + statenum].state + '.ans');

                    if ($scope['state' + statenum]["out_api_details"].url == "") {
                        $scope.showSimpleToast("Empty URL request", "warning");
                    } else {
                        $scope.apiintres = {};
                        var body_data = {}
                        var header_data = { 'Content-Type': 'application/json' }
                        $scope['state' + statenum]["out_api_details"].showparams = false;

                        if ($scope['state' + statenum]["out_api_details"].headerparams.length > 0) {

                            angular.forEach($scope['state' + statenum]["out_api_details"].headerparams, function(value, key) {

                                header_data[value.paramkey] = value.paramvalue
                            });
                        }
                        if ($scope['state' + statenum]["out_api_details"].body_type == "urlencoded") {
                            if ($scope["out_api_details"].urlencoded_body.length > 0) {
                                angular.forEach($scope["out_api_details"].urlencoded_body, function(value, key) {
                                    body_data[value.bodykey] = value.bodyvalue;
                                    $scope.apiintres[value.bodykey] = value.bodyvalue;
                                });
                            }

                        } else if ($scope['state' + statenum]["out_api_details"].body_type == "form_data") {
                            if ($scope['state' + statenum]["out_api_details"].form_data_body.length > 0) {
                                angular.forEach($scope['state' + statenum]["out_api_details"].form_data_body, function(value, key) {
                                    body_data[value.bodykey] = value.bodyvalue;
                                    $scope.apiintres[value.bodykey] = value.bodyvalue;
                                });
                            }


                        } else {
                            if ($scope['state' + statenum]["out_api_details"].raw_json != undefined) {
                                if (Object.keys($scope['state' + statenum]["out_api_details"].raw_json).length > 0) {
                                    body_data = $scope['state' + statenum]["out_api_details"].raw_json;
                                    $scope['state' + statenum]["out_api_details"].raw_json = JSON.stringify($scope['state' + statenum]["out_api_details"].raw_json);
                                    $scope.apiintres = body_data;
                                } else {
                                    body_data = {};
                                    $scope['state' + statenum]["out_api_details"].raw_json = '{}';
                                }
                            } else {
                                body_data = {};
                                $scope['state' + statenum]["out_api_details"].raw_json = '{}';
                            }

                        }
                        var parameter = ""
                        if ($scope['state' + statenum]["out_api_details"].params.length > 0) {
                            angular.forEach($scope['state' + statenum]["out_api_details"].params, function(value, key) {

                                parameter = parameter + value.paramkey + "=" + value.paramvalue + '&';
                                $scope['state' + statenum]["out_api_details"].showparams = true;
                            });
                        }
                        //  var url = $scope['state' + statenum]["ent_api_details"].url + "/?"+parameter
                        var req_body = {
                            'method': $scope['state' + statenum]["out_api_details"].method,
                            'url': $scope['state' + statenum]["out_api_details"].url,
                            'headers': header_data,
                            'body': body_data,
                            'param': parameter
                        }
                        $scope.apiintresponse = "";
                        $scope.apiintstatus = "";
                        $scope.apiintstatustext = "";

                        $http.defaults.headers.common['user'] = $window.sessionStorage.id;
                        $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
                        $http.post('/execute_api', req_body, {
                            headers: {
                                'Content-Type': 'application/json'
                            }
                        }).then(function(res) {

                            $scope.apiintresponse = JSON.stringify(res.data, undefined, 2);
                            $scope.apiintstatus = res.data.status;
                            $scope.apiintstatustext = res.data.statusText;
                        });
                        if ($scope.apiintres !== undefined && Object.keys($scope.apiintres).length !== 0) {
                            if ($scope.apiintkeys == undefined) {
                                $scope.apiintkeys = [{ mainkey: "", subkey: "", apivalue: "" }];
                            }
                            $scope.intobj = {};
                            if ($scope['state' + statenum]["out_api_details"].apires.length != 0) {
                                var apival = Object.keys($scope['state' + statenum]["out_api_details"].apires[0]);
                                $scope.intobj = $scope['state' + statenum]["out_api_details"].apires[0];
                                var keys = [];

                                apival.forEach(function(obj, i) {
                                    if (obj.includes("&")) {
                                        keys = obj.replace(/['"]+/g, '').split("&");
                                        $scope.apiintkeys[i].mainkey = keys[0];
                                        $scope.apiintkeys[i].subkey = keys[1];
                                    } else {
                                        $scope.apiintkeys[i].mainkey = obj;
                                    }
                                });
                                // if (keys.length == 0) {
                                //     $scope.apiintkeys.reverse();
                                // }
                                $scope.apiintkeys.forEach(function(obj, i) {
                                    var key = obj.mainkey;
                                    var subkey = (obj.subkey != undefined) ? obj.subkey : "";
                                    if (typeof($scope.apiintres) == 'object' && !Array.isArray($scope.apiintres)) {
                                        if (typeof($scope.apiintres[key]) == 'object' && !Array.isArray($scope.apiintres[key])) {
                                            $scope.apiintkeys[i].apivalue = $scope['state' + statenum]["out_api_details"].apires[0]['"' + key + '&' + subkey + '"'];
                                        } else if (Array.isArray($scope.apiintres[key])) {
                                            $scope.apiintkeys[i].apivalue = $scope['state' + statenum]["out_api_details"].apires[0][key][0][subkey];
                                        } else {
                                            $scope.apiintkeys[i].apivalue = $scope['state' + statenum]["out_api_details"].apires[0][key];
                                        }
                                    } else if (Array.isArray($scope.apiintres)) {
                                        if (typeof($scope.apiintres[0][key]) == 'object' && !Array.isArray($scope.apiintres[0][key])) {
                                            $scope.apiintkeys[i].apivalue = $scope['state' + statenum]["out_api_details"].apires[0]['"' + key + '&' + subkey + '"'];
                                        } else if (Array.isArray($scope.apiintres[0][key])) {
                                            $scope.apiintkeys[i].apivalue = $scope['state' + statenum]["out_api_details"].apires[0][key][0][subkey];
                                        } else {
                                            $scope.apiintkeys[i].apivalue = $scope['state' + statenum]["out_api_details"].apires[0][key];
                                        }
                                    } else {
                                        $scope.apiintkeys[i].apivalue = $scope['state' + statenum]["out_api_details"].apires[0][key];
                                    }

                                });
                            }
                        } else {
                            $scope['state' + $scope.statenum]["out_api_details"].apires[0] = {};
                        }
                    }
                }
                $scope.subapikeyres = function(prop, i) {
                    // var obj = {};
                    if (typeof($scope.apires) == 'object' && !Array.isArray($scope.apires)) {
                        if (typeof($scope.apires[prop]) == 'object' && !Array.isArray($scope.apires[prop])) {
                            $scope['state' + $scope.statenum]["ent_api_details"].apikeys[i].subkeys = Object.keys($scope.apires[prop]);
                            if ($scope['state' + $scope.statenum]["ent_api_details"].apikeys[0].subkey == "") {
                                $scope['state' + $scope.statenum]["ent_api_details"].apikeys[0].subkey = $scope['state' + $scope.statenum]["ent_api_details"].apikeys[0].subkeys[0];
                            }
                        } else if (Array.isArray($scope.apires[prop])) {
                            // $.each(res.data.data,function(obj,value){
                            $scope['state' + $scope.statenum]["ent_api_details"].apikeys[i].subkeys = Object.keys($scope.apires[prop][0]);
                            $scope.obj[prop] = [];
                            $scope['state' + $scope.statenum]["ent_api_details"].apikeys[i].apires[0] = $scope.obj;
                            if ($scope['state' + $scope.statenum]["ent_api_details"].apikeys[0].subkey == "") {
                                $scope['state' + $scope.statenum]["ent_api_details"].apikeys[0].subkey = $scope['state' + $scope.statenum]["ent_api_details"].apikeys[0].subkeys[0];
                            }
                            if ($scope['state' + $scope.statenum]["ent_api_details"].apires.length)
                                $scope['state' + $scope.statenum]["ent_api_details"].apires[0] = $scope.obj;
                            // });
                        } else if ($scope.apires[prop] != undefined) {
                            $scope['state' + $scope.statenum]["ent_api_details"].apikeys[i].subkeys = [];
                            $scope.obj[prop] = $scope['state' + $scope.statenum]["ent_api_details"].apikeys[0].apivalue;
                            $scope['state' + $scope.statenum]["ent_api_details"].apikeys[i].apires[0] = $scope.obj;
                            if ($scope['state' + $scope.statenum]["ent_api_details"].apikeys[0].subkey == "") {
                                $scope['state' + $scope.statenum]["ent_api_details"].apikeys[0].subkey = $scope['state' + $scope.statenum]["ent_api_details"].apikeys[0].subkeys[0];
                            }
                            $scope['state' + $scope.statenum]["ent_api_details"].apires[0] = $scope.obj;
                        }
                    } else if (Array.isArray($scope.apires)) {
                        if (typeof($scope.apires[0][prop]) == 'object' && !Array.isArray($scope.apires[0][prop])) {
                            $scope['state' + $scope.statenum]["ent_api_details"].apikeys[i].subkeys = Object.keys($scope.apires[0][prop]);
                            if ($scope['state' + $scope.statenum]["ent_api_details"].apikeys[0].subkey == "") {
                                $scope['state' + $scope.statenum]["ent_api_details"].apikeys[0].subkey = $scope['state' + $scope.statenum]["ent_api_details"].apikeys[0].subkeys[0];
                            }
                            // obj[prop] = {};
                            // $scope['state' + $scope.statenum]["ent_api_details"].apikeys[i].apires[0] = obj;
                        } else if (Array.isArray($scope.apires[0][prop])) {
                            // $.each(res.data.data,function(obj,value){
                            $scope['state' + $scope.statenum]["ent_api_details"].apikeys[i].subkeys = Object.keys($scope.apires[0][prop]);
                            $scope.obj[prop] = [];
                            $scope['state' + $scope.statenum]["ent_api_details"].apikeys[i].apires[0] = $scope.obj;
                            if ($scope['state' + $scope.statenum]["ent_api_details"].apikeys[0].subkey == "") {
                                $scope['state' + $scope.statenum]["ent_api_details"].apikeys[0].subkey = $scope['state' + $scope.statenum]["ent_api_details"].apikeys[0].subkeys[0];
                            }
                            $scope['state' + $scope.statenum]["ent_api_details"].apires[0] = $scope.obj;
                            // });
                        } else if ($scope.apires[prop] != undefined) {
                            $scope['state' + $scope.statenum]["ent_api_details"].apikeys[i].subkeys = [];
                            $scope.obj[prop] = $scope['state' + $scope.statenum]["ent_api_details"].apikeys[0].apivalue;
                            $scope['state' + $scope.statenum]["ent_api_details"].apikeys[i].apires[0] = $scope.obj;
                            if ($scope['state' + $scope.statenum]["ent_api_details"].apikeys[0].subkey == "") {
                                $scope['state' + $scope.statenum]["ent_api_details"].apikeys[0].subkey = $scope['state' + $scope.statenum]["ent_api_details"].apikeys[0].subkeys[0];
                            }
                            $scope['state' + $scope.statenum]["ent_api_details"].apires[0] = $scope.obj;
                        }
                    }
                }
                $scope.subapiintkeyres = function(prop, i) {
                    // var obj = {};
                    if (typeof($scope.apiintres) == 'object' && !Array.isArray($scope.apiintres)) {
                        if (typeof($scope.apiintres[prop]) == 'object' && !Array.isArray($scope.apiintres[prop])) {
                            $scope['state' + $scope.statenum]["out_api_details"].apikeys[i].subkeys = Object.keys($scope.apiintres[prop]);
                            if ($scope['state' + $scope.statenum]["out_api_details"].apikeys[0].subkey == "") {
                                $scope['state' + $scope.statenum]["out_api_details"].apikeys[0].subkey = $scope['state' + $scope.statenum]["out_api_details"].apikeys[0].subkeys[0];
                            }
                        } else if (Array.isArray($scope.apiintres[prop])) {
                            // $.each(res.data.data,function(obj,value){
                            $scope['state' + $scope.statenum]["out_api_details"].apikeys[i].subkeys = Object.keys($scope.apiintres[prop][0]);
                            $scope.intobj[prop] = [];
                            $scope['state' + $scope.statenum]["out_api_details"].apikeys[i].apires[0] = $scope.intobj;
                            if ($scope['state' + $scope.statenum]["out_api_details"].apikeys[0].subkey == "") {
                                $scope['state' + $scope.statenum]["out_api_details"].apikeys[0].subkey = $scope['state' + $scope.statenum]["out_api_details"].apikeys[0].subkeys[0];
                            }
                            $scope['state' + $scope.statenum]["out_api_details"].apires[0] = $scope.intobj;
                            // });
                        } else if ($scope.apiintres[prop] != undefined) {
                            $scope['state' + $scope.statenum]["out_api_details"].apikeys[i].subkeys = [];
                            $scope.intobj[prop] = $scope['state' + $scope.statenum]["out_api_details"].apikeys[0].apivalue;
                            $scope['state' + $scope.statenum]["out_api_details"].apikeys[i].apires[0] = $scope.intobj;
                            if ($scope['state' + $scope.statenum]["out_api_details"].apikeys[0].subkey == "") {
                                $scope['state' + $scope.statenum]["out_api_details"].apikeys[0].subkey = $scope['state' + $scope.statenum]["out_api_details"].apikeys[0].subkeys[0];
                            }
                            $scope['state' + $scope.statenum]["out_api_details"].apires[0] = $scope.intobj;
                        }
                    } else if (Array.isArray($scope.apires)) {
                        if (typeof($scope.apires[0][prop]) == 'object' && !Array.isArray($scope.apires[0][prop])) {
                            $scope['state' + $scope.statenum]["out_api_details"].apikeys[i].subkeys = Object.keys($scope.apires[0][prop]);
                            if ($scope['state' + $scope.statenum]["out_api_details"].apikeys[0].subkey == "") {
                                $scope['state' + $scope.statenum]["out_api_details"].apikeys[0].subkey = $scope['state' + $scope.statenum]["out_api_details"].apikeys[0].subkeys[0];
                            }
                            // obj[prop] = {};
                            // $scope['state' + $scope.statenum]["ent_api_details"].apikeys[i].apires[0] = obj;
                        } else if (Array.isArray($scope.apires[0][prop])) {
                            // $.each(res.data.data,function(obj,value){
                            $scope['state' + $scope.statenum]["out_api_details"].apikeys[i].subkeys = Object.keys($scope.apires[0][prop]);
                            $scope.intobj[prop] = [];
                            $scope['state' + $scope.statenum]["out_api_details"].apikeys[i].apires[0] = $scope.intobj;
                            if ($scope['state' + $scope.statenum]["out_api_details"].apikeys[0].subkey == "") {
                                $scope['state' + $scope.statenum]["out_api_details"].apikeys[0].subkey = $scope['state' + $scope.statenum]["out_api_details"].apikeys[0].subkeys[0];
                            }
                            $scope['state' + $scope.statenum]["out_api_details"].apires[0] = $scope.intobj;
                            // });
                        } else if ($scope.apiintres[prop] != undefined) {
                            $scope['state' + $scope.statenum]["out_api_details"].apikeys[i].subkeys = [];
                            $scope.intobj[prop] = $scope['state' + $scope.statenum]["out_api_details"].apikeys[0].apivalue;
                            $scope['state' + $scope.statenum]["out_api_details"].apikeys[i].apires[0] = $scope.intobj;
                            if ($scope['state' + $scope.statenum]["out_api_details"].apikeys[0].subkey == "") {
                                $scope['state' + $scope.statenum]["out_api_details"].apikeys[0].subkey = $scope['state' + $scope.statenum]["out_api_details"].apikeys[0].subkeys[0];
                            }
                            $scope['state' + $scope.statenum]["out_api_details"].apires[0] = $scope.intobj;
                        }
                    }
                }
                $scope.prevkey = function(key) {
                    $scope.prev_key = key;
                }
                $scope.addapikey = function(prop, i) {
                    if (prop !== $scope.prev_key) {
                        var exists = $scope['state' + $scope.statenum]["ent_api_details"].apires.indexOf($scope.prev_key);
                        if (exists != -1) {
                            delete $scope['state' + $scope.statenum]["ent_api_details"].apires[0][$scope.prev_key];
                        }
                        var keys = prop.split("&");
                        var subprop = keys[1];
                        // var obj = {};
                        if (keys[0] != null) {
                            if (typeof($scope.apires) == 'object' && !Array.isArray($scope.apires)) {
                                if (typeof($scope.apires[keys[0]]) == 'object' && !Array.isArray($scope.apires[keys[0]])) {
                                    $scope.obj['"' + prop + '"'] = $scope.apikeys[i].apivalue;
                                    $scope['state' + $scope.statenum]["ent_api_details"].apires[0] = $scope.obj;
                                } else if (Array.isArray($scope.apires[keys[0]])) {
                                    $scope.obj[subprop] = $scope.apikeys[i].apivalue;
                                    $scope['state' + $scope.statenum]["ent_api_details"].apires[0][keys[0]][0] = $scope.obj;

                                } else {
                                    $scope.obj[prop] = $scope.apikeys[i].apivalue;
                                    $scope['state' + $scope.statenum]["ent_api_details"].apires[0] = $scope.obj;
                                }
                            } else if (Array.isArray($scope.apires)) {
                                if (typeof($scope.apires[0][keys[0]]) == 'object' && !Array.isArray($scope.apires[0][keys[0]])) {
                                    $scope.obj['"' + prop + '"'] = $scope.apikeys[i].apivalue;
                                    $scope['state' + $scope.statenum]["ent_api_details"].apires[0] = $scope.obj;
                                } else if (Array.isArray($scope.apires[0][keys[0]])) {
                                    $scope.obj[subprop] = $scope.apikeys[i].apivalue;
                                    $scope['state' + $scope.statenum]["ent_api_details"].apires[0][keys[0]][0] = $scope.obj;
                                } else {
                                    $scope.obj[prop] = $scope.apikeys[i].apivalue;
                                    $scope['state' + $scope.statenum]["ent_api_details"].apires[0] = $scope.obj;
                                }
                            }
                        }
                    } else {

                    }
                }
                $scope.previntkey = function(key) {
                    $scope.prev_int_key = key;
                }
                $scope.addapiintkey = function(prop, i) {
                    if (prop !== $scope.prev_int_key) {
                        var keys = prop.split("&");
                        var subprop = keys[1];
                        // var obj = {};
                        if (prop != null && subprop != null) {
                            if (typeof($scope.apiintres) == 'object' && !Array.isArray($scope.apiintres)) {
                                if (typeof($scope.apiintres[keys[0]]) == 'object' && !Array.isArray($scope.apiintres[prop])) {
                                    $scope.intobj['"' + prop + '"'] = $scope.apiintkeys[0].apivalue;
                                    $scope['state' + $scope.statenum]["out_api_details"].apires[0] = $scope.intobj;
                                } else if (Array.isArray($scope.apiintres[keys[0]])) {
                                    $scope.intobj[subprop] = $scope.apiintkeys[0].apivalue;
                                    $scope['state' + $scope.statenum]["out_api_details"].apires[0][keys[0]][0] = $scope.intobj;
                                } else {
                                    $scope.obj[prop] = $scope.apikeys[i].apivalue;
                                    $scope['state' + $scope.statenum]["out_api_details"].apires[0] = $scope.obj;
                                }
                            } else if (Array.isArray($scope.apiintres)) {
                                if (typeof($scope.apiintres[0][keys[0]]) == 'object' && !Array.isArray($scope.apiintres[0][keys[0]])) {
                                    $scope.intobj['"' + prop + '&' + subprop + '"'] = $scope.apiintkeys[0].apivalue;
                                    $scope['state' + $scope.statenum]["out_api_details"].apires[0] = $scope.intobj;
                                } else if (Array.isArray($scope.apiintres[0][keys[0]])) {
                                    $scope.intobj[subprop] = $scope.apiintkeys[0].apivalue;
                                    $scope['state' + $scope.statenum]["out_api_details"].apires[0][prop][0] = $scope.intobj;
                                } else {
                                    $scope.obj[prop] = $scope.apiintkeys[i].apivalue;
                                    $scope['state' + $scope.statenum]["out_api_details"].apires[0] = $scope.obj;
                                }
                            }
                        }
                    }
                }
                $scope.focusval = function(val) {
                    $scope.prevsval = val;
                }
                $scope.addapivalue = function(prop, val, i) {
                    if (val !== $scope.prevsval) {
                        var keys = prop.split("&");
                        var subprop = keys[1];
                        if (typeof($scope.apires) == 'object' && !Array.isArray($scope.apires)) {
                            if (typeof($scope.apires[keys[0]]) == 'object' && !Array.isArray($scope.apires[keys[0]])) {
                                $scope['state' + $scope.statenum]["ent_api_details"].apires[0]['"' + prop + '"'] = val;
                            } else if (Array.isArray($scope.apires[keys[0]])) {
                                $scope['state' + $scope.statenum]["ent_api_details"].apires[0][keys[0]][0][subprop] = val;
                            } else {
                                $scope['state' + $scope.statenum]["ent_api_details"].apires[0][prop] = val;
                            }
                        } else if (Array.isArray($scope.apires)) {
                            if (typeof($scope.apires[0][keys[0]]) == 'object' && !Array.isArray($scope.apires[0][keys[0]])) {
                                $scope['state' + $scope.statenum]["ent_api_details"].apires[0]['"' + prop + '"'] = val;
                            } else if (Array.isArray($scope.apires[0][keys[0]])) {
                                $scope['state' + $scope.statenum]["ent_api_details"].apires[0][keys[0]][0][subprop] = val;
                            } else {
                                $scope['state' + $scope.statenum]["ent_api_details"].apires[0][prop] = val;
                            }
                        }
                    }
                }
                $scope.focusintval = function(val) {
                    $scope.prevsintval = val;
                }
                $scope.addapiintvalue = function(prop, val, i) {
                    if (val !== $scope.prevsintval) {
                        var keys = prop.split("&");
                        var subprop = keys[1];
                        if (typeof($scope.apiintres) == 'object' && !Array.isArray($scope.apiintres)) {
                            if (typeof($scope.apiintres[keys[0]]) == 'object' && !Array.isArray($scope.apiintres[keys[0]])) {
                                $scope['state' + $scope.statenum]["out_api_details"].apires[0]['"' + prop + '"'] = val;
                            } else if (Array.isArray($scope.apiintres[keys[0]])) {
                                $scope['state' + $scope.statenum]["out_api_details"].apires[0][prop][0][subprop] = val;
                            } else {
                                $scope['state' + $scope.statenum]["out_api_details"].apires[0][prop] = val;
                            }
                        } else if (Array.isArray($scope.apiintres)) {
                            if (typeof($scope.apiintres[0][keys[0]]) == 'object' && !Array.isArray($scope.apiintres[0][keys[0]])) {
                                $scope['state' + $scope.statenum]["out_api_details"].apires[0]['"' + prop + '"'] = val;
                            } else if (Array.isArray($scope.apiintres[0][keys[0]])) {
                                $scope['state' + $scope.statenum]["out_api_details"].apires[0][prop][0][subprop] = val;
                            } else {
                                $scope['state' + $scope.statenum]["out_api_details"].apires[0][prop] = val;
                            }
                        }
                    }
                }
                $scope.addapikeyvalues = function(statenum) {
                    var apivalues = $scope.apivalues.filter(e => e !== $scope['state' + statenum].state);
                    var obj = {
                        mainkey: "",
                        subkey: "",
                        apivalue: ""
                    };
                    $scope.apikeys.push(obj);
                };
                $scope.addapiintkeyvalues = function(statenum) {
                    // var apivalues = $scope.apivalues.filter(e => e !== $scope['state' + statenum].state + '.ans');
                    var obj = {
                        mainkey: "",
                        subkey: "",
                        apivalue: ""
                    };
                    $scope.apiintkeys.push(obj);
                };
                $scope.rmapikeyvalues = function(statenum, keysindex) {
                    var key = "";
                    if ($scope.apikeys[keysindex].subkey == undefined || $scope.apikeys[keysindex].subkey == "") {
                        key = $scope.apikeys[keysindex].mainkey;
                    } else {
                        key = $scope.apikeys[keysindex].mainkey + '&' + $scope.apikeys[keysindex].subkey;
                    }
                    delete $scope['state' + statenum]["ent_api_details"].apires[0][key];
                    $scope.apikeys.splice(keysindex, 1);
                };
                $scope.rmapiintkeyvalues = function(statenum, keysindex) {
                    var key = "";
                    if ($scope.apiintkeys[keysindex].subkey == undefined || $scope.apiintkeys[keysindex].subkey == "") {
                        key = $scope.apiintkeys[keysindex].mainkey;
                    } else {
                        key = $scope.apiintkeys[keysindex].mainkey + '&' + $scope.apiintkeys[keysindex].subkey;
                    }
                    delete $scope['state' + statenum]["out_api_details"].apires[0][key];
                    $scope.apiintkeys.splice(keysindex, 1);
                };
                if ($scope['state' + $scope.statenum].entry_response == true) {
                    if ($scope['state' + $scope.statenum]["ent_api_details"] == undefined) {
                        var apivalues = $scope.apivalues.filter(e => e !== $scope['state' + $scope.statenum].state);
                        var api_obj = {
                            "method": "get",
                            "url": "",
                            "params": [],
                            "body": [],
                            "headerparams": [],
                            "auth": {},
                            "showparams": false,
                            "body_type": "raw",
                            "raw_json": JSON.stringify({}),
                            "api_name": "",
                            "apires": [{}],
                            "api_id": "",
                            "failure_state": ""
                        }
                        $scope['state' + $scope.statenum]["ent_api_details"] = api_obj;
                        $scope.entry_response_msg = "";

                        $http.defaults.headers.common['user'] = $window.sessionStorage.id;
                        $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
                        $scope.apilist = [];
                        $scope.apikeys = [{ mainkey: "", subkey: "", apivalue: "" }];
                        $http.get('/getapis', {}).then(function(res) {
                            for (var i = 0; i < res.data.length; i++) {
                                $scope.apilist[i] = {
                                    api_id: res.data[i]._id,
                                    api_name: (res.data[i].ApiName == undefined) ? res.data[i].api_info.url : res.data[i].ApiName,
                                    method: res.data[i].api_info.method,
                                    params: res.data[i].api_info.params,
                                    body: res.data[i].api_info.body,
                                    headerparams: res.data[i].api_info.headerparams,
                                    showparams: res.data[i].api_info.showparams,
                                    url: res.data[i].api_info.url,
                                    body_type: res.data[i].api_info.body_type,
                                    raw_json: res.data[i].api_info.raw_json
                                };
                            }

                            $scope.getapires($scope.statenum, $scope['state' + $scope.statenum].api_name, "");
                        });
                    } else if ($scope['state' + $scope.statenum]["ent_api_details"][0] != undefined) {
                        // var apivalues = $scope.apivalues.filter(e => e !== $scope['state' + $scope.statenum].state);
                        var api_obj = {
                            "method": "get",
                            "url": "",
                            "params": [],
                            "body": [],
                            "headerparams": [],
                            "auth": {},
                            "showparams": false,
                            "body_type": "raw",
                            "raw_json": JSON.stringify({}),
                            "api_name": ($scope['state' + $scope.statenum]["ent_api_details"][0].api_name != "") ? $scope['state' + $scope.statenum]["ent_api_details"][0].api_name : "",
                            "apires": ($scope['state' + $scope.statenum]["ent_api_details"][0].apires.length != 0 && $scope['state' + $scope.statenum]["ent_api_details"][0].apires[0] != null) ? $scope['state' + $scope.statenum]["ent_api_details"][0].apires : [{}],
                            "api_id": ($scope['state' + $scope.statenum]["ent_api_details"][0].api_id != "") ? $scope['state' + $scope.statenum]["ent_api_details"][0].api_id : "",
                            "failure_state": $scope['state' + $scope.statenum]["ent_api_details"][0].failure_state
                        }
                        if ($scope['state' + $scope.statenum]["ent_api_details"][0].apires[0] !== null) {
                            var keys = Object.keys($scope['state' + $scope.statenum]["ent_api_details"][0].apires[0]);
                            for (let i = 0; i < keys.length; i++) {
                                var obj = { mainkey: "", subkey: "", apivalue: "" };
                                $scope.apikeys[i] = obj;
                            }
                        }

                        $scope['state' + $scope.statenum]["ent_api_details"] = api_obj;
                        $scope.entry_response_msg = "";

                        $http.defaults.headers.common['user'] = $window.sessionStorage.id;
                        $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
                        $scope.apilist = [];
                        $http.get('/getapis', {}).then(function(res) {
                            for (var i = 0; i < res.data.length; i++) {
                                $scope.apilist[i] = {
                                    api_id: res.data[i]._id,
                                    api_name: (res.data[i].ApiName == undefined) ? res.data[i].api_info.url : res.data[i].ApiName,
                                    method: res.data[i].api_info.method,
                                    params: res.data[i].api_info.params,
                                    body: res.data[i].api_info.body,
                                    headerparams: res.data[i].api_info.headerparams,
                                    showparams: res.data[i].api_info.showparams,
                                    url: res.data[i].api_info.url,
                                    body_type: res.data[i].api_info.body_type,
                                    raw_json: res.data[i].api_info.raw_json
                                };
                            }
                            // $scope['state' + $scope.statenum].api_name = ($scope['state' + $scope.statenum].api_name != undefined || $scope['state' + $scope.statenum].api_name != "") ? $scope['state' + $scope.statenum].api_name : $scope['state' + $scope.statenum]["ent_api_details"].api_name;
                            $scope.getapires($scope.statenum, $scope['state' + $scope.statenum]["ent_api_details"].api_id, "");
                        });
                    } else if ($scope['state' + $scope.statenum]["ent_api_details"] != undefined) {
                        var api_obj = {
                            "method": "get",
                            "url": "",
                            "params": [],
                            "body": [],
                            "headerparams": [],
                            "auth": {},
                            "showparams": false,
                            "body_type": "raw",
                            "raw_json": JSON.stringify({}),
                            "api_name": ($scope['state' + $scope.statenum]["ent_api_details"].api_name != "") ? $scope['state' + $scope.statenum]["ent_api_details"].api_name : "",
                            "apires": ($scope['state' + $scope.statenum]["ent_api_details"].apires.length != 0 && $scope['state' + $scope.statenum]["ent_api_details"].apires[0] != null) ? $scope['state' + $scope.statenum]["ent_api_details"].apires : [{}],
                            "api_id": ($scope['state' + $scope.statenum]["ent_api_details"].api_id != "") ? $scope['state' + $scope.statenum]["ent_api_details"].api_id : "",
                            "failure_state": $scope['state' + $scope.statenum]["ent_api_details"].failure_state
                        }
                        if ($scope['state' + $scope.statenum]["ent_api_details"].apires[0] !== null) {
                            var keys = Object.keys($scope['state' + $scope.statenum]["ent_api_details"].apires[0]);
                            for (let i = 0; i < keys.length; i++) {
                                var obj = { mainkey: "", subkey: "", apivalue: "" };
                                $scope.apikeys[i] = obj;
                            }
                        }

                        $scope['state' + $scope.statenum]["ent_api_details"] = api_obj;
                        $scope.entry_response_msg = "";

                        $http.defaults.headers.common['user'] = $window.sessionStorage.id;
                        $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
                        $scope.apilist = [];
                        $http.get('/getapis', {}).then(function(res) {
                            for (var i = 0; i < res.data.length; i++) {
                                $scope.apilist[i] = {
                                    api_id: res.data[i]._id,
                                    api_name: (res.data[i].ApiName == undefined) ? res.data[i].api_info.url : res.data[i].ApiName,
                                    method: res.data[i].api_info.method,
                                    params: res.data[i].api_info.params,
                                    body: res.data[i].api_info.body,
                                    headerparams: res.data[i].api_info.headerparams,
                                    showparams: res.data[i].api_info.showparams,
                                    url: res.data[i].api_info.url,
                                    body_type: res.data[i].api_info.body_type,
                                    raw_json: res.data[i].api_info.raw_json
                                };
                            }
                            $scope.getapires($scope.statenum, $scope['state' + $scope.statenum]["ent_api_details"].api_id, "");
                        });
                    } else {
                        if ($scope['state' + $scope.statenum].api_name != undefined) {
                            $scope.getapires($scope.statenum, $scope['state' + $scope.statenum].api_name, "");
                        } else {
                            $scope.getapires($scope.statenum, $scope.apilist[0], "");
                        }
                        // else if ($scope['state' + $scope.statenum]["ent_api_details"][0].api_id != undefined) {
                        //     $scope.getapires($scope.statenum, $scope['state' + $scope.statenum]["ent_api_details"][0].api_id);
                        // }
                    }
                } else {
                    $scope.entry_response_msg = "Take Gambit Entry Api Response";
                }

                if ($scope['state' + $scope.statenum].send_res == true) {
                    // var apivalues = [];
                    // if ($scope['state' + $scope.statenum].previous_res == false && $scope.statenum !== "0") {
                    //     apiintvalues = $scope.apivalues.filter(e => e == $scope['state' + $scope.statenum].state);
                    // } else {
                    //     apiintvalues = $scope.apivalues;
                    // }
                    // $scope.previousres();
                    if ($scope['state' + $scope.statenum]["out_api_details"] == undefined) {
                        // var apivalues = $scope.apivalues.filter(e => e !== $scope['state' + $scope.statenum].state + '.ans');
                        var api_obj = {
                            "method": "get",
                            "url": "",
                            "params": [],
                            "body": [],
                            "headerparams": [],
                            "auth": {},
                            "showparams": false,
                            "body_type": "raw",
                            "raw_json": JSON.stringify({}),
                            "api_name": "",
                            "apires": [{}],
                            "api_url": "",
                            "api_id": ""
                        }
                        $scope['state' + $scope.statenum]["out_api_details"] = api_obj;
                        $http.defaults.headers.common['user'] = $window.sessionStorage.id;
                        $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
                        $scope.apiintlist = [];
                        $scope.apiintkeys = [{ mainkey: "", subkey: "", apivalue: "" }];
                        $http.get('/getapis', {}).then(function(res) {
                            for (var i = 0; i < res.data.length; i++) {
                                $scope.apiintlist[i] = {
                                    api_id: res.data[i]._id,
                                    api_name: (res.data[i].ApiName == undefined) ? res.data[i].api_info.url : res.data[i].ApiName,
                                    method: res.data[i].api_info.method,
                                    params: res.data[i].api_info.params,
                                    body: res.data[i].api_info.body,
                                    headerparams: res.data[i].api_info.headerparams,
                                    showparams: res.data[i].api_info.showparams,
                                    url: res.data[i].api_info.url,
                                    body_type: res.data[i].api_info.body_type,
                                    raw_json: res.data[i].api_info.raw_json
                                };
                            }

                            $scope.getapiintres($scope.statenum, $scope['state' + $scope.statenum]["out_api_details"].api_url, "");
                        });
                    } else if ($scope['state' + $scope.statenum]["out_api_details"][0] != undefined) {
                        // var apivalues = $scope.apivalues.filter(e => e !== $scope['state' + $scope.statenum].state);
                        var api_obj = {
                            "method": "get",
                            "url": "",
                            "params": [],
                            "body": [],
                            "headerparams": [],
                            "auth": {},
                            "showparams": false,
                            "body_type": "raw",
                            "raw_json": JSON.stringify({}),
                            "api_name": ($scope['state' + $scope.statenum]["out_api_details"][0].api_name != "") ? $scope['state' + $scope.statenum]["out_api_details"][0].api_name : "",
                            "apires": ($scope['state' + $scope.statenum]["out_api_details"][0].apires.length != 0 && $scope['state' + $scope.statenum]["out_api_details"][0].apires[0] !== null) ? $scope['state' + $scope.statenum]["out_api_details"][0].apires : [{}],
                            "api_id": ($scope['state' + $scope.statenum]["out_api_details"][0].api_id != "") ? $scope['state' + $scope.statenum]["out_api_details"][0].api_id : ""
                        }
                        if ($scope['state' + $scope.statenum]["out_api_details"][0].apires[0] !== null) {
                            var keys = Object.keys($scope['state' + $scope.statenum]["out_api_details"][0].apires[0]);
                            for (let i = 0; i < keys.length; i++) {
                                var obj = { mainkey: "", subkey: "", apivalue: "" };
                                $scope.apiintkeys[i] = obj;
                            }
                        }

                        $scope['state' + $scope.statenum]["out_api_details"] = api_obj;
                        $scope.entry_response_msg = "";

                        $http.defaults.headers.common['user'] = $window.sessionStorage.id;
                        $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
                        $scope.apiintlist = [];
                        $http.get('/getapis', {}).then(function(res) {
                            for (var i = 0; i < res.data.length; i++) {
                                $scope.apiintlist[i] = {
                                    api_id: res.data[i]._id,
                                    api_name: (res.data[i].ApiName == undefined) ? res.data[i].api_info.url : res.data[i].ApiName,
                                    method: res.data[i].api_info.method,
                                    params: res.data[i].api_info.params,
                                    body: res.data[i].api_info.body,
                                    headerparams: res.data[i].api_info.headerparams,
                                    showparams: res.data[i].api_info.showparams,
                                    url: res.data[i].api_info.url,
                                    body_type: res.data[i].api_info.body_type,
                                    raw_json: res.data[i].api_info.raw_json
                                };
                            }
                            // $scope['state' + $scope.statenum].api_name = ($scope['state' + $scope.statenum].api_name != undefined || $scope['state' + $scope.statenum].api_name != "") ? $scope['state' + $scope.statenum].api_name : $scope['state' + $scope.statenum]["ent_api_details"].api_name;
                            $scope.getapiintres($scope.statenum, $scope['state' + $scope.statenum]["out_api_details"].api_id, "");
                        });
                    } else if ($scope['state' + $scope.statenum]["out_api_details"] != undefined) {
                        // var apivalues = $scope.apivalues.filter(e => e !== $scope['state' + $scope.statenum].state);
                        var api_obj = {
                            "method": "get",
                            "url": "",
                            "params": [],
                            "body": [],
                            "headerparams": [],
                            "auth": {},
                            "showparams": false,
                            "body_type": "raw",
                            "raw_json": JSON.stringify({}),
                            "api_name": ($scope['state' + $scope.statenum]["out_api_details"].api_name != "") ? $scope['state' + $scope.statenum]["out_api_details"].api_name : "",
                            "apires": ($scope['state' + $scope.statenum]["out_api_details"].apires.length != 0 && $scope['state' + $scope.statenum]["out_api_details"].apires[0] !== null) ? $scope['state' + $scope.statenum]["out_api_details"].apires : [{}],
                            "api_id": ($scope['state' + $scope.statenum]["out_api_details"].api_id != "") ? $scope['state' + $scope.statenum]["out_api_details"].api_id : ""
                        }
                        if ($scope['state' + $scope.statenum]["out_api_details"].apires[0] !== null) {
                            var keys = Object.keys($scope['state' + $scope.statenum]["out_api_details"].apires[0]);
                            for (let i = 0; i < keys.length; i++) {
                                var obj = { mainkey: "", subkey: "", apivalue: "" };
                                $scope.apiintkeys[i] = obj;
                            }
                        }

                        $scope['state' + $scope.statenum]["out_api_details"] = api_obj;
                        $scope.entry_response_msg = "";

                        $http.defaults.headers.common['user'] = $window.sessionStorage.id;
                        $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
                        $scope.apiintlist = [];
                        $http.get('/getapis', {}).then(function(res) {
                            for (var i = 0; i < res.data.length; i++) {
                                $scope.apiintlist[i] = {
                                    api_id: res.data[i]._id,
                                    api_name: (res.data[i].ApiName == undefined) ? res.data[i].api_info.url : res.data[i].ApiName,
                                    method: res.data[i].api_info.method,
                                    params: res.data[i].api_info.params,
                                    body: res.data[i].api_info.body,
                                    headerparams: res.data[i].api_info.headerparams,
                                    showparams: res.data[i].api_info.showparams,
                                    url: res.data[i].api_info.url,
                                    body_type: res.data[i].api_info.body_type,
                                    raw_json: res.data[i].api_info.raw_json
                                };
                            }
                            // $scope['state' + $scope.statenum].api_name = ($scope['state' + $scope.statenum].api_name != undefined || $scope['state' + $scope.statenum].api_name != "") ? $scope['state' + $scope.statenum].api_name : $scope['state' + $scope.statenum]["ent_api_details"].api_name;
                            $scope.getapiintres($scope.statenum, $scope['state' + $scope.statenum]["out_api_details"].api_id, "");
                        });
                    } else {
                        if ($scope['state' + $scope.statenum]["out_api_details"].api_url != undefined) {
                            $scope.getapiintres($scope.statenum, $scope['state' + $scope.statenum]["out_api_details"].api_url, "");
                        } else {
                            $scope.getapiintres($scope.statenum, $scope['state' + $scope.statenum]["out_api_details"].apilist[0], "");
                        }

                    }
                }
                $scope.body_type = function(post_body_type) {
                    // // console.log(post_body_type);
                }
                $scope.body_type_int = function(post_body_type) {
                    // console.log(post_body_type);
                }
                $scope.post_body_type = "raw";

                $scope.formatting = { color: 'green', 'background-color': '#d0e9c6' };
                $scope.message = {};

                $scope.isValid = true;

                $scope.script = JSON.stringify($scope.message);

                $scope.updateJsonObject = function() {
                    try {
                        JSON.parse($scope['state' + $scope.statenum]['ent_api_details'].raw_json);
                    } catch (e) {
                        $scope.isValid = false;
                        $scope.formatting = { color: 'red', 'background-color': '#f2dede' };
                    }

                    $scope.message = JSON.parse($scope['state' + $scope.statenum]['ent_api_details'].raw_json);
                    $scope.isValid = true;
                    $scope.formatting = { color: 'green', 'background-color': '#d0e9c6' };
                };
                $scope.updateapiJsonObject = function() {
                    try {
                        JSON.parse($scope['state' + $scope.statenum]['out_api_details'].raw_json);
                    } catch (e) {
                        $scope.isValid = false;
                        $scope.formatting = { color: 'red', 'background-color': '#f2dede' };
                    }

                    $scope.message = JSON.parse($scope['state' + $scope.statenum]['out_api_details'].raw_json);
                    $scope.isValid = true;
                    $scope.formatting = { color: 'green', 'background-color': '#d0e9c6' };
                };

                // api code ends
                //gambit template default
                $scope.gambittemplate = function(value, statenum) {
                    // console.log(value, statenum);
                    ids = [];
                    if (value == "form") {
                        if ($scope['state' + statenum]['forms'] == undefined) {
                            $scope['state' + statenum]['forms'] = [];
                            $scope.fields = {
                                "header": "Form 1",
                                fields: [{
                                    type: 'text',
                                    name: 'Name',
                                    placeholder: 'Please enter your name',
                                    order: 1
                                }]
                            };

                            $scope['state' + statenum]['forms'].push($scope.fields);
                            $scope['state' + statenum].forms[0].heading = "Form 1";
                        }
                    }
                    $scope['state' + statenum].template_option = value;
                    // console.log("changed");
                }
                $scope.delform = function(statenum, index) {

                        $scope['state' + statenum]['forms'].splice(index, 1);
                    }
                    // state delete code
                $scope.delete_gambit = function(targetBoxId) {
                        $modal.open({
                            // locals: { gambit_init_mess: $scope.gambit_init_mess },
                            templateUrl: 'confirm.html',
                            controller: function($scope) {
                                $scope.confirm = function() {
                                    $scope.close();
                                    jsPlumb.detachAllConnections(targetBoxId);
                                    jsPlumb.removeAllEndpoints(targetBoxId);
                                    jsPlumb.detach(targetBoxId);
                                    $('#' + targetBoxId).remove();
                                    $scope.ok();
                                    $scope.final_obj.find(function(item, i) {
                                        if (item.state_name === targetBoxId) {
                                            $scope.final_obj.splice(i, 1);
                                        }
                                    });
                                };
                                $scope.close = function() {
                                    $scope.$close();
                                    $('.modal-backdrop').remove()
                                    $(document.body).removeClass("modal-open");
                                }
                            },
                            scope: $scope,
                            controllerAs: 'ctrl',
                            targetEvent: ev,
                            size: 'sm'
                        });
                    }
                    // multi conn code starts
                $scope.mul_conn = function(statenum) {
                    // // console.log($scope['state' + statenum]);
                    if ($scope['state' + statenum].multiple_conn == true) {
                        // // console.log("in if");
                        var ep = jsPlumb.selectEndpoints({ target: 'state' + statenum }).getParameters();
                        jsPlumb.deleteEndpoint(ep[0][1]);

                        var commons = {
                            isTarget: true,
                            maxConnections: -1,
                            connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                            endpoint: "Rectangle",
                            paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "blue" }
                        };
                        jsPlumb.addEndpoint('state' + statenum, {
                            anchors: ["Top"]
                        }, commons);
                    } else {
                        // console.log("in else");
                        var ep = jsPlumb.selectEndpoints({ target: 'state' + statenum }).getParameters();
                        jsPlumb.deleteEndpoint(ep[0][1]);
                        var commons = {
                            isTarget: true,
                            maxConnections: 1,
                            connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                            endpoint: "Rectangle",
                            paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "blue" }
                        };
                        jsPlumb.addEndpoint('state' + statenum, {
                            anchors: ["Top"]
                        }, commons);
                    }
                }
                $scope.change = function(statenum, option) {
                        // console.log("funct", option);
                        if (option == "Text") {
                            $scope.suggest_branches = false;
                            $scope.exp_in_mess = {
                                inputType: "Custom Text",
                                keyType: "Normal Keyboard",
                                capitalizeText: "Disabled",
                                inputValidation: "No Validation",
                                text_opt_Input: $scope.text_opt_Input,
                                text_mul_bub_Input: $scope.text_mul_bub_Input,
                                suggestions: '',
                                suggestion_box: '',
                                suggest_preview_list: $scope.suggest_preview_list,
                                suggest_opt_input: $scope.suggest_opt_input,
                                suggest_mul_bub_Input: $scope.suggest_mul_bub_Input
                            }
                            $scope['state' + statenum].exp_in_mess = $scope.exp_in_mess;
                        } else if (option == "Suggestion") {
                            $scope.suggest_branches = true;
                            $scope.exp_in_mess = {
                                inputType: "Custom Text",
                                suggestions: 'Custom List',
                                suggestion_box: [],
                                suggest_preview_list: $scope.suggest_preview_list,
                                suggest_opt_input: $scope.suggest_opt_input,
                                suggest_mul_bub_Input: $scope.suggest_mul_bub_Input,
                                suggest_branches: $scope.suggest_branches
                            }
                            $scope['state' + statenum].exp_in_mess = $scope.exp_in_mess;
                        } else if (option == "Carousel") {

                            $scope.suggest_branches = false;
                            var carouselprop = {
                                "title": "",
                                "sub_title": "",
                                "small_desc": "",
                                "img_form": "url",
                                "image": "",
                                "img_name": "",
                                "img_url": "",
                                "title2": "",
                                "sub_title2": "",
                                "small_desc2": "",
                                "button_text": "",
                                "bt_act": "",
                                "bt_payload": "",
                                "bt_link": "",
                                "footer": ""
                            };
                            $scope.exp_in_mess = {
                                carousel: [carouselprop]
                            }
                            $scope['state' + statenum].exp_in_mess = $scope.exp_in_mess;

                        } else {
                            $scope.suggest_branches = false;
                        }
                    }
                    // multi conn code ends
                    // multi lingual code start
                $scope.enable_multi_language = function() {
                    if ($scope['state' + $scope.statenum].enable_multilang == true) {
                        $scope.onLoad1();

                    } else if ($scope['state' + $scope.statenum].enable_multilang == false) {
                        $scope.checkboxClickHandler();
                        // transliterationControl.disableTransliteration();
                    }
                }

                // $scope.check = { multi: false };
                $scope.add_to_ids = function() {
                    if (transliterationControl.isTransliterationEnabled() == true) {
                        var current_element_id = document.activeElement.id;
                        var index_of_elem = ids.indexOf(current_element_id);

                        if (index_of_elem == -1) {
                            ids.push(current_element_id);
                            transliterationControl.makeTransliteratable(ids);
                            $scope.languageChangeHandler();
                        } else {
                            transliterationControl.makeTransliteratable(ids);
                            $scope.languageChangeHandler();
                        }
                    } else {}


                }
                var transliterationControl;
                var options = {
                    sourceLanguage: 'en',
                    destinationLanguage: ['ar', 'hi', 'kn', 'ml', 'ta', 'te'],
                    transliterationEnabled: false,
                    shortcutKey: 'ctrl+g'
                };
                transliterationControl =
                    new google.elements.transliteration.TransliterationControl(options);
                // transliterationControl.disableTransliteration()
                $scope.onLoad1 = function() {

                    var options = {
                        sourceLanguage: 'en',
                        destinationLanguage: ['ar', 'hi', 'kn', 'ml', 'ta', 'te'],
                        transliterationEnabled: true,
                        shortcutKey: 'ctrl+g'
                    };

                    // Create an instance on TransliterationControl with the required
                    // options.
                    transliterationControl =
                        new google.elements.transliteration.TransliterationControl(options);

                    // Enable transliteration in the textfields with the given ids.

                    // transliterationControl.makeTransliteratable(ids);

                    // Add the STATE_CHANGED event handler to correcly maintain the state
                    // of the checkbox.
                    transliterationControl.addEventListener(
                        google.elements.transliteration.TransliterationControl.EventType.STATE_CHANGED,
                        $scope.transliterateStateChangeHandler);

                    // Add the SERVER_UNREACHABLE event handler to display an error message
                    // if unable to reach the server.
                    transliterationControl.addEventListener(
                        google.elements.transliteration.TransliterationControl.EventType.SERVER_UNREACHABLE,
                        serverUnreachableHandler);

                    // Add the SERVER_REACHABLE event handler to remove the error message
                    // once the server becomes reachable.
                    transliterationControl.addEventListener(
                        google.elements.transliteration.TransliterationControl.EventType.SERVER_REACHABLE,
                        serverReachableHandler);

                    // Set the checkbox to the correct state.
                    // document.getElementById('checkboxId').checked = transliterationControl.isTransliterationEnabled();

                    // Populate the language dropdown
                    var destinationLanguage =
                        transliterationControl.getLanguagePair().destinationLanguage;
                    var languageSelect = document.getElementById('languageDropDown');
                    var supportedDestinationLanguages =
                        google.elements.transliteration.getDestinationLanguages(
                            google.elements.transliteration.LanguageCode.ENGLISH);
                    for (var lang in supportedDestinationLanguages) {
                        var opt = document.createElement('option');
                        opt.text = lang;
                        opt.value = supportedDestinationLanguages[lang];
                        if ($scope['state' + $scope.statenum].enable_multilang) {
                            if (destinationLanguage == opt.value) {
                                opt.selected = true;
                            }
                        }

                        try {
                            languageSelect.add(opt, null);
                        } catch (ex) {
                            languageSelect.add(opt);
                        }
                    }
                }

                // Handler for STATE_CHANGED event which makes sure checkbox status
                // reflects the transliteration enabled or disabled status.
                $scope.transliterateStateChangeHandler = function(e) {
                    document.getElementById('checkboxId').checked = e.transliterationEnabled;
                }

                // Handler for checkbox's click event.  Calls toggleTransliteration to toggle
                $scope.checkboxClickHandler = function() {
                    transliterationControl.toggleTransliteration();
                }

                // Handler for dropdown option change event.  Calls setLanguagePair to
                // set the new language.
                $scope.languageChangeHandler = function() {
                        var dropdown = document.getElementById('languageDropDown');
                        transliterationControl.setLanguagePair(
                            google.elements.transliteration.LanguageCode.ENGLISH,
                            dropdown.options[dropdown.selectedIndex].value);
                        $scope['state' + $scope.statenum].language = dropdown.options[dropdown.selectedIndex].value;

                    }
                    // SERVER_UNREACHABLE event handler which displays the error message.
                function serverUnreachableHandler(e) {
                    document.getElementById("errorDiv").innerHTML =
                        "Transliteration Server unreachable";
                }
                // SERVER_UNREACHABLE event handler which clears the error message.
                function serverReachableHandler(e) {
                    document.getElementById("errorDiv").innerHTML = "";
                }
                // multi lingual code ends
                // message setting code start
                $scope.show_option = function(e) {
                    var ind = e.currentTarget.dataset.value;
                    $scope['state' + $scope.statenum].gambit_init_mess[ind]["ellipsis" + ind] = true;
                    $scope['state' + $scope.statenum].gambit_init_mess[ind]["plus" + ind] = true;
                }

                $scope.hide_option = function(e) {
                    var ind = e.currentTarget.dataset.value;
                    $scope['state' + $scope.statenum].gambit_init_mess[ind]["ellipsis" + ind] = false;
                    $scope['state' + $scope.statenum].gambit_init_mess[ind]["picture" + ind] = false;
                    $scope['state' + $scope.statenum].gambit_init_mess[ind]["times" + ind] = false;
                    $scope['state' + $scope.statenum].gambit_init_mess[ind]["plus" + ind] = false;
                    $scope['state' + $scope.statenum].gambit_init_mess[ind]["text" + ind] = false;
                    $scope['state' + $scope.statenum].gambit_init_mess[ind]["video" + ind] = false;
                }
                $scope.apply_line = function() {
                    document.execCommand('underline');
                }
                $scope.apply_italic = function() {
                    document.execCommand('italic');
                }

                $scope.apply_bold = function(e) {
                    document.execCommand('bold');
                }
                $scope.apply_href = function() {}
                $scope.apply_h = function() {}
                $scope.tbubble = "12px";
                $scope.toppos = "39px";
                $scope.getSelectionText = function(event, index, type) {
                    var text = "";
                    if (window.getSelection) {
                        text = window.getSelection().toString();
                    } else if (document.selection && document.selection.type != "Control") {
                        text = document.selection.createRange().text;
                    }
                    if (text.length > 0) {
                        document.getElementById("state_tool" + index).style.display = "block"
                        $scope.tbubble = event.offsetX + "px";
                        $scope.toppos = (type == "image") ? '99px' : '39px';
                        // console.log("there exists text")
                    } else {
                        document.getElementById("state_tool" + index).style.display = "none";
                        $scope.display_tool = "none";
                        // console.log("no text was selected");
                    }
                }
                $scope.showButtonMenu = function(statenum, e, index, parentidx) {
                    var indexValue = e.currentTarget.dataset.value;
                    $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["ellipsis" + indexValue] = true;
                    $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["plus" + indexValue] = true;
                    console.log("entered");
                };

                $scope.hideButtonMenu = function(statenum, e, index, parentidx) {
                    var indexValue = e.currentTarget.dataset.value;
                    $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["ellipsis" + indexValue] = false;
                    $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["plus" + indexValue] = false;
                    $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["external-link" + indexValue] = false;
                    $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["phone" + indexValue] = false;
                    $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["text" + indexValue] = false;
                    $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["times" + indexValue] = false;
                    console.log("left");
                };
                $scope.addOption = function(statenum, index) {
                    if ($scope['state' + statenum].gambit_init_mess[index]["button_states"] == undefined) {
                        $scope['state' + statenum].gambit_init_mess[index]["button_states"] = [];
                    }
                    if ($scope['state' + statenum].gambit_init_mess[index].message.buttons == undefined) {
                        $scope['state' + statenum].gambit_init_mess[index].message.buttons = [];
                    }
                    $scope.buttonindex = $scope.buttonindex + 1;
                    $scope.buttonobj = {};
                    $scope.buttonobj["button_inputFields"] = { "url": false, "phone": false, "payload": false };
                    $scope.buttonobj["ellipsis" + $scope.buttonindex] = false;
                    $scope.buttonobj["ellipsis" + $scope.buttonindex] = false;
                    $scope.buttonobj["external-link" + $scope.buttonindex] = false;
                    $scope.buttonobj["phone" + $scope.buttonindex] = false;
                    $scope.buttonobj["text" + $scope.buttonindex] = false;
                    $scope.buttonobj["times" + $scope.buttonindex] = false;
                    $scope['state' + statenum].gambit_init_mess[index]["button_states"].push($scope.buttonobj);

                    var button_value = { "url": "", "phone": "", "payload": "", "title": "Option Text", "type": "" };

                    $scope['state' + statenum].gambit_init_mess[index].message.buttons.push(button_value);
                }
                $scope.ShowButtonIcons = function(statenum, e, index, parentidx) {
                    var indexValue = e.currentTarget.dataset.value;

                    if ($scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["phone" + indexValue] == true) {
                        $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["external-link" + indexValue] = false;
                        $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["phone" + indexValue] = false;
                        $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["text" + indexValue] = false;
                        $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["times" + indexValue] = false;
                    } else {
                        $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["external-link" + indexValue] = true;
                        $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["phone" + indexValue] = true;
                        $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["text" + indexValue] = true;
                        $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["times" + indexValue] = true;
                    }

                }
                $scope.externalLink = function(statenum, indexValue, index) {

                    $scope['state' + statenum].gambit_init_mess[index].message.buttons[indexValue].type = "externalLink";
                    $scope['state' + statenum].gambit_init_mess[index].button_states[indexValue].button_inputFields.url = true;
                    $scope['state' + statenum].gambit_init_mess[index].button_states[indexValue].button_inputFields.phone = false;
                    $scope['state' + statenum].gambit_init_mess[index].button_states[indexValue].button_inputFields.payload = false;
                }

                $scope.phoneContact = function(statenum, indexValue, index) {

                    $scope['state' + statenum].gambit_init_mess[index].message.buttons[indexValue].type = "phone";
                    $scope.maxlength = "13";
                    $scope['state' + statenum].gambit_init_mess[index].button_states[indexValue].button_inputFields.url = false;
                    $scope['state' + statenum].gambit_init_mess[index].button_states[indexValue].button_inputFields.phone = true;
                    $scope['state' + statenum].gambit_init_mess[index].button_states[indexValue].button_inputFields.payload = false;
                }

                $scope.payloadMessage = function(statenum, indexValue, index) {

                    $scope['state' + statenum].gambit_init_mess[index].message.buttons[indexValue].type = "payload";
                    $scope['state' + statenum].gambit_init_mess[index].button_states[indexValue].button_inputFields.url = false;
                    $scope['state' + statenum].gambit_init_mess[index].button_states[indexValue].button_inputFields.payload = true;
                    $scope['state' + statenum].gambit_init_mess[index].button_states[indexValue].button_inputFields.phone = false;
                }

                $scope.removeOption = function(statenum, indexValue, index) {
                    $scope['state' + statenum].gambit_init_mess[index].message.buttons.splice(indexValue, 1);
                    $scope['state' + statenum].gambit_init_mess[index].button_states.splice(indexValue, 1);
                }
                $scope.media_id_chat = false;
                $scope.enable_text_input = function(statenum, ind) {
                    $scope['state' + statenum].gambit_init_mess[ind].input_fields.text = true;
                    $scope['state' + statenum].gambit_init_mess[ind].input_fields.image = false;
                    $scope['state' + statenum].gambit_init_mess[ind].input_fields.video = false;
                    $scope['state' + statenum].gambit_init_mess[ind].message.mess_type = "text";
                    if (!$scope.tog_whatsapp.chat) {
                        $scope.media_id_chat = false;
                    }
                }
                $scope.enable_video = function(statenum, ind) {
                    $scope['state' + statenum].gambit_init_mess[ind].input_fields.text = false;
                    $scope['state' + statenum].gambit_init_mess[ind].input_fields.video = true;
                    $scope['state' + statenum].gambit_init_mess[ind].input_fields.image = false;
                }
                $scope.enable_image_input = function(statenum, ind) {
                        $scope['state' + statenum].gambit_init_mess[ind].input_fields.text = false;
                        $scope['state' + statenum].gambit_init_mess[ind].input_fields.image = true;
                        $scope['state' + statenum].gambit_init_mess[ind].input_fields.video = false;
                        $scope['state' + statenum].gambit_init_mess[ind].message.mess_type = "media";
                        $scope['state' + statenum].gambit_init_mess[ind].message.media_type = "image";
                        if ($scope.tog_whatsapp.chat) {
                            $scope.media_id_chat = true;
                        }
                    }
                    // message setting code end
                    // carousel code start
                $scope.templates = [
                        "Templateone"
                    ]
                    // carousel code end
                    // state save
                $scope.crtstateobj = function(statename, statenum) {

                    if ($scope[statename].state) {
                        $('#' + statename + ' .title').text($scope[statename].state + '[' + statenum + ']');
                    }

                    if ($scope[statename].exp_in_mess != undefined && $scope[statename].exp_in_mess.carousel != undefined) {
                        if ($scope[statename].entry_response == true || $scope[statename].entry_response == "true") {
                            var carousel = $scope[statename].exp_in_mess.carousel[0];
                            $scope[statename].exp_in_mess.carousel = [{}];
                            for (var key in carousel) {
                                if (carousel[key] != "") {
                                    $scope[statename].exp_in_mess.carousel[0][key] = "";
                                    $scope[statename].exp_in_mess.carousel[0][key] = carousel[key];
                                    if ($scope[statename].exp_in_mess.carousel[0].image != undefined && $scope[statename].exp_in_mess.carousel[0].image['$ngfBlobUrl'] !== undefined) {
                                        delete $scope[statename].exp_in_mess.carousel[0].image['$ngfBlobUrl'];
                                    }
                                }
                            }

                        } else {
                            for (var i = 0; i < $scope[statename].exp_in_mess.carousel.length; i++) {
                                if ($scope[statename].exp_in_mess.carousel[i].image != undefined && $scope[statename].exp_in_mess.carousel[i].image['$ngfBlobUrl'] !== undefined) {
                                    delete $scope[statename].exp_in_mess.carousel[i].image['$ngfBlobUrl'];
                                }
                            }
                        }
                    }
                    if ($scope.final_obj.length == 0) {
                        $scope[statename].out_mess = [];
                        for (var i = 0; i < $scope[statename].gambit_init_mess.length; i++) {
                            if ($scope[statename].gambit_init_mess[i].message.image != undefined && $scope[statename].gambit_init_mess[i].message.image['$ngfBlobUrl'] !== undefined) {
                                delete $scope[statename].gambit_init_mess[i].message.image['$ngfBlobUrl'];
                            }
                            $scope[statename].out_mess.push($scope[statename].gambit_init_mess[i].message);
                        }

                        $scope.final_obj.push($scope[statename]);
                        $scope.final_obj.sort(function(a, b) {
                            var nameA = a.state_name.substr(5),
                                nameB = b.state_name.substr(5)
                            return nameA - nameB
                        });
                        $scope.ok();
                        // $mdDialog.hide();
                    } else {

                        for (var i = 0; i < $scope.final_obj.length; i++) {

                            if ($scope.final_obj[i].state_name == statename) {
                                $scope.final_obj.splice(i, 1);
                                $scope[statename].out_mess = [];
                                for (var i = 0; i < $scope[statename].gambit_init_mess.length; i++) {
                                    if ($scope[statename].gambit_init_mess[i].message.image != undefined && $scope[statename].gambit_init_mess[i].message.image['$ngfBlobUrl'] !== undefined) {
                                        delete $scope[statename].gambit_init_mess[i].message.image['$ngfBlobUrl'];
                                    }
                                    $scope[statename].out_mess.push($scope[statename].gambit_init_mess[i].message);
                                }
                                $scope.final_obj.push($scope[statename]);
                                $scope.final_obj.sort(function(a, b) {
                                    var nameA = a.state_name.substr(5),
                                        nameB = b.state_name.substr(5)
                                    return nameA - nameB
                                });
                                $scope.ok();
                                // $mdDialog.hide();
                                break;
                            } else {
                                if (i == $scope.final_obj.length - 1) {
                                    $scope[statename].out_mess = [];
                                    for (var i = 0; i < $scope[statename].gambit_init_mess.length; i++) {
                                        if ($scope[statename].gambit_init_mess[i].message.image != undefined && $scope[statename].gambit_init_mess[i].message.image['$ngfBlobUrl'] !== undefined) {
                                            delete $scope[statename].gambit_init_mess[i].message.image['$ngfBlobUrl'];
                                        }
                                        $scope[statename].out_mess.push($scope[statename].gambit_init_mess[i].message);
                                    }
                                    $scope.final_obj.push($scope[statename]);
                                    $scope.final_obj.sort(function(a, b) {
                                        var nameA = a.state_name.substr(5),
                                            nameB = b.state_name.substr(5)
                                        return nameA - nameB
                                    });
                                    $scope.ok();
                                    // $mdDia/log.hide();
                                }
                            }
                        }
                    }

                    // console.log($scope[statename].input_option, "option");
                    if ($scope[statename].input_option == "Suggestion") {
                        if ($scope[statename].exp_in_mess.suggest_branches == true) {
                            var state_endpoints = jsPlumb.getEndpoints(statename);
                            // console.log(state_endpoints);
                            var length = $scope[statename].exp_in_mess.suggestion_box.length;
                            var endlength = state_endpoints.length - 1;
                            var newanchor = length - endlength;

                            // console.log($scope[statename].exp_in_mess.suggestion_box.length, "length of sugg", endlength, newanchor, state_endpoints.length);
                            if (state_endpoints.length == 2) {
                                var left_val = 0.6;
                                var j = 0;
                            } else if (state_endpoints.length - 1 > length) {
                                // console.log("it is greater", state_endpoints.length, length);
                                for (var i = (state_endpoints.length - 1); i >= (length + 1); i--) {
                                    var ep_id = state_endpoints[i];
                                    jsPlumb.deleteEndpoint(ep_id);
                                }
                            } else {
                                left_val = 0.6 + ((endlength - 1) * 0.1);
                                j = 0;
                            }

                            for (var i = 0; i < newanchor; i++) {
                                var common = {
                                    isSource: true,
                                    connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }]
                                };
                                if (j > 0) {
                                    left_val = left_val + 0.1;
                                }
                                j = j + 1;
                                var state = statename;
                                if (length > 1) {
                                    var width = document.getElementById(state).offsetWidth;
                                    document.getElementById(state).style.width = width + 10 + "px";
                                }
                                jsPlumb.addEndpoint(state, {
                                    anchor: [
                                        [left_val, 1.07, 1, 1, 1, -4]
                                    ],
                                    maxConnections: 1,
                                    connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                                    endpoint: ["Rectangle", { width: 10, height: 9 }],
                                    paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "black" }
                                }, common);
                                // if(i == newanchor - 1){
                                //   jsPlumb.recalculateOffsets(state);
                                // }
                            }
                            jsPlumb.recalculateOffsets(state)
                        } else {
                            var endpoints = jsPlumb.getEndpoints(statename);
                            // console.log(endpoints, "ep");
                            if (endpoints.length > 2) {
                                for (var i = 2; i < endpoints.length; i++) {
                                    var ep_id = endpoints[i];
                                    jsPlumb.deleteEndpoint(ep_id);
                                }
                            }
                        }
                    } else if ($scope[statename].input_option != "Suggestion" && $scope[statename].condif == false && $scope[statename].input_option != "Star Rating") {
                        var endpoints = jsPlumb.getEndpoints(statename);
                        // console.log(endpoints, "ep");
                        if (endpoints.length > 2) {
                            for (var i = 2; i < endpoints.length; i++) {
                                var ep_id = endpoints[i];
                                jsPlumb.deleteEndpoint(ep_id);
                            }
                        }

                    } else {

                    }

                    if ($scope[statename].input_option == "Star Rating") {
                        var endpoints = jsPlumb.getEndpoints(statename);
                        var length = $scope[statename].exp_in_mess.star_rating;
                        // console.log(endpoints.length, length);
                        var endlength = endpoints.length - 1;
                        var starcount = length - endlength;

                        if (endpoints.length == 2) {
                            var left_val = 0.6;
                            var j = 0;
                        } else if (endpoints.length > length) {
                            var starlength = parseInt(length);
                            // console.log("in else", starlength);
                            for (var i = (endpoints.length - 1); i >= (starlength + 1); i--) {
                                // console.log(i, "in for", endpoints, endpoints[i], ep_id);
                                var ep_id = endpoints[i];
                                jsPlumb.deleteEndpoint(ep_id);
                            }
                        } else {
                            left_val = 0.6 + ((endlength - 1) * 0.1);
                            j = 0;
                        }

                        for (var i = 0; i < starcount; i++) {
                            var common = {
                                isSource: true,
                                maxConnections: 1,
                                deleteEndpointsOnDetach: false,
                                connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                                endpoint: ["Rectangle", { width: 10, height: 8 }],
                                paintStyle: { strokeStyle: "blue", strokeWidth: 3, fillStyle: "black" }
                            };
                            if (j > 0) {
                                left_val = left_val + 0.1;
                            }
                            j = j + 1;
                            var state = statename;
                            if (length > 1) {
                                var width = document.getElementById(state).offsetWidth;
                                document.getElementById(state).style.width = width + 20 + "px";
                            }
                            var newval = i * 5;
                            jsPlumb.addEndpoint(state, {
                                anchor: [
                                    [left_val, 1.07, 1, 1, 1, -4], "Bottom"
                                ],
                                // anchor:[ "Bottom","Bottom"],
                            }, common);
                            if (i == starcount - 1) {
                                //   $scope.repaint();

                            }
                        }
                    }
                    if ($scope[statename].condif == true) {
                        var endpoints = jsPlumb.getEndpoints(statename);
                        $scope[statename]['cond_exps'] = [];
                        var exp = "";
                        var inter_exp = "";
                        for (var j = 0; j < $scope[statename].cond_details.A.length; j++) {

                            for (var k = 0; k < $scope[statename].cond_details.A[j].cond.length; k++) {


                                if ($scope[statename].cond_details.A[j].cond[k].cond == "is") {
                                    inter_exp = "=="
                                } else if ($scope[statename].cond_details.A[j].cond[k].cond == "is not") {
                                    inter_exp = "!="
                                } else if ($scope[statename].cond_details.A[j].cond[k].cond == "equals") {
                                    inter_exp = "==="
                                } else if ($scope[statename].cond_details.A[j].cond[k].cond == "not equals") {
                                    inter_exp = "!="
                                } else if ($scope[statename].cond_details.A[j].cond[k].cond == "less than") {
                                    inter_exp = "<"
                                } else if ($scope[statename].cond_details.A[j].cond[k].cond == "greater than") {
                                    inter_exp = ">"
                                } else {
                                    // string.indexOf(substring) !== -1
                                }
                                if (k == 0) {

                                    exp = "(" + $scope[statename].cond_details.A[j].cond[k].field + " " + inter_exp + " '" + $scope[statename].cond_details.A[j].cond[k].value + "' )";
                                } else {
                                    var cond_exp = "";
                                    if ($scope[statename].cond_details.A[j].cond[k].intercond == "OR") {
                                        cond_exp = "||"
                                    } else {
                                        cond_exp = "&&"
                                    }
                                    exp = exp + " " + cond_exp + "(" + $scope[statename].cond_details.A[j].cond[k].field + " " + inter_exp + "  '" + $scope[statename].cond_details.A[j].cond[k].value + "' )";
                                }
                                if (k == $scope[statename].cond_details.A[j].cond.length - 1) {

                                    $scope[statename]['cond_exps'].push(exp)

                                }

                            }
                        }

                        var length = $scope[statename].cond_details.A.length + 1;
                        var endlength = endpoints.length - 1;
                        var newanchor = length - endlength;
                        if (endpoints.length == 2) {
                            var left_val = 0.6;
                            var j = 0;
                        } else if (endpoints.length > length) {

                            for (var i = (endpoints.length - 1); i >= (length + 1); i--) {


                                var ep_id = endpoints[i];
                                jsPlumb.deleteEndpoint(ep_id);
                            }
                        } else {
                            left_val = 0.6 + ((endlength - 1) * 0.1);
                            j = 0;
                        }
                        for (var i = 0; i < newanchor; i++) {
                            var common = {
                                isSource: true,
                                connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                                endpoint: ["Rectangle", { width: 10, height: 8 }],
                                paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "black" }
                            };
                            if (j > 0) {
                                left_val = left_val + 0.1;
                            }
                            j = j + 1;
                            var state = statename;
                            if (length > 1) {
                                var width = document.getElementById(state).offsetWidth;
                                document.getElementById(state).style.width = width + 10 + "px";
                            }

                            jsPlumb.addEndpoint(state, {
                                anchor: [
                                    [left_val, 1.07, 1, 1, 1, -4], "Continuous"
                                ],
                                maxConnections: 1,
                                connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                                endpoint: ["Rectangle", { width: 10, height: 9 }],
                                paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "black" }
                            }, common);
                            if (i == newanchor - 1) {
                                $scope.repaint(state);
                                // jsPlumb.recalculateOffsets(state);
                            }
                        }
                        //jsPlumb.recalculateOffsets(state)
                    } else {

                    }
                    console.log("$scope[statename]", $scope[statename]);
                }
                $scope.repaint = function(state) {
                    // var state = "state0";
                    jsPlumb.recalculateOffsets(state);
                    var endpoints = jsPlumb.getEndpoints(state);
                    jsPlumb.repaintEverything();
                    for (var i = 0; i < endpoints.length; i++) {
                        if (endpoints[i].isSource == true) {
                            endpoints[i].endpoint.bounds.minY = endpoints[i].endpoint.bounds.minY + 56;
                        }
                    }
                }
            },
            scope: $scope,
            controllerAs: 'ctrl',
            targetEvent: ev,
            size: 'lg',
            resolve: {
                gambit_init_mess: function() {
                    return $scope.gambit_init_mess;
                }
            }
        });
    };

    $scope.optionIndexVal = 0;
    $scope.items = [];
    $scope.addOption = function(statenum) {
        $scope.buttonindex = $scope.buttonindex + 1;
        $scope.buttonobj = {};
        $scope.buttonobj["button_inputFields"] = { "url": false, "phone": false, "payload": false };
        $scope.buttonobj["ellipsis" + $scope.buttonindex] = false;
        $scope.buttonobj["plus" + $scope.buttonindex] = false;
        $scope.buttonobj["external-link" + $scope.buttonindex] = false;
        $scope.buttonobj["phone" + $scope.buttonindex] = false;
        $scope.buttonobj["text" + $scope.buttonindex] = false;
        $scope.buttonobj["times" + $scope.buttonindex] = false;
        $scope['state' + statenum].button_states.push($scope.buttonobj);
        var button_value = { "url": "", "phone": "", "payload": "", "title": "Option Text", "type": " " };
        // console.log($scope['state' + statenum].exp_in_mess);
        $scope['state' + statenum].exp_in_mess.button_data.push(button_value);


    }


    // $scope.showButtonMenu = function(statenum, e) {
    //     var indexValue = e.currentTarget.dataset.value
    //     $scope['state' + statenum].button_states[indexValue]["ellipsis" + indexValue] = true;
    //     $scope['state' + statenum].button_states[indexValue]["plus" + indexValue] = true;
    // };

    // $scope.hideButtonMenu = function(statenum, e) {
    //     var indexValue = e.currentTarget.dataset.value
    //     $scope['state' + statenum].button_states[indexValue]["ellipsis" + indexValue] = false;
    //     $scope['state' + statenum].button_states[indexValue]["plus" + indexValue] = false;
    //     $scope['state' + statenum].button_states[indexValue]["external-link" + indexValue] = false;
    //     $scope['state' + statenum].button_states[indexValue]["phone" + indexValue] = false;
    //     $scope['state' + statenum].button_states[indexValue]["text" + indexValue] = false;
    //     $scope['state' + statenum].button_states[indexValue]["times" + indexValue] = false;
    // };
    $scope.ShowButtonIcons = function(statenum, e) {
        var indexValue = e.currentTarget.dataset.value;
        if ($scope['state' + statenum].button_states[indexValue]["phone" + indexValue] == true) {
            $scope['state' + statenum].button_states[indexValue]["external-link" + indexValue] = false;
            $scope['state' + statenum].button_states[indexValue]["phone" + indexValue] = false;
            $scope['state' + statenum].button_states[indexValue]["text" + indexValue] = false;
            $scope['state' + statenum].button_states[indexValue]["times" + indexValue] = false;
        } else {
            $scope['state' + statenum].button_states[indexValue]["external-link" + indexValue] = true;
            $scope['state' + statenum].button_states[indexValue]["phone" + indexValue] = true;
            $scope['state' + statenum].button_states[indexValue]["text" + indexValue] = true;
            $scope['state' + statenum].button_states[indexValue]["times" + indexValue] = true;
        }

    }
    $scope.externalLink = function(statenum, indexValue) {
        // console.log($scope['state' + statenum].button_states);
        $scope['state' + statenum].exp_in_mess.button_data[indexValue].type = "externalLink";
        $scope['state' + statenum].button_states[indexValue].button_inputFields.url = true;
        $scope['state' + statenum].button_states[indexValue].button_inputFields.phone = false;
        $scope['state' + statenum].button_states[indexValue].button_inputFields.payload = false;
    }

    $scope.phoneContact = function(statenum, indexValue) {
        // console.log($scope['state' + statenum].button_states);
        $scope['state' + statenum].exp_in_mess.button_data[indexValue].type = "phone";
        $scope.maxlength = "13";
        $scope['state' + statenum].button_states[indexValue].button_inputFields.url = false;
        $scope['state' + statenum].button_states[indexValue].button_inputFields.phone = true;
        $scope['state' + statenum].button_states[indexValue].button_inputFields.payload = false;
    }

    $scope.payloadMessage = function(statenum, indexValue) {
        // console.log('payload')
        $scope['state' + statenum].exp_in_mess.button_data[indexValue].type = "payload";
        $scope['state' + statenum].button_states[indexValue].button_inputFields.url = false;
        $scope['state' + statenum].button_states[indexValue].button_inputFields.payload = true;
        $scope['state' + statenum].button_states[indexValue].button_inputFields.phone = false;
    }

    $scope.removeOption = function(statenum, i) {
        var index = $scope.items.indexOf(i);
        // console.log(i, $scope['state' + statenum].button_states);
        $scope['state' + statenum].button_states.splice(i, 1);
        // $scope['state' + statenum].exp_in_mess.exp_in_messsplice(index, 1);
    }

    var top = 20,
        left = 30;
    $scope.user = { phone_number: false };
    $scope.time = { stamp: false };
    $scope.addnewgambit = function(e) {
        // console.log("in edit");
        var newState = $('<div>').attr('id', 'state' + $scope.i).addClass('item');
        var start = $('<div>').attr('id', 'start' + $scope.i).addClass('fa fa-check-circle-o maincolor');
        var title = $('<div>').addClass('title').text('State ' + '[' + $scope.i + ']');
        var connect = $('<div>').addClass('connect');
        var divsize = ((Math.random() * 100) + 50).toFixed();
        start.css({
            'float': 'right',
            'margin': 5 + 'px',
            'font-size': 'large'
        });
        $newdiv = $('</div>').css({
            'width': divsize + 'px',
            'height': divsize + 'px'
        });
        var key = '#start' + $scope.i;
        var val = 'state' + $scope.i;
        $scope.startcheck[0][key] = val;
        var posx = (Math.random() * ($(document).width() - divsize)).toFixed();
        var posy = (Math.random() * ($(document).height() - divsize)).toFixed();

        newState.css({
            'top': posx,
            'left': posy
        });


        if ($scope.i == 0) {

        } else {

        }


        jsPlumb.draggable(newState, {
            containment: 'body',
            endpoint: "Rectangle"
        });

        var gambit_init_mess = [];
        var button_states = [];
        $scope.indexval = 0;
        $scope.buttonindex = 0;
        $scope.obj = {

        }
        $scope.buttonobj = {

        }
        $scope.text_mul_bub_Input = false;
        $scope.text_opt_Input = false;

        $scope.obj["message"] = { "text": "", "img_url": "", "title": "", "sub_title": "", "videoID": "", "img_form": "url", "image": "", "img_name": "", "buttons": [], "mess_type": "text", "media_type": "image", "media_id": "" };
        $scope.obj["input_fields"] = { "text": true, "image": false };
        $scope.obj["ellipsis" + $scope.indexval] = false;
        $scope.obj["plus" + $scope.indexval] = false;
        $scope.obj["picture" + $scope.indexval] = false;
        $scope.obj["times" + $scope.indexval] = false;
        $scope.obj["text" + $scope.indexval] = false;
        // $scope.obj["button_message"+$scope.buttonindex] = { "url": "", "phone": "", "payload": "" };
        $scope.buttonobj["button_inputFields"] = { "url": false, "phone": false, "payload": false };
        $scope.buttonobj["ellipsis" + $scope.buttonindex] = false;
        $scope.buttonobj["plus" + $scope.buttonindex] = false;
        $scope.buttonobj["external-link" + $scope.buttonindex] = false;
        $scope.buttonobj["phone" + $scope.buttonindex] = false;
        $scope.buttonobj["text" + $scope.buttonindex] = false;
        $scope.buttonobj["times" + $scope.buttonindex] = false;
        button_states.push($scope.buttonobj);
        $scope.obj["button_states"] = []
        $scope.obj["button_states"].push(button_states);
        gambit_init_mess.push($scope.obj);

        if ($scope.option == "Text") {
            $scope.exp_in_mess = {
                inputType: "Custom Text",
                keyType: "Normal Keyboard",
                capitalizeText: "Disabled",
                inputValidation: "No Validation",
                text_opt_Input: $scope.text_opt_Input,
                text_mul_bub_Input: $scope.text_mul_bub_Input,
                suggestions: '',
                suggestion_box: '',
                suggest_preview_list: $scope.suggest_preview_list,
                suggest_opt_input: $scope.suggest_opt_input,
                suggest_mul_bub_Input: $scope.suggest_mul_bub_Input
            }
        } else if ($scope.option == "Suggestion") {
            $scope.exp_in_mess = {
                suggestions: $scope.suggestions,
                suggest_preview_list: $scope.suggest_preview_list,
                suggest_opt_input: $scope.suggest_opt_input,
                suggest_mul_bub_Input: $scope.suggest_mul_bub_Input
            }
        } else if ($scope.option == "Carousel") {
            var carouselprop = {
                "title": "",
                "sub_title": "",
                "small_desc": "",
                "img_form": "url",
                "image": "",
                "img_name": "",
                "img_url": "",
                "title2": "",
                "sub_title2": "",
                "small_desc2": "",
                "button_text": "",
                "bt_act": "",
                "bt_payload": "",
                "bt_link": "",
                "footer": ""
            };
            $scope.exp_in_mess = {
                carousel: [carouselprop]
            }
        } else {

        }
        $scope['state' + $scope.i] = {
            entry_response: false,
            send_res: false,
            res_timeout: false,
            state_name: 'state' + $scope.i,
            out_mess: [],
            carousel: $scope.carousel,
            template: "",
            carousel_message: "",
            template_option: "message",
            // state :'State' +'['+i+']',
            state: '',
            exp_in_mess: $scope.exp_in_mess,
            next_state: "",
            input_option: $scope.option,
            gambit_init_mess: gambit_init_mess,
            connected_state: '',
            button_states: button_states,
            multiple_conn: false,
            api_type: "POST",
            res_timeout_type: "message",
            api_url: "",
            previous_res: false,
            condif: false,
            conditional_states: [],
            timeout_message: '',
            timeout_state: '',
            timeout_url: '',
            timeout_delay: 50000,
            url_params: [{ paramname: '' }],
            expresType: "text",
            enable_multilang: false,
            language: 'en',
            optional_input: false
        };
        // console.log($scope['state' + $scope.i], "this is state 0", 'state' + $scope.i);
        // start state
        start.on('click', function(e) {
            start.toggleClass("maincolor changecolor");
            var list = e.target.classList;
            $scope.startState = 'state' + e.target.id.substr(5);
            if (list.contains('maincolor')) {
                $.each($scope.startcheck[0], function(key1, val1) {
                    if ($scope.startState != val1) {
                        $(key1).attr('ng-disabled', 'false').css("pointer-events", "auto");
                    }
                });
                $scope.startState = "";
            } else if (list.contains('changecolor')) {
                $.each($scope.startcheck[0], function(key2, val2) {
                    if ($scope.startState != val2) {
                        $(key2).attr('ng-disabled', 'true').css("pointer-events", "none");
                    }
                });
            }
        });

        newState.dblclick(function(e) {
            // console.log(e, "e");
            $scope.showAlert(e);
            /*  jsPlumb.detachAllConnections($(this));
              $(this).remove();
              e.stopPropagation();*/
        });
        newState.append(start);
        newState.append(title);
        // newState.append(connect);

        $('#diagramContainer').append(newState);
        if ($scope.startState != "") {
            $.each($scope.startcheck[0], function(obj, value) {
                if ($scope.startState != value) {
                    $(obj).attr('ng-disabled', 'true');
                    $(obj).css("pointer-events", "none");
                }
            });
        }
        var state = 'state' + $scope.i;
        // console.log("current state", state);
        var common = {
            isSource: true,

            deleteEndpointsOnDetach: false,
            connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
            endpoint: ["Rectangle", { width: 10, height: 8 }],
            paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "black" }
        };
        jsPlumb.addEndpoint(state, {
            anchors: ["Bottom"]
        }, common);
        var commons = {
            isTarget: true,
            connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
            endpoint: "Rectangle",
            paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "blue" }
        };
        jsPlumb.addEndpoint(state, {
            anchors: ["Top"]
        }, commons);
        $scope.i++;
    }

    var last = {
        bottom: false,
        top: true,
        left: false,
        right: true
    };

    $scope.showSimpleToast = function(message, clsname) {
        ngToast.create({
            className: clsname,
            content: message,
            dismissOnTimeout: true,
            timeout: 2500,
            dismissButton: true,
            dismissButtonHtml: '&times',
            dismissOnClick: true
        });
    };

    $scope.addcond = function(statenum) {

        if ($scope['state' + statenum].condif == true) {
            $scope.cond = {
                "cond": [{
                    "cond": "is",
                    "field": "",
                    "value": "",
                    "intercond": "OR"
                }]
            }

            $scope['state' + statenum].cond_details.A.push($scope.cond);
            var length = $scope['state' + statenum].cond_details.A.length - 1;
            var ind = $scope['state' + statenum].conditional_states.indexOf('Default');
            $scope['state' + statenum].conditional_states.splice(ind, 0, 'cond' + length);
        } else {
            // $scope['state' + statenum]["cond_tetails"] = []
        }

    }
    $scope.addfirstcond = function(statenum, i) {


        $scope['state' + statenum]['cond_details'] = {
            "A": []
        }
        $scope.cond = {
            "cond": [{
                "cond": "is",
                "field": "",
                "value": "",
                "intercond": "OR"
            }]
        }

        $scope['state' + statenum].cond_details.A.push($scope.cond);
        $scope['state' + statenum].conditional_states.push('cond0');
        $scope['state' + statenum].conditional_states.push('Default');

        //   $scope['state' + statenum].cond_details['A'].push($scope.cond);

    }
    $scope.addchildcond = function(statenum, index) {
        var new_cond = {
            "cond": "is",
            "field": "",
            "value": "",
            "intercond": "OR"
        }

        $scope['state' + statenum].cond_details.A[index].cond.push(new_cond);
    }

    $scope.del_childcond = function(statenum, parentindex, childindex) {

        $scope['state' + statenum].cond_details.A[parentindex].cond.splice(childindex, 1);
        //$scope.schema.splice(i, 1);
    }

    $scope.del_parentcond = function(statenum, index) {
        $scope['state' + statenum].cond_details.A.splice(index, 1);
        $scope['state' + statenum].conditional_states.splice(index, 1);
        if ($scope['state' + statenum].cond_conn_state != undefined && $scope['state' + statenum].cond_conn_state['cond' + index] != undefined) {
            delete $scope['state' + statenum].cond_conn_state['cond' + index];
        }
    }
    $scope.download = function(ev) {
        var db_obj = {
            "bot_id": $scope.botId
        }
        $http.defaults.headers.common['user'] = $window.sessionStorage.id;
        $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
        $http.get('/getformlist?bot_id=' + $scope.botId, {
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(res) {
            // console.log(res);
            $scope.bot_forms = res.data;

        })
        $modal.open({
            templateUrl: 'report.tmpl.html',
            controller: function($scope) {
                $scope.ok = function() {
                    $scope.$close();
                }
            },
            scope: $scope,
            controllerAs: 'ctrl',
            targetEvent: ev,
            size: 'md'
        });
    }

    $scope.download_conv = function() {

        var url = '/download_conv?csv=true&bot_id=' + $scope.botId;
        window.open(url);

    }

    $scope.download_media = function() {
        var url = '/download_media?bot_id=' + $scope.botId;
        window.open(url);
    }

    $scope.getform_conv = function() {
        var db_obj = {
            "bot_id": $scope.botId
        }
        var url = '/getform_conv?csv=true&bot_id=' + $scope.botId + "&form=" + $scope.selected_form + '&media_files=' + $scope.form_media_files;
        window.open(url);
    }
    $scope.getform_conv_media = function() {
        var db_obj = {
            "bot_id": $scope.botId
        }
        var url = '/getform_conv_media?bot_id=' + $scope.botId + "&form=" + $scope.selected_form;
        window.open(url);
    }
    $scope.showPrompt = function(ev) {
        // Appending dialog to document.body to cover sidenav in docs app
        var confirm = prompt("Bot Name", "");
        if (confirm == "") {
            $scope.showSimpleToast("Please enter bot name", "danger");
        } else if (confirm == null) {
            $scope.status = 'You Cancelled';
        } else {
            var db_obj = {
                botId: "",
                botName: confirm,
                titlebarColor: $scope.band.Color,
                botDesc: $scope.bot.Description,
                webLink: $scope.web.link,
                postemail: $scope.post.email,
                tog_brand_icon: $scope.tog_brand.icon,
                tog_normal_chat: $scope.tog_normal.chat,
                tog_whatsapp_chat: $scope.tog_whatsapp.chat,
                titleColor: $scope.title.Color,
                textfieldColor: $scope.textfield.Color,
                buttonhoverColor: $scope.buttonhover.Color,
                backgroundColor: $scope.background.Color,
                bbubbleColor: $scope.bbubble.Color,
                carousel_background: $scope.carouselbackground.Color,
                carousel_button: $scope.carousel.button,
                ububbleColor: $scope.ububble.Color,
                buttonColor: $scope.button.Color,
                suggesColor: $scope.sugges.Color,
                fontColor: $scope.font.Color,
                botImage: $scope.bot_img_logo,
                webImage: $scope.web_bg_logo,
                botBgImage: $scope.bot_bg_logo,
                botFlow: $scope.final_obj,
                startState: $scope.startState,
                userPhoneNumber: $scope.user.phone_number,
                timeStamp: $scope.time.stamp,
                sender_id: $scope.sender.id
            }
            for (var i = 0; i < $scope.final_obj.length; i++) {
                $scope.final_obj[i]['left'] = angular.element('#' + $scope.final_obj[i].state_name).prop('offsetLeft');
                $scope.final_obj[i]['top'] = angular.element('#' + $scope.final_obj[i].state_name).prop('offsetTop');
                if (i == $scope.final_obj.length - 1) {
                    $http.defaults.headers.common['user'] = $window.sessionStorage.id;
                    $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
                    $http.post('/savebotflow', db_obj, {
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    }).then(function(res) {
                        // console.log(res);
                        if (res.data.status == "Accepted") {
                            //   $scope.botId = res.data.botid
                            $scope.showSimpleToast("Successfully Saved", "success");
                        }
                    })
                }
            }
            $scope.status = 'You decided to name your Bot ' + confirm + '.';
        }
    };

    $scope.mul_conn = function(statenum) {
            // console.log($scope['state' + statenum]);
            if ($scope['state' + statenum].multiple_conn == true) {
                // console.log("in if");
                var ep = jsPlumb.selectEndpoints({ target: 'state' + statenum }).getParameters();
                jsPlumb.deleteEndpoint(ep[0][1]);
                var commons = {
                    isTarget: true,
                    maxConnections: -1,
                    connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                    endpoint: "Rectangle",
                    paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "blue" }
                };
                jsPlumb.addEndpoint('state' + statenum, {
                    anchors: ["Top"]
                }, commons);
            } else {
                // console.log("in else");
                var ep = jsPlumb.selectEndpoints({ target: 'state' + statenum }).getParameters();
                jsPlumb.deleteEndpoint(ep[0][1]);

                var commons = {
                    isTarget: true,
                    maxConnections: 1,
                    connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                    endpoint: "Rectangle",
                    paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "blue" }
                };
                jsPlumb.addEndpoint('state' + statenum, {
                    anchors: ["Top"]
                }, commons);
            }
        }
        /* form */




    $scope.newField = {};
    $scope.form_list = [];
    $scope.fields = {
        "header": "Form 1",
        fields: [{
            type: 'text',
            name: 'Name',
            placeholder: 'Please enter your name',
            order: 1
        }]
    };
    $scope.form_list.push($scope.fields);

    $scope.editing = false;
    $scope.add_new_form = function(statenum) {

        $scope.fields = {
            "header": "",
            fields: [{
                type: 'text',
                name: 'Name',
                placeholder: 'Please enter your name',
                order: 1
            }]
        };
        // console.log($scope['state' + statenum], statenum);
        $scope['state' + statenum].forms.push($scope.fields);
        var len = $scope['state' + statenum].forms.length;
        $scope['state' + statenum].forms[len - 1].heading = "Form " + len;
    }
    $scope.delete_form = function(formindex, statenum) {
        $scope['state' + statenum].forms.splice(formindex, 1);
    };
    $scope.tokenize = function(slug1, slug2) {
        var result = slug1;
        result = result.replace(/[^-a-zA-Z0-9,&\s]+/ig, '');
        result = result.replace(/-/gi, "_");
        result = result.replace(/\s/gi, "-");
        if (slug2) {
            result += '-' + $scope.token(slug2);
        }
        return result;
    };

    $scope.addCarouselItem = function() {

        var carousel = {
            "title": "",
            "sub_title": "",
            "small_desc": "",
            "img_form": "url",
            "image": "",
            "img_name": "",
            "img_url": "",
            "title2": "",
            "sub_title2": "",
            "small_desc2": "",
            "button_text": "",
            "bt_act": "",
            "bt_payload": "",
            "bt_link": "",
            "footer": ""
        }
        $scope['state' + $scope.statenum].exp_in_mess.carousel.push(carousel);

    }
    $scope.delete_carousel = function(carouselindex, statenum) {
        $scope['state' + $scope.statenum].exp_in_mess.carousel.splice(index, 1);
    }

    $scope.saveField = function(index, statenum) {
        ids = [];
        // console.log("entered save");
        if ($scope.newField.type == 'checkboxes') {
            $scope.newField.value = {};
        }
        if ($scope.editing !== false) {
            $scope.fields.fields[$scope.editing] = $scope.newField;
            $scope.editing = false;
        } else {
            if ($scope.newField.type == 'date') {
                // console.log($scope.newField, $scope.newField.mindate.getFullYear(), $scope.newField.mindate.getMonth() + 1, $scope.newField.mindate.getDate());
                if ($scope.newField.mindate.getDate() < 10) {
                    var min_date = '0' + $scope.newField.mindate.getDate()
                } else {
                    var min_date = $scope.newField.mindate.getDate()
                }

                if ($scope.newField.maxdate.getDate() < 10) {
                    var max_date = '0' + $scope.newField.maxdate.getDate()
                } else {
                    var max_date = $scope.newField.maxdate.getDate()
                }
                if ($scope.newField.mindate.getMonth() < 10) {
                    var min_month = '0' + ($scope.newField.mindate.getMonth() + 1)
                } else {
                    var min_month = $scope.newField.mindate.getDate()
                }

                if ($scope.newField.maxdate.getMonth() < 10) {
                    var max_month = '0' + ($scope.newField.maxdate.getMonth() + 1)
                } else {
                    var max_month = $scope.newField.maxdate.getMonth()
                }
                $scope.newField.mindate = $scope.newField.mindate.getFullYear() + '-' + min_month + '-' + min_date;
                $scope.newField.maxdate = $scope.newField.maxdate.getFullYear() + '-' + max_month + '-' + max_date;
                $scope['state' + statenum].forms[index].fields.header = $scope['state' + statenum].forms[index].heading;
                $scope['state' + statenum].forms[index].fields.push($scope.newField);
            } else {
                $scope['state' + statenum].forms[index].fields.push($scope.newField);
            }

        }
        $scope.newField = {
            order: 0
        };
    };
    $scope.editField = function(field, formindex, fieldindex, statenum) {
        $scope.editing = $scope['state' + statenum].forms[formindex].fields[fieldindex];
        $scope.newField = field;
    };
    $scope.splice = function(field, formindex, fieldindex, statenum) {
        // console.log(field, formindex, fieldindex);
        $scope['state' + statenum].forms[formindex].fields.splice(fieldindex, 1);
    };
    $scope.addOptions = function() {
        if ($scope.newField.options === undefined) {
            $scope.newField.options = [];
        }
        $scope.newField.options.push({
            order: 0
        });
    };
    $scope.typeSwitch = function(type) {
        /*if (angular.Array.indexOf(['checkboxes','select','radio'], type) === -1)
          return type;*/
        if (type == 'checkboxes')
            return 'multiple';
        if (type == 'select')
            return 'multiple';
        if (type == 'radio')
            return 'multiple';

        return type;
    }

    /* end of form*/


    $scope.delete_gambit = function(targetBoxId) {
        // console.log(targetBoxId);

        jsPlumb.detachAllConnections(targetBoxId);
        jsPlumb.removeAllEndpoints(targetBoxId);
        jsPlumb.detach(targetBoxId); // <--
        $('#' + targetBoxId).remove();
        $mdDialog.hide();
        var index = $scope.final_obj.find(function(item, i) {
            if (item.state_name === targetBoxId) {
                $scope.final_obj.splice(i, 1);

            }

        });
    }

    $scope.getSelectionHtml = function() {
        var html = "";
        if (typeof window.getSelection != "undefined") {
            var sel = window.getSelection();
            if (sel.rangeCount) {
                var container = document.createElement("div");
                for (var i = 0, len = sel.rangeCount; i < len; ++i) {
                    container.appendChild(sel.getRangeAt(i).cloneContents());
                }
                html = container.innerHTML;
            }
        } else if (typeof document.selection != "undefined") {
            if (document.selection.type == "Text") {
                html = document.selection.createRange().htmlText;
            }
        }
        // console.log(html);
    }
    $scope.tbubble = "12px"
    $scope.display_tool = "none";
    // $scope.getSelectionText = function(event, index) {

    //     // console.log("function called", index, event.offsetX);

    //     var text = "";
    //     if (window.getSelection) {
    //         text = window.getSelection().toString();
    //     } else if (document.selection && document.selection.type != "Control") {
    //         text = document.selection.createRange().text;
    //     }
    //     if (text.length > 0) {
    //         document.getElementById("state_tool" + index).style.display = "block"
    //         $scope.tbubble = event.offsetX + "px"

    //         // console.log("there exists text")
    //     } else {
    //         document.getElementById("state_tool" + index).style.display = "none";
    //         $scope.display_tool = "none";
    //         // console.log("no text was selected");
    //     }
    // }
    // $scope.apply_line = function() {

    //     document.execCommand('underline');
    // }
    // $scope.apply_italic = function() {

    //     document.execCommand('italic');
    // }

    // $scope.apply_bold = function(e) {

    //     document.execCommand('bold');
    //     /* var text = "";
    //      if (window.getSelection) {
    //          text = window.getSelection().toString();
    //      } else if (document.selection && document.selection.type != "Control") {
    //          text = document.selection.createRange().text;
    //      }
    //      // console.log(text);
    //      var sel, range, node;
    //      if (window.getSelection) {
    //          sel = window.getSelection();
    //          if (sel.getRangeAt && sel.rangeCount) {
    //              range = window.getSelection().getRangeAt(0);
    //              // console.log(sel,document.queryCommandState("bold"));
    //             if(document.queryCommandState("bold") == false){
    //              var html = '<b>' + sel + '</b>'
    //             }
    //             else{
    //               var
    //             }

    //              range.deleteContents();

    //              var el = document.createElement("div");
    //              el.innerHTML = html;
    //              var frag = document.createDocumentFragment(), node, lastNode;
    //              while ( (node = el.firstChild) ) {
    //                  lastNode = frag.appendChild(node);
    //              }
    //              range.insertNode(frag);
    //          }
    //      } else if (document.selection && document.selection.createRange) {
    //          range = document.selection.createRange();
    //          range.collapse(false);
    //          range.pasteHTML(html);
    //      }*/

    // }


    var transliterationControl;
    var options = {
        sourceLanguage: 'en',
        destinationLanguage: ['ar', 'hi', 'kn', 'ml', 'ta', 'te'],
        transliterationEnabled: true,
        shortcutKey: 'ctrl+g'
    };
    transliterationControl =
        new google.elements.transliteration.TransliterationControl(options);
    transliterationControl.disableTransliteration()
    $scope.onLoad1 = function() {
        // console.log("it comes here");
        var options = {
            sourceLanguage: 'en',
            destinationLanguage: ['ar', 'hi', 'kn', 'ml', 'ta', 'te'],
            transliterationEnabled: true,
            shortcutKey: 'ctrl+g'
        };

        // Create an instance on TransliterationControl with the required
        // options.
        transliterationControl =
            new google.elements.transliteration.TransliterationControl(options);

        // Enable transliteration in the textfields with the given ids.

        //transliterationControl.makeTransliteratable(ids);

        // Add the STATE_CHANGED event handler to correcly maintain the state
        // of the checkbox.
        transliterationControl.addEventListener(
            google.elements.transliteration.TransliterationControl.EventType.STATE_CHANGED,
            $scope.transliterateStateChangeHandler);

        // Add the SERVER_UNREACHABLE event handler to display an error message
        // if unable to reach the server.
        transliterationControl.addEventListener(
            google.elements.transliteration.TransliterationControl.EventType.SERVER_UNREACHABLE,
            serverUnreachableHandler);

        // Add the SERVER_REACHABLE event handler to remove the error message
        // once the server becomes reachable.
        transliterationControl.addEventListener(
            google.elements.transliteration.TransliterationControl.EventType.SERVER_REACHABLE,
            serverReachableHandler);

        // Set the checkbox to the correct state.
        document.getElementById('checkboxId').checked =
            transliterationControl.isTransliterationEnabled();

        // Populate the language dropdown
        var destinationLanguage =
            transliterationControl.getLanguagePair().destinationLanguage;
        var languageSelect = document.getElementById('languageDropDown');
        var supportedDestinationLanguages =
            google.elements.transliteration.getDestinationLanguages(
                google.elements.transliteration.LanguageCode.ENGLISH);
        for (var lang in supportedDestinationLanguages) {
            var opt = document.createElement('option');
            opt.text = lang;
            opt.value = supportedDestinationLanguages[lang];

            if (destinationLanguage == opt.value) {
                opt.selected = true;
            }
            try {
                languageSelect.add(opt, null);
            } catch (ex) {
                languageSelect.add(opt);
            }
        }
    }

    // Handler for STATE_CHANGED event which makes sure checkbox status
    // reflects the transliteration enabled or disabled status.
    $scope.transliterateStateChangeHandler = function(e) {
        document.getElementById('checkboxId').checked = e.transliterationEnabled;
    }

    // Handler for checkbox's click event.  Calls toggleTransliteration to toggle
    $scope.checkboxClickHandler = function() {
        transliterationControl.toggleTransliteration();
    }

    // Handler for dropdown option change event.  Calls setLanguagePair to
    // set the new language.
    $scope.languageChangeHandler = function() {
        var dropdown = document.getElementById('languageDropDown');
        transliterationControl.setLanguagePair(
            google.elements.transliteration.LanguageCode.ENGLISH,
            dropdown.options[dropdown.selectedIndex].value);
        // console.log(dropdown.options[dropdown.selectedIndex].text, dropdown.options[dropdown.selectedIndex].value);
        // $scope.lang_selected = dropdown.options[dropdown.selectedIndex].text;
    }

    // SERVER_UNREACHABLE event handler which displays the error message.
    function serverUnreachableHandler(e) {
        document.getElementById("errorDiv").innerHTML =
            "Transliteration Server unreachable";
    }

    // SERVER_UNREACHABLE event handler which clears the error message.
    function serverReachableHandler(e) {
        document.getElementById("errorDiv").innerHTML = "";
    }


    $scope.check = false;
    $scope.add_to_ids = function() {
        if (transliterationControl.isTransliterationEnabled() == true) {
            var current_element_id = document.activeElement.id;
            var index_of_elem = ids.indexOf(current_element_id);

            // console.log("in add to ids", document.activeElement.id, index_of_elem);
            if (index_of_elem == -1) {
                // console.log(current_element_id);
                ids.push(current_element_id);
                // console.log(ids);
                transliterationControl.makeTransliteratable(ids);
                $scope.languageChangeHandler();
            } else {

            }
        } else {

        }


    }

    $scope.enable_multi_language = function() {
        // console.log($scope.check);
        if ($scope.check == false) {
            $scope.onLoad1();
        } else {
            $scope.checkboxClickHandler();
        }
    }
    $scope.ok = function() {
        $mdDialog.hide();
    }
    $scope.answer = function(answer) {
        $mdDialog.hide(answer);
    };
    $scope.showsettings = function(ev) {
        $modal.open({
            // locals: { gambit_init_mess: $scope.gambit_init_mess },
            templateUrl: 'botsettings.html',
            controller: function($scope) {
                // // console.log($scope.gambit_init_mess);
                $scope.ok = function() {
                    // $mdDialog.hide();
                    // $scope.$close();
                    $scope.$dismiss();
                    $('.modal-backdrop').remove();
                    $(document.body).removeClass("modal-open");
                }
            },
            scope: $scope,
            controllerAs: 'ctrl',
            targetEvent: ev,
            size: 'lg',
            resolve: {
                gambit_init_mess: function() {
                    return $scope.gambit_init_mess;
                }
            }
        });
        // $mdDialog.show({
        //         locals: { gambit_init_mess: $scope.gambit_init_mess },
        //         controller: () => $scope,
        //         controllerAs: 'ctrl',
        //         templateUrl: 'botsettings.html',
        //         targetEvent: ev,
        //         clickOutsideToClose: true,
        //         fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
        //     })
        //     .then(function(answer) {
        //         $scope.status = 'You said the information was "' + answer + '".';
        //     }, function() {
        //         $scope.status = 'You cancelled the dialog.';
        //     });
    }



    /*  function DialogController($scope, $mdDialog) {
        $scope.hide = function() {
          $mdDialog.hide();
        };

        $scope.cancel = function() {
          $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
          $mdDialog.hide(answer);
        };
      }*/

});
myApp.directive('contenteditable', ['$sce', function($sce) {
    return {
        restrict: 'A', // only activate on element attribute
        require: '?ngModel', // get a hold of NgModelController
        link: function(scope, element, attrs, ngModel) {
            if (!ngModel) return; // do nothing if no ng-model

            // Specify how UI should be updated
            ngModel.$render = function() {
                element.html($sce.getTrustedHtml(ngModel.$viewValue || ''));
                read(); // initialize
            };

            // Listen for change events to enable binding
            element.on('blur keyup change', function() {
                scope.$evalAsync(read);
            });

            // Write data to the model
            function read() {
                var html = element.html();
                // When we clear the content editable the browser leaves a <br> behind
                // If strip-br attribute is provided then we strip this out
                if (attrs.stripBr && html == '<br>') {
                    html = '';
                }
                ngModel.$setViewValue(html);
            }
        }
    };
}]);
myApp.directive('elementDraggable', ['$document', function($document) {
    return {
        link: function(scope, element, attr) {
            element.on('dragstart', function(event) {
                // console.log("javsjd");
                event.originalEvent.dataTransfer.setData('templateIdx', $(element).data('index'));
            });
        }
    };
}]);

myApp.directive('elementDrop', ['$document', function($document) {
    return {
        link: function(scope, element, attr) {

            element.on('dragover', function(event) {
                event.preventDefault();
            });

            $('.drop').on('dragenter', function(event) {
                event.preventDefault();
            })
            element.on('drop', function(event) {
                event.stopPropagation();
                var self = $(this);
                scope.$apply(function() {
                    // console.log(event);
                    var idx = event.originalEvent.dataTransfer.getData('templateIdx');

                    var insertIdx = self.data('index')
                        // console.log(idx);
                    scope.addElement(scope.dragElements[idx], insertIdx);
                });
            });
        }
    };
}]);