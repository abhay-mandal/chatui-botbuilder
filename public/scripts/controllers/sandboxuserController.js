var myApp = angular.module('webchatbot', ['ui.bootstrap', 'angularUtils.directives.dirPagination', 'ngAnimate', 'ngSanitize', 'ngToast']);
angular.module('webchatbot').controller('SandboxUserController', function($scope, sandboxuserService, $state, $window) {
    if (!$window.localStorage.id) {
        $state.go("login");
    }
    $scope.sanboxConfigs = [];
    $scope.page = 5;
    getAllSandboxConfigs();

    function getAllSandboxConfigs() {
        sandboxuserService.getAll()
        .then(function (res) {
           $scope.sanboxConfigs = res.data;
        })
        .catch(function (err) {
            $scope.sanboxConfigs = [];
            console.log("Error in getting sandbox configs" + err);
        });
    }

    $scope.sortBy = function(keyname) {
        $scope.sortKey = keyname;
        $scope.reverse = !$scope.reverse;
    }
    
});
