var myApp = angular.module('webchatbot', ['daterangepicker', 'ejangular']);
myApp.controller("reportdashboardCtrl", function($scope, reportServiceData) {

    $scope.totalconversations = 0;
    $scope.totalfiles = 0;
    $scope.totalUsersCount = 0;
    $scope.totalMessageCount = 0;
    $scope.conversationVsUsers = false;
    $scope.messageVsUsers = false;
    $scope.usersVsFiles = false;
    $scope.reportGridFlag = false;
    $scope.sunburstchartFlag = false;
    $scope.botDetails;
    $scope.reportGridData;

    $scope.filterType = { filterType: "menu" };
    
    getBotDetails();

    $scope.datePicker = {
        startDate: moment().subtract(29, "days"),
        endDate: moment()
    };

    //dafault radio flow type
    $scope.messageByType = ""; //both bot and user

    $scope.opts = {
        locale: {
            applyClass: 'btn-green',
            applyLabel: "Apply",
            fromLabel: "From",
            format: "MMM DD, YYYY",
            toLabel: "To",
            cancelLabel: 'Cancel',
            customRangeLabel: 'Custom range'
        },
        ranges: {
            'Last 10 Days': [moment().subtract(9, 'days'), moment()],
            'Last 60 Days': [moment().subtract(59, 'days'), moment()]
        }
    };

    function getBotDetails() {
        reportServiceData.loadBotDetails()
            .then(function(resp) {
                $scope.botDetails = resp.data;

                //set default bot to drop down
                $scope.botDetailsModel = { botId: resp.data[0].botId, botName: resp.data[0].botName };
                return true;
            })
            .then(() => {
                //Watch for date changes
                $scope.$watch('datePicker', function(newDate) {
                    callCommonFunction(newDate, $scope.botDetailsModel.botId, $scope.messageByType);
                }, false);
            })
            .catch(function(err) {
                console.log("Error", err);
            });
    }

    function callCommonFunction(newDate, botId, messageBy) {
        getTotalConversations(newDate, botId);
        getTotalUsersCount(newDate, botId);
        getTotalMessageCount(newDate, botId, messageBy);
        getTotalfiles(newDate, botId, messageBy);
        getConversationVsUsers(newDate, botId, messageBy);
        getMessageVsUsers(newDate, botId, messageBy);
        getUsersVsFiles(newDate, botId, messageBy);
        getReportGridData(newDate, botId, messageBy);
        //getSunburstchart(newDate, botId, messageBy);
    }

    $scope.changeBotFlowType = function() {
        callCommonFunction($scope.datePicker, $scope.botDetailsModel.botId, $scope.messageByType);
    }

    $scope.changeMessageByType = function(messageBy) {
        getTotalMessageCount($scope.datePicker, $scope.botDetailsModel.botId, messageBy);
        getTotalfiles($scope.datePicker, $scope.botDetailsModel.botId, messageBy);
        getMessageVsUsers($scope.datePicker, $scope.botDetailsModel.botId, messageBy);
        getUsersVsFiles($scope.datePicker, $scope.botDetailsModel.botId, messageBy);
        getReportGridData($scope.datePicker, $scope.botDetailsModel.botId, messageBy);
    }

    function getTotalConversations(dateRange, botId) {
        reportServiceData.totalconversations(dateRange, botId)
            .then(function(resp) {
                $scope.totalconversations = resp.data.total_conversations;
            })
            .catch(function(err) {
                $scope.totalconversations = 0;
            });
    }

    function getTotalfiles(dateRange, botId, messageBy) {
        reportServiceData.totalfiles(dateRange, botId, messageBy)
            .then(function(resp) {
                $scope.totalfiles = resp.data.total_files;
            })
            .catch(function(err) {
                $scope.totalfiles = 0;
            });
    }

    function getTotalUsersCount(dateRange, botId) {
        reportServiceData.totalUsersCount(dateRange, botId)
            .then(function(resp) {
                $scope.totalUsersCount = resp.data.total_users;
            })
            .catch(function(err) {
                $scope.totalUsersCount = 0;
            });
    }

    function getTotalMessageCount(dateRange, botId, messageBy) {
        reportServiceData.totalMessageCount(dateRange, botId, messageBy)
            .then(function(resp) {
                $scope.totalMessageCount = resp.data.total_messages;
            })
            .catch(function(err) {
                $scope.totalMessageCount = 0;
            });
    }

    function getConversationVsUsers(dateRange, botId) {
        reportServiceData.conversationVsUsers(dateRange, botId)
            .then(function(resp) {
                if (resp.data.length) {
                    $scope.conversationVsUsers = true;
                    generateChartConversationVsUsers(resp.data, dateRange);
                } else {
                    $scope.conversationVsUsers = false;
                }
            })
            .catch(function(err) {
                $scope.conversationVsUsers = false;
            });
    }

    function getMessageVsUsers(dateRange, botId, messageBy) {
        reportServiceData.messageVsUsers(dateRange, botId, messageBy)
            .then(function(resp) {
                if (resp.data.length) {
                    $scope.messageVsUsers = true;
                    generateChartMessageVsUsers(resp.data, dateRange);
                } else $scope.messageVsUsers = false;
            })
            .catch(function(err) {
                $scope.messageVsUsers = false;
                //console.log(err);
            });
    }

    function getUsersVsFiles(dateRange, botId, messageBy) {
        reportServiceData.usersVsFiles(dateRange, botId, messageBy)
            .then(function(resp) {
                if (resp.data.length) {
                    $scope.usersVsFiles = true;
                    generateChartUsersVsFiles(resp.data, dateRange);
                } else $scope.usersVsFiles = false;
            })
            .catch(function(err) {
                $scope.usersVsFiles = false;
                console.log(err);
            });
    }

    function getReportGridData(dateRange, botId, messageBy) {
        reportServiceData.reportGridData(dateRange, botId, messageBy)
            .then(function(resp) {
                if (resp.data.length) {
                    $scope.reportGridFlag = true;
                    $scope.reportGridData = resp.data;
                    $scope.col = [
                        { field: "date", width: 50, headerText: "Date", textAlign: "left" },
                        { field: "users", width: 75, headerText: "Number of Users", textAlign: "center" },
                        { field: "messages", width: 75, headerText: "Number of Messages", textAlign: "center" },
                        { field: "conversations", headerText: 'Number of Conversations', width: 100, textAlign: "center" },
                        { field: "files", headerText: 'Files', width: 90, textAlign: "center" }
                    ];

                    $scope.toolbar = ["excelExport"]; //["excelExport", "pdfExport"];
                    $scope.toolbarHandler = function(args) {
                        if (args.itemName == "Excel Export") {
                            args.cancel = true;
                            downloadExcelReport(dateRange, botId, messageBy, true);
                        }
                    }
                } else $scope.reportGridFlag = false;
            })
            .catch(function(err) {
                $scope.reportGridFlag = false;
                console.log(err);
            });
    }

    // function getSunburstchart(dateRange, botId) {
    //     reportServiceData.sunburstchart(dateRange, botId)
    //         .then(function(resp) {
    //             if (resp.data[0].children.length) {
    //                 $scope.sunburstchartFlag = true;
    //                 console.log($scope.sunburstchartFlag);
    //                 generateSunburstchart(resp.data[0]);
    //             } else {
    //                 $scope.sunburstchartFlag = false;
    //             }
    //         })
    //         .catch(function(err) {
    //             $scope.sunburstchartFlag = false;
    //         });
    // }

    $scope.downloadReport = function() {
        var reportUrl = reportServiceData.getReportUrl($scope.datePicker, $scope.botDetailsModel.botId);
        if (reportUrl) {
            window.open(reportUrl)
        }
    }

    downloadExcelReport = function(dateRange, botId, messageBy, exportCsv) {
        var ReportExcel = reportServiceData.reportGridData(dateRange, botId, messageBy, exportCsv);
        if (ReportExcel) {
            window.open(ReportExcel, '_blank');
        }
    }

    $scope.resetReport = function(){
        getBotDetails();
        $scope.datePicker = {
            startDate: moment().subtract(29, "days"),
            endDate: moment()
        };
    
        //dafault radio flow type
        $scope.messageByType = ""; //both bot and user
    
        $scope.opts = {
            locale: {
                applyClass: 'btn-green',
                applyLabel: "Apply",
                fromLabel: "From",
                format: "MMM DD, YYYY",
                toLabel: "To",
                cancelLabel: 'Cancel',
                customRangeLabel: 'Custom range'
            },
            ranges: {
                'Last 10 Days': [moment().subtract(9, 'days'), moment()],
                'Last 60 Days': [moment().subtract(59, 'days'), moment()]
            }
        };
    }

});