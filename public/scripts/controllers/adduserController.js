var myApp = angular.module('webchatbot', ['ui.bootstrap', 'ng-mfb', 'ngAnimate', 'ngSanitize', 'ngToast']);
myApp.controller('AddUserCtrl', function($scope, $http, $window, $state, $stateParams, $modal, $rootScope, ngToast) {
    if (!$window.localStorage.id) {
        $state.go("login");
    }
    $scope.formData = {
        username: '',
        email: '',
        password: '',
        role: ''
    };
    $http.defaults.headers.common['user'] = $window.sessionStorage.id;
    $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
    $http.get('/getroles', {}).then(function(res) {
        $scope.rolelist = [];
        $scope.rolelist = res.data;
    });

    $scope.submit = function(ev) {
        $scope.formData.role_id = parseInt($scope.formData.role_id);
        $scope.postdata = $scope.formData;
        $http.defaults.headers.common['user'] = $window.sessionStorage.id;
        $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
        $http.post('/create_user', $scope.postdata, {
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(res) {
            if (res.data == "Accepted") {
                $scope.message = "Successfully the user";
                $scope.showAlert(ev);
            } else {
                $scope.message = res.data;
                $scope.showAlert(ev);
            }
        });
    }
    $scope.showAlert = function(ev) {
        $modal.open({
            templateUrl: 'confirm.html',
            controller: function($scope) {
                $scope.close = function() {
                    $scope.$dismiss();
                    $('.modal-backdrop').remove()
                    $(document.body).removeClass("modal-open");
                };
            },
            scope: $scope,
            controllerAs: 'ctrl',
            targetEvent: ev,
            size: 'sm'
        });
    };
    $scope.clear = function() {
        $scope.formData = {
            username: '',
            email: '',
            password: '',
            role: ''
        };
    };
});