var myApp = angular.module('webClientModule', []);
myApp.controller('webCtrl', function($scope) {


    $scope.showSearch = true;

    $scope.close = function() {
        document.getElementById("searchPlace").innerHTML = "";
        var node = " <input id='pac-input' type='text' ng-model='autocomplete' placeholder='Search Place'> <div id='map'></div>"
        document.getElementById("searchPlace").innerHTML = node;
        var x = document.getElementById("mapModal");
        $('#mapModal').modal('hide');
    }


    $scope.initAutocomplete = function() {

        if (navigator.geolocation) {

            $scope.showSearch = false;

            navigator.geolocation.getCurrentPosition(function(p) {

                $scope.latitude = p.coords.latitude;
                $scope.longitude = p.coords.longitude;

                var currentLatLng = new google.maps.LatLng($scope.latitude, $scope.longitude);

                var map = new google.maps.Map(document.getElementById('map'), {
                    center: currentLatLng,
                    zoom: 13,
                    mapTypeId: 'roadmap'
                });

                var marker = new google.maps.Marker({
                    position: currentLatLng,
                    map: map,
                    title: "<div style='height:60px;width:200px'><b>Your location:</b><br />Latitude: " + $scope.latitude + "<br />Longitude: " + $scope.longitude
                });

                // Create the search box and link it to the UI element.
                var input = document.getElementById('pac-input');
                var searchBox = new google.maps.places.SearchBox(input);
                map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

                // Bias the SearchBox results towards current map's viewport.
                map.addListener(marker, 'bounds_changed', function(event) {
                    var infoWindow = new google.maps.InfoWindow();
                    infoWindow.setContent(marker.title);
                    infoWindow.open(map, marker);
                    searchBox.setBounds(map.getBounds());
                });

                var markers = [];
                // Listen for the event fired when the user selects a prediction and retrieve
                // more details for that place.
                searchBox.addListener('places_changed', function() {
                    var places = searchBox.getPlaces();
                    var address = places[0].formatted_address;
                    getLatitudeLongitude(showResult, address);

                    if (places.length == 0) {
                        return;
                    }

                    function getLatitudeLongitude(callback, address) {
                        // If adress is not supplied, use default value 'Ferrol, Galicia, Spain'
                        address = address || 'Ferrol, Galicia, Spain';
                        // Initialize the Geocoder
                        geocoder = new google.maps.Geocoder();
                        if (geocoder) {
                            geocoder.geocode({
                                'address': address
                            }, function(results, status) {
                                if (status == google.maps.GeocoderStatus.OK) {
                                    callback(results[0]);
                                }
                            });
                        }
                    }

                    function showResult(result) {
                        $scope.latitude = result.geometry.location.lat();
                        $scope.longitude = result.geometry.location.lng();
                    }

                    // Clear out the old markers.
                    markers.forEach(function(marker) {
                        marker.setMap(null);
                    });
                    markers = [];

                    // For each place, get the icon, name and location.
                    var bounds = new google.maps.LatLngBounds();

                    places.forEach(function(place) {
                        if (!place.geometry) {
                            // console.log("Returned place contains no geometry");
                            return;
                        }

                        // Create a marker for each place.
                        markers.push(new google.maps.Marker({
                            map: map,
                            title: place.name,
                            position: place.geometry.location,
                            marker: marker
                        }));

                        /*console.log(marker.getPosition());
                        console.log('markers array : ', markers[0]);
                        for (var k in markers[0]) {

                          var latlong = markers[0][k];
                          console.log(latlong[0], latlong.Yd);
                          break;
                        }*/

                        if (place.geometry.viewport) {
                            // Only geocodes have viewport.
                            bounds.union(place.geometry.viewport);
                        } else {
                            bounds.extend(place.geometry.location);
                        }
                    });
                    map.fitBounds(bounds);
                });
            })
        }

    }

});