var myApp = angular.module('webchatbot', []);

//controllers manage an object $scope in AngularJS (this is the view model)
myApp.controller('RegisterCtrl', function($scope, $mdDialog, $http, $element, $state, $mdToast) {

        $scope.submit = function() {
            $scope.postdata = $scope.vm.formData;
            // console.log($scope.postdata);
            $http.post('/create_user', $scope.postdata, {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }

            ).then(function(res) {
                // console.log(res);
                if (res.data == "Accepted") {
                    $state.go("login");
                } else {
                    $scope.message = res.data;
                    $scope.showAlert();
                }
            })
        }
        $scope.showAlert = function(ev, message) {

            $mdDialog.show(
                $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title()
                .textContent($scope.message)
                .ariaLabel('Alert Dialog Demo')
                .ok('ok')
                .targetEvent(ev)
            );
        };



    })
    .directive('ngEnter', function() {
        return function(scope, element, attrs) {
            element.bind("keydown keypress", function(event) {
                if (event.which === 13) {
                    scope.$apply(function() {
                        scope.$eval(attrs.ngEnter, { 'event': event });
                    });

                    event.preventDefault();
                }
            });
        };
    });