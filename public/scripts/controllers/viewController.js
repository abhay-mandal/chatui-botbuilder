var myApp = angular.module('webchatbot', ['ui.bootstrap', 'mgcrea.ngStrap.datepicker', 'ng-mfb', 'ngAnimate', 'ngSanitize', 'ngToast']);
myApp.config(['ngToastProvider',
    function(ngToastProvider) {
        ngToastProvider.configure({
            animation: 'slide',
            horizontalPosition: 'center',
            verticalPosition: 'top'
        });
    }
]);

//controllers manage an object $scope in AngularJS (this is the view model)
myApp.controller('ViewCtrl', function($scope, $mdDialog, $http, $element, $state, $window, $mdToast, $modal, $rootScope, $timeout, ngToast) {
    if (!$window.localStorage.id) {
        $state.go("login");
    }

    $scope.botId = $state.params.botId;
    var ids = [];

    function module(library_id, schema_id, title, description, x, y) {
        this.library_id = library_id;
        this.schema_id = schema_id;
        this.title = title;
        this.description = description;
        this.x = x;
        this.y = y;
    }

    // module should be visualized by title, icon
    $scope.library = [];

    // library_uuid is a unique identifier per module type in the library
    $scope.library_uuid = 0;

    // state is [identifier, x position, y position, title, description]
    $scope.schema = [];

    // schema_uuid should always yield a unique identifier, can never be decreased
    $scope.schema_uuid = 0;

    // todo: find out how to go back and forth between css and angular
    $scope.library_topleft = {
        x: 15,
        y: 145,
        item_height: 50,
        margin: 5,
    };

    $scope.module_css = {
        width: 150,
        height: 100, // actually variable
    };

    $scope.redraw = function() {
        $scope.schema_uuid = 0;
        jsPlumb.detachEveryConnection();
        $scope.schema = [];
        $scope.library = [];
        $scope.addModuleToLibrary("Sum", "Aggregates an incoming sequences of values and returns the sum",
            $scope.library_topleft.x + $scope.library_topleft.margin,
            $scope.library_topleft.y + $scope.library_topleft.margin);
        $scope.addModuleToLibrary("Camera", "Hooks up to hardware camera and sends out an image at 20 Hz",
            $scope.library_topleft.x + $scope.library_topleft.margin,
            $scope.library_topleft.y + $scope.library_topleft.margin + $scope.library_topleft.item_height);
    };

    // add a module to the library
    $scope.addModuleToLibrary = function(title, description, posX, posY) {
        // console.log("Add module " + title + " to library, at position " + posX + "," + posY);
        var library_id = $scope.library_uuid++;
        var schema_id = -1;
        var m = new module(library_id, schema_id, title, description, posX, posY);
        $scope.library.push(m);
    };

    // add a module to the schema
    $scope.addModuleToSchema = function(library_id, posX, posY) {
        // console.log("Add module " + title + " to schema, at position " + posX + "," + posY);
        var schema_id = $scope.schema_uuid++;
        var title = "Unknown";
        var description = "Likewise unknown";
        for (var i = 0; i < $scope.library.length; i++) {
            if ($scope.library[i].library_id == library_id) {
                title = $scope.library[i].title;
                description = $scope.library[i].description;
            }
        }
        var m = new module(library_id, schema_id, title, description, posX, posY);
        $scope.schema.push(m);
    };


    $scope.init = function() {
            jsPlumb.bind("ready", function() {
                jsPlumb.Defaults.Container = $("#diagramContainer");
                // jsPlumb.setSuspendDrawing(true);
                jsPlumb.bind("connection", function(info, ev) {
                    // console.log("in connection", info, ev);
                    if (ev != undefined) {
                        $scope.$apply(function() {

                            // console.log("Possibility to push connection into array", info.sourceId, info.targetId, "info", info, ev);

                            if ($scope[info.sourceId].input_option == "Suggestion") {
                                var endpoints_length = jsPlumb.getEndpoints(info.sourceId);
                                // console.log(jsPlumb.getEndpoints(info.sourceId), "ee");
                                $scope[info.sourceId]['sugg_conn_state'] = {

                                };
                                if ($scope[info.sourceId].exp_in_mess.suggest_branches == false) {
                                    $scope[info.sourceId].connected_state = info.targetId;
                                    for (var i = 0; i < $scope[info.sourceId].exp_in_mess.suggestion_box.length; i++) {
                                        $scope[info.sourceId]['sugg_conn_state'][$scope[info.sourceId].exp_in_mess.suggestion_box[i]] = info.targetId;
                                    }

                                } else {
                                    for (var i = 0; i < endpoints_length.length; i++) {

                                        if (i == 0) {
                                            if (endpoints_length[i].isSource == true) {

                                                if (endpoints_length[i].connections.length > 0) {
                                                    $scope[info.sourceId]['sugg_conn_state'][$scope[info.sourceId].exp_in_mess.suggestion_box[i]] = endpoints_length[i].connections[0].targetId;
                                                } else {

                                                }

                                            } else {

                                            }
                                        } else {
                                            if (endpoints_length[i].isSource == true) {

                                                if (endpoints_length[i].connections.length > 0) {
                                                    $scope[info.sourceId]['sugg_conn_state'][$scope[info.sourceId].exp_in_mess.suggestion_box[i - 1]] = endpoints_length[i].connections[0].targetId;
                                                } else {

                                                }

                                            } else {

                                            }
                                        }

                                    }
                                }
                            } else if ($scope[info.sourceId].input_option == "Star Rating") {
                                var endpoints_length = jsPlumb.getEndpoints(info.sourceId);
                                $scope[info.sourceId]['star_states'] = {

                                };
                                for (var i = 0; i < endpoints_length.length; i++) {
                                    if (i == 0) {
                                        if (endpoints_length[i].isSource == true) {
                                            // console.log($scope[info.sourceId]);
                                            if (endpoints_length[i].connections.length > 0) {
                                                $scope[info.sourceId]['star_states'][i + 1] = endpoints_length[i].connections[0].targetId;
                                                // console.log($scope[info.sourceId]['star_states']);
                                            } else {

                                            }

                                        } else {

                                        }
                                    } else {
                                        if (endpoints_length[i].isSource == true) {
                                            if (endpoints_length[i].connections.length > 0) {
                                                $scope[info.sourceId]['star_states'][i] = endpoints_length[i].connections[0].targetId;
                                                // console.log($scope[info.sourceId]['star_states']);
                                            } else {

                                            }
                                        } else {

                                        }
                                    }
                                }

                            } else {
                                $scope[info.sourceId].connected_state = info.targetId;
                                // console.log($scope[info.sourceId]);
                            }

                        });
                    } else {

                    }
                    // jsPlumb.setSuspendDrawing(false, true);
                });
                jsPlumb.bind("connectionDetached", function(info, ev) {

                    $scope.$apply(function() {

                        // console.log("Connection detached", info.sourceId, info.targetId, "info", ev);
                        $scope[info.sourceId].connected_state = "";
                        // console.log($scope[info.sourceId]);
                    });
                });
            });
        }
        // $timeout(function() {
    $scope.init();
    // }, 2000);
    $http.defaults.headers.common['user'] = $window.sessionStorage.id;
    $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
    $http.get("/getbot/?botId=" + $scope.botId, {
        headers: { 'Content-Type': 'application/json' }
    }).then(function(res) {
        // console.log(res, "res from db");
        var botFlow = res.data[0].botFlow;
        $scope.botFlow = res.data[0].botFlow;
        $scope.i = $scope.botFlow.length;
        $scope.bandColor = "#cc0000"
        $scope.titleColor = "#ffffff"
        $scope.backgroundColor = "#ffffff"
        $scope.bbubbleColor = "#99ccff"
        $scope.ububbleColor = "#cc99ff"
        $scope.buttonColor = "#cc6699"
        $scope.suggesColor = "#cc6699"
        $scope.fontColor = "#000000"
        $scope.botName = res.data[0].botName
        $scope.bandColor = res.data[0].titlebarColor
        $scope.botDescription = res.data[0].botDesc
        $scope.web_link = res.data[0].webLink
        $scope.postemail = res.data[0].postemail
        $scope.tog_brand = { icon: res.data[0].tog_brand_icon }
        $scope.tog_normal = { chat: res.data[0].tog_normal_chat }
        $scope.tog_whatsapp = { chat: res.data[0].tog_whatsapp_chat }
        $scope.titleColor = res.data[0].titleColor
        $scope.textfieldColor = res.data[0].textfieldColor
        $scope.buttonhoverColor = res.data[0].buttonhoverColor
        $scope.backgroundColor = res.data[0].backgroundColor
        $scope.bbubbleColor = res.data[0].bbubbleColor
        $scope.carousel_background = res.data[0].carousel_background
        $scope.carousel_button = res.data[0].carousel_button
        $scope.ububbleColor = res.data[0].ububbleColor
        $scope.buttonColor = res.data[0].buttonColor
        $scope.suggesColor = res.data[0].suggesColor
        $scope.fontColor = res.data[0].fontColor
        $scope.bot_img = res.data[0].botImage
        $scope.web_bg = res.data[0].webImage
        $scope.bot_bg = res.data[0].botBgImage
            // console.log(res.data[0]);
        for (let i = 0; i < botFlow.length; i++) {
            $timeout(function() {
                $scope.addgambit_1(botFlow[i], i);
                if (i == botFlow.length - 1) {
                    for (var j = 0; j < botFlow.length; j++) {
                        if (botFlow[j].exp_in_mess) {
                            if (botFlow[j].input_option == 'Suggestion') {
                                if (botFlow[j].exp_in_mess.suggestion_box) {
                                    if (botFlow[j].exp_in_mess.suggestion_box.length > 0) {

                                        if (botFlow[j].exp_in_mess.suggest_branches == false) {
                                            var conn = jsPlumb.connect({ source: $scope[botFlow[j].state_name + "_src"], target: $scope[botFlow[j].connected_state + "_tgt"] });
                                        } else {
                                            // // console.log("else");
                                            // // console.log(botFlow[j]);
                                            for (var k = 0; k < botFlow[j].exp_in_mess.suggestion_box.length; k++) {
                                                //  // console.log(k,"in for", botFlow[j].exp_in_mess, $scope[botFlow[j].state_name+"_src_"+k], $scope[botFlow[j].sugg_conn_state[botFlow[j].exp_in_mess.suggestion_box[k]]+"_tgt"],botFlow[j] );
                                                var conn = jsPlumb.connect({ source: $scope[botFlow[j].state_name + "_src_" + k], target: $scope[botFlow[j].sugg_conn_state[botFlow[j].exp_in_mess.suggestion_box[k]] + "_tgt"] });

                                            }
                                        }
                                    } else if (botFlow[j].connected_state != "") {
                                        var conn = jsPlumb.connect({ source: $scope[botFlow[j].state_name + "_src"], target: $scope[botFlow[j].connected_state + "_tgt"] });
                                    } else {

                                    }
                                } else {
                                    // console.log("does not have");
                                    if (botFlow[j].connected_state != "") {
                                        var conn = jsPlumb.connect({ source: $scope[botFlow[j].state_name + "_src"], target: $scope[botFlow[j].connected_state + "_tgt"] });
                                    } else if (botFlow[j].connected_state == "") {

                                    } else {

                                    }

                                }
                            } else if (botFlow[j].input_option == 'Star Rating') {

                                for (var k = 0; k < botFlow[j].exp_in_mess.star_rating; k++) {
                                    //   // console.log(k,"in for", botFlow[j]);
                                    //var conn = jsPlumb.connect({source: $scope[botFlow[j].state_name+"_src_"+k], target: $scope[botFlow[j].sugg_conn_state[botFlow[j].exp_in_mess.suggestion_box[k]]+"_tgt"] });
                                    var conn = jsPlumb.connect({ source: $scope[botFlow[j].state_name + "_src_" + k], target: $scope[botFlow[j].star_states[k + 1] + "_tgt"] });

                                }
                            } else {
                                if (botFlow[j].connected_state != "") {
                                    // console.log(botFlow[j]);
                                    var conn = jsPlumb.connect({ source: $scope[botFlow[j].state_name + "_src"], target: $scope[botFlow[j].connected_state + "_tgt"] });
                                } else if (botFlow[j].connected_state == "") {

                                } else {

                                }
                            }
                        } else if (botFlow[j].condif == true) {
                            // console.log(botFlow[j].cond_details);
                            $scope[botFlow[j].state_name]['cond_details'] == botFlow[j].cond_details
                            for (var k = 0; k < botFlow[j].conditional_states.length; k++) {
                                //   // console.log(k,"in for",botFlow[j].conditional_states,botFlow[j].cond_conn_state[conditional_states[k]], botFlow[j].conditional_states[botFlow[j].cond_conn_state[k]]);
                                var conn = jsPlumb.connect({ source: $scope[botFlow[j].state_name + "_src_" + k], target: $scope[botFlow[j].cond_conn_state[botFlow[j].conditional_states[k]] + "_tgt"] });

                            }
                        } else {
                            // console.log("its in else kjhsbcjh ");
                            if (botFlow[j].connected_state != "") {
                                // console.log(botFlow[j]);
                                var conn = jsPlumb.connect({ source: $scope[botFlow[j].state_name + "_src"], target: $scope[botFlow[j].connected_state + "_tgt"] });
                            } else if (botFlow[j].connected_state == "") {

                            } else {

                            }
                        }

                    }
                }
            }, 1500);



        }
    });
    // jsPlumb.repaintEverything();
    $scope.final_obj = [];
    $scope.addgambit_1 = function(state, i) {

        var top = state.top,
            left = state.left;
        // console.log(top, left);
        var newState = $('<div>').attr('id', state.state_name).addClass('item');
        if (state.state == "" || state.state == undefined) {
            var title = $('<div>').addClass('title').text(state.state_name);

        } else {
            var title = $('<div>').addClass('title').text(state.state);
        }

        var connect = $('<div>').addClass('connect');
        var divsize = ((Math.random() * 100) + 50).toFixed();

        $newdiv = $('</div>').css({
            'width': divsize + 'px',
            'height': divsize + 'px'
        });

        var posx = (Math.random() * ($(document).width() - divsize)).toFixed();
        var posy = (Math.random() * ($(document).height() - divsize)).toFixed();

        newState.css({
            'top': top,
            'left': left
        });



        newState.dblclick(function(e) {
            $scope.showAlert(e);
        });

        newState.append(title);
        // newState.append(connect);

        $('#diagramContainer').append(newState);
        jsPlumb.draggable(newState, {
            containment: 'parent',
            endpoint: "Rectangle"
        });
        // var state = state.state_name;
        // // console.log("current state",state);
        /*  var st_name = botFlow[j].state_name*/
        // console.log(state);
        if (state.exp_in_mess) {
            if (state.input_option == 'Suggestion') {
                if (state.exp_in_mess.suggestion_box) {
                    if (state.exp_in_mess.suggestion_box.length > 0) {
                        if (state.exp_in_mess.suggest_branches == false) {
                            $scope[state.state_name + "_src"] = jsPlumb.addEndpoint(state.state_name, {
                                isSource: true,
                                anchors: ["Bottom"],
                                deleteEndpointsOnDetach: false,
                                connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                                endpoint: ["Rectangle", { width: 10, height: 8 }],
                                paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "black" }

                            });

                            if (state.multiple_conn == true) {
                                $scope[state.state_name + "_tgt"] = jsPlumb.addEndpoint(state.state_name, {
                                    isTarget: true,
                                    anchors: ["Top"],
                                    maxConnections: -1,
                                    connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                                    endpoint: "Rectangle",
                                    paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "blue" }
                                });
                            } else {
                                $scope[state.state_name + "_tgt"] = jsPlumb.addEndpoint(state.state_name, {
                                    isTarget: true,
                                    anchors: ["Top"],
                                    connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                                    endpoint: "Rectangle",
                                    paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "blue" }
                                });
                            }
                            $scope[state.state_name + "_tgt"] = jsPlumb.addEndpoint(state.state_name, {
                                isTarget: true,
                                anchors: ["Top"],
                                maxConnections: -1,
                                connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                                endpoint: "Rectangle",
                                paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "blue" }
                            });

                        } else {
                            if (state.multiple_conn == true) {
                                $scope[state.state_name + "_tgt"] = jsPlumb.addEndpoint(state.state_name, {
                                    isTarget: true,
                                    anchors: ["Top"],
                                    maxConnections: -1,
                                    connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                                    endpoint: "Rectangle",
                                    paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "blue" }
                                });
                            } else {
                                $scope[state.state_name + "_tgt"] = jsPlumb.addEndpoint(state.state_name, {
                                    isTarget: true,
                                    anchors: ["Top"],
                                    connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                                    endpoint: "Rectangle",
                                    paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "blue" }
                                });
                            }
                            var width = document.getElementById(state.state_name).offsetWidth;
                            document.getElementById(state.state_name).style.width = width + 30 + "px";
                            var left_val = 0.4;
                            for (var k = 0; k < state.exp_in_mess.suggestion_box.length; k++) {
                                left_val = left_val + 0.1;
                                $scope[state.state_name + "_src_" + k] = jsPlumb.addEndpoint(state.state_name, {
                                    isSource: true,
                                    anchor: [
                                        [left_val, 1.07, 1, 1, 1, -4]
                                    ],
                                    deleteEndpointsOnDetach: false,
                                    connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                                    endpoint: ["Rectangle", { width: 10, height: 8 }],
                                    paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "black" }

                                });
                            }
                        }
                    } else {
                        $scope[state.state_name + "_src"] = jsPlumb.addEndpoint(state.state_name, {
                            isSource: true,
                            anchors: ["Bottom"],
                            deleteEndpointsOnDetach: false,
                            connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                            endpoint: ["Rectangle", { width: 10, height: 8 }],
                            paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "black" }

                        });
                        $scope[state.state_name + "_tgt"] = jsPlumb.addEndpoint(state.state_name, {
                            isTarget: true,
                            anchors: ["Top"],
                            maxConnections: -1,
                            connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                            endpoint: "Rectangle",
                            paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "blue" }
                        });
                    }
                } else {
                    $scope[state.state_name + "_src"] = jsPlumb.addEndpoint(state.state_name, {
                        isSource: true,
                        anchors: ["Bottom"],
                        deleteEndpointsOnDetach: false,
                        connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                        endpoint: ["Rectangle", { width: 10, height: 8 }],
                        paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "black" }

                    });
                    $scope[state.state_name + "_tgt"] = jsPlumb.addEndpoint(state.state_name, {
                        isTarget: true,
                        anchors: ["Top"],
                        maxConnections: -1,
                        connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                        endpoint: "Rectangle",
                        paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "blue" }
                    });
                }
            } else if (state.input_option == 'Star Rating') {
                if (state.multiple_conn == true) {
                    $scope[state.state_name + "_tgt"] = jsPlumb.addEndpoint(state.state_name, {
                        isTarget: true,
                        anchors: ["Top"],
                        maxConnections: -1,
                        connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                        endpoint: "Rectangle",
                        paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "blue" }
                    });
                } else {
                    $scope[state.state_name + "_tgt"] = jsPlumb.addEndpoint(state.state_name, {
                        isTarget: true,
                        anchors: ["Top"],
                        connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                        endpoint: "Rectangle",
                        paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "blue" }
                    });
                }
                var width = document.getElementById(state.state_name).offsetWidth;
                document.getElementById(state.state_name).style.width = width + 30 + "px";
                var left_val = 0.4;
                for (var k = 0; k < state.exp_in_mess.star_rating; k++) {
                    left_val = left_val + 0.1;
                    $scope[state.state_name + "_src_" + k] = jsPlumb.addEndpoint(state.state_name, {
                        isSource: true,
                        anchor: [
                            [left_val, 1.07, 1, 1, 1, -4]
                        ],
                        deleteEndpointsOnDetach: false,
                        connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                        endpoint: ["Rectangle", { width: 10, height: 8 }],
                        paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "black" }

                    });
                }
            } else {
                $scope[state.state_name + "_src"] = jsPlumb.addEndpoint(state.state_name, {
                    isSource: true,
                    anchors: ["Bottom"],
                    deleteEndpointsOnDetach: false,
                    connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                    endpoint: ["Rectangle", { width: 10, height: 8 }],
                    paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "black" }

                });
                $scope[state.state_name + "_tgt"] = jsPlumb.addEndpoint(state.state_name, {
                    isTarget: true,
                    anchors: ["Top"],
                    maxConnections: -1,
                    connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                    endpoint: "Rectangle",
                    paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "blue" }
                });
            }
        } else if (state.condif == true) {

            if (state.multiple_conn == true) {
                $scope[state.state_name + "_tgt"] = jsPlumb.addEndpoint(state.state_name, {
                    isTarget: true,
                    anchors: ["Top"],
                    maxConnections: -1,
                    connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                    endpoint: "Rectangle",
                    paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "blue" }
                });
            } else {
                $scope[state.state_name + "_tgt"] = jsPlumb.addEndpoint(state.state_name, {
                    isTarget: true,
                    anchors: ["Top"],
                    connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                    endpoint: "Rectangle",
                    paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "blue" }
                });
            }
            var width = document.getElementById(state.state_name).offsetWidth;
            document.getElementById(state.state_name).style.width = width + 30 + "px";
            var left_val = 0.4;
            for (var k = 0; k < state.conditional_states.length; k++) {
                left_val = left_val + 0.1;
                $scope[state.state_name + "_src_" + k] = jsPlumb.addEndpoint(state.state_name, {
                    isSource: true,
                    anchor: [
                        [left_val, 1.07, 1, 1, 1, -4]
                    ],
                    deleteEndpointsOnDetach: false,
                    connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                    endpoint: ["Rectangle", { width: 10, height: 8 }],
                    paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "black" }

                });
            }
        } else {
            $scope[state.state_name + "_src"] = jsPlumb.addEndpoint(state.state_name, {
                isSource: true,
                anchors: ["Bottom"],
                deleteEndpointsOnDetach: false,
                connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                endpoint: ["Rectangle", { width: 10, height: 8 }],
                paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "black" }

            });
            if (state.multiple_conn == true) {
                $scope[state.state_name + "_tgt"] = jsPlumb.addEndpoint(state.state_name, {
                    isTarget: true,
                    anchors: ["Top"],
                    maxConnections: -1,
                    connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                    endpoint: "Rectangle",
                    paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "blue" }
                });
            } else {
                $scope[state.state_name + "_tgt"] = jsPlumb.addEndpoint(state.state_name, {
                    isTarget: true,
                    anchors: ["Top"],
                    connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                    endpoint: "Rectangle",
                    paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "blue" }
                });
            }
        }


        var gambit_init_mess = [];
        var button_states = [];
        $scope.indexval = 0;
        $scope.buttonindex = 0;

        $scope.buttonobj = {

        }


        $scope.text_mul_bub_Input = false;
        $scope.text_opt_Input = false;
        $scope.option = state.input_option;

        for (var i = 0; i < state.out_mess.length; i++) {
            $scope.obj = {

            }

            if (state.out_mess[i].text != "") {
                // console.log(state.out_mess[i].text, "this is the mess");
                $scope.obj["message"] = { "text": state.out_mess[i].text, "img_url": "", "title": "", "sub_title": "", "videoID": "", "img_form": "url", "image": "", "img_name": "", "buttons": state.out_mess[i].buttons, "mess_type": state.out_mess[i].mess_type, "media_type": state.out_mess[i].media_type, "media_id": state.out_mess[i].media_id };
                $scope.obj["input_fields"] = { "text": true, "image": false };
                $scope.obj["ellipsis" + $scope.indexval] = false;
                $scope.obj["plus" + $scope.indexval] = false;
                $scope.obj["picture" + $scope.indexval] = false;
                $scope.obj["times" + $scope.indexval] = false;
                $scope.obj["text" + $scope.indexval] = false;
                $scope.obj["video" + $scope.indexval] = false;

            } else {
                $scope.obj["message"] = { "text": "", "img_url": state.out_mess[i].img_url, "title": state.out_mess[i].title, "sub_title": state.out_mess[i].sub_title, "videoID": state.out_mess[i].sub_title, "img_form": state.out_mess[i].img_form, "image": state.out_mess[i].image, "img_name": state.out_mess[i].img_name, "buttons": state.out_mess[i].buttons, "mess_type": state.out_mess[i].mess_type, "media_type": state.out_mess[i].media_type, "media_id": state.out_mess[i].media_id };
                $scope.obj["input_fields"] = { "text": false, "image": true };
                $scope.obj["ellipsis" + $scope.indexval] = false;
                $scope.obj["plus" + $scope.indexval] = false;
                $scope.obj["picture" + $scope.indexval] = false;
                $scope.obj["times" + $scope.indexval] = false;
                $scope.obj["text" + $scope.indexval] = false;
                $scope.obj["video" + $scope.indexval] = false;
            }
            if (state.out_mess[i].buttons != undefined) {
                $scope.obj["button_states"] = [];
                for (var j = 0; j < state.out_mess[i].buttons.length; j++) {
                    $scope.buttonobj["button_inputFields"] = { "url": false, "phone": false, "payload": false };
                    $scope.buttonobj["button_ellipsis" + $scope.buttonindex] = false;
                    $scope.buttonobj["button_plus" + $scope.buttonindex] = false;
                    $scope.buttonobj["button_external-link" + $scope.buttonindex] = false;
                    $scope.buttonobj["button_phone" + $scope.buttonindex] = false;
                    $scope.buttonobj["button_text" + $scope.buttonindex] = false;
                    $scope.buttonobj["button_times" + $scope.buttonindex] = false;
                    $scope.obj["button_states"].push($scope.buttonobj);
                }
            }
            gambit_init_mess.push($scope.obj);

        }

        // $scope.obj["button_message"+$scope.buttonindex] = { "url": "", "phone": "", "payload": "" };

        if ($scope.option == "Text") {
            $scope.exp_in_mess = {
                inputType: state.exp_in_mess.inputType,
                keyType: state.exp_in_mess.keyType,
                capitalizeText: state.exp_in_mess.capitalizeText,
                inputValidation: state.exp_in_mess.inputValidation,
                text_opt_Input: state.exp_in_mess.text_opt_Input,
                text_mul_bub_Input: state.exp_in_mess.text_mul_bub_Input,
                min_date: state.exp_in_mess.min_date,
                max_date: state.exp_in_mess.max_date,
                time: state.exp_in_mess.time,
                suggestions: '',
                suggestion_box: '',
                suggest_preview_list: $scope.suggest_preview_list,
                suggest_opt_input: $scope.suggest_opt_input,
                suggest_mul_bub_Input: $scope.suggest_mul_bub_Input
            }
        } else if ($scope.option == "Suggestion") {
            $scope.exp_in_mess = {
                inputType: state.exp_in_mess.inputType,
                suggestion_box: state.exp_in_mess.suggestion_box,
                suggestions: state.exp_in_mess.suggestions,
                suggest_preview_list: state.exp_in_mess.suggest_preview_list,
                suggest_opt_input: state.exp_in_mess.suggest_opt_input,
                suggest_mul_bub_Input: state.exp_in_mess.suggest_mul_bub_Input,
                suggest_branches: state.exp_in_mess.suggest_branches
            }
        } else if ($scope.option == "Button") {
            var button_value = { "url": "", "phone": "", "payload": "", "title": "Option Text", "type": " " };
            $scope.exp_in_mess = {
                button_data: [button_value]
            }
        } else if ($scope.option == "Star Rating") {
            $scope.exp_in_mess = {
                star_rating: state.exp_in_mess.star_rating
            }
        } else {

        }
        $scope[state.state_name] = {
            entry_response: state.entry_response,
            send_res: state.send_res,
            res_timeout: state.res_timeout,
            state_name: state.state_name,
            out_mess: state.out_mess,
            carousel: state.carousel,
            template: state.template,
            carousel_message: state.carousel_message,
            template_option: state.template_option,
            state: state.state,
            exp_in_mess: $scope.exp_in_mess,
            next_state: "",
            input_option: $scope.option,
            sugg_conn_state: state.sugg_conn_state,
            gambit_init_mess: gambit_init_mess,
            connected_state: state.connected_state,
            button_states: button_states,
            multiple_conn: state.multiple_conn,
            api_type: state.api_type,
            res_timeout_type: state.res_timeout_type,
            api_url: state.api_url,
            previous_res: state.previous_res,
            condif: state.condif,
            conditional_states: state.conditional_states,
            timeout_message: state.timeout_message,
            timeout_state: state.timeout_state,
            timeout_url: state.timeout_url,
            timeout_delay: state.timeout_delay,
            url_params: state.url_params,
            expresType: state.expresType
        }
        if ($scope.option == "Star Rating") {
            $scope[state.state_name]['star_states'] = state.star_states
        }
        $scope.final_obj.push($scope[state.state_name]);
        // console.log($scope[state.state_name], "to check button");
        if (state.template_option == 'form') {
            $scope[state.state_name]['forms'] = state.forms;
        }

    }
    $scope.options = [
        "Text",
        "Button",
        "Suggestion",
        "Location",
        "Upload",
        "None",
        "Star Rating"

    ];

    $scope.change = function(statenum, option) {
        // console.log("funct", option);
        if (option == "Text") {
            $scope.suggest_branches = false;
            $scope.exp_in_mess = {
                inputType: "Custom Text",
                keyType: "Normal Keyboard",
                capitalizeText: "Disabled",
                inputValidation: "No Validation",
                text_opt_Input: $scope.text_opt_Input,
                text_mul_bub_Input: $scope.text_mul_bub_Input,
                suggestions: '',
                suggestion_box: '',
                suggest_preview_list: $scope.suggest_preview_list,
                suggest_opt_input: $scope.suggest_opt_input,
                suggest_mul_bub_Input: $scope.suggest_mul_bub_Input
            }
            $scope['state' + statenum].exp_in_mess = $scope.exp_in_mess;
        } else if (option == "Suggestion") {
            $scope.suggest_branches = true;
            $scope.exp_in_mess = {
                inputType: "Custom Text",
                suggestions: '',
                suggestion_box: '',
                suggest_preview_list: $scope.suggest_preview_list,
                suggest_opt_input: $scope.suggest_opt_input,
                suggest_mul_bub_Input: $scope.suggest_mul_bub_Input,
                suggest_branches: $scope.suggest_branches
            }
            $scope['state' + statenum].exp_in_mess = $scope.exp_in_mess;
        } else if (option == "Button") {
            $scope.suggest_branches = false;
            var button_value = { "url": "", "phone": "", "payload": "", "title": "Option Text", "type": " " };
            $scope.exp_in_mess = {
                button_data: [button_value]
            }
            $scope['state' + statenum].exp_in_mess = $scope.exp_in_mess;
            // console.log($scope['state' + statenum].exp_in_mess);
        } else {
            $scope.suggest_branches = false;
        }
    }
    $scope.showAlert1 = function(ev) {
        $mdDialog.show(
            $mdDialog.alert()
            .parent(angular.element(document.querySelector('#popupContainer')))
            .clickOutsideToClose(true)
            .title('Dialog')
            .textContent('Desc.')
            .ariaLabel('Dialog')
            .ok('ok!')
            .targetEvent(ev)
        );
    };
    // $scope.cancel = function() {
    //     $mdDialog.hide();
    // };

    $scope.enable_text_input = function(statenum, ind) {
        $scope['state' + statenum].gambit_init_mess[ind].input_fields.text = true;
        $scope['state' + statenum].gambit_init_mess[ind].input_fields.image = false;
        // $scope['state' + statenum].gambit_init_mess[ind].input_fields.video = false;
        // $scope['state' + statenum].gambit_init_mess[ind].message.mess_type = "text";
    }
    $scope.enable_image_input = function(statenum, ind) {
        $scope['state' + statenum].gambit_init_mess[ind].input_fields.text = false;
        $scope['state' + statenum].gambit_init_mess[ind].input_fields.image = true;
        // $scope['state' + statenum].gambit_init_mess[ind].input_fields.video = false;
        // $scope['state' + statenum].gambit_init_mess[ind].message.mess_type = "media";

    }
    $scope.crtstateobj = function(statename, statenum) {

        if ($scope[statename].state) {
            $('#' + statename + ' .title').text($scope[statename].state + '[' + statenum + ']');
        }
        if ($scope.final_obj.length == 0) {
            $scope[statename].out_mess = [];
            for (var i = 0; i < $scope[statename].gambit_init_mess.length; i++) {
                $scope[statename].out_mess.push($scope[statename].gambit_init_mess[i].message);
            }

            $scope.final_obj.push($scope[statename]);
            // console.log($scope.final_obj, "i = 0");
            $mdDialog.hide();
        } else {

            for (var i = 0; i < $scope.final_obj.length; i++) {

                if ($scope.final_obj[i].state_name == statename) {
                    // console.log("found");
                    $scope.final_obj.splice(i, 1);
                    $scope[statename].out_mess = [];
                    for (var i = 0; i < $scope[statename].gambit_init_mess.length; i++) {
                        $scope[statename].out_mess.push($scope[statename].gambit_init_mess[i].message);
                    }
                    $scope.final_obj.push($scope[statename]);
                    // console.log($scope.final_obj, "here in for");
                    $mdDialog.hide();
                    break;
                } else {
                    if (i == $scope.final_obj.length - 1) {
                        $scope[statename].out_mess = [];
                        for (var i = 0; i < $scope[statename].gambit_init_mess.length; i++) {
                            $scope[statename].out_mess.push($scope[statename].gambit_init_mess[i].message);
                        }
                        $scope.final_obj.push($scope[statename]);
                        // console.log($scope.final_obj, "in for");
                        $mdDialog.hide();
                    }
                }
            }
        }

        // console.log($scope[statename].input_option, "option");
        if ($scope[statename].input_option == "Suggestion") {
            if ($scope[statename].exp_in_mess.suggest_branches == true) {
                var state_endpoints = jsPlumb.getEndpoints(statename);
                // console.log(state_endpoints);
                var length = $scope[statename].exp_in_mess.suggestion_box.length;
                var endlength = state_endpoints.length - 1;
                var newanchor = length - endlength;

                // console.log($scope[statename].exp_in_mess.suggestion_box.length, "length of sugg", endlength, newanchor, state_endpoints.length);
                if (state_endpoints.length == 2) {
                    var left_val = 0.6;
                    var j = 0;
                } else if (state_endpoints.length - 1 > length) {
                    // console.log("it is greater", state_endpoints.length, length);
                    for (var i = (state_endpoints.length - 1); i >= (length + 1); i--) {
                        var ep_id = state_endpoints[i];
                        jsPlumb.deleteEndpoint(ep_id);
                    }
                } else {
                    left_val = 0.6 + ((endlength - 1) * 0.1);
                    j = 0;
                }

                for (var i = 0; i < newanchor; i++) {
                    var common = {
                        isSource: true,
                        connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }]
                    };
                    if (j > 0) {
                        left_val = left_val + 0.1;
                    }
                    j = j + 1;
                    var state = statename;
                    if (length > 1) {
                        var width = document.getElementById(state).offsetWidth;
                        document.getElementById(state).style.width = width + 10 + "px";
                    }
                    jsPlumb.addEndpoint(state, {
                        anchor: [
                            [left_val, 1.07, 1, 1, 1, -4]
                        ],
                        maxConnections: 1,
                        connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                        endpoint: ["Rectangle", { width: 10, height: 9 }],
                        paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "black" }
                    }, common);
                    // if(i == newanchor - 1){
                    //   jsPlumb.recalculateOffsets(state);
                    // }
                }
                jsPlumb.recalculateOffsets(state)
            } else {
                var endpoints = jsPlumb.getEndpoints(statename);
                // console.log(endpoints, "ep");
                if (endpoints.length > 2) {
                    for (var i = 2; i < endpoints.length; i++) {
                        var ep_id = endpoints[i];
                        jsPlumb.deleteEndpoint(ep_id);
                    }
                }
            }
        } else if ($scope[statename].input_option != "Suggestion" && $scope[statename].condif == false && $scope[statename].input_option != "Star Rating") {
            var endpoints = jsPlumb.getEndpoints(statename);
            // console.log(endpoints, "ep");
            if (endpoints.length > 2) {
                for (var i = 2; i < endpoints.length; i++) {
                    var ep_id = endpoints[i];
                    jsPlumb.deleteEndpoint(ep_id);
                }
            }

        } else {

        }

        if ($scope[statename].input_option == "Star Rating") {
            var endpoints = jsPlumb.getEndpoints(statename);
            var length = $scope[statename].exp_in_mess.star_rating;
            // console.log(endpoints.length, length);
            var endlength = endpoints.length - 1;
            var starcount = length - endlength;

            if (endpoints.length == 2) {
                var left_val = 0.6;
                var j = 0;
            } else if (endpoints.length > length) {
                var starlength = parseInt(length);
                // console.log("in else", starlength);
                for (var i = (endpoints.length - 1); i >= (starlength + 1); i--) {
                    // console.log(i, "in for", endpoints, endpoints[i], ep_id);
                    var ep_id = endpoints[i];
                    jsPlumb.deleteEndpoint(ep_id);
                }
            } else {
                left_val = 0.6 + ((endlength - 1) * 0.1);
                j = 0;
            }

            for (var i = 0; i < starcount; i++) {
                var common = {
                    isSource: true,
                    maxConnections: 1,
                    deleteEndpointsOnDetach: false,
                    connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                    endpoint: ["Rectangle", { width: 10, height: 8 }],
                    paintStyle: { strokeStyle: "blue", strokeWidth: 3, fillStyle: "black" }
                };
                if (j > 0) {
                    left_val = left_val + 0.1;
                }
                j = j + 1;
                var state = statename;
                if (length > 1) {
                    var width = document.getElementById(state).offsetWidth;
                    document.getElementById(state).style.width = width + 20 + "px";
                }
                var newval = i * 5;
                jsPlumb.addEndpoint(state, {
                    anchor: [
                        [left_val, 1.07, 1, 1, 1, -4], "Bottom"
                    ],
                    // anchor:[ "Bottom","Bottom"],
                }, common);
                if (i == starcount - 1) {
                    //   $scope.repaint();

                }
            }
        }
    }

    $scope.addnewtxt = function() {
        // console.log("function called");
        $scope.indexval = $scope.indexval + 1;
        var text = "text" + $scope.indexval;
        $scope.obj = {

        }
        $scope.obj["message"] = { "text": "", "img_url": "", "title": "", "sub_title": "", "mess_type": "text", "media_type": "image", "media_id": "" };
        $scope.obj["input_fields"] = { "text": true, "image": false };
        $scope.obj["ellipsis" + $scope.indexval] = false;
        $scope.obj["plus" + $scope.indexval] = false;
        $scope.obj["picture" + $scope.indexval] = false;
        $scope.obj["times" + $scope.indexval] = false;
        $scope.obj["text" + $scope.indexval] = false;
        $scope.buttonobj["button_inputFields"] = { "url": false, "phone": false, "payload": false };
        $scope.buttonobj["button_ellipsis" + $scope.buttonindex] = false;
        $scope.buttonobj["button_plus" + $scope.buttonindex] = false;
        $scope.buttonobj["button_external-link" + $scope.buttonindex] = false;
        $scope.buttonobj["button_phone" + $scope.buttonindex] = false;
        $scope.buttonobj["button_text" + $scope.buttonindex] = false;
        $scope.buttonobj["button_times" + $scope.buttonindex] = false;
        $scope.obj["button_states"] = [];
        $scope.obj["button_states"].push($scope.buttonobj);
        $scope['state' + $scope.statenum].gambit_init_mess.push($scope.obj);
    }

    $scope.show_option = function(e) {

        // console.log(e, e.currentTarget.dataset.value);
        var ind = e.currentTarget.dataset.value;
        $scope['state' + $scope.statenum].gambit_init_mess[ind]["ellipsis" + ind] = true;
        $scope['state' + $scope.statenum].gambit_init_mess[ind]["plus" + ind] = true;

    }
    $scope.hide_option = function(e) {
        // console.log(e.currentTarget.dataset.index, e.currentTarget.dataset.value);
        var ind = e.currentTarget.dataset.value;

        $scope['state' + $scope.statenum].gambit_init_mess[ind]["ellipsis" + ind] = false;
        $scope['state' + $scope.statenum].gambit_init_mess[ind]["picture" + ind] = false;
        $scope['state' + $scope.statenum].gambit_init_mess[ind]["times" + ind] = false;
        $scope['state' + $scope.statenum].gambit_init_mess[ind]["plus" + ind] = false;
        $scope['state' + $scope.statenum].gambit_init_mess[ind]["text" + ind] = false;

    }

    $scope.remove_message = function(statenum, ind) {
        $scope['state' + statenum].gambit_init_mess.splice(ind, 1);
    }

    $scope.save = function() {
        var db_obj = {
            botId: $scope.botId,
            botName: $scope.botName,
            titlebarColor: $scope.bandColor,
            botDesc: $scope.botDescription,
            webLink: $scope.web_link,
            postemail: $scope.postemail,
            tog_brand_icon: $scope.tog_brand_icon,
            tog_normal_chat: $scope.tog_normal_chat,
            tog_whatsapp_chat: $scope.tog_whatsapp.chat,
            titleColor: $scope.titleColor,
            textfieldColor: $scope.textfieldColor,
            buttonhoverColor: $scope.buttonhoverColor,
            backgroundColor: $scope.backgroundColor,
            bbubbleColor: $scope.bbubbleColor,
            carousel_background: $scope.carousel_background,
            carousel_button: $scope.carousel_button,
            ububbleColor: $scope.ububbleColor,
            buttonColor: $scope.buttonColor,
            suggesColor: $scope.suggesColor,
            fontColor: $scope.fontColor,
            botImage: $scope.bot_img,
            webImage: $scope.web_bg,
            botBgImage: $scope.bot_bg,
            botFlow: $scope.final_obj
        }
        for (var i = 0; i < $scope.final_obj.length; i++) {
            // console.log($scope.final_obj[i].state_name);

            // console.log(angular.element('#' + $scope.final_obj[i].state_name).prop('offsetLeft'), angular.element('#' + $scope.final_obj[i].state_name).prop('offsetTop'));
            $scope.final_obj[i]['left'] = angular.element('#' + $scope.final_obj[i].state_name).prop('offsetLeft');
            $scope.final_obj[i]['top'] = angular.element('#' + $scope.final_obj[i].state_name).prop('offsetTop');
            if (i == $scope.final_obj.length - 1) {
                // console.log($scope.final_obj);
                $http.defaults.headers.common['user'] = $window.sessionStorage.id;
                $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
                $http.put('/update_bot', db_obj, {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).then(function(res) {
                    // console.log(res);
                    if (res.data.status == "success") {
                        $scope.showSimpleToast("Successfully Updated", "success");
                    }

                })
            }
        }
    }

    $scope.show_icons = function(e) {
        var ind = e.currentTarget.dataset.value;
        if ($scope['state' + $scope.statenum].gambit_init_mess[ind]["picture" + ind] == true) {
            $scope['state' + $scope.statenum].gambit_init_mess[ind]["picture" + ind] = false;
            $scope['state' + $scope.statenum].gambit_init_mess[ind]["times" + ind] = false;
            $scope['state' + $scope.statenum].gambit_init_mess[ind]["text" + ind] = false;
            $scope['state' + $scope.statenum].gambit_init_mess[ind]["video" + ind] = false;
        } else {
            $scope['state' + $scope.statenum].gambit_init_mess[ind]["picture" + ind] = true;
            $scope['state' + $scope.statenum].gambit_init_mess[ind]["times" + ind] = true;
            $scope['state' + $scope.statenum].gambit_init_mess[ind]["text" + ind] = true;
            $scope['state' + $scope.statenum].gambit_init_mess[ind]["video" + ind] = true;
        }
    }

    $scope.showAlert = function(ev) {
        $scope.statename = ev.currentTarget.id;
        var str = $scope.statename;
        var statenum = str.split("state");
        $scope.statenum = statenum[1];
        $modal.open({
            templateUrl: 'dialog1.tmpl.html',
            controller: function($scope) {
                $scope.ok = function() {
                        // $scope.$close();
                        $scope.$dismiss();
                        $('.modal-backdrop').remove()
                        $(document.body).removeClass("modal-open");
                    }
                    // $scope['state' + $scope.statenum].state = 'state' + $scope.statenum;
                $scope.msgmenu = true;
                $scope.showconfigs = function(type) {
                    switch (type) {
                        case 'msgsett':
                            $scope.msgmenu = false;
                            $scope.botmsgsett = true;
                            $scope.usrresptimeout = false;
                            $scope.apiintg = false;
                            $scope.condjump = false;
                            $scope.urlparams = false;
                            break;

                        case 'restimout':
                            $scope.msgmenu = false;
                            $scope.botmsgsett = false;
                            $scope.usrresptimeout = true;
                            $scope.apiintg = false;
                            $scope.condjump = false;
                            $scope.urlparams = false;
                            break;

                        case 'api':
                            $scope.msgmenu = false;
                            $scope.botmsgsett = false;
                            $scope.usrresptimeout = false;
                            $scope.apiintg = true;
                            $scope.condjump = false;
                            $scope.urlparams = false;
                            break;

                        case 'cond':
                            $scope.msgmenu = false;
                            $scope.botmsgsett = false;
                            $scope.usrresptimeout = false;
                            $scope.apiintg = false;
                            $scope.condjump = true;
                            $scope.urlparams = false;
                            break;

                        case 'urlparam':
                            $scope.msgmenu = false;
                            $scope.botmsgsett = false;
                            $scope.usrresptimeout = false;
                            $scope.apiintg = false;
                            $scope.condjump = false;
                            $scope.urlparams = true;
                            break;

                        default:
                            $scope.msgmenu = false;
                            $scope.botmsgsett = true;
                            $scope.usrresptimeout = false;
                            $scope.apiintg = false;
                            $scope.condjump = false;
                            $scope.urlparams = false;
                            break;
                    }
                }
                $scope.backtomenu = function() {
                        $scope.msgmenu = true;
                        $scope.botmsgsett = false;
                        $scope.usrresptimeout = false;
                        $scope.apiintg = false;
                        $scope.condjump = false;
                        $scope.urlparams = false;
                    }
                    // api code starts
                if ($scope['state' + $scope.statenum].entry_response == true) {
                    $scope.entry_response_msg = "";
                    var api_obj = {
                        "method": "get",
                        "url": "",
                        "params": [],
                        "body": [],
                        "headerparams": [],
                        "auth": {},
                        "showparams": false

                    }
                    $scope['state' + $scope.statenum]["ent_api_details"] = api_obj;
                    $scope.entry_response_msg = "";
                    $http.defaults.headers.common['user'] = $window.sessionStorage.id;
                    $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
                    $http.get('/getapis', {}).then(function(res) {
                        $scope.apilist = [];
                        for (var i = 0; i < res.data.length; i++) {
                            $scope.apilist[i] = {
                                api_id: res.data[i]._id,
                                api_name: (res.data[i].ApiName == undefined) ? res.data[i].api_info.url : res.data[i].ApiName,
                                method: res.data[i].api_info.method,
                                params: res.data[i].api_info.params,
                                body: res.data[i].api_info.body,
                                headerparams: res.data[i].api_info.headerparams,
                                showparams: res.data[i].api_info.showparams,
                                url: res.data[i].api_info.url
                            }
                        }
                    });
                } else {
                    $scope.entry_response_msg = "Take Gambit Entry Api Response";
                }
                $scope.addentryres = function(statenum) {
                    $scope['state' + statenum].entry_response;

                    if ($scope['state' + statenum].entry_response == true) {
                        var api_obj = {
                            "method": "get",
                            "url": "",
                            "params": [],
                            "body": [],
                            "headerparams": [],
                            "auth": {},
                            "showparams": false

                        }
                        $scope['state' + statenum]["ent_api_details"] = api_obj;
                        $scope.entry_response_msg = "";
                        $http.defaults.headers.common['user'] = $window.sessionStorage.id;
                        $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
                        $http.get('/getapis', {}).then(function(res) {
                            $scope.apilist = [];
                            for (var i = 0; i < res.data.length; i++) {
                                $scope.apilist[i] = {
                                    api_id: res.data[i]._id,
                                    api_name: (res.data[i].ApiName == undefined) ? res.data[i].api_info.url : res.data[i].ApiName,
                                    method: res.data[i].api_info.method,
                                    params: res.data[i].api_info.params,
                                    body: res.data[i].api_info.body,
                                    headerparams: res.data[i].api_info.headerparams,
                                    showparams: res.data[i].api_info.showparams,
                                    url: res.data[i].api_info.url
                                }
                            }
                        });
                    } else {
                        $scope['state' + statenum]["ent_api_details"] = {};
                        $scope.entry_response_msg = "Take Gambit Entry Api Response";
                    }
                }
                $scope.apiparams = [{
                    "paramkey": "",
                    "paramvalue": ""
                }]
                $scope.headerparams = [{ "paramkey": "", "paramvalue": "" }]
                $scope.addurlparams = function(statenum) {
                    if ($scope['state' + statenum]["url_params"] == undefined) {
                        $scope['state' + statenum]["url_params"] = [];
                    }
                    var obj = {
                        "paramname": ""
                    }
                    $scope['state' + statenum]["url_params"].push(obj);
                }
                $scope.removeurlparams = function(statenum, paramsindex) {

                    $scope['state' + statenum]["url_params"].splice(paramsindex, 1);
                }
                $scope.addparams = function(statenum) {
                    if ($scope['state' + statenum]["ent_api_details"].params == undefined) {
                        $scope['state' + statenum]["ent_api_details"].params = [];
                    }
                    var obj = {
                        "paramkey": "",
                        "paramvalue": ""
                    }
                    $scope['state' + statenum]["ent_api_details"].params.push(obj);
                }
                $scope.removeparams = function(statenum, paramsindex) {

                    $scope['state' + statenum]["ent_api_details"].params.splice(paramsindex, 1);
                }

                $scope.rmhdrparams = function(statenum, paramsindex) {

                    $scope['state' + statenum]["ent_api_details"].headerparams.splice(paramsindex, 1);
                }
                $scope.rmbodyparams = function(statenum, paramsindex) {

                    $scope['state' + statenum]["ent_api_details"].body.splice(paramsindex, 1);
                }

                $scope.addbodyparams = function(statenum) {
                    if ($scope['state' + statenum]["ent_api_details"].body == undefined) {
                        $scope['state' + statenum]["ent_api_details"].body = [];
                    }
                    var obj = {
                        "paramkey": "",
                        "paramvalue": ""
                    }
                    $scope['state' + statenum]["ent_api_details"].body.push(obj);
                }

                $scope.showparams = true;
                $scope.toggleparams = function(statenum) {

                    if ($scope['state' + statenum]["ent_api_details"].showparams == true) {
                        $scope['state' + statenum]["ent_api_details"].showparams = false;
                    } else {

                        $scope['state' + statenum]["ent_api_details"].showparams = true
                    }
                }

                $scope.addheadparams = function(statenum) {
                    if ($scope['state' + statenum]["ent_api_details"].headerparams == undefined) {
                        $scope['state' + statenum]["ent_api_details"].headerparams = [];
                    }
                    var obj = {
                        "paramkey": "",
                        "paramvalue": ""
                    }
                    $scope['state' + statenum]["ent_api_details"].headerparams.push(obj);
                }
                $scope.getapires = function(statenum, item) {
                        var ind = $scope.apilist.indexOf(item);
                        $scope['state' + statenum]["ent_api_details"].url = $scope.apilist[ind].url;
                        $scope['state' + statenum]["ent_api_details"].body = $scope.apilist[ind].body;
                        $scope['state' + statenum]["ent_api_details"].headerparams = $scope.apilist[ind].headerparams;
                        $scope['state' + statenum]["ent_api_details"].params = $scope.apilist[ind].params;
                        if ($scope['state' + statenum]["ent_api_details"].url == "") {
                            $scope.showSimpleToast("Empty URL request", "warning");
                        } else {

                            var body_data = {}
                            var header_data = { 'Content-Type': 'application/json' }
                            if ($scope['state' + statenum]["ent_api_details"].body.length > 0) {

                                angular.forEach($scope['state' + statenum]["ent_api_details"].body, function(value, key) {
                                    body_data[value.bodykey] = value.bodyvalue
                                });

                            }

                            if ($scope['state' + statenum]["ent_api_details"].headerparams.length > 0) {

                                angular.forEach($scope['state' + statenum]["ent_api_details"].headerparams, function(value, key) {

                                    header_data[value.paramkey] = value.paramvalue
                                });
                            }
                            var parameter = ""
                            if ($scope['state' + statenum]["ent_api_details"].params.length > 0) {
                                angular.forEach($scope['state' + statenum]["ent_api_details"].params, function(value, key) {

                                    parameter = parameter + value.paramkey + "=" + value.paramvalue + '&'
                                });
                            }
                            //  var url = $scope['state' + statenum]["ent_api_details"].url + "/?"+parameter
                            var req_body = {
                                'method': $scope['state' + statenum]["ent_api_details"].method,
                                'url': $scope['state' + statenum]["ent_api_details"].url,
                                'headers': header_data,
                                'body': body_data,
                                'param': parameter
                            }
                            $http.defaults.headers.common['user'] = $window.sessionStorage.id;
                            $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
                            $http.post('/execute_api', req_body, {
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(function(res) {

                                $scope.apiresponse = JSON.stringify(res.data.data, undefined, 2);
                                $scope.apistatus = res.data.status;
                                $scope.apistatustext = res.data.statusText;
                            })

                        }
                    }
                    // api code ends
                    //gambit template default
                $scope.gambittemplate = function(value, statenum) {
                        // console.log(value, statenum);
                        ids = [];
                        if (value == "form") {
                            if ($scope['state' + statenum]['forms'] == undefined) {
                                $scope['state' + statenum]['forms'] = [];
                            }

                            $scope.fields = {
                                "header": "",
                                fields: [{
                                    type: 'text',
                                    name: 'Name',
                                    placeholder: 'Please enter your name',
                                    order: 1
                                }]
                            };
                            $scope['state' + statenum]['forms'].push($scope.fields);
                        }
                    }
                    // state delete code
                $scope.delete_gambit = function(targetBoxId) {
                        // // console.log(targetBoxId);

                        jsPlumb.detachAllConnections(targetBoxId);
                        jsPlumb.removeAllEndpoints(targetBoxId);
                        jsPlumb.detach(targetBoxId); // <--
                        $('#' + targetBoxId).remove();
                        // $mdDialog.hide();
                        $scope.ok();
                        var index = $scope.final_obj.find(function(item, i) {
                            if (item.state_name === targetBoxId) {
                                $scope.final_obj.splice(i, 1);

                            }

                        });
                    }
                    // multi conn code starts
                $scope.mul_conn = function(statenum) {
                    // // console.log($scope['state' + statenum]);
                    if ($scope['state' + statenum].multiple_conn == true) {
                        // // console.log("in if");
                        var ep = jsPlumb.selectEndpoints({ target: 'state' + statenum }).getParameters();
                        jsPlumb.deleteEndpoint(ep[0][1]);

                        var commons = {
                            isTarget: true,
                            maxConnections: -1,
                            connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                            endpoint: "Rectangle",
                            paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "blue" }
                        };
                        jsPlumb.addEndpoint('state' + statenum, {
                            anchors: ["Top"]
                        }, commons);
                    } else {
                        // console.log("in else");
                        var ep = jsPlumb.selectEndpoints({ target: 'state' + statenum }).getParameters();
                        jsPlumb.deleteEndpoint(ep[0][1]);
                        var commons = {
                            isTarget: true,
                            maxConnections: 1,
                            connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                            endpoint: "Rectangle",
                            paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "blue" }
                        };
                        jsPlumb.addEndpoint('state' + statenum, {
                            anchors: ["Top"]
                        }, commons);
                    }
                }
                $scope.change = function(statenum, option) {
                        // console.log("funct", option);
                        if (option == "Text") {
                            $scope.suggest_branches = false;
                            $scope.exp_in_mess = {
                                inputType: "Custom Text",
                                keyType: "Normal Keyboard",
                                capitalizeText: "Disabled",
                                inputValidation: "No Validation",
                                text_opt_Input: $scope.text_opt_Input,
                                text_mul_bub_Input: $scope.text_mul_bub_Input,
                                suggestions: '',
                                suggestion_box: '',
                                suggest_preview_list: $scope.suggest_preview_list,
                                suggest_opt_input: $scope.suggest_opt_input,
                                suggest_mul_bub_Input: $scope.suggest_mul_bub_Input
                            }
                            $scope['state' + statenum].exp_in_mess = $scope.exp_in_mess;
                        } else if (option == "Suggestion") {
                            $scope.suggest_branches = true;
                            $scope.exp_in_mess = {
                                inputType: "Custom Text",
                                suggestions: '',
                                suggestion_box: '',
                                suggest_preview_list: $scope.suggest_preview_list,
                                suggest_opt_input: $scope.suggest_opt_input,
                                suggest_mul_bub_Input: $scope.suggest_mul_bub_Input,
                                suggest_branches: $scope.suggest_branches
                            }
                            $scope['state' + statenum].exp_in_mess = $scope.exp_in_mess;
                        } else if (option == "Button") {
                            $scope.suggest_branches = false;
                            var button_value = { "url": "", "phone": "", "payload": "", "title": "Option Text", "type": " " };
                            $scope.exp_in_mess = {
                                button_data: [button_value]
                            }
                            $scope['state' + statenum].exp_in_mess = $scope.exp_in_mess;
                            // console.log($scope['state' + statenum].exp_in_mess);
                        } else {
                            $scope.suggest_branches = false;
                        }
                    }
                    // multi conn code ends
                    // multi lingual code start
                $scope.enable_multi_language = function() {
                    if ($scope.check.multi == true) {
                        $scope.onLoad1();

                    } else if ($scope.check.multi == false) {
                        $scope.checkboxClickHandler();
                        // transliterationControl.disableTransliteration();
                    }
                }

                $scope.check = { multi: false };
                $scope.add_to_ids = function() {
                    if (transliterationControl.isTransliterationEnabled() == true) {
                        var current_element_id = document.activeElement.id;
                        var index_of_elem = ids.indexOf(current_element_id);


                        if (index_of_elem == -1) {

                            ids.push(current_element_id);

                            transliterationControl.makeTransliteratable(ids);
                            $scope.languageChangeHandler();
                        } else {
                            // if ($scope.check == true) {
                            ids.push(current_element_id);

                            transliterationControl.makeTransliteratable(ids);
                            $scope.languageChangeHandler();
                            // }
                        }
                    } else {}


                }
                var transliterationControl;
                var options = {
                    sourceLanguage: 'en',
                    destinationLanguage: ['ar', 'hi', 'kn', 'ml', 'ta', 'te'],
                    transliterationEnabled: true,
                    shortcutKey: 'ctrl+g'
                };
                transliterationControl =
                    new google.elements.transliteration.TransliterationControl(options);
                // transliterationControl.disableTransliteration()
                $scope.onLoad1 = function() {

                    var options = {
                        sourceLanguage: 'en',
                        destinationLanguage: ['ar', 'hi', 'kn', 'ml', 'ta', 'te'],
                        transliterationEnabled: true,
                        shortcutKey: 'ctrl+g'
                    };

                    // Create an instance on TransliterationControl with the required
                    // options.
                    transliterationControl =
                        new google.elements.transliteration.TransliterationControl(options);

                    // Enable transliteration in the textfields with the given ids.

                    // transliterationControl.makeTransliteratable(ids);

                    // Add the STATE_CHANGED event handler to correcly maintain the state
                    // of the checkbox.
                    transliterationControl.addEventListener(
                        google.elements.transliteration.TransliterationControl.EventType.STATE_CHANGED,
                        $scope.transliterateStateChangeHandler);

                    // Add the SERVER_UNREACHABLE event handler to display an error message
                    // if unable to reach the server.
                    transliterationControl.addEventListener(
                        google.elements.transliteration.TransliterationControl.EventType.SERVER_UNREACHABLE,
                        serverUnreachableHandler);

                    // Add the SERVER_REACHABLE event handler to remove the error message
                    // once the server becomes reachable.
                    transliterationControl.addEventListener(
                        google.elements.transliteration.TransliterationControl.EventType.SERVER_REACHABLE,
                        serverReachableHandler);

                    // Set the checkbox to the correct state.
                    // document.getElementById('checkboxId').checked = transliterationControl.isTransliterationEnabled();

                    // Populate the language dropdown
                    var destinationLanguage =
                        transliterationControl.getLanguagePair().destinationLanguage;
                    var languageSelect = document.getElementById('languageDropDown');
                    var supportedDestinationLanguages =
                        google.elements.transliteration.getDestinationLanguages(
                            google.elements.transliteration.LanguageCode.ENGLISH);
                    for (var lang in supportedDestinationLanguages) {
                        var opt = document.createElement('option');
                        opt.text = lang;
                        opt.value = supportedDestinationLanguages[lang];
                        if ($scope.check.multi) {
                            if (destinationLanguage == opt.value) {
                                opt.selected = true;
                            }
                        }

                        try {
                            languageSelect.add(opt, null);
                        } catch (ex) {
                            languageSelect.add(opt);
                        }
                    }
                }

                // Handler for STATE_CHANGED event which makes sure checkbox status
                // reflects the transliteration enabled or disabled status.
                $scope.transliterateStateChangeHandler = function(e) {
                    document.getElementById('checkboxId').checked = e.transliterationEnabled;
                }

                // Handler for checkbox's click event.  Calls toggleTransliteration to toggle
                $scope.checkboxClickHandler = function() {
                    transliterationControl.toggleTransliteration();
                }

                // Handler for dropdown option change event.  Calls setLanguagePair to
                // set the new language.
                $scope.languageChangeHandler = function() {
                        var dropdown = document.getElementById('languageDropDown');
                        transliterationControl.setLanguagePair(
                            google.elements.transliteration.LanguageCode.ENGLISH,
                            dropdown.options[dropdown.selectedIndex].value);

                    }
                    // SERVER_UNREACHABLE event handler which displays the error message.
                function serverUnreachableHandler(e) {
                    document.getElementById("errorDiv").innerHTML =
                        "Transliteration Server unreachable";
                }
                // SERVER_UNREACHABLE event handler which clears the error message.
                function serverReachableHandler(e) {
                    document.getElementById("errorDiv").innerHTML = "";
                }
                // multi lingual code ends
                // message setting code start
                $scope.show_option = function(e) {
                    var ind = e.currentTarget.dataset.value;
                    $scope['state' + $scope.statenum].gambit_init_mess[ind]["ellipsis" + ind] = true;
                    $scope['state' + $scope.statenum].gambit_init_mess[ind]["plus" + ind] = true;
                }

                $scope.hide_option = function(e) {
                    var ind = e.currentTarget.dataset.value;
                    $scope['state' + $scope.statenum].gambit_init_mess[ind]["ellipsis" + ind] = false;
                    $scope['state' + $scope.statenum].gambit_init_mess[ind]["picture" + ind] = false;
                    $scope['state' + $scope.statenum].gambit_init_mess[ind]["times" + ind] = false;
                    $scope['state' + $scope.statenum].gambit_init_mess[ind]["plus" + ind] = false;
                    $scope['state' + $scope.statenum].gambit_init_mess[ind]["text" + ind] = false;
                    $scope['state' + $scope.statenum].gambit_init_mess[ind]["video" + ind] = false;
                }
                $scope.apply_line = function() {
                    document.execCommand('underline');
                }
                $scope.apply_italic = function() {
                    document.execCommand('italic');
                }

                $scope.apply_bold = function(e) {
                    document.execCommand('bold');
                }
                $scope.apply_href = function() {}
                $scope.apply_h = function() {}
                $scope.tbubble = "12px";
                $scope.getSelectionText = function(event, index) {
                    var text = "";
                    if (window.getSelection) {
                        text = window.getSelection().toString();
                    } else if (document.selection && document.selection.type != "Control") {
                        text = document.selection.createRange().text;
                    }
                    if (text.length > 0) {
                        document.getElementById("state_tool" + index).style.display = "block"
                        $scope.tbubble = event.offsetX + "px"

                        // console.log("there exists text")
                    } else {
                        document.getElementById("state_tool" + index).style.display = "none";
                        $scope.display_tool = "none";
                        // console.log("no text was selected");
                    }
                }
                $scope.showButtonMenu = function(statenum, e, index, parentidx) {
                    var indexValue = e.currentTarget.dataset.value;
                    $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["button_ellipsis" + indexValue] = true;
                    $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["button_plus" + indexValue] = true;
                };

                $scope.hideButtonMenu = function(statenum, e, index, parentidx) {
                    var indexValue = e.currentTarget.dataset.value;
                    $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["button_ellipsis" + indexValue] = false;
                    $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["button_plus" + indexValue] = false;
                    $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["button_external-link" + indexValue] = false;
                    $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["button_phone" + indexValue] = false;
                    $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["button_text" + indexValue] = false;
                    $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["button_times" + indexValue] = false;
                };
                $scope.addOption = function(statenum, index) {
                    if ($scope['state' + statenum].gambit_init_mess[index]["button_states"] == undefined) {
                        $scope['state' + statenum].gambit_init_mess[index]["button_states"] = [];
                    }
                    if ($scope['state' + statenum].gambit_init_mess[index].message.buttons == undefined) {
                        $scope['state' + statenum].gambit_init_mess[index].message.buttons = [];
                    }
                    $scope.buttonindex = $scope.buttonindex + 1;
                    $scope.buttonobj = {};
                    $scope.buttonobj["button_inputFields"] = { "url": false, "phone": false, "payload": false };
                    $scope.buttonobj["button_ellipsis" + $scope.buttonindex] = false;
                    $scope.buttonobj["button_plus" + $scope.buttonindex] = false;
                    $scope.buttonobj["button_external-link" + $scope.buttonindex] = false;
                    $scope.buttonobj["button_phone" + $scope.buttonindex] = false;
                    $scope.buttonobj["button_text" + $scope.buttonindex] = false;
                    $scope.buttonobj["button_times" + $scope.buttonindex] = false;
                    $scope['state' + statenum].gambit_init_mess[index]["button_states"].push($scope.buttonobj);

                    var button_value = { "url": "", "phone": "", "payload": "", "title": "Option Text", "type": "" };

                    $scope['state' + statenum].gambit_init_mess[index].message.buttons.push(button_value);
                }
                $scope.ShowButtonIcons = function(statenum, e, index, parentidx) {
                    var indexValue = e.currentTarget.dataset.value;

                    if ($scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["button_phone" + indexValue] == true) {
                        $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["button_external-link" + indexValue] = false;
                        $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["button_phone" + indexValue] = false;
                        $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["button_text" + indexValue] = false;
                        $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["button_times" + indexValue] = false;
                    } else {
                        $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["button_external-link" + indexValue] = true;
                        $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["button_phone" + indexValue] = true;
                        $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["button_text" + indexValue] = true;
                        $scope['state' + statenum].gambit_init_mess[parentidx].button_states[index]["button_times" + indexValue] = true;
                    }

                }
                $scope.externalLink = function(statenum, indexValue, index) {

                    $scope['state' + statenum].gambit_init_mess[index].message.buttons[indexValue].type = "externalLink";
                    $scope['state' + statenum].gambit_init_mess[index].button_states[indexValue].button_inputFields.url = true;
                    $scope['state' + statenum].gambit_init_mess[index].button_states[indexValue].button_inputFields.phone = false;
                    $scope['state' + statenum].gambit_init_mess[index].button_states[indexValue].button_inputFields.payload = false;
                }

                $scope.phoneContact = function(statenum, indexValue, index) {

                    $scope['state' + statenum].gambit_init_mess[index].message.buttons[indexValue].type = "phone";
                    $scope.maxlength = "13";
                    $scope['state' + statenum].gambit_init_mess[index].button_states[indexValue].button_inputFields.url = false;
                    $scope['state' + statenum].gambit_init_mess[index].button_states[indexValue].button_inputFields.phone = true;
                    $scope['state' + statenum].gambit_init_mess[index].button_states[indexValue].button_inputFields.payload = false;
                }

                $scope.payloadMessage = function(statenum, indexValue, index) {

                    $scope['state' + statenum].gambit_init_mess[index].message.buttons[indexValue].type = "payload";
                    $scope['state' + statenum].gambit_init_mess[index].button_states[indexValue].button_inputFields.url = false;
                    $scope['state' + statenum].gambit_init_mess[index].button_states[indexValue].button_inputFields.payload = true;
                    $scope['state' + statenum].gambit_init_mess[index].button_states[indexValue].button_inputFields.phone = false;
                }

                $scope.removeOption = function(statenum, indexValue, index) {
                    $scope['state' + statenum].gambit_init_mess[index].message.buttons.splice(indexValue, 1);
                    $scope['state' + statenum].gambit_init_mess[index].button_states.splice(indexValue, 1);
                }
                $scope.media_id_chat = false;
                $scope.enable_text_input = function(statenum, ind) {
                    $scope['state' + statenum].gambit_init_mess[ind].input_fields.text = true;
                    $scope['state' + statenum].gambit_init_mess[ind].input_fields.image = false;
                    $scope['state' + statenum].gambit_init_mess[ind].input_fields.video = false;
                    $scope['state' + statenum].gambit_init_mess[ind].message.mess_type = "text";
                    if (!$scope.tog_whatsapp.chat) {
                        $scope.media_id_chat = false;
                    }
                }
                $scope.enable_video = function(statenum, ind) {
                    $scope['state' + statenum].gambit_init_mess[ind].input_fields.text = false;
                    $scope['state' + statenum].gambit_init_mess[ind].input_fields.video = true;
                    $scope['state' + statenum].gambit_init_mess[ind].input_fields.image = false;
                }
                $scope.enable_image_input = function(statenum, ind) {
                        $scope['state' + statenum].gambit_init_mess[ind].input_fields.text = false;
                        $scope['state' + statenum].gambit_init_mess[ind].input_fields.image = true;
                        $scope['state' + statenum].gambit_init_mess[ind].input_fields.video = false;
                        $scope['state' + statenum].gambit_init_mess[ind].message.mess_type = "media";
                        if ($scope.tog_whatsapp.chat) {
                            $scope.media_id_chat = true;
                        }
                    }
                    // message setting code end
                    // carousel code start
                $scope.templates = [
                        "Templateone"
                    ]
                    // carousel code end
                    // state save
                $scope.crtstateobj = function(statename, statenum) {

                    if ($scope[statename].state) {
                        $('#' + statename + ' .title').text($scope[statename].state + '[' + statenum + ']');
                    }
                    if ($scope.final_obj.length == 0) {
                        $scope[statename].out_mess = [];
                        for (var i = 0; i < $scope[statename].gambit_init_mess.length; i++) {
                            $scope[statename].out_mess.push($scope[statename].gambit_init_mess[i].message);
                        }

                        $scope.final_obj.push($scope[statename]);
                        $scope.ok();
                        // $mdDialog.hide();
                    } else {

                        for (var i = 0; i < $scope.final_obj.length; i++) {

                            if ($scope.final_obj[i].state_name == statename) {
                                $scope.final_obj.splice(i, 1);
                                $scope[statename].out_mess = [];
                                for (var i = 0; i < $scope[statename].gambit_init_mess.length; i++) {
                                    $scope[statename].out_mess.push($scope[statename].gambit_init_mess[i].message);
                                }
                                $scope.final_obj.push($scope[statename]);
                                $scope.ok();
                                // $mdDialog.hide();
                                break;
                            } else {
                                if (i == $scope.final_obj.length - 1) {
                                    $scope[statename].out_mess = [];
                                    for (var i = 0; i < $scope[statename].gambit_init_mess.length; i++) {
                                        $scope[statename].out_mess.push($scope[statename].gambit_init_mess[i].message);
                                    }
                                    $scope.final_obj.push($scope[statename]);
                                    $scope.ok();
                                    // $mdDia/log.hide();
                                }
                            }
                        }
                    }

                    // console.log($scope[statename].input_option, "option");
                    if ($scope[statename].input_option == "Suggestion") {
                        if ($scope[statename].exp_in_mess.suggest_branches == true) {
                            var state_endpoints = jsPlumb.getEndpoints(statename);
                            // console.log(state_endpoints);
                            var length = $scope[statename].exp_in_mess.suggestion_box.length;
                            var endlength = state_endpoints.length - 1;
                            var newanchor = length - endlength;

                            // console.log($scope[statename].exp_in_mess.suggestion_box.length, "length of sugg", endlength, newanchor, state_endpoints.length);
                            if (state_endpoints.length == 2) {
                                var left_val = 0.6;
                                var j = 0;
                            } else if (state_endpoints.length - 1 > length) {
                                // console.log("it is greater", state_endpoints.length, length);
                                for (var i = (state_endpoints.length - 1); i >= (length + 1); i--) {
                                    var ep_id = state_endpoints[i];
                                    jsPlumb.deleteEndpoint(ep_id);
                                }
                            } else {
                                left_val = 0.6 + ((endlength - 1) * 0.1);
                                j = 0;
                            }

                            for (var i = 0; i < newanchor; i++) {
                                var common = {
                                    isSource: true,
                                    connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }]
                                };
                                if (j > 0) {
                                    left_val = left_val + 0.1;
                                }
                                j = j + 1;
                                var state = statename;
                                if (length > 1) {
                                    var width = document.getElementById(state).offsetWidth;
                                    document.getElementById(state).style.width = width + 10 + "px";
                                }
                                jsPlumb.addEndpoint(state, {
                                    anchor: [
                                        [left_val, 1.07, 1, 1, 1, -4]
                                    ],
                                    maxConnections: 1,
                                    connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                                    endpoint: ["Rectangle", { width: 10, height: 9 }],
                                    paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "black" }
                                }, common);
                                // if(i == newanchor - 1){
                                //   jsPlumb.recalculateOffsets(state);
                                // }
                            }
                            jsPlumb.recalculateOffsets(state)
                        } else {
                            var endpoints = jsPlumb.getEndpoints(statename);
                            // console.log(endpoints, "ep");
                            if (endpoints.length > 2) {
                                for (var i = 2; i < endpoints.length; i++) {
                                    var ep_id = endpoints[i];
                                    jsPlumb.deleteEndpoint(ep_id);
                                }
                            }
                        }
                    } else if ($scope[statename].input_option != "Suggestion" && $scope[statename].input_option != "Star Rating") {
                        var endpoints = jsPlumb.getEndpoints(statename);
                        // console.log(endpoints, "ep");
                        if (endpoints.length > 2) {
                            for (var i = 2; i < endpoints.length; i++) {
                                var ep_id = endpoints[i];
                                jsPlumb.deleteEndpoint(ep_id);
                            }
                        }

                    } else {

                    }

                    if ($scope[statename].input_option == "Star Rating") {
                        var endpoints = jsPlumb.getEndpoints(statename);
                        var length = $scope[statename].exp_in_mess.star_rating;
                        // console.log(endpoints.length, length);
                        var endlength = endpoints.length - 1;
                        var starcount = length - endlength;

                        if (endpoints.length == 2) {
                            var left_val = 0.6;
                            var j = 0;
                        } else if (endpoints.length > length) {
                            var starlength = parseInt(length);
                            // console.log("in else", starlength);
                            for (var i = (endpoints.length - 1); i >= (starlength + 1); i--) {
                                // console.log(i, "in for", endpoints, endpoints[i], ep_id);
                                var ep_id = endpoints[i];
                                jsPlumb.deleteEndpoint(ep_id);
                            }
                        } else {
                            left_val = 0.6 + ((endlength - 1) * 0.1);
                            j = 0;
                        }

                        for (var i = 0; i < starcount; i++) {
                            var common = {
                                isSource: true,
                                maxConnections: 1,
                                deleteEndpointsOnDetach: false,
                                connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                                endpoint: ["Rectangle", { width: 10, height: 8 }],
                                paintStyle: { strokeStyle: "blue", strokeWidth: 3, fillStyle: "black" }
                            };
                            if (j > 0) {
                                left_val = left_val + 0.1;
                            }
                            j = j + 1;
                            var state = statename;
                            if (length > 1) {
                                var width = document.getElementById(state).offsetWidth;
                                document.getElementById(state).style.width = width + 20 + "px";
                            }
                            var newval = i * 5;
                            jsPlumb.addEndpoint(state, {
                                anchor: [
                                    [left_val, 1.07, 1, 1, 1, -4], "Bottom"
                                ],
                                // anchor:[ "Bottom","Bottom"],
                            }, common);
                            if (i == starcount - 1) {
                                //   $scope.repaint();

                            }
                        }
                    }
                }
            },
            scope: $scope,
            controllerAs: 'ctrl',
            targetEvent: ev,
            size: 'lg',
            resolve: {
                gambit_init_mess: function() {
                    return $scope.gambit_init_mess;
                }
            }
        });
    };

    $scope.optionIndexVal = 0;
    $scope.items = [];
    $scope.addOption = function(statenum) {
        $scope.buttonindex = $scope.buttonindex + 1;
        $scope.buttonobj = {};
        $scope.buttonobj["button_inputFields"] = { "url": false, "phone": false, "payload": false };
        $scope.buttonobj["button_ellipsis" + $scope.buttonindex] = false;
        $scope.buttonobj["button_plus" + $scope.buttonindex] = false;
        $scope.buttonobj["button_external-link" + $scope.buttonindex] = false;
        $scope.buttonobj["button_phone" + $scope.buttonindex] = false;
        $scope.buttonobj["button_text" + $scope.buttonindex] = false;
        $scope.buttonobj["button_times" + $scope.buttonindex] = false;
        $scope['state' + statenum].button_states.push($scope.buttonobj);
        var button_value = { "url": "", "phone": "", "payload": "", "title": "Option Text", "type": " " };
        // console.log($scope['state' + statenum].exp_in_mess);
        $scope['state' + statenum].exp_in_mess.button_data.push(button_value);


    }


    $scope.showButtonMenu = function(statenum, e) {
        var indexValue = e.currentTarget.dataset.value
        $scope['state' + statenum].button_states[indexValue]["ellipsis" + indexValue] = true;
        $scope['state' + statenum].button_states[indexValue]["plus" + indexValue] = true;
    };

    $scope.hideButtonMenu = function(statenum, e) {
        var indexValue = e.currentTarget.dataset.value
        $scope['state' + statenum].button_states[indexValue]["ellipsis" + indexValue] = false;
        $scope['state' + statenum].button_states[indexValue]["plus" + indexValue] = false;
        $scope['state' + statenum].button_states[indexValue]["external-link" + indexValue] = false;
        $scope['state' + statenum].button_states[indexValue]["phone" + indexValue] = false;
        $scope['state' + statenum].button_states[indexValue]["text" + indexValue] = false;
        $scope['state' + statenum].button_states[indexValue]["times" + indexValue] = false;
    };
    $scope.ShowButtonIcons = function(statenum, e) {
        var indexValue = e.currentTarget.dataset.value;
        if ($scope['state' + statenum].button_states[indexValue]["phone" + indexValue] == true) {
            $scope['state' + statenum].button_states[indexValue]["external-link" + indexValue] = false;
            $scope['state' + statenum].button_states[indexValue]["phone" + indexValue] = false;
            $scope['state' + statenum].button_states[indexValue]["text" + indexValue] = false;
            $scope['state' + statenum].button_states[indexValue]["times" + indexValue] = false;
        } else {
            $scope['state' + statenum].button_states[indexValue]["external-link" + indexValue] = true;
            $scope['state' + statenum].button_states[indexValue]["phone" + indexValue] = true;
            $scope['state' + statenum].button_states[indexValue]["text" + indexValue] = true;
            $scope['state' + statenum].button_states[indexValue]["times" + indexValue] = true;
        }

    }
    $scope.externalLink = function(statenum, indexValue) {
        // console.log($scope['state' + statenum].button_states);
        $scope['state' + statenum].exp_in_mess.button_data[indexValue].type = "externalLink";
        $scope['state' + statenum].button_states[indexValue].button_inputFields.url = true;
        $scope['state' + statenum].button_states[indexValue].button_inputFields.phone = false;
        $scope['state' + statenum].button_states[indexValue].button_inputFields.payload = false;
    }

    $scope.phoneContact = function(statenum, indexValue) {
        // console.log($scope['state' + statenum].button_states);
        $scope['state' + statenum].exp_in_mess.button_data[indexValue].type = "phone";
        $scope.maxlength = "13";
        $scope['state' + statenum].button_states[indexValue].button_inputFields.url = false;
        $scope['state' + statenum].button_states[indexValue].button_inputFields.phone = true;
        $scope['state' + statenum].button_states[indexValue].button_inputFields.payload = false;
    }

    $scope.payloadMessage = function(statenum, indexValue) {
        // console.log('payload')
        $scope['state' + statenum].exp_in_mess.button_data[indexValue].type = "payload";
        $scope['state' + statenum].button_states[indexValue].button_inputFields.url = false;
        $scope['state' + statenum].button_states[indexValue].button_inputFields.payload = true;
        $scope['state' + statenum].button_states[indexValue].button_inputFields.phone = false;
    }

    $scope.removeOption = function(statenum, i) {
        var index = $scope.items.indexOf(i);
        // console.log(i, $scope['state' + statenum].button_states);
        $scope['state' + statenum].button_states.splice(i, 1);
        // $scope['state' + statenum].exp_in_mess.exp_in_messsplice(index, 1);
    }

    var top = 20,
        left = 30;
    $scope.addnewgambit = function(e) {
        //console.log("in edit");
        var newState = $('<div>').attr('id', 'state' + $scope.i).addClass('item');

        var title = $('<div>').addClass('title').text('State ' + '[' + $scope.i + ']');
        var connect = $('<div>').addClass('connect');
        var divsize = ((Math.random() * 100) + 50).toFixed();

        $newdiv = $('</div>').css({
            'width': divsize + 'px',
            'height': divsize + 'px'
        });

        var posx = (Math.random() * ($(document).width() - divsize)).toFixed();
        var posy = (Math.random() * ($(document).height() - divsize)).toFixed();

        newState.css({
            'top': posx,
            'left': posy
        });


        if ($scope.i == 0) {

        } else {

        }


        jsPlumb.draggable(newState, {
            containment: 'parent',
            endpoint: "Rectangle"
        });

        var gambit_init_mess = [];
        var button_states = [];
        $scope.indexval = 0;
        $scope.buttonindex = 0;
        $scope.obj = {

        }
        $scope.buttonobj = {

        }
        $scope.text_mul_bub_Input = false;
        $scope.text_opt_Input = false;

        $scope.obj["message"] = { "text": "", "img_url": "", "title": "", "sub_title": "", "videoID": "", "img_form": "url", "image": "", "img_name": "", "buttons": [], "mess_type": "text", "media_type": "image", "media_id": "" };
        $scope.obj["input_fields"] = { "text": true, "image": false };
        $scope.obj["ellipsis" + $scope.indexval] = false;
        $scope.obj["plus" + $scope.indexval] = false;
        $scope.obj["picture" + $scope.indexval] = false;
        $scope.obj["times" + $scope.indexval] = false;
        $scope.obj["text" + $scope.indexval] = false;
        // $scope.obj["button_message"+$scope.buttonindex] = { "url": "", "phone": "", "payload": "" };
        $scope.buttonobj["button_inputFields"] = { "url": false, "phone": false, "payload": false };
        $scope.buttonobj["button_ellipsis" + $scope.buttonindex] = false;
        $scope.buttonobj["button_plus" + $scope.buttonindex] = false;
        $scope.buttonobj["button_external-link" + $scope.buttonindex] = false;
        $scope.buttonobj["button_phone" + $scope.buttonindex] = false;
        $scope.buttonobj["button_text" + $scope.buttonindex] = false;
        $scope.buttonobj["button_times" + $scope.buttonindex] = false;
        button_states.push($scope.buttonobj);
        $scope.obj["button_states"] = []
        $scope.obj["button_states"].push(button_states);
        gambit_init_mess.push($scope.obj);

        if ($scope.option == "Text") {
            $scope.exp_in_mess = {
                inputType: "Custom Text",
                keyType: "Normal Keyboard",
                capitalizeText: "Disabled",
                inputValidation: "No Validation",
                text_opt_Input: $scope.text_opt_Input,
                text_mul_bub_Input: $scope.text_mul_bub_Input,
                suggestions: '',
                suggestion_box: '',
                suggest_preview_list: $scope.suggest_preview_list,
                suggest_opt_input: $scope.suggest_opt_input,
                suggest_mul_bub_Input: $scope.suggest_mul_bub_Input
            }
        } else if ($scope.option == "Suggestion") {
            $scope.exp_in_mess = {
                suggestions: $scope.suggestions,
                suggest_preview_list: $scope.suggest_preview_list,
                suggest_opt_input: $scope.suggest_opt_input,
                suggest_mul_bub_Input: $scope.suggest_mul_bub_Input
            }
        } else if ($scope.option == "Button") {
            var button_value = { "url": "", "phone": "", "payload": "", "title": "Option Text", "type": " " };
            $scope.exp_in_mess = {
                button_data: [button_value]
            }
        } else {

        }
        $scope['state' + $scope.i] = {
                entry_response: false,
                send_res: false,
                res_timeout: false,
                state_name: "",
                out_mess: [],
                carousel: $scope.carousel,
                template: "",
                carousel_message: "",
                template_option: "message",
                // state :'State' +'['+i+']',
                state: '',
                exp_in_mess: $scope.exp_in_mess,
                next_state: "",
                input_option: $scope.option,
                gambit_init_mess: gambit_init_mess,
                connected_state: '',
                button_states: button_states,
                multiple_conn: false,
                api_type: "POST",
                res_timeout_type: "message",
                api_url: "",
                previous_res: false,
                condif: false,
                conditional_states: [],
                timeout_message: '',
                timeout_state: '',
                timeout_url: '',
                timeout_delay: 50000,
                url_params: [{ paramname: '' }],
                expresType: "text"
            }
            // console.log($scope['state' + $scope.i], "this is state 0", 'state' + $scope.i);


        newState.dblclick(function(e) {
            // console.log(e, "e");
            $scope.showAlert(e);
            /*  jsPlumb.detachAllConnections($(this));
              $(this).remove();
              e.stopPropagation();*/
        });

        newState.append(title);
        // newState.append(connect);

        $('#diagramContainer').append(newState);
        var state = 'state' + $scope.i;
        // console.log("current state", state);
        var common = {
            isSource: true,

            deleteEndpointsOnDetach: false,
            connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
            endpoint: ["Rectangle", { width: 10, height: 8 }],
            paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "black" }
        };
        jsPlumb.addEndpoint(state, {
            anchors: ["Bottom"]
        }, common);
        var commons = {
            isTarget: true,
            connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
            endpoint: "Rectangle",
            paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "blue" }
        };
        jsPlumb.addEndpoint(state, {
            anchors: ["Top"]
        }, commons);
        $scope.i++;
    }

    var last = {
        bottom: false,
        top: true,
        left: false,
        right: true
    };

    $scope.showSimpleToast = function(message, clsname) {
        ngToast.create({
            className: clsname,
            content: message,
            dismissOnTimeout: true,
            timeout: 2500,
            dismissButton: true,
            dismissButtonHtml: '&times',
            dismissOnClick: true
        });
    };

    $scope.addcond = function(statenum) {

        if ($scope['state' + statenum].condif == true) {
            $scope.cond = {
                "cond": [{
                    "cond": "is",
                    "field": "",
                    "value": "",
                    "intercond": "OR"
                }]
            }

            $scope['state' + statenum].cond_details.A.push($scope.cond);
            var length = $scope['state' + statenum].cond_details.A.length - 1;
            $scope['state' + statenum].conditional_states.push('cond' + length);
        } else {
            // $scope['state' + statenum]["cond_tetails"] = []
        }

    }
    $scope.addfirstcond = function(statenum, i) {


        $scope['state' + statenum]['cond_details'] = {
            "A": []
        }
        $scope.cond = {
            "cond": [{
                "cond": "is",
                "field": "",
                "value": "",
                "intercond": "OR"
            }]
        }

        $scope['state' + statenum].cond_details.A.push($scope.cond);
        $scope['state' + statenum].conditional_states.push('Default');
        $scope['state' + statenum].conditional_states.push('cond0');
        //   $scope['state' + statenum].cond_details['A'].push($scope.cond);

    }
    $scope.addchildcond = function(statenum, index) {
        var new_cond = {
            "cond": "is",
            "field": "",
            "value": "",
            "intercond": "OR"
        }

        $scope['state' + statenum].cond_details.A[index].cond.push(new_cond);
    }

    $scope.del_childcond = function(statenum, parentindex, childindex) {

        $scope['state' + statenum].cond_details.A[parentindex].cond.splice(childindex, 1);
        //$scope.schema.splice(i, 1);
    }

    $scope.del_parentcond = function(statenum, index) {
        $scope['state' + statenum].cond_details.A.splice(index, 1);
    }
    $scope.download = function(ev) {
        var db_obj = {
            "bot_id": $scope.botId
        }
        $http.defaults.headers.common['user'] = $window.sessionStorage.id;
        $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
        $http.get('/getformlist?bot_id=' + $scope.botId, {
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(res) {
            // console.log(res);
            $scope.bot_forms = res.data;

        })
        $modal.open({
            templateUrl: 'report.tmpl.html',
            controller: function($scope) {
                $scope.ok = function() {
                    $scope.$close();
                }
            },
            scope: $scope,
            controllerAs: 'ctrl',
            targetEvent: ev,
            size: 'md'
        });
    }

    $scope.download_conv = function() {

        var url = '/download_conv?csv=true&bot_id=' + $scope.botId;
        window.open(url);

    }

    $scope.download_media = function() {
        var url = '/download_media?bot_id=' + $scope.botId;
        window.open(url);
    }

    $scope.getform_conv = function() {
        var db_obj = {
            "bot_id": $scope.botId
        }
        var url = '/getform_conv?csv=true&bot_id=' + $scope.botId + "&form=" + $scope.selected_form + '&media_files=' + $scope.form_media_files;
        window.open(url);
    }
    $scope.getform_conv_media = function() {
        var db_obj = {
            "bot_id": $scope.botId
        }
        var url = '/getform_conv_media?bot_id=' + $scope.botId + "&form=" + $scope.selected_form;
        window.open(url);
    }
    $scope.showPrompt = function(ev) {
        // Appending dialog to document.body to cover sidenav in docs app
        var confirm = prompt("Bot Name", "");
        if (confirm == "") {
            $scope.showSimpleToast("Please enter bot name", "danger");
        } else if (confirm == null) {
            $scope.status = 'You Cancelled';
        } else {
            var db_obj = {
                botId: "",
                botName: confirm,
                titlebarColor: $scope.bandColor,
                botDesc: $scope.botDescription,
                webLink: $scope.web_link,
                postemail: $scope.postemail,
                tog_brand_icon: $scope.tog_brand.icon,
                tog_normal_chat: $scope.tog_normal.chat,
                tog_whatsapp_chat: $scope.tog_whatsapp.chat,
                titleColor: $scope.titleColor,
                textfieldColor: $scope.textfieldColor,
                buttonhoverColor: $scope.buttonhoverColor,
                backgroundColor: $scope.backgroundColor,
                bbubbleColor: $scope.bbubbleColor,
                carousel_background: $scope.carousel_background,
                carousel_button: $scope.carousel_button,
                ububbleColor: $scope.ububbleColor,
                buttonColor: $scope.buttonColor,
                suggesColor: $scope.suggesColor,
                fontColor: $scope.fontColor,
                botImage: $scope.bot_img,
                webImage: $scope.web_bg,
                botBgImage: $scope.bot_bg,
                botFlow: $scope.final_obj
            }
            for (var i = 0; i < $scope.final_obj.length; i++) {
                $scope.final_obj[i]['left'] = angular.element('#' + $scope.final_obj[i].state_name).prop('offsetLeft');
                $scope.final_obj[i]['top'] = angular.element('#' + $scope.final_obj[i].state_name).prop('offsetTop');
                if (i == $scope.final_obj.length - 1) {
                    $http.defaults.headers.common['user'] = $window.sessionStorage.id;
                    $http.defaults.headers.common['role_id'] = $window.sessionStorage.role_id;
                    $http.post('/savebotflow', db_obj, {
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    }).then(function(res) {
                        // console.log(res);
                        if (res.data.status == "Accepted") {
                            //   $scope.botId = res.data.botid
                            $scope.showSimpleToast("Successfully Saved", "success");
                        }
                    })
                }
            }
            $scope.status = 'You decided to name your Bot ' + confirm + '.';
        }
    };

    $scope.mul_conn = function(statenum) {
            // console.log($scope['state' + statenum]);
            if ($scope['state' + statenum].multiple_conn == true) {
                // console.log("in if");
                var ep = jsPlumb.selectEndpoints({ target: 'state' + statenum }).getParameters();
                jsPlumb.deleteEndpoint(ep[0][1]);
                var commons = {
                    isTarget: true,
                    maxConnections: -1,
                    connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                    endpoint: "Rectangle",
                    paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "blue" }
                };
                jsPlumb.addEndpoint('state' + statenum, {
                    anchors: ["Top"]
                }, commons);
            } else {
                // console.log("in else");
                var ep = jsPlumb.selectEndpoints({ target: 'state' + statenum }).getParameters();
                jsPlumb.deleteEndpoint(ep[0][1]);

                var commons = {
                    isTarget: true,
                    maxConnections: 1,
                    connector: ["Flowchart", { stub: 2, gap: 0, cornerRadius: 0.1, alwaysRespectStubs: false }],
                    endpoint: "Rectangle",
                    paintStyle: { strokeStyle: "blue", strokeWidth: 1, fillStyle: "blue" }
                };
                jsPlumb.addEndpoint('state' + statenum, {
                    anchors: ["Top"]
                }, commons);
            }
        }
        /* form */




    $scope.newField = {};
    $scope.form_list = [];
    $scope.fields = {
        "header": "",
        fields: [{
            type: 'text',
            name: 'Name',
            placeholder: 'Please enter your name',
            order: 1
        }]
    };
    $scope.form_list.push($scope.fields);

    $scope.editing = false;
    $scope.add_new_form = function(statenum) {
        $scope.fields = {
            "header": "",
            fields: [{
                type: 'text',
                name: 'Name',
                placeholder: 'Please enter your name',
                order: 1
            }]
        };
        // console.log($scope['state' + statenum], statenum);
        $scope['state' + statenum].forms.push($scope.fields);
    }
    $scope.delete_form = function(formindex, statenum) {
        $scope['state' + statenum].forms.splice(formindex, 1);
    };
    $scope.tokenize = function(slug1, slug2) {
        var result = slug1;
        result = result.replace(/[^-a-zA-Z0-9,&\s]+/ig, '');
        result = result.replace(/-/gi, "_");
        result = result.replace(/\s/gi, "-");
        if (slug2) {
            result += '-' + $scope.token(slug2);
        }
        return result;
    };

    $scope.addCarouselItem = function() {

        var carousel = {
            "title": "",
            "sub_title": "",
            "small_desc": "",
            "img_name": "",
            "title2": "",
            "sub_title2": "",
            "small_desc2": "",
            "button_text": "",
            "bt_act": "",
            "bt_payload": "",
            "bt_link": "",
            "footer": ""
        }
        $scope['state' + $scope.statenum].carousel.push(carousel);

    }
    $scope.delete_carousel = function(carouselindex, statenum) {
        $scope['state' + statenum].carousel.splice(carouselindex, 1);
    }

    $scope.saveField = function(index, statenum) {
        ids = [];
        // console.log("entered save");
        if ($scope.newField.type == 'checkboxes') {
            $scope.newField.value = {};
        }
        if ($scope.editing !== false) {
            $scope.fields.fields[$scope.editing] = $scope.newField;
            $scope.editing = false;
        } else {
            if ($scope.newField.type == 'date') {
                // console.log($scope.newField, $scope.newField.mindate.getFullYear(), $scope.newField.mindate.getMonth() + 1, $scope.newField.mindate.getDate());
                if ($scope.newField.mindate.getDate() < 10) {
                    var min_date = '0' + $scope.newField.mindate.getDate()
                } else {
                    var min_date = $scope.newField.mindate.getDate()
                }

                if ($scope.newField.maxdate.getDate() < 10) {
                    var max_date = '0' + $scope.newField.maxdate.getDate()
                } else {
                    var max_date = $scope.newField.maxdate.getDate()
                }
                if ($scope.newField.mindate.getMonth() < 10) {
                    var min_month = '0' + ($scope.newField.mindate.getMonth() + 1)
                } else {
                    var min_month = $scope.newField.mindate.getDate()
                }

                if ($scope.newField.maxdate.getMonth() < 10) {
                    var max_month = '0' + ($scope.newField.maxdate.getMonth() + 1)
                } else {
                    var max_month = $scope.newField.maxdate.getMonth()
                }
                $scope.newField.mindate = $scope.newField.mindate.getFullYear() + '-' + min_month + '-' + min_date;
                $scope.newField.maxdate = $scope.newField.maxdate.getFullYear() + '-' + max_month + '-' + max_date;

                $scope['state' + statenum].forms[index].fields.push($scope.newField);
            } else {
                $scope['state' + statenum].forms[index].fields.push($scope.newField);
            }

        }
        $scope.newField = {
            order: 0
        };
    };
    $scope.editField = function(field, formindex, fieldindex, statenum) {
        $scope.editing = $scope['state' + statenum].forms[formindex].fields[fieldindex];
        $scope.newField = field;
    };
    $scope.splice = function(field, formindex, fieldindex, statenum) {
        // console.log(field, formindex, fieldindex);
        $scope['state' + statenum].forms[formindex].fields.splice(fieldindex, 1);
    };
    $scope.addOptions = function() {
        if ($scope.newField.options === undefined) {
            $scope.newField.options = [];
        }
        $scope.newField.options.push({
            order: 0
        });
    };
    $scope.typeSwitch = function(type) {
        /*if (angular.Array.indexOf(['checkboxes','select','radio'], type) === -1)
          return type;*/
        if (type == 'checkboxes')
            return 'multiple';
        if (type == 'select')
            return 'multiple';
        if (type == 'radio')
            return 'multiple';

        return type;
    }

    /* end of form*/


    $scope.delete_gambit = function(targetBoxId) {
        // console.log(targetBoxId);

        jsPlumb.detachAllConnections(targetBoxId);
        jsPlumb.removeAllEndpoints(targetBoxId);
        jsPlumb.detach(targetBoxId); // <--
        $('#' + targetBoxId).remove();
        $mdDialog.hide();
        var index = $scope.final_obj.find(function(item, i) {
            if (item.state_name === targetBoxId) {
                $scope.final_obj.splice(i, 1);

            }

        });
    }

    $scope.getSelectionHtml = function() {
        var html = "";
        if (typeof window.getSelection != "undefined") {
            var sel = window.getSelection();
            if (sel.rangeCount) {
                var container = document.createElement("div");
                for (var i = 0, len = sel.rangeCount; i < len; ++i) {
                    container.appendChild(sel.getRangeAt(i).cloneContents());
                }
                html = container.innerHTML;
            }
        } else if (typeof document.selection != "undefined") {
            if (document.selection.type == "Text") {
                html = document.selection.createRange().htmlText;
            }
        }
        // console.log(html);
    }
    $scope.tbubble = "12px"
    $scope.display_tool = "none";
    $scope.getSelectionText = function(event, index) {

        // console.log("function called", index, event.offsetX);

        var text = "";
        if (window.getSelection) {
            text = window.getSelection().toString();
        } else if (document.selection && document.selection.type != "Control") {
            text = document.selection.createRange().text;
        }
        if (text.length > 0) {
            document.getElementById("state_tool" + index).style.display = "block"
            $scope.tbubble = event.offsetX + "px"

            // console.log("there exists text")
        } else {
            document.getElementById("state_tool" + index).style.display = "none";
            $scope.display_tool = "none";
            // console.log("no text was selected");
        }
    }
    $scope.apply_line = function() {

        document.execCommand('underline');
    }
    $scope.apply_italic = function() {

        document.execCommand('italic');
    }

    $scope.apply_bold = function(e) {

        document.execCommand('bold');
        /* var text = "";
         if (window.getSelection) {
             text = window.getSelection().toString();
         } else if (document.selection && document.selection.type != "Control") {
             text = document.selection.createRange().text;
         }
         // console.log(text);
         var sel, range, node;
         if (window.getSelection) {
             sel = window.getSelection();
             if (sel.getRangeAt && sel.rangeCount) {
                 range = window.getSelection().getRangeAt(0);
                 // console.log(sel,document.queryCommandState("bold"));
                if(document.queryCommandState("bold") == false){
                 var html = '<b>' + sel + '</b>'
                }
                else{
                  var 
                }
               
                 range.deleteContents();
                 
                 var el = document.createElement("div");
                 el.innerHTML = html;
                 var frag = document.createDocumentFragment(), node, lastNode;
                 while ( (node = el.firstChild) ) {
                     lastNode = frag.appendChild(node);
                 }
                 range.insertNode(frag);
             }
         } else if (document.selection && document.selection.createRange) {
             range = document.selection.createRange();
             range.collapse(false);
             range.pasteHTML(html);
         }*/

    }


    var transliterationControl;
    var options = {
        sourceLanguage: 'en',
        destinationLanguage: ['ar', 'hi', 'kn', 'ml', 'ta', 'te'],
        transliterationEnabled: true,
        shortcutKey: 'ctrl+g'
    };
    transliterationControl =
        new google.elements.transliteration.TransliterationControl(options);
    transliterationControl.disableTransliteration()
    $scope.onLoad1 = function() {
        // console.log("it comes here");
        var options = {
            sourceLanguage: 'en',
            destinationLanguage: ['ar', 'hi', 'kn', 'ml', 'ta', 'te'],
            transliterationEnabled: true,
            shortcutKey: 'ctrl+g'
        };

        // Create an instance on TransliterationControl with the required
        // options.
        transliterationControl =
            new google.elements.transliteration.TransliterationControl(options);

        // Enable transliteration in the textfields with the given ids.

        //transliterationControl.makeTransliteratable(ids);

        // Add the STATE_CHANGED event handler to correcly maintain the state
        // of the checkbox.
        transliterationControl.addEventListener(
            google.elements.transliteration.TransliterationControl.EventType.STATE_CHANGED,
            $scope.transliterateStateChangeHandler);

        // Add the SERVER_UNREACHABLE event handler to display an error message
        // if unable to reach the server.
        transliterationControl.addEventListener(
            google.elements.transliteration.TransliterationControl.EventType.SERVER_UNREACHABLE,
            serverUnreachableHandler);

        // Add the SERVER_REACHABLE event handler to remove the error message
        // once the server becomes reachable.
        transliterationControl.addEventListener(
            google.elements.transliteration.TransliterationControl.EventType.SERVER_REACHABLE,
            serverReachableHandler);

        // Set the checkbox to the correct state.
        document.getElementById('checkboxId').checked =
            transliterationControl.isTransliterationEnabled();

        // Populate the language dropdown
        var destinationLanguage =
            transliterationControl.getLanguagePair().destinationLanguage;
        var languageSelect = document.getElementById('languageDropDown');
        var supportedDestinationLanguages =
            google.elements.transliteration.getDestinationLanguages(
                google.elements.transliteration.LanguageCode.ENGLISH);
        for (var lang in supportedDestinationLanguages) {
            var opt = document.createElement('option');
            opt.text = lang;
            opt.value = supportedDestinationLanguages[lang];

            if (destinationLanguage == opt.value) {
                opt.selected = true;
            }
            try {
                languageSelect.add(opt, null);
            } catch (ex) {
                languageSelect.add(opt);
            }
        }
    }

    // Handler for STATE_CHANGED event which makes sure checkbox status
    // reflects the transliteration enabled or disabled status.
    $scope.transliterateStateChangeHandler = function(e) {
        document.getElementById('checkboxId').checked = e.transliterationEnabled;
    }

    // Handler for checkbox's click event.  Calls toggleTransliteration to toggle
    $scope.checkboxClickHandler = function() {
        transliterationControl.toggleTransliteration();
    }

    // Handler for dropdown option change event.  Calls setLanguagePair to
    // set the new language.
    $scope.languageChangeHandler = function() {
        var dropdown = document.getElementById('languageDropDown');
        transliterationControl.setLanguagePair(
            google.elements.transliteration.LanguageCode.ENGLISH,
            dropdown.options[dropdown.selectedIndex].value);
        // console.log(dropdown.options[dropdown.selectedIndex].text, dropdown.options[dropdown.selectedIndex].value);
        // $scope.lang_selected = dropdown.options[dropdown.selectedIndex].text;
    }

    // SERVER_UNREACHABLE event handler which displays the error message.
    function serverUnreachableHandler(e) {
        document.getElementById("errorDiv").innerHTML =
            "Transliteration Server unreachable";
    }

    // SERVER_UNREACHABLE event handler which clears the error message.
    function serverReachableHandler(e) {
        document.getElementById("errorDiv").innerHTML = "";
    }


    $scope.check = false;
    $scope.add_to_ids = function() {
        if (transliterationControl.isTransliterationEnabled() == true) {
            var current_element_id = document.activeElement.id;
            var index_of_elem = ids.indexOf(current_element_id);

            // console.log("in add to ids", document.activeElement.id, index_of_elem);
            if (index_of_elem == -1) {
                // console.log(current_element_id);
                ids.push(current_element_id);
                // console.log(ids);
                transliterationControl.makeTransliteratable(ids);
                $scope.languageChangeHandler();
            } else {

            }
        } else {

        }


    }

    $scope.enable_multi_language = function() {
        // console.log($scope.check);
        if ($scope.check == false) {
            $scope.onLoad1();
        } else {
            $scope.checkboxClickHandler();
        }
    }
    $scope.ok = function() {
        $mdDialog.hide();
    }
    $scope.answer = function(answer) {
        $mdDialog.hide(answer);
    };
    $scope.showsettings = function(ev) {
        $modal.open({
            // locals: { gambit_init_mess: $scope.gambit_init_mess },
            templateUrl: 'botsettings.html',
            controller: function($scope) {
                // // console.log($scope.gambit_init_mess);
                $scope.ok = function() {
                    // $mdDialog.hide();
                    // $scope.$close();
                    $scope.$dismiss();
                    $('.modal-backdrop').remove();
                    $(document.body).removeClass("modal-open");
                }
            },
            scope: $scope,
            controllerAs: 'ctrl',
            targetEvent: ev,
            size: 'lg',
            resolve: {
                gambit_init_mess: function() {
                    return $scope.gambit_init_mess;
                }
            }
        });
        // $mdDialog.show({
        //         locals: { gambit_init_mess: $scope.gambit_init_mess },
        //         controller: () => $scope,
        //         controllerAs: 'ctrl',
        //         templateUrl: 'botsettings.html',
        //         targetEvent: ev,
        //         clickOutsideToClose: true,
        //         fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
        //     })
        //     .then(function(answer) {
        //         $scope.status = 'You said the information was "' + answer + '".';
        //     }, function() {
        //         $scope.status = 'You cancelled the dialog.';
        //     });
    }



    /*  function DialogController($scope, $mdDialog) {
        $scope.hide = function() {
          $mdDialog.hide();
        };
        
        $scope.cancel = function() {
          $mdDialog.cancel();
        };
        
        $scope.answer = function(answer) {
          $mdDialog.hide(answer);
        };
      }*/

});

myApp.directive('contenteditable', ['$sce', function($sce) {
    return {
        restrict: 'A', // only activate on element attribute
        require: '?ngModel', // get a hold of NgModelController
        link: function(scope, element, attrs, ngModel) {
            if (!ngModel) return; // do nothing if no ng-model

            // Specify how UI should be updated
            ngModel.$render = function() {
                element.html($sce.getTrustedHtml(ngModel.$viewValue || ''));
                read(); // initialize
            };

            // Listen for change events to enable binding
            element.on('blur keyup change', function() {
                scope.$evalAsync(read);
            });

            // Write data to the model
            function read() {
                var html = element.html();
                // When we clear the content editable the browser leaves a <br> behind
                // If strip-br attribute is provided then we strip this out
                if (attrs.stripBr && html == '<br>') {
                    html = '';
                }
                ngModel.$setViewValue(html);
            }
        }
    };
}]);
myApp.directive('elementDraggable', ['$document', function($document) {
    return {
        link: function(scope, element, attr) {
            element.on('dragstart', function(event) {
                // console.log("javsjd");
                event.originalEvent.dataTransfer.setData('templateIdx', $(element).data('index'));
            });
        }
    };
}]);

myApp.directive('elementDrop', ['$document', function($document) {
    return {
        link: function(scope, element, attr) {

            element.on('dragover', function(event) {
                event.preventDefault();
            });

            $('.drop').on('dragenter', function(event) {
                event.preventDefault();
            })
            element.on('drop', function(event) {
                event.stopPropagation();
                var self = $(this);
                scope.$apply(function() {
                    // console.log(event);
                    var idx = event.originalEvent.dataTransfer.getData('templateIdx');

                    var insertIdx = self.data('index')
                        // console.log(idx);
                    scope.addElement(scope.dragElements[idx], insertIdx);
                });
            });
        }
    };
}]);