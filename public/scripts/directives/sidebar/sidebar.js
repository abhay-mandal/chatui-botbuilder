angular.module('webchatbot', ['ui.bootstrap'])
    .directive('sidebar', ['$location', '$window', '$rootScope', '$timeout','$http', function($location, $window, $rootScope, $timeout,$http) {
        return {
            templateUrl: 'scripts/directives/sidebar/sidebar.html',
            restrict: 'E',
            replace: true,
            scope: {},
            controller: function($scope, $state) {
                if (!$window.localStorage.id) {
                    $state.go("login");
                }
                if ($window.sessionStorage.role_id == 0 || $window.sessionStorage.role_id == 1) {
                    $scope.role = true;
                } else if ($window.sessionStorage.role_id == 2 || $window.sessionStorage.role_id == 3) {
                    $scope.role = false;
                }
                $scope.status = {
                    isopen: false
                };
                $(document).ready(function() {
                    $('[data-toggle=tooltip]').hover(function() {
                        // on mouseenter
                        $(this).tooltip('show');
                    }, function() {
                        // on mouseleave
                        $(this).tooltip('hide');
                    });
                });
                $scope.reload = function(url, e) {
                    e.preventDefault();
                    $state.go(url);
                };
                $scope.botId = $rootScope.getBotId;
                $timeout(function() {
                    $scope.botlen = $rootScope.botCount;
                }, 2000);
                $scope.$watch(function() {
                    return $rootScope.botCount;
                }, function() {
                    $scope.botlen = $rootScope.botCount;
                }, true);
                $scope.toggleDropdown = function($event) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.status.isopen = !$scope.status.isopen;
                };
                $scope.username = $window.sessionStorage.user;
                $scope.selectedMenu = 'dashboard';
                $scope.collapseVar = false;

                $scope.postdata = {
                    username :$window.sessionStorage.user,

                }
                $scope.logout = function() {
                    $scope.postdata = {
                        username :$window.localStorage.user,
                    }
                    $http.defaults.headers.common['Authorization'] = $window.localStorage.token;
                    
                    $http.post('/logout', $scope.postdata, {
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    }).then(function(res) {
                        console.log("res", res);
                        if (res.data.status == "successful") {
                            delete $window.localStorage.id;
                            delete $window.localStorage.role_id;
                            delete $window.localStorage.user;
                            delete $window.localStorage.token;
                            delete $window.sessionStorage.id; 
                            delete $window.sessionStorage.role_id;
                            delete  $window.sessionStorage.user; 
                            delete $window.sessionStorage.token;
                            $state.go('login');
                        } else {
                            $scope.message = res.data.status;
                        }
                    })

                }
            }
        }
    }]);