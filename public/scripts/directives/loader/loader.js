angular.module('webchatbot')
    .directive('loader', [function() {
        return {
            templateUrl: 'scripts/directives/loader/loader.html',
            restrict: 'E',
            replace: true,
            scope: {},
            controller: function() {}
        }
    }]);