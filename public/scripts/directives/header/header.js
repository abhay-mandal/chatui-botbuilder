angular.module('webchatbot')
    .directive('header', ['$location', '$window', function($location, $window) {
        return {
            templateUrl: 'scripts/directives/header/header.html',
            restrict: 'E',
            replace: true,
            scope: false,
            controller: function($scope, $state) {
                $scope.username = $window.sessionStorage.user;
                // $scope.selectedMenu = 'dashboard';
                // $scope.logoUrl = 'images/karix_logo.png';
                // $scope.minLogoUrl = 'images/karixlogo.png';
                $scope.collapseVar = false;
                // $scope.show_sidebar = function() {
                //     $scope.collapseVar = !$scope.collapseVar;
                // };
                $scope.logout = function() {
                    // console.log("here")
                    delete $window.sessionStorage.id;
                    delete $window.localStorage.id;
                    delete $window.sessionStorage.user;
                    $state.go('login');
                }
            }
        }
    }]);