angular.module('webchatbot').factory('sandboxuserService', ['$http', function($http) {

    var sandboxuserService = {};

    sandboxuserService.getAll = function() {
        $http.defaults.headers.common['user'] = $window.localStorage.id;
        $http.defaults.headers.common['role_id'] = $window.localStorage.role_id;
        $http.defaults.headers.common['token'] = $window.localStorage.token;
        return $http.get('/sandboxconfig');
    };

    return sandboxuserService;
}]);