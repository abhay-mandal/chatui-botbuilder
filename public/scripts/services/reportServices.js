angular.module('webchatbot').factory('reportServiceData', ['$http','$window', function($http,$window) {

    var reportServiceData = {};

    reportServiceData.loadBotDetails = function() {
        $http.defaults.headers.common['user'] = $window.localStorage.id;
        $http.defaults.headers.common['role_id'] = $window.localStorage.role_id;
        $http.defaults.headers.common['token'] = $window.localStorage.token;
        var getBotDetailsUrl = `/listbots`;
        return $http.get(getBotDetailsUrl);
    };

    reportServiceData.totalconversations = function(dateRange, botId) {
        $http.defaults.headers.common['user'] = $window.localStorage.id;
        $http.defaults.headers.common['role_id'] = $window.localStorage.role_id;
        $http.defaults.headers.common['token'] = $window.localStorage.token;
        var totalConversationUrl = '/stats?type=conversations';
        if (dateRange) {
            var fromDate = new Date(dateRange.startDate).toISOString()
            var toDate = new Date(dateRange.endDate).toISOString()
            totalConversationUrl = `${totalConversationUrl}&fromDate=${fromDate}&toDate=${toDate}`;
            if (botId) {
                totalConversationUrl = `${totalConversationUrl}&botId=${botId}`;
            }
        } else if (botId) {
            totalConversationUrl = `${totalConversationUrl}&botId=${botId}`;
        }
        return $http.get(totalConversationUrl);
    };

    reportServiceData.totalfiles = function(dateRange, botId, messageBy) {
        $http.defaults.headers.common['user'] = $window.localStorage.id;
        $http.defaults.headers.common['role_id'] = $window.localStorage.role_id;
        $http.defaults.headers.common['token'] = $window.localStorage.token;
        var totalFilesUrl = '/stats?type=files';
        if (dateRange) {
            var fromDate = new Date(dateRange.startDate).toISOString()
            var toDate = new Date(dateRange.endDate).toISOString()
            totalFilesUrl = `${totalFilesUrl}&fromDate=${fromDate}&toDate=${toDate}`;

            if (botId) {
                totalFilesUrl = `${totalFilesUrl}&botId=${botId}&messageBy=${messageBy}`;
            }
        } else if (botId) {
            totalFilesUrl = `${totalFilesUrl}&botId=${botId}&messageBy=${messageBy}`;
        }
        return $http.get(totalFilesUrl);
    };

    reportServiceData.totalUsersCount = function(dateRange, botId) {
        $http.defaults.headers.common['user'] = $window.localStorage.id;
        $http.defaults.headers.common['role_id'] = $window.localStorage.role_id;
        $http.defaults.headers.common['token'] = $window.localStorage.token;
        var totalUsersCountUrl = '/stats?type=users';
        if (dateRange) {
            var fromDate = new Date(dateRange.startDate).toISOString()
            var toDate = new Date(dateRange.endDate).toISOString()
            totalUsersCountUrl = `${totalUsersCountUrl}&fromDate=${fromDate}&toDate=${toDate}`;

            if (botId) {
                totalUsersCountUrl = `${totalUsersCountUrl}&botId=${botId}`;
            }
        } else if (botId) {
            totalUsersCountUrl = `${totalUsersCountUrl}&botId=${botId}`;
        }
        return $http.get(totalUsersCountUrl);
    };

    reportServiceData.totalMessageCount = function(dateRange, botId, messageBy) {
        $http.defaults.headers.common['user'] = $window.localStorage.id;
        $http.defaults.headers.common['role_id'] = $window.localStorage.role_id;
        $http.defaults.headers.common['token'] = $window.localStorage.token;
        var totalMessageCountUrl = '/stats?type=messages';
        if (dateRange) {
            var fromDate = new Date(dateRange.startDate).toISOString()
            var toDate = new Date(dateRange.endDate).toISOString()
            totalMessageCountUrl = `${totalMessageCountUrl}&fromDate=${fromDate}&toDate=${toDate}`;

            if (botId) {
                totalMessageCountUrl = `${totalMessageCountUrl}&botId=${botId}&messageBy=${messageBy}`;
            }
        } else if (botId) {
            totalMessageCountUrl = `${totalMessageCountUrl}&botId=${botId}&messageBy=${messageBy}`;
        }
        return $http.get(totalMessageCountUrl);
    };

    reportServiceData.messageVsUsers = function(dateRange, botId, messageBy) {
        $http.defaults.headers.common['user'] = $window.localStorage.id;
        $http.defaults.headers.common['role_id'] = $window.localStorage.role_id;
        $http.defaults.headers.common['token'] = $window.localStorage.token;
        var messageVsUsersUrl = '/charts?type=messages_users';
        if (dateRange) {
            var fromDate = new Date(dateRange.startDate).toISOString()
            var toDate = new Date(dateRange.endDate).toISOString()
            messageVsUsersUrl = `${messageVsUsersUrl}&fromDate=${fromDate}&toDate=${toDate}`;

            if (botId) {
                messageVsUsersUrl = `${messageVsUsersUrl}&botId=${botId}&messageBy=${messageBy}`;
            }
        } else if (botId) {
            messageVsUsersUrl = `${messageVsUsersUrl}&botId=${botId}&messageBy=${messageBy}`;
        }
        return $http.get(messageVsUsersUrl);
    };

    reportServiceData.conversationVsUsers = function(dateRange, botId) {
        $http.defaults.headers.common['user'] = $window.localStorage.id;
        $http.defaults.headers.common['role_id'] = $window.localStorage.role_id;
        $http.defaults.headers.common['token'] = $window.localStorage.token;
        var conversationVsUsersUrl = '/charts?type=conversations_users';
        if (dateRange) {
            var fromDate = new Date(dateRange.startDate).toISOString()
            var toDate = new Date(dateRange.endDate).toISOString()
            conversationVsUsersUrl = `${conversationVsUsersUrl}&fromDate=${fromDate}&toDate=${toDate}`;

            if (botId) {
                conversationVsUsersUrl = `${conversationVsUsersUrl}&botId=${botId}`;
            }
        } else if (botId) {
            conversationVsUsersUrl = `${conversationVsUsersUrl}&botId=${botId}`;
        }
        return $http.get(conversationVsUsersUrl);
    };

    reportServiceData.usersVsFiles = function(dateRange, botId, messageBy) {
        $http.defaults.headers.common['user'] = $window.localStorage.id;
        $http.defaults.headers.common['role_id'] = $window.localStorage.role_id;
        $http.defaults.headers.common['token'] = $window.localStorage.token;
        var usersVsFilesUrl = '/charts?type=users_files';
        if (dateRange) {
            var fromDate = new Date(dateRange.startDate).toISOString()
            var toDate = new Date(dateRange.endDate).toISOString()
            usersVsFilesUrl = `${usersVsFilesUrl}&fromDate=${fromDate}&toDate=${toDate}`;

            if (botId) {
                usersVsFilesUrl = `${usersVsFilesUrl}&botId=${botId}&messageBy=${messageBy}`;
            }
        } else if (botId) {
            usersVsFilesUrl = `${usersVsFilesUrl}&botId=${botId}&messageBy=${messageBy}`;
        }
        return $http.get(usersVsFilesUrl);
    };

    reportServiceData.getReportUrl = function(dateRange, botId) {
        $http.defaults.headers.common['user'] = $window.localStorage.id;
        $http.defaults.headers.common['role_id'] = $window.localStorage.role_id;
        $http.defaults.headers.common['token'] = $window.localStorage.token;
        var downloadConversationUrl = '/downloadConversation';
        if (dateRange) {
            var fromDate = new Date(dateRange.startDate).toISOString()
            var toDate = new Date(dateRange.endDate).toISOString()
            downloadConversationUrl = `${downloadConversationUrl}?&fromDate=${fromDate}&toDate=${toDate}`;

            if (botId) {
                downloadConversationUrl = `${downloadConversationUrl}&botId=${botId}`;
            }
        } else if (botId) {
            downloadConversationUrl = `${downloadConversationUrl}?botId=${botId}`;
        }
        return downloadConversationUrl;
    };

    reportServiceData.reportGridData = function(dateRange, botId, messageBy, exportCsv) {
        $http.defaults.headers.common['user'] = $window.localStorage.id;
        $http.defaults.headers.common['role_id'] = $window.localStorage.role_id;
        $http.defaults.headers.common['token'] = $window.localStorage.token;
        var path = '/report';
        if (dateRange) {
            var fromDate = new Date(dateRange.startDate).toISOString()
            var toDate = new Date(dateRange.endDate).toISOString()
            path = `${path}?fromDate=${fromDate}&toDate=${toDate}`;

            if (botId) {
                path = `${path}&botId=${botId}&messageBy=${messageBy}`;

                if (exportCsv) {
                    path = `${path}&export=${exportCsv}`;
                    return path;
                }
            }
        } else if (botId) {
            path = `${path}?botId=${botId}&messageBy=${messageBy}`;

            if (exportCsv) {
                path = `${path}&export=${exportCsv}`;
                return path;
            }
        }
        return $http.get(path);
    };

    // reportServiceData.sunburstchart = function(dateRange, botId) {
        // $http.defaults.headers.common['user'] = $window.localStorage.id;
        // $http.defaults.headers.common['role_id'] = $window.localStorage.role_id;
        // $http.defaults.headers.common['token'] = $window.localStorage.token;
    //     var sunburstchartUrl = '/charts?type=conversation_sunburst';
    //     if (dateRange) {
    //         var fromDate = new Date(dateRange.startDate).toISOString()
    //         var toDate = new Date(dateRange.endDate).toISOString()
    //         sunburstchartUrl = `${sunburstchartUrl}&fromDate=${fromDate}&toDate=${toDate}`;

    //         if (botId) {
    //             sunburstchartUrl = `${sunburstchartUrl}&botId=${botId}`;
    //         }
    //     } else if (botId) {
    //         sunburstchartUrl = `${sunburstchartUrl}&botId=${botId}`;
    //     }
    //     return $http.get(sunburstchartUrl);
    // };

    return reportServiceData;
}]);