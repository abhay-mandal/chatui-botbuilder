var logger = require('../log/logger');
module.exports = (function (env) {
  var config = {};
  switch (env) {
    case 'production':
      logger.info(`Environment - ${env}`);
      config = require('../env/production');
      break;
    case 'development':
      logger.info(`Environment - ${env}`);
      config = require('../env/development');
      break;
    case 'testing':
      logger.info(`Environment - ${env}`);
      config = require('../env/testing');
      break;
    case 'staging':
      logger.info(`Environment - ${env}`);
      config = require('../env/staging');
      break;
    default:
      logger.error('NODE_ENV environment variable not set');
      process.exit(1);
  }
  return config;
})(process.env.NODE_ENV);